# How to use: Quick Guide

## Connect Controller
1.  Pair controller in "Bluetooth & other devices" Windows panel
2.   Click on Discover Joycons

![JcMgr_Input_NewUI](https://gitlab.com/LucasBarbe/JoyConManager/uploads/a51a0f0fb860d142d2de8ca568b25405/JcMgr_Input_NewUI.PNG)
    
## HeadTracker
1.  Set OpenTrack input to `UDP over network`   

![OpenTrack__Input](https://gitlab.com/LucasBarbe/JoyConManager/uploads/a4cc223f1c3de9dae873e711e7a5dcda/OpenTrack__Input.PNG)

2. Check than OpenTrack and JoyconManger use the same port
3. Select input Joycon (don't forgot to calibrate it)
4. Select fusion mod, I recommend to use Mahony

![JcMgr_HeadTracker_NewUI_Edit](https://gitlab.com/LucasBarbe/JoyConManager/uploads/02f9f48ec1ab250ab95d2ef61c9caa30/JcMgr_HeadTracker_NewUI_Edit.png)

5.  Click on "Configure Initial Attitude"
6.  Set initial Joycon rotation (green arrow is forward direction)

![JcMgr_HeadTracker_Initial_Attitude_NewUI](https://gitlab.com/LucasBarbe/JoyConManager/uploads/b94b47d1773b029d097888a92c2fcc89/JcMgr_HeadTracker_Initial_Attitude_NewUI.png)

7.  Now, you can Start HeadTracker and OpenTrack
 
*  Center tracking with center button, left arrow or Y (Joycon)
