#include "Quaternion.h"

void Quaternion::SetEuler(Vector3 euler)
{
    Vector3 eulerRad = Vector3(euler.X * DEG_TO_RAD, euler.Y * DEG_TO_RAD, euler.Z * DEG_TO_RAD);

    double yaw = eulerRad.Z;
    double pitch = eulerRad.Y;
    double roll = eulerRad.X;

    double cy = cos(yaw * 0.5);//Yaw
    double sy = sin(yaw * 0.5);
    double cp = cos(pitch * 0.5);//Pitch
    double sp = sin(pitch * 0.5);
    double cr = cos(roll * 0.5);//Roll
    double sr = sin(roll * 0.5);

    W = cy * cp * cr + sy * sp * sr;
    X = cy * cp * sr - sy * sp * cr;
    Y = sy * cp * sr + cy * sp * cr;
    Z = sy * cp * cr - cy * sp * sr;

}

Vector3 Quaternion::GetEuler()
{
    Vector3 euler;

    // roll (x-axis rotation)
    double sinr_cosp = 2 * (W * X + Y * Z);
    double cosr_cosp = 1 - 2 * (X * X + Y * Y);
    euler.X = atan2(sinr_cosp, cosr_cosp);

    // pitch (y-axis rotation)
    double sinp = 2 * (W * Y - Z * X);
    if (abs(sinp) >= 1)
        euler.Y = copysign(M_PI_2, sinp); // use 90 degrees if out of range
    else
        euler.Y = asin(sinp);

    // yaw (z-axis rotation)
    double siny_cosp = 2 * (W * Z + X * Y);
    double cosy_cosp = 1 - 2 * (Y * Y + Z * Z);
    euler.Z = atan2(siny_cosp, cosy_cosp);

    return Vector3(euler.X / DEG_TO_RAD, euler.Y / DEG_TO_RAD, euler.Z / DEG_TO_RAD);
}

Quaternion::Quaternion()
{
    W = 1;
    X = 0;
    Y = 0;
    Z = 0;
}

Quaternion::Quaternion(double w, double x, double y, double z)
{
    W = w;
    X = x;
    Y = y;
    Z = z;
}

double Quaternion::Magnitude()
{
    return Dot(*this, *this);
}

Quaternion Quaternion::operator*(const Quaternion& q)
{
	return Quaternion(
        W * q.W - X * q.X - Y * q.Y - Z * q.Z,
        W * q.X + X * q.W + Y * q.Z - Z * q.Y,
        W * q.Y + Y * q.W + Z * q.X - X * q.Z,
        W * q.Z + Z * q.W + X * q.Y - Y * q.X
    );
}

Quaternion Quaternion::operator*(const double& f) 
{
    return Quaternion(
        W * f,
        X * f,
        Y * f,
        Z * f
    );
}

bool Quaternion::operator==(const Quaternion& q) 
{
    return this->W == q.W && this->X == q.X && this->Y == q.Y && this->Z == q.Z;
}

Quaternion Quaternion::Identity()
{
    return Quaternion(1,0,0,0);
}

double Quaternion::Dot(Quaternion& a, Quaternion& b)
{
    return a.X * b.X + a.Y * b.Y + a.Z * b.Z + a.W * b.W;
}

Quaternion Quaternion::Normalize(Quaternion& q)
{
    double magnitude = sqrt(Dot(q, q));

    return Quaternion(q.W / magnitude, q.X / magnitude, q.Y / magnitude, q.Z / magnitude);
}

Quaternion Quaternion::Conjugate(Quaternion& q)
{
    Quaternion res;
    res.W = q.W;
    res.X = -q.X;
    res.Y = -q.Y;
    res.Z = -q.Z;
    return res;
}

Quaternion Quaternion::Inverse(Quaternion& q)
{
    Quaternion res;

    double magnitude2 = pow(q.Magnitude(), 2);

    res = Quaternion::Conjugate(q);

    res.W = res.W / magnitude2;
    res.X = res.X / magnitude2;
    res.Y = res.Y / magnitude2;
    res.Z = res.Z / magnitude2;

    return res;
}

Quaternion Quaternion::Lerp(Quaternion& a, Quaternion& b, double t) {
    if (t > 1) t = 1; 
    if (t < 0) t = 0;
    double omt = 1 - t;

    Quaternion q;

    q.W = a.W * omt + b.W * t;
    q.X = a.X * omt + b.X * t;
    q.Y = a.Y * omt + b.Y * t;
    q.Z = a.Z * omt + b.Z * t;

    return Quaternion::Normalize(q);
}

Quaternion Quaternion::Slerp(Quaternion& a, Quaternion& b, double t) {
    if (t > 1) t = 1;
    if (t < 0) t = 0;
    double omt = 1 - t;

    if (a == b) return a;

    Quaternion q;

    double theta = acos(a.X * b.X + a.Y * b.Y + a.Z * b.Z + a.W * b.W);
    double sinTheta = sin(theta);

    double weightA = sin(theta * omt) / sinTheta;
    double weightB = sin(theta * t) / sinTheta;

    q.W = weightA * a.W + weightB * b.W;
    q.X = weightA * a.X + weightB * b.X;
    q.Y = weightA * a.Y + weightB * b.Y;
    q.Z = weightA * a.Z + weightB * b.Z;

    return Quaternion::Normalize(q);
}
