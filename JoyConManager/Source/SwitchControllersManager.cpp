#include "SwitchControllersManager.h"

SwitchControllersManager::SwitchControllersManager()
{
	m_thread = gcnew Thread(gcnew ThreadStart(this, &SwitchControllersManager::UpdateLoop));
}

void SwitchControllersManager::DiscoverControllers()
{
	hid_device_info * devices, *currentDevice;
	devices = hid_enumerate(JOYCON_VENDOR, 0x0);
	//devices = hid_enumerate(0x0, 0x0);

	currentDevice = devices;
	while (currentDevice)
	{

		/*std::cout << std::endl << currentDevice->interface_number << " | " << currentDevice->usage << " | " <<
			currentDevice->usage_page << " | " << _wcsdup(currentDevice->manufacturer_string) << " | "
			<< _wcsdup(currentDevice->product_string) << " |-| " << _strdup(currentDevice->path);

		std::string pat = _strdup(currentDevice->path);
		size_t f = pat.find("hid#{");
		if (f != std::string::npos) {
			std::cout << " -= BL =- ";
		}*/

		//if (currentDevice->vendor_id == JOYCON_VENDOR) 
		{
			//std::cout << std::endl << _strdup(currentDevice->path);
			std::wstring serialNumber = _wcsdup(currentDevice->serial_number);
			//std::cout << " JOYCON ";
			bool isAlreadyListed = false;
			m_M_controllers->WaitOne();
			for (size_t i = 0; i < (size_t)m_controllers->Count; i++)//TODO: check get length
			{

				if (serialNumber == m_controllers[i]->controller->GetSerialNumber())
				{
					isAlreadyListed = true;
					break;
				}
			}
			m_M_controllers->ReleaseMutex();
			if (!isAlreadyListed) 
			{
				m_M_controllers->WaitOne();
				SwitchControllerTrack^ nCtrl_T = gcnew SwitchControllerTrack();
				nCtrl_T->controller->Init(currentDevice);

				switch (nCtrl_T->controller->GetControllerType())
				{
				case SwitchController::ControllerType::JoyCon_Left:
					std::wcout << std::endl << "Joycon Left [SN:" << serialNumber << "] found";
					break;
				case SwitchController::ControllerType::JoyCon_Right:
					std::wcout << std::endl << "Joycon Right [SN:" << serialNumber << "] found";
					break;
				case SwitchController::ControllerType::Pro_Controller:
					std::wcout << std::endl << "Pro Controller [SN:" << serialNumber << "] found";
					break;
				}

				nCtrl_T->controller->SetInputReportMode(SwitchController::InputReportMode::Standard);
				//nCtrl_T->controller->SetInputReportMode(SwitchController::InputReportMode::SimpleHID);
				nCtrl_T->controller->EnableVibration(false);
				nCtrl_T->controller->EnableIMU(true);
				nCtrl_T->controller->SetPlayerLights(
					SwitchController::PlayerLight::Flash,
					SwitchController::PlayerLight::Off,
					SwitchController::PlayerLight::Off,
					SwitchController::PlayerLight::Flash);

				/*while (nCtrl_T->controller->Update() != SwitchController::UpdateReturn::Updated)
				{

				}

				Sleep(100);

				while (nCtrl_T->controller->Update() != SwitchController::UpdateReturn::Updated)
				{

				}

				nCtrl_T->controller->CalibrateGyroscope(nullptr);*/

				nCtrl_T->track = true;
				m_M_controllers->ReleaseMutex();
				m_M_controllers->WaitOne();
				m_controllers->Add(nCtrl_T);
				m_M_controllers->ReleaseMutex();
			}
		}

		currentDevice = currentDevice->next;
	}

	hid_free_enumeration(devices);

	OnControllerListChange(m_controllers->Count);
}

void SwitchControllersManager::DisconnectController(int joyconIndex)
{
	m_M_controllers->WaitOne();

	if (m_controllers->Count > joyconIndex && joyconIndex > -1) {
		m_controllers[joyconIndex]->controller->Disconnect();
	}

	m_M_controllers->ReleaseMutex();
}

void SwitchControllersManager::CalibrateController(int joyconIndex)
{
	m_M_controllers->WaitOne();

	if (m_controllers->Count > joyconIndex && joyconIndex > -1) {
		m_controllers[joyconIndex]->calibration->StartCalibration();
	}

	m_M_controllers->ReleaseMutex();
}

//List<SwitchControllersManager::SwitchControllerTrack^> SwitchControllersManager::GetControllersList()
//{
//	return List<SwitchControllerTrack^>();
//}

void SwitchControllersManager::ToggleAutoDiscover()
{
	m_M_autoDiscover->WaitOne();
	m_autoDiscover = !m_autoDiscover;
	OnAutoDiscoverSettingsChange(m_autoDiscover, m_autoDiscoverDelay);
	m_M_autoDiscover->ReleaseMutex();
}

void SwitchControllersManager::SetAutoDiscover(bool enable)
{
	m_M_autoDiscover->WaitOne();
	m_autoDiscover = enable;
	OnAutoDiscoverSettingsChange(m_autoDiscover, m_autoDiscoverDelay);
	m_M_autoDiscover->ReleaseMutex();
}

void SwitchControllersManager::SetAutoDiscoverDelay(int delayms)
{
	m_M_autoDiscover->WaitOne();
	m_autoDiscoverDelay = delayms;
	OnAutoDiscoverSettingsChange(m_autoDiscover, m_autoDiscoverDelay);
	m_M_autoDiscover->ReleaseMutex();
}

SwitchControllerState SwitchControllersManager::GetStateOf(int joyconIndex)
{
	m_M_controllers->WaitOne();
	int ctrlCount = m_controllers->Count;
	m_M_controllers->ReleaseMutex();
	SwitchControllerState sState;
	if(joyconIndex < 0 || !(joyconIndex < ctrlCount))
		return sState;

	int i = joyconIndex;

	m_M_controllers->WaitOne();
	sState.Type = m_controllers[i]->controller->GetControllerType();
	sState.Buttons = m_controllers[i]->controller->GetButtonState();
	sState.Sticks = m_controllers[i]->controller->GetStickState();
	sState.IMUEnabled = m_controllers[i]->controller->GetIMUIsEnable();
	sState.IMU0 = m_controllers[i]->controller->GetIMU0State();
	sState.IMU1 = m_controllers[i]->controller->GetIMU1State();
	sState.IMU2 = m_controllers[i]->controller->GetIMU2State();
	sState.IMUAvg = m_controllers[i]->controller->GetIMUAvgState();
	sState.DeltaUpdateTime = m_controllers[i]->controller->GetDeltaUpdateTime();
	sState.MacAdress = m_controllers[i]->controller->GetMacAdress();

	sState.BodyColor = m_controllers[i]->controller->GetBodyColor();
	sState.ButtonsColor = m_controllers[i]->controller->GetButtonsColor();

	sState.Charging = m_controllers[i]->controller->GetCharging();
	sState.BatteryLevel = m_controllers[i]->controller->GetBatteryLevel();

	if (!m_controllers[i]->calibration->IsCalibrated()) {
		m_controllers[i]->calibration->AddSample(sState.IMU0);
		m_controllers[i]->calibration->AddSample(sState.IMU1);
		m_controllers[i]->calibration->AddSample(sState.IMU2);
	}
	else {
		sState.IMU0 = m_controllers[i]->calibration->ApplyMovementCalibration(sState.IMU0);
		sState.IMU1 = m_controllers[i]->calibration->ApplyMovementCalibration(sState.IMU1);
		sState.IMU2 = m_controllers[i]->calibration->ApplyMovementCalibration(sState.IMU2);
	}

	sState.SN = m_controllers[i]->controller->GetSerialNumber();

	sState.IsCalibrated = m_controllers[i]->calibration->IsCalibrated();

	for (size_t j = 0; j < 4; j++)
	{
		sState.PlayerLights[j] = m_controllers[i]->controller->GetPlayerLight(j);
	}

	m_M_controllers->ReleaseMutex();

	return sState;
}

void SwitchControllersManager::RunThread()
{
	if (!m_thread->IsAlive) {
		m_thread->Start();
	}
}

void SwitchControllersManager::StopThread()
{
	if (m_thread->IsAlive) {
		m_M_loop->WaitOne();

		m_loop = false;

		m_M_loop->ReleaseMutex();
	}
}

void SwitchControllersManager::AbortThread()
{
	if (m_thread->IsAlive)
		m_thread->Abort();
}

void SwitchControllersManager::UpdateLoop()
{
	bool loop = true;

	DateTime lastUpdateTime = DateTime::UtcNow;
	DateTime currentUpdateTime;
	TimeSpan timeSince;

	int deltaUpdateTime = 0;

	List<int>^ removeIndex = gcnew List<int>();
	while (loop)
	{

		m_M_autoDiscover->WaitOne();
		if (m_autoDiscover) {
			currentUpdateTime = DateTime::UtcNow;
			timeSince = currentUpdateTime - lastUpdateTime;

			deltaUpdateTime = timeSince.TotalMilliseconds;


			if (deltaUpdateTime >= m_autoDiscoverDelay) {
				DiscoverControllers();
				lastUpdateTime = currentUpdateTime;
			}
		}
		m_M_autoDiscover->ReleaseMutex();

		m_M_controllers->WaitOne();

		size_t ctrlCount = (size_t)m_controllers->Count;

		m_M_controllers->ReleaseMutex();

		//std::cout << std::endl << "azer";
		for (size_t i = 0; i < ctrlCount; i++)
		{
			m_M_controllers->WaitOne();
			bool track = m_controllers[i]->track;
			m_M_controllers->ReleaseMutex();

			if (track) {
				m_M_controllers->WaitOne();

				bool forceSync = false;

				if (m_controllers[i]->checkCount > 12) {

					//std::cout << std::endl << "=============================================================================================";

					forceSync = true;
					m_controllers[i]->checkCount = 0;
				}

				SwitchController::UpdateReturn res = m_controllers[i]->controller->Update(forceSync);
				m_M_controllers->ReleaseMutex();
				
				switch (res)
				{
				case SwitchController::Error:
					if (removeIndex->Count == 0)
						removeIndex->Add(i);
					else
						removeIndex->Insert(i, 0);
					break;
				case SwitchController::NotUpdated:
					//Do nothing
					break;
				case SwitchController::Updated:

					SwitchControllerState sState;
					m_M_controllers->WaitOne();
					sState.Type = m_controllers[i]->controller->GetControllerType();
					sState.Buttons = m_controllers[i]->controller->GetButtonState();
					sState.Sticks = m_controllers[i]->controller->GetStickState();
					sState.IMUEnabled = m_controllers[i]->controller->GetIMUIsEnable();
					sState.IMU0 = m_controllers[i]->controller->GetIMU0State();
					sState.IMU1 = m_controllers[i]->controller->GetIMU1State();
					sState.IMU2 = m_controllers[i]->controller->GetIMU2State();
					sState.IMUAvg = m_controllers[i]->controller->GetIMUAvgState();
					sState.DeltaUpdateTime = m_controllers[i]->controller->GetDeltaUpdateTime();
					sState.MacAdress = m_controllers[i]->controller->GetMacAdress();

					sState.BodyColor = m_controllers[i]->controller->GetBodyColor();
					sState.ButtonsColor = m_controllers[i]->controller->GetButtonsColor();
					
					sState.Charging = m_controllers[i]->controller->GetCharging();
					sState.BatteryLevel = m_controllers[i]->controller->GetBatteryLevel();

					if (!m_controllers[i]->calibration->IsCalibrated()) {
						m_controllers[i]->calibration->AddSample(sState.IMU0);
						m_controllers[i]->calibration->AddSample(sState.IMU1);
						m_controllers[i]->calibration->AddSample(sState.IMU2);
					}
					else {
						sState.IMU0 = m_controllers[i]->calibration->ApplyMovementCalibration(sState.IMU0);
						sState.IMU1 = m_controllers[i]->calibration->ApplyMovementCalibration(sState.IMU1);
						sState.IMU2 = m_controllers[i]->calibration->ApplyMovementCalibration(sState.IMU2);

						SwitchController::Movement avg;
						avg.Accelerometer = (sState.IMU0.Accelerometer + sState.IMU1.Accelerometer + sState.IMU2.Accelerometer) / 3;
						avg.Gyroscope = (sState.IMU0.Gyroscope + sState.IMU1.Gyroscope + sState.IMU2.Gyroscope) / 3;
						sState.IMUAvg = avg;
					}

					sState.SN = m_controllers[i]->controller->GetSerialNumber();

					sState.IsCalibrated = m_controllers[i]->calibration->IsCalibrated();

					for (size_t j = 0; j < 4; j++)
					{
						sState.PlayerLights[j] = m_controllers[i]->controller->GetPlayerLight(j);
					}
					

					/*OnUpdate_Delegate* call = &(m_controllers[i]->CallOnUpdate);

					m_controllers[i]->CallOnUpdate*/

					//std::cout << std::endl << m_controllers[i]->controller->GetDeltaUpdateTime();

					if (m_controllers[i]->controller->GetDeltaUpdateTime() > 0.022) {
						m_controllers[i]->checkCount += 2;
						//std::cout << " ++ " << (int)m_controllers[i]->checkCount;
					}
					else if (m_controllers[i]->checkCount > 0){
						//std::cout << " -- " << (int)m_controllers[i]->checkCount;
						m_controllers[i]->checkCount --;
					}
						
					m_M_controllers->ReleaseMutex();

					/*call(sState);*/

					OnUpdateController(i, sState);
					//std::cout << std::endl << m_controllers[i]->controller->GetDeltaUpdateTime();

					break;
				/*default:
					break;*/
				}
			}
		}

		m_M_controllers->WaitOne();

		for (size_t i = 0; i < removeIndex->Count; i++)
		{
			m_controllers->RemoveAt(removeIndex[i]);
			OnControllerListChange(m_controllers->Count);
		}
		
		removeIndex->Clear();

		m_M_controllers->ReleaseMutex();
		//m_M_controllers->ReleaseMutex();

		m_M_loop->WaitOne();

		loop = m_loop;

		m_M_loop->ReleaseMutex();
		
		//Sleep(1);
	}

}

void SwitchControllersManager::SwitchControllerTrack::CallOnUpdate(SwitchControllerState state)
{
	//if(OnUpdate != nullptr)
	OnUpdate(state);
}

SwitchControllersManager::SwitchControllerTrack::SwitchControllerTrack()
{
	controller = new SwitchController();
	calibration = new IMUCalibration();
}

SwitchControllersManager::SwitchControllerTrack::~SwitchControllerTrack()
{
	this->!SwitchControllerTrack();
}

SwitchControllersManager::SwitchControllerTrack::!SwitchControllerTrack()
{
	delete controller;
	delete calibration;
}
