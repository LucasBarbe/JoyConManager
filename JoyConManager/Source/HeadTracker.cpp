#include "HeadTracker.h"
#include "..\Header\HeadTracker.h"

HeadTracker::HeadTracker(): m_madgwick(0.015), m_mahony(0.015)//, m_udpSender("127.0.0.1",4242)
{
	m_driftCorrectionEuler = Vector3();
	m_driftCorrection = Quaternion();
	m_initialAttitude = Quaternion();
	m_initialAttitudeOffset = Quaternion();
	m_rotation = Quaternion();
	m_centerOffset = Quaternion();
	m_fusionAlgorithm = HeadTracker::FusionAlgorithm::None;
}

HeadTracker::~HeadTracker()
{
	m_udpSender.Close();
}

Vector3 HeadTracker::GetRotationEuler()
{
	return this->GetRotation().GetEuler();
}

Vector3 HeadTracker::GetRotationEuler(bool applyInversion)
{
	if (!applyInversion)
		return this->GetRotationEuler();

	Vector3 rotated = this->GetRotationEuler();

	rotated = rotated * Vector3(
		m_invertX ? -1 : 1,
		m_invertY ? -1 : 1,
		m_invertZ ? -1 : 1
	);

	return rotated;
}

Quaternion HeadTracker::GetRotation()
{
	return (m_rotation * m_initialAttitudeOffset) * m_centerOffset;
}

Quaternion HeadTracker::GetRotation(bool applyInversion) 
{
	if (!applyInversion)
		return this->GetRotation();

	Quaternion q;
	q.SetEuler(this->GetRotationEuler(true));

	return q;
}

void HeadTracker::SetRotation(Quaternion rotation)
{
	m_rotation = rotation;
}

void HeadTracker::SetTarget(std::string ip, unsigned short port)
{
	m_udpSender.Close();

	m_udpSender = UdpSender(ip, port);
}

void HeadTracker::SetFusionAlgorithm(FusionAlgorithm algo)
{
	m_fusionAlgorithm = algo;
}

void HeadTracker::Update(Vector3 gyroData, Vector3 accData, double deltaTime)
{
	Quaternion gyro;
	//gyro.SetEuler(gyroData * deltaTime);
	gyro.SetEuler((gyroData + m_driftCorrectionEuler) * deltaTime);

	switch (m_fusionAlgorithm)
	{
	case HeadTracker::None:
		m_rotation = m_rotation * gyro;

		//m_rotation = m_rotation * Quaternion::Slerp(Quaternion::Identity(), m_driftCorrection, deltaTime);

		break;
	case HeadTracker::MadgwickAlgorithm:
		m_madgwick.Rotation = m_rotation;
		m_madgwick.SamplePeriod = deltaTime;
		m_madgwick.UpdateIMU((gyroData + m_driftCorrectionEuler), accData);
		m_rotation = m_madgwick.Rotation;

		//m_rotation = Quaternion::Slerp(Quaternion::Identity(), m_driftCorrection, deltaTime) * m_rotation;

		break;
	case HeadTracker::MahonyAlgorithm:
		m_mahony.Rotation = m_rotation;
		m_mahony.SamplePeriod = deltaTime;
		m_mahony.UpdateIMU((gyroData + m_driftCorrectionEuler), accData);
		m_rotation = m_mahony.Rotation;

		//m_rotation = Quaternion::Slerp(Quaternion::Identity(), m_driftCorrection, deltaTime) * m_rotation;

		break;
	}


	Vector3 rotatedEuler = this->GetRotationEuler(true);

	//double head[6] = { 0, 0, 0, rotatedEuler.X, rotatedEuler.Y, rotatedEuler.Z };

	double head[6] = { 0, 0, 0, rotatedEuler.Z, rotatedEuler.X, rotatedEuler.Y }; //Remap
	double* headPtr = head;

	m_udpSender.Send((const char*)headPtr, sizeof(double[6]));
}

void HeadTracker::SetInvertAxis(bool x, bool y, bool z)
{
	m_invertX = x;
	m_invertY = y;
	m_invertZ = z;
}

void HeadTracker::SetInitialAttitude()
{

	m_initialAttitude = m_rotation;
	//Calc InitialAtitude Offset
	m_initialAttitudeOffset = Quaternion::Inverse(m_rotation);
}

void HeadTracker::SetInitialAttitude(Quaternion attitude)
{
	m_initialAttitude = attitude;
	m_initialAttitudeOffset = Quaternion::Inverse(attitude);
}

void HeadTracker::SetDriftCorrection(Vector3 correction)
{
	m_driftCorrectionEuler = correction;
	m_driftCorrection.SetEuler(correction);
}

Vector3 HeadTracker::GetDriftCorrection()
{
	return m_driftCorrectionEuler;
}

void HeadTracker::Center()
{
	//Calc Centering Offset
	m_centerOffsetEuler = (m_rotation * m_initialAttitudeOffset).GetEuler();

	m_centerOffset = Quaternion::Inverse(m_rotation * m_initialAttitudeOffset);
}

void HeadTracker::Reset()
{
	m_rotation = m_initialAttitude;
}
