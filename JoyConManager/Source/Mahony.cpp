#include "Mahony.h"

Mahony::Mahony(double samplePeriod)
{
	SamplePeriod = samplePeriod;
}

void Mahony::UpdateIMU(Vector3 gyro, Vector3 acc)
{
	gyro.ConvertToRad();

	double q0 = Rotation.W, q1 = Rotation.X, q2 = Rotation.Y, q3 = Rotation.Z;   // short name local variable for readability

	double recipNorm;
	double halfvx, halfvy, halfvz;
	double halfex, halfey, halfez;
	double qa, qb, qc;

	// Compute feedback only if accelerometer measurement valid
	// (avoids NaN in accelerometer normalisation)
	if (!((acc.X == 0.0f) && (acc.Y == 0.0f) && (acc.Z == 0.0f))) {

		// Normalise accelerometer measurement
		recipNorm = ToolBox::Maths::InvSqrt(acc.X * acc.X + acc.Y * acc.Y + acc.Z * acc.Z);
		acc.X *= recipNorm;
		acc.Y *= recipNorm;
		acc.Z *= recipNorm;

		// Estimated direction of gravity
		halfvx = q1 * q3 - q0 * q2;
		halfvy = q0 * q1 + q2 * q3;
		halfvz = q0 * q0 - 0.5f + q3 * q3;

		// Error is sum of cross product between estimated
		// and measured direction of gravity
		halfex = (acc.Y * halfvz - acc.Z * halfvy);
		halfey = (acc.Z * halfvx - acc.X * halfvz);
		halfez = (acc.X * halfvy - acc.Y * halfvx);

		// Compute and apply integral feedback if enabled
		if (twoKiDef > 0.0) {
			// integral error scaled by Ki
			m_integralFBx += twoKiDef * halfex * SamplePeriod;
			m_integralFBy += twoKiDef * halfey * SamplePeriod;
			m_integralFBz += twoKiDef * halfez * SamplePeriod;
			gyro.X += m_integralFBx;	// apply integral feedback
			gyro.Y += m_integralFBy;
			gyro.Z += m_integralFBz;
		}
		else {
			m_integralFBx = 0.0;	// prevent integral windup
			m_integralFBy = 0.0;
			m_integralFBz = 0.0;
		}

		// Apply proportional feedback
		gyro.X += twoKpDef * halfex;
		gyro.Y += twoKpDef * halfey;
		gyro.Z += twoKpDef * halfez;
	}

	// Integrate rate of change of quaternion
	gyro.X *= (0.5 * SamplePeriod);		// pre-multiply common factors
	gyro.Y *= (0.5 * SamplePeriod);
	gyro.Z *= (0.5 * SamplePeriod);
	qa = q0;
	qb = q1;
	qc = q2;
	q0 += (-qb * gyro.X - qc * gyro.Y - q3 * gyro.Z);
	q1 += (qa * gyro.X + qc * gyro.Z - q3 * gyro.Y);
	q2 += (qa * gyro.Y - qb * gyro.Z + q3 * gyro.X);
	q3 += (qa * gyro.Z + qb * gyro.Y - qc * gyro.X);

	// Normalise quaternion
	recipNorm = ToolBox::Maths::InvSqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3);
	q0 *= recipNorm;
	q1 *= recipNorm;
	q2 *= recipNorm;
	q3 *= recipNorm;

	Rotation.W = q0;
	Rotation.X = q1;
	Rotation.Y = q2;
	Rotation.Z = q3;
}