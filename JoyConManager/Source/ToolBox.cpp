#include "ToolBox.h"

int16_t ToolBox::Converter::CopyBitsUint16ToInt16(uint16_t a) {
	int16_t res;
	uint16_t* aPtr = &a;
	memcpy(&res, aPtr, sizeof(int16_t));
	return res;
}

uint16_t ToolBox::Converter::MergeUint8(uint8_t a, uint8_t b)
{
	//a = FF    b = 00 ->  output = FF 00
	return (((uint16_t)a) << 8) | b;
}

//-------------------------------------------------------------------------------------------
// Fast inverse square-root
// See: http://en.wikipedia.org/wiki/Fast_inverse_square_root

float ToolBox::Maths::InvSqrt(float x)
{
	float halfx = 0.5 * x;
	float y = x;
	long i = *(long*)&y;
	i = 0x5f3759df - (i >> 1);
	y = *(float*)&i;
	y = y * (1.5 - (halfx * y * y));
	y = y * (1.5 - (halfx * y * y));
	return y;
}

//-------------------------------------------------------------------------------------------
// Fast inverse square-root double
// See: https://stackoverflow.com/questions/11644441/fast-inverse-square-root-on-x64/11644533

double ToolBox::Maths::InvSqrt(double x)
{
	double y = x;
	double x2 = y * 0.5;
	std::int64_t i = *(std::int64_t*) & y;
	// The magic number is for doubles is from https://cs.uwaterloo.ca/~m32rober/rsqrt.pdf
	i = 0x5fe6eb50c7b537a9 - (i >> 1);
	y = *(double*)&i;
	y = y * (1.5 - (x2 * y * y));   // 1st iteration
	y = y * (1.5 - (x2 * y * y));   // 2nd iteration, [this can be removed] -> Nop
	return y;
}

String^ ToolBox::BuildInfo::GetGitBranch()
{
#ifdef GIT_BRANCH
	return ADD_QUOTES(GIT_BRANCH);
#endif
	return nullptr;
}

String^ ToolBox::BuildInfo::GetGitCurrentCommit()
{
#ifdef GIT_CURRENT_COMMIT
	return ADD_QUOTES(GIT_CURRENT_COMMIT);
#endif
	return nullptr;
}

String^ ToolBox::BuildInfo::GetBuildFullDate()
{
#ifdef BUILD_DATE 
#ifdef BUILD_TIME

	return ADD_QUOTES(BUILD_DATE) + " " + ADD_QUOTES(BUILD_TIME);
#endif
#endif

	return nullptr;
}

String^ ToolBox::BuildInfo::GetBuildVersion()
{
	String^ version;

	System::Reflection::Assembly^ assembly = System::Reflection::Assembly::GetExecutingAssembly();
	version = System::Diagnostics::FileVersionInfo::GetVersionInfo(assembly->Location)->FileVersion;

#ifdef _DEBUG
	version += "  Debug";
#endif // _DEBUG

	return version;
}

String^ ToolBox::BuildInfo::GetBuildVersionFormated()
{
	array<String^>^ splited = ToolBox::BuildInfo::GetBuildVersion()->Split('.');

	return splited[0] + "." + splited[1] + "." + splited[2] + " - " + splited[3];
}

