#include "IMUCalibration.h"

IMUCalibration::IMUCalibration()
{
	m_isCalibrated = true;
}

IMUCalibration::~IMUCalibration()
{
}

void IMUCalibration::Reset()
{
	m_offset.Accelerometer = Vector3();
	m_offset.Gyroscope = Vector3();

	m_isCalibrated = true;
}

void IMUCalibration::StartCalibration()
{
	std::cout << std::endl << "Calibration start";
	m_isCalibrated = false;
}

SwitchController::Movement IMUCalibration::ApplyMovementCalibration(SwitchController::Movement sample)
{
	sample.Gyroscope = ApplyGyroCalibration(sample.Gyroscope);

	return sample;
}

Vector3 IMUCalibration::ApplyGyroCalibration(Vector3 &gyro)
{
	return gyro - m_offset.Gyroscope;
}

void IMUCalibration::AddSample(SwitchController::Movement &sample)
{
	if (m_isCalibrated)
		return;

	m_samples.push_back(sample);

	if (m_samples.size() > 4000-1) {
		//avg
		CalcGyroOffset();

		m_samples.clear();
		m_isCalibrated = true;
	}
}

bool IMUCalibration::IsCalibrated()
{
	return m_isCalibrated;
}

void IMUCalibration::CalcGyroOffset()
{
	Vector3 sum = Vector3::Zero();

	size_t count = (size_t)m_samples.size();

	for (size_t i = 0; i < count; i++)
	{
		sum += m_samples[i].Gyroscope;
	}

	m_offset.Gyroscope = sum / count;

	std::cout << std::endl << "Calibration ended / offsets = x: " <<  m_offset.Gyroscope.X << " y: " << m_offset.Gyroscope.Y << " z: " << m_offset.Gyroscope.Z;
}
