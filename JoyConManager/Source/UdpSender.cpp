#include "UdpSender.h"

UdpSender::UdpSender(std::string ip, unsigned short port)
{
	m_targetIP = ip;
	m_targetPort = port;

	m_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(m_socket == SOCKET_ERROR)
		std::cout << std::endl << "socket error - code: " << WSAGetLastError();

	m_to = { 0 };
	inet_pton(AF_INET, m_targetIP.c_str(), &m_to.sin_addr.s_addr);
	m_to.sin_family = AF_INET;
	m_to.sin_port = htons(m_targetPort);
}

UdpSender::UdpSender()
{
	/*m_targetIP = "127.0.0.1";
	m_targetPort = 4242;

	m_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (m_socket == SOCKET_ERROR)
		std::cout << std::endl << "socket error - code: " << WSAGetLastError();

	m_to = { 0 };
	inet_pton(AF_INET, m_targetIP.c_str(), &m_to.sin_addr.s_addr);
	m_to.sin_family = AF_INET;
	m_to.sin_port = htons(m_targetPort);*/
}

bool UdpSender::Send(const char* data, int dataLenght)
{
	int res = sendto(m_socket, data, dataLenght, 0, reinterpret_cast<const sockaddr*>(&m_to), sizeof(m_to));

	if(!(res > 0))
		std::cout << std::endl << "code: " << WSAGetLastError();

	return res > 0;
}

bool UdpSender::Send(std::string data)
{
	return Send(data.data(), static_cast<int>(data.length()));
}

void UdpSender::Close()
{
	CloseSocket(m_socket);
}

bool UdpSender::Init()
{
	WSAData wsaData;
	return WSAStartup(MAKEWORD(2, 2), &wsaData) == 0;
}

void UdpSender::Release()
{
	WSACleanup();
}

void UdpSender::CloseSocket(SOCKET socket)
{
	closesocket(socket);
}
