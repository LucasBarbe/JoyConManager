//#define WIN32_LEAN_AND_MEAN
#undef UNICODE
#define UNICODE
#undef _WINSOCKAPI_
#define _WINSOCKAPI_

#include "UdpSender.h"

#include <cstdio>
#include <Windows.h>

#include "MainGUI.h"
#include "JoyconResource.h"
#include <hidapi.h>
#include <string>

#include <chrono>
#include <iostream>
#include <vector>
//#include <thread>
//#include <iostream>

#include <iomanip>
#include <sstream>

#include "SwitchController.h"
#include "SwitchControllersManager.h"
//#include <vXboxInterface.h>
#include "VirtualController.h"
#include "Global.h"
#include "AppManager_Managed.h"
#include "ManagedObject.h"

#include "Madgwick.h"

#include "Quaternion.h"

using namespace System::Threading;
#ifdef _DEBUG
#define CONSOLE  1
#endif // DEBUG

//disable console
#if CONSOLE == 0
	#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
#endif // CONSOLE

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

using namespace JoyConManager;
using namespace std;

UdpSender Sender;// = UdpSender("127.0.0.1", 4242);
//Madgwick Gyro = Madgwick(0.015, 0.041);
bool windowClosed = false;

VirtualControllersManager VCtrlMgr;

HeadTracker Tracker;

class AppManager {
public:
	AppManager() {
		__hook(&VirtualControllersManager::OnVirtualControllerConnect, &VCtrlMgr, &AppManager::OnVirtualControllerConnect_Callback, this);
		__hook(&VirtualControllersManager::OnVirtualControllerDisconnect, &VCtrlMgr, &AppManager::OnVirtualControllerDisconnect_Callback, this);
		__hook(&VirtualControllersManager::OnVirtualControllerBeginInputSearch, &VCtrlMgr, &AppManager::OnVirtualControllerBeginInputSearch_Callback, this);
		__hook(&VirtualControllersManager::OnVirtualControllerEndInputSearch, &VCtrlMgr, &AppManager::OnVirtualControllerEndInputSearch_Callback, this);
	}

private:
	void OnVirtualControllerConnect_Callback(VirtualControllersManager::VirtualControllerInfo info) {
		AppManager_Managed::Trigger_OnVirtualControllerConnect(info);
	}

	void OnVirtualControllerDisconnect_Callback(uint8_t ctrlIndex) {
		AppManager_Managed::Trigger_OnVirtualControllerDisconnect(ctrlIndex);
	}

	void OnVirtualControllerBeginInputSearch_Callback(uint8_t ctrlIndex) {
		AppManager_Managed::Trigger_OnVirtualControllerBeginInputSearch(ctrlIndex);
	}

	void OnVirtualControllerEndInputSearch_Callback(uint8_t ctrlIndex) {
		AppManager_Managed::Trigger_OnVirtualControllerEndInputSearch(ctrlIndex);
	}

};


//[MTAThread]
[STAThread]
int main() {

	cout << "udp: " << UdpSender::Init();
	//Sender = UdpSender("127.0.0.1", 4242);
	//Global::Sender = gcnew CLI::ManagedObject<UdpSender>(&Sender);
	//Global::MadgwickFusion = gcnew CLI::ManagedObject<Madgwick>(&Gyro);
	//Global::Rotation = gcnew CLI::ManagedObject<Quaternion>(Quaternion::Identity());
	//Tracker.SetTarget("127.0.0.1", 42420);
	Global::Tracker = gcnew CLI::ManagedObject<HeadTracker>(&Tracker);
	Global::VirtualControllersManager = gcnew CLI::ManagedObject<VirtualControllersManager>(&VCtrlMgr);

	AppManager EvtReciever;
	AppManager_Managed::Init();
	printf("\nVBus: %i", isVBusExists());
	UnPlugForce(1);
	UnPlugForce(2);
	UnPlugForce(3);
	UnPlugForce(4);
	Sleep(500);

	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);

	/*double head[6] = { 0,0,0,0,0,0 };
	double* headPtr = head;*/

	/*bool a = Sender.Send((const char*)headPtr, sizeof(double[6]));

	if (a)
		std::cout << std::endl << "good";
	else
		std::cout << std::endl << "error";*/

	//Tracker.Update(Vector3(50, 50, 50), Vector3(0, 1, 0), 0.1);
	
	//Global::ControllersManager.DiscoverControllers();
	Global::ControllersManager.RunThread();

	MainGUI^ mainGUI = gcnew MainGUI();

	Application::Run(mainGUI);
	windowClosed = true;
	//Threaded^ Ti = gcnew Threaded();
	//Thread^ T = gcnew Thread(gcnew ThreadStart(Ti, &Threaded::readData));

	//T->Start();

	//mainGUI->BackColor = System::Drawing::Color::Transparent;
	//mainGUI->FormBorderStyle = System::Windows::Forms::FormBorderStyle::None;
	//mainGUI->Opacity = 0.50f;

	Global::ControllersManager.StopThread();

	Sender.Close();

	UdpSender::Release();

	return 0;

}