#include "SwitchController.h"

SwitchController::SwitchController() :
	m_globalCount(0),
	m_bluetooth(true)
{
	for (size_t i = 0; i < 4; i++)
	{
		m_playerLights[i] = PlayerLight::Off;
	}

	m_buttonState.right.Home = false;

}


SwitchController::~SwitchController()
{
	hid_close(m_pHandle);
	//this = nullptr;
}

SwitchController::UpdateReturn SwitchController::Update(bool forceSync)
{
	//m_mutex.lock();

	uint8_t buffer[0x40];
	memset(buffer, 0, 0x40);
	if (forceSync)
		SendCommand(0x1E, 0, 0);
	hid_set_nonblocking(m_pHandle, 1);

	int res = hid_read(m_pHandle, buffer, 0x40);
	int resLoop = res;
	while (0 < resLoop){
		res = resLoop;
		resLoop = hid_read(m_pHandle, buffer, 0x40);
	}
	//for (size_t i = 0; hid_read(m_pHandle, buffer, 0x40) && i < 100; i++){	}
	uint8_t USBOffset = m_bluetooth ? 0 : 10;

	if (res > 0) {
		DecodeBattery(buffer + 2 + USBOffset);
		DecodeButton(buffer + 3 + USBOffset);
		DecodeStick(buffer + 6 + USBOffset);
		if (m_imuIsEnable) {
			m_movement_0.Accelerometer = DecodeAccelerometer(buffer + 13 + USBOffset);
			m_movement_0.Gyroscope = DecodeGyroscope(buffer + 19 + USBOffset);

			m_movement_1.Accelerometer = DecodeAccelerometer(buffer + 13 + 12 + USBOffset);
			m_movement_1.Gyroscope = DecodeGyroscope(buffer + 19 + 12 + USBOffset);

			m_movement_2.Accelerometer = DecodeAccelerometer(buffer + 13 + 12 + USBOffset);
			m_movement_2.Gyroscope = DecodeGyroscope(buffer + 19 + 12 + USBOffset);

			m_movement_avg.Accelerometer = (m_movement_0.Accelerometer + m_movement_1.Accelerometer + m_movement_2.Accelerometer) / 3;
			m_movement_avg.Gyroscope = (m_movement_0.Gyroscope + m_movement_1.Gyroscope + m_movement_2.Gyroscope) / 3;

		}
		std::chrono::steady_clock::time_point currentUpdateTime = std::chrono::high_resolution_clock::now();
		auto timeSince = std::chrono::duration_cast<std::chrono::microseconds>(currentUpdateTime - m_lastUpdateTime);

		m_deltaUpdateTime = timeSince.count() / 1000000.0;

		m_lastUpdateTime = currentUpdateTime;
		//std::cout << std::endl << "UpdateInput";
		//m_mutex.unlock();

		return SwitchController::UpdateReturn::Updated;
	}
	if (resLoop < 0) {
		std::cout << std::endl << "Error: " << static_cast<int>(m_controllerType) << std::endl;
		std::wcout << L"===" << _wcsdup(hid_error(m_pHandle));
		//SwitchController::Disconnect();
		//m_mutex.unlock();
		return SwitchController::UpdateReturn::Error;
	}
	//uint8_t a[1] = { 0x01 };
	////hid_set_nonblocking(m_pHandle, 0);
	//res = hid_write(m_pHandle, a, 1);
	//if (res < 0){
	//	//Handle hid error
	//	std::cout << "Error: " << static_cast<int>(m_controllerType) << std::endl;
	//	//delete this;
	//}

	//m_mutex.unlock();
	//std::chrono::steady_clock::time_point currentUpdateTime = std::chrono::high_resolution_clock::now();
	//auto timeSince = std::chrono::duration_cast<std::chrono::microseconds>(currentUpdateTime - m_lastUpdateTime);

	//double deltaUpdateTime = timeSince.count() / 1000000.0;
	//if(deltaUpdateTime > 0.015)
	//	SendCommand(0x1E, 0, 0);

	return SwitchController::UpdateReturn::NotUpdated;
}

void SwitchController::CalibrateGyroscope(void(*f)(SwitchController&))
{
	m_gyroscopeMinNoise = Vector3(3, 3, 3);
	m_gyroscopeMaxNoise = Vector3(-3, -3, -3);

	m_gyroscopeOffsetsSum += m_movement_0.Gyroscope;
	m_gyroscopeOffsetsSum += m_movement_1.Gyroscope;
	m_gyroscopeOffsetsSum += m_movement_2.Gyroscope;

	m_gyroscopeCalibrationSampleCount = 3;
}


void SwitchController::SetPlayerLight(uint8_t index, PlayerLight state)
{

	//m_mutex.lock();

	index = index > 3 ? 3 : index; // 0 <= index <= 3

	uint8_t buf[1] = { 0 };

	m_playerLights[index] = state;

	for (size_t i = 0; i < 4; i++)
	{

		buf[0] = buf[0] | (static_cast<uint8_t>(m_playerLights[i]) << i);

	}

	SendSubcommand(SUBCMD_SET_PLAYERLIGHTS, buf, 1);

	//m_mutex.unlock();

}


//a refaire
void SwitchController::SetPlayerLights(PlayerLight playerLightA, PlayerLight playerLightB, PlayerLight playerLightC, PlayerLight playerLightD)
{

	//m_mutex.lock();

	uint8_t buf[1];
	buf[0] = 0;

	//===================[ PlayerLights Data ]=================================
	//
	//      bit#----------: 7654 3210
	//      playerlight#--: 4321 4321
	//      flash bit-----: 1111 0000
	//      on bit--------: 0000 1111
	//      
	//      ex - : p1: on
	//             p2: flash
	//             p3: off         =>     binary data: 0010 0001
	//             p4: off
	//
	//=========================================================================
	m_playerLights[0] = playerLightA;
	m_playerLights[1] = playerLightB;
	m_playerLights[2] = playerLightC;
	m_playerLights[3] = playerLightD;


	for (size_t i = 0; i < 4; i++)
	{

		buf[0] = buf[0] | (static_cast<uint8_t>(m_playerLights[i]) << i);

	}

	SendSubcommand(SUBCMD_SET_PLAYERLIGHTS, buf, 1);

	//m_mutex.unlock();

}

SwitchController::CommandRespond SwitchController::SendCommand(uint8_t command, uint8_t * pData, int len, bool waitResponse)
{
	//m_mutex.lock();

	uint8_t buf[0x40];
	uint8_t cleanbuf[0x40];

	memset(buf, 0, 0x40);

	if (!m_bluetooth) {
		buf[0x00] = 0x80;
		buf[0x01] = 0x92;
		buf[0x03] = 0x31;
	}

	buf[m_bluetooth ? 0x0 : 0x8] = command;
	if (pData != __nullptr && len != 0) {
		memcpy(buf + (m_bluetooth ? 0x1 : 0x9), pData, len);
	}

	if (waitResponse) {
		hid_set_nonblocking(m_pHandle, 1);
		while (hid_read(m_pHandle, cleanbuf, 0x40)) {}
	}

	hid_set_nonblocking(m_pHandle, 0);
	//hid_exchange(handle, buf, len + (m_bluetooth ? 0x1 : 0x9));
	hid_write(m_pHandle, buf, len + (m_bluetooth ? 0x1 : 0x9));
	memset(buf, 0, 0x40);
	hid_read(m_pHandle, buf, 0x40);

	SwitchController::CommandRespond res;
	if (waitResponse) {
		memcpy(res.buffer, buf, 0x40);
		/*for (size_t i = 0; i < 0x40; i++)
		{
			res.buffer[i] = buf[i];
		}*/
	}
	
	//m_mutex.unlock();

	return res;
	//if (pData) {
	//	memcpy(data, buf, 0x40);
	//}

}

SwitchController::CommandRespond SwitchController::SendSubcommand(uint8_t subcommand, uint8_t * pData, int len, bool waitResponse)
{
	//m_mutex.lock();

	uint8_t buf[0x40];
	memset(buf, 0, 0x40);

	uint8_t rumble_base[9] = { (++m_globalCount) & 0xF, 0x00, 0x01, 0x40, 0x40, 0x00, 0x01, 0x40, 0x40 };
	//rumble_base[0] = (++m_globalCount) & 0xF;
	memcpy(buf, rumble_base, 9);

	buf[9] = subcommand;
	if (pData && len != 0) {
		memcpy(buf + 10, pData, len);
	}

	uint8_t USBOffset = m_bluetooth ? 0 : 10;

	SwitchController::CommandRespond res = SendCommand(CMD_RUMBLE_SUBCMD, buf, 10 + len, waitResponse);
	uint8_t output[0x40];
	if (waitResponse && (res.buffer[0] != 0x21 || res.buffer[13] != 0x80 + subcommand || res.buffer[14] != subcommand)) {
		while (output[0] != 0x21 || output[13] != 0x80 + subcommand || output[14 + USBOffset] != subcommand)
		{
			hid_read(m_pHandle, output, 0x40);
		}
		memcpy(res.buffer, output, 0x40);
		/*for (size_t i = 0; i < 0x40; i++)
		{
			res.buffer[i] = output[i];
		}*/
	}

	//m_mutex.unlock();

	return res;
}

bool SwitchController::GetSPIData(uint32_t offset, uint8_t size, uint8_t * pOutBuffer)
{
	//m_mutex.lock();

	uint8_t USBOffset = m_bluetooth ? 0 : 10;

	uint8_t buf[5];
	SPILocation *spiLoc = (SPILocation *)buf;

	spiLoc->Offset = offset;
	spiLoc->Size = size;

	SwitchController::CommandRespond res = SendSubcommand(SUBCMD_READ_SPIFLASH, buf, 5, true);
	while (res.buffer[0] != 0x21 || 
		(*(uint16_t*)&res.buffer[0xD + USBOffset] != 0x1090) || 
		(*(uint32_t*)&res.buffer[0xF + USBOffset] != spiLoc->Offset) ||
		res.buffer[0x13 + USBOffset] != spiLoc->Size) {

		hid_read(m_pHandle, res.buffer, res.bufferLen);
	}
	/*printf("\nGetSPIData: ");
	for (size_t i = 0; i < 0x20; i++)
	{
		printf("%02x ", res.buffer[i]);
	}*/
	memcpy(pOutBuffer, res.buffer + 0x14 + USBOffset, spiLoc->Size);

	//m_mutex.unlock();

	return true;
}

void SwitchController::EnableVibration(bool isEnable)
{
	//m_mutex.lock();

	uint8_t buf[1] = { isEnable ? 0x01 : 0x00 };

	SendSubcommand(SUBCMD_ENABLE_VIBRATION, buf, 1);
	
	//m_mutex.unlock();

}

void SwitchController::EnableIMU(bool isEnable)
{
	//m_mutex.lock();

	uint8_t buf[1] = { isEnable ? 0x01 : 0x00 };

	m_imuIsEnable = isEnable;

	SendSubcommand(SUBCMD_ENABLE_IMU, buf, 1);

	//m_mutex.unlock();

}

bool SwitchController::GetIMUIsEnable()
{
	//m_mutex.lock();

	bool res = m_imuIsEnable;

	//m_mutex.unlock();

	return res;
}

SwitchController::MacAdress SwitchController::GetMacAdress()
{
	//m_mutex.lock();

	MacAdress m;
	memcpy(m.u8_6,m_macAdress,6);
	m.Str = m_macAdress_str;
	//m_mutex.unlock();

	return m;
}

SwitchController::PlayerLight SwitchController::GetPlayerLight(uint8_t index)
{
	if (index < 0 || index > 3)
		return SwitchController::PlayerLight::Off;

	return m_playerLights[index];
}

//char* SwitchController::GetPath()
//{
//	return m_path;
//}

std::wstring SwitchController::GetSerialNumber()
{
	return m_serialNumber;
}

SwitchController::ButtonState SwitchController::GetButtonState()
{
	//m_mutex.lock();

	SwitchController::ButtonState res = m_buttonState;

	//m_mutex.unlock();

	return res;
}

SwitchController::StickState SwitchController::GetStickState()
{
	//m_mutex.lock();

	SwitchController::StickState res = m_stickState;

	//m_mutex.unlock();

	return res;
}

Vector3 SwitchController::GetGyroscopeState() 
{
	//m_mutex.lock();

	Vector3 res = m_movement_0.Gyroscope;

	//m_mutex.unlock();

	return res;
}

SwitchController::Movement SwitchController::GetIMU0State()
{
	return m_movement_0;
}

SwitchController::Movement SwitchController::GetIMU1State()
{
	return m_movement_1;
}

SwitchController::Movement SwitchController::GetIMU2State()
{
	return m_movement_2;
}

SwitchController::Movement SwitchController::GetIMUAvgState()
{
	return m_movement_avg;
}

double SwitchController::GetDeltaUpdateTime()
{
	//m_mutex.lock();
	double res = m_deltaUpdateTime;
	//m_mutex.unlock();

	return res;
}

bool SwitchController::GetCharging()
{
	return m_charging;
}

SwitchController::BatteryLevel SwitchController::GetBatteryLevel()
{
	return m_batteryLevel;
}

SwitchController::ControllerType SwitchController::GetControllerType()
{
	//m_mutex.lock();

	SwitchController::ControllerType res = m_controllerType;

	//m_mutex.unlock();

	return res;
}

Color24 SwitchController::GetBodyColor()
{
	return m_bodyColor;
}

Color24 SwitchController::GetButtonsColor()
{
	return m_buttonsColor;
}

Color24 SwitchController::GetLeftGripColor()
{
	return m_leftGripColor;
}

Color24 SwitchController::GetRightGripColor()
{
	return m_rightGripColor;
}

void SwitchController::Disconnect()
{
	//m_mutex.lock();
	uint8_t buf[1] = { HCI_STATE_SLEEPMODE };
	SendSubcommand(SUBCMD_SET_HCI_STATE, buf, 1);
	//hid_close(m_pHandle);
	//m_controllerType = SwitchController::ControllerType::None;
	//m_serialNumber = std::wstring();
	for (size_t i = 0; i < 4; i++)
	{
		m_playerLights[i] = SwitchController::PlayerLight::Off;
	}

	//m_mutex.unlock();

}

void SwitchController::SetInputReportMode(InputReportMode reportMode)
{
	//m_mutex.lock();

	uint8_t buf[1] = { static_cast<uint8_t>(reportMode) };

	SendSubcommand(SUBCMD_SET_INPUTREPORTMODE, buf, 1);

	//m_mutex.unlock();

}

void SwitchController::SetHandle(hid_device * pHandle)
{
	//m_mutex.lock();

	m_pHandle = pHandle;
	//SetInputReportMode(SwitchController::InputReportMode::SimpleHID);

	if (!m_bluetooth) {
		m_pCurrentLeftStickCalibration = &m_factoryLeftStickCalibration;
		m_pCurrentRightStickCalibration = &m_factoryRightStickCalibration;
		m_pCurrentMotionCalibration = &m_factoryMotionCalibration;

		uint8_t forceUSBHID[2] = {80, 04};
		uint8_t handShake[2] = {80, 02};
		uint8_t baudRate[2] = {80, 03};

		hid_write(m_pHandle, handShake, 2);
		hid_write(m_pHandle, baudRate, 2);
		hid_write(m_pHandle, handShake, 2);
		hid_write(m_pHandle, forceUSBHID, 2);

		return;
	}
	
	RetrieveFactoryStickCalibration();
	RetrieveUserStickCalibration();
	//l
	if (m_userLeftStickCalibrationIsAvailable) {
		m_pCurrentLeftStickCalibration = &m_userLeftStickCalibration;
	}
	else {
		m_pCurrentLeftStickCalibration = &m_factoryLeftStickCalibration;
	}
	//r
	if (m_userRightStickCalibrationIsAvailable) {
		m_pCurrentRightStickCalibration = &m_userRightStickCalibration;
	}
	else {
		m_pCurrentRightStickCalibration = &m_factoryRightStickCalibration;
	}

	//Movement
	RetrieveFactoryMotionCalibration();
	RetrieveUserMotionCalibration();
	if (m_userMotionCalibrationIsAvailable)
		m_pCurrentMotionCalibration = &m_userMotionCalibration;
	else
		m_pCurrentMotionCalibration = &m_factoryMotionCalibration;

	//std::cout << std::endl << m_pCurrentMotionCalibration->Origin.Gyroscope.X;

	RetrieveControllerInfo();

	RetrieveColors();

	//m_mutex.unlock();

}

bool SwitchController::Init(hid_device_info * device)
{
	//m_mutex.lock();

	//Test

	/*uint16_t a = 65522;
	int16_t b = ToolBox::Converter::CopyBitsUint16ToInt16(a);
	double c = b;

	std::cout << std::endl << "a: " << a << " b: " << b << " c :" << c;*/

	if (device->vendor_id != JOYCON_VENDOR ||
		!(
			device->product_id == JOYCON_L_BT ||
			device->product_id == JOYCON_R_BT ||
			device->product_id == PRO_CONTROLLER /*||
			device->product_id == JOYCON_CHARGING_GRIP*/
			)) {
		//m_mutex.unlock();
		return false;
	}
	m_serialNumber = _wcsdup(device->serial_number);

	std::string path = _strdup(device->path);
	if (path.find("hid#{") != std::string::npos)
		m_bluetooth = true;
	else
		m_bluetooth = false;

	/*m_path = device->path;*/
	hid_device *handle = hid_open_path(device->path);
	if (handle == NULL) {
		//m_mutex.unlock();
		return false;
	}
	//SetHandle(hid_open_path(device->path));
	
	SetHandle(handle);
	//EnableIMU(false);
	//m_mutex.unlock();

	return true;
}

void SwitchController::DecodeButton(uint8_t * pData)
{
	//Left Part															//Horizontal Mode
	m_buttonState.left.Down = (pData[2] & (1 << 0)) ? true : false;		//Right
	m_buttonState.left.Up = (pData[2] & (1 << 1)) ? true : false;		//Left
	m_buttonState.left.Right = (pData[2] & (1 << 2)) ? true : false;	//Up
	m_buttonState.left.Left = (pData[2] & (1 << 3)) ? true : false;		//Down

	m_buttonState.left.SR = (pData[2] & (1 << 4)) ? true : false;
	m_buttonState.left.SL = (pData[2] & (1 << 5)) ? true : false;
	m_buttonState.left.L = (pData[2] & (1 << 6)) ? true : false;
	m_buttonState.left.ZL = (pData[2] & (1 << 7)) ? true : false;

	m_buttonState.left.Minus = (pData[1] & (1 << 0)) ? true : false;
	m_buttonState.left.Stick = (pData[1] & (1 << 3)) ? true : false;
	m_buttonState.left.ScreenShot = (pData[1] & (1 << 5)) ? true : false;


	//Right Part
	m_buttonState.right.Y = (pData[0] & (1 << 0)) ? true : false;
	m_buttonState.right.X = (pData[0] & (1 << 1)) ? true : false;
	m_buttonState.right.B = (pData[0] & (1 << 2)) ? true : false;
	m_buttonState.right.A = (pData[0] & (1 << 3)) ? true : false;

	m_buttonState.right.SR = (pData[0] & (1 << 4)) ? true : false;
	m_buttonState.right.SL = (pData[0] & (1 << 5)) ? true : false;
	m_buttonState.right.R = (pData[0] & (1 << 6)) ? true : false;
	m_buttonState.right.ZR = (pData[0] & (1 << 7)) ? true : false;

	m_buttonState.right.Plus = (pData[1] & (1 << 1)) ? true : false;
	m_buttonState.right.Stick = (pData[1] & (1 << 2)) ? true : false;
	m_buttonState.right.Home = (pData[1] & (1 << 4)) ? true : false;
}

void SwitchController::DecodeStick(uint8_t * pData)
{

	RawStick leftStick;
	RawStick rightStick;

	leftStick.X = pData[0] | ((pData[1] & 0xF) << 8);
	leftStick.Y = (pData[1] >> 4) | (pData[2] << 4);

	rightStick.Y = (pData[1 + 3] >> 4) | (pData[2 + 3] << 4);
	rightStick.X = pData[0 + 3] | ((pData[1 + 3] & 0xF) << 8);

	switch (m_controllerType)
	{
	case SwitchController::ControllerType::None:
		break;
	case SwitchController::ControllerType::JoyCon_Left:
		SwitchController::ApplyStickCalibration(leftStick, *m_pCurrentLeftStickCalibration, m_stickState.Left);
		m_stickState.Right.X = m_stickState.Right.Y = 0.0f;
		break;
	case SwitchController::ControllerType::JoyCon_Right:
		SwitchController::ApplyStickCalibration(rightStick, *m_pCurrentRightStickCalibration, m_stickState.Right);
		m_stickState.Left.X = m_stickState.Left.Y = 0.0f;
		break;
	case SwitchController::ControllerType::Pro_Controller:
		SwitchController::ApplyStickCalibration(leftStick, *m_pCurrentLeftStickCalibration, m_stickState.Left);
		SwitchController::ApplyStickCalibration(rightStick, *m_pCurrentRightStickCalibration, m_stickState.Right);
		break;
	default:
		break;
	}

	//std::cout << std::endl << "L-[" << m_stickState.Left.X << ":" << m_stickState.Left.Y << "]   R-[" << m_stickState.Right.X << ":" << m_stickState.Right.Y << "]";

}

void SwitchController::DecodeBattery(uint8_t* pData)
{
	uint8_t battery = pData[0] >> 4;

	m_charging = (battery & 0x1);

	m_batteryLevel = static_cast<BatteryLevel>(battery >> 1);
}

Vector3 SwitchController::DecodeGyroscope(uint8_t* pData) {

	double gyroXRaw = ToolBox::Converter::CopyBitsUint16ToInt16(ToolBox::Converter::MergeUint8(pData[1], pData[0])) - ToolBox::Converter::CopyBitsUint16ToInt16(m_pCurrentMotionCalibration->Origin.Gyroscope.X);
	double gyroYRaw = ToolBox::Converter::CopyBitsUint16ToInt16(ToolBox::Converter::MergeUint8(pData[1 + 2], pData[0 + 2])) - ToolBox::Converter::CopyBitsUint16ToInt16(m_pCurrentMotionCalibration->Origin.Gyroscope.Y);
	double gyroZRaw = ToolBox::Converter::CopyBitsUint16ToInt16(ToolBox::Converter::MergeUint8(pData[1 + 4], pData[0 + 4])) - ToolBox::Converter::CopyBitsUint16ToInt16(m_pCurrentMotionCalibration->Origin.Gyroscope.Z);

	//std::cout << std::endl << gyroXRaw << " | " << gyroXRaw * m_pCurrentMotionCalibration->Coef.Gyroscope.X;
	/*m_movement.Gyroscope.X = gyroXRaw * m_pCurrentMotionCalibration->Coef.Gyroscope.X;
	m_movement.Gyroscope.Y = gyroYRaw * m_pCurrentMotionCalibration->Coef.Gyroscope.Y;
	m_movement.Gyroscope.Z = gyroZRaw * m_pCurrentMotionCalibration->Coef.Gyroscope.Z;*/

	return Vector3(
		gyroXRaw * m_pCurrentMotionCalibration->Coef.Gyroscope.X,
		gyroYRaw * m_pCurrentMotionCalibration->Coef.Gyroscope.Y,
		gyroZRaw * m_pCurrentMotionCalibration->Coef.Gyroscope.Z
	);
}

void SwitchController::ApplyGyroscopeDeadzone(Vector3 &gyro)
{
	Vector3 offset = (m_gyroscopeMinNoise + m_gyroscopeMaxNoise) / 2;

	Vector3 centerGyro = gyro -offset;

	Vector3 deadOffset = (m_gyroscopeMaxNoise - m_gyroscopeMinNoise) / 2;
	
	deadOffset += Vector3(0.0697, 0.0697, 0.0697);

	std::cout << std::endl << "Nm: " << m_gyroscopeMinNoise.Z << " NM:" << m_gyroscopeMaxNoise.Z << "Zr: " << gyro.Z << " Zo: " << offset.Z << " Zc: " << centerGyro.Z;

	//0.2091

	/*if (-deadOffset.X < centerGyro.X && centerGyro.X < deadOffset.X) {
		gyro.X = 0;
	}
	if (-deadOffset.Y < centerGyro.Y && centerGyro.Y < deadOffset.Y) {
		gyro.Y = 0;
	}
	if (-deadOffset.Z < centerGyro.Z && centerGyro.Z < deadOffset.Z) {
		std::cout << " C ";
		gyro.Z = 0;
	}*/

	if (m_gyroscopeMinNoise.X - 0.0697 < gyro.X && gyro.X < m_gyroscopeMaxNoise.X + 0.0697) {
		gyro.X = 0;
	}
	if (m_gyroscopeMinNoise.Y - 0.0697 < gyro.Y && gyro.Y < m_gyroscopeMaxNoise.Y + 0.0697) {
		gyro.Y = 0;
	}
	if (m_gyroscopeMinNoise.Z - 0.0697 < gyro.Z && gyro.Z < m_gyroscopeMaxNoise.Z + 0.0697) {
		std::cout << " C ";
		gyro.Z = 0;
	}

	std::cout << " Zf: " << gyro.Z;

	/*gyro = centerGyro;*/
}

Vector3 SwitchController::DecodeAccelerometer(uint8_t* pData) {

	double accelXRaw = ToolBox::Converter::CopyBitsUint16ToInt16(ToolBox::Converter::MergeUint8(pData[1], pData[0]));
	double accelYRaw = ToolBox::Converter::CopyBitsUint16ToInt16(ToolBox::Converter::MergeUint8(pData[1 + 2], pData[0 + 2]));
	double accelZRaw = ToolBox::Converter::CopyBitsUint16ToInt16(ToolBox::Converter::MergeUint8(pData[1 + 4], pData[0 + 4]));

	/*m_movement.Accelerometer.X = accelXRaw * m_pCurrentMotionCalibration->Coef.Accelerometer.X;
	m_movement.Accelerometer.Y = accelYRaw * m_pCurrentMotionCalibration->Coef.Accelerometer.Y;
	m_movement.Accelerometer.Z = accelZRaw * m_pCurrentMotionCalibration->Coef.Accelerometer.Z;*/

	return Vector3(
		accelXRaw * m_pCurrentMotionCalibration->Coef.Accelerometer.X,
		accelYRaw * m_pCurrentMotionCalibration->Coef.Accelerometer.Y,
		accelZRaw * m_pCurrentMotionCalibration->Coef.Accelerometer.Z
	);
}

void SwitchController::ApplyStickCalibration(RawStick & rRawSrick, StickCalibration &rStickCalibration, Stick & rCalibratedStick)
{
	float x, y;

	x = (float)(rRawSrick.X - rStickCalibration.X.Center);
	if (x >= 0) {
		x = x / (float)rStickCalibration.X.MaxAboveCenter;
	}
	else {
		x = x / (float)rStickCalibration.X.MinBelowCenter;
	}
	x = std::clamp(x, -1.0f , 1.0f);

	y = (float)(rRawSrick.Y - rStickCalibration.Y.Center);
	if (y >= 0) {
		y = y / (float)rStickCalibration.Y.MaxAboveCenter;
	}
	else {
		y = y / (float)rStickCalibration.Y.MinBelowCenter;
	}
	y = std::clamp(y, -1.0f, 1.0f);


	// ~15% joy-con          ~10% pro-ctrl
	float deadZoneCenter = 0.10f;

	// !alreadyClamp
	float deadZoneOuter = 0.08f;


	// Credit to Hypersect (Ryan Juckett)
	// http://blog.hypersect.com/interpreting-analog-sticks/

	float mag = sqrtf(x*x + y * y);

	if (mag > deadZoneCenter)
	{
		// scale such that output magnitude is in the range [0.0f, 1.0f]
		float legalRange = 1.0f - deadZoneOuter - deadZoneCenter;
		float normalizedMag = min(1.0f, (mag - deadZoneCenter) / legalRange);
		float scale = normalizedMag / mag;

		rCalibratedStick.X = x * scale;
		rCalibratedStick.Y = y * scale;
	}
	else
	{
		// stick is in the inner dead zone
		rCalibratedStick.X = 0.0f;
		rCalibratedStick.Y = 0.0f;
	}
}	

void SwitchController::DecodeDeviceInfo(uint8_t * pData)
{
	//uint8_t *data = pData + 13;

	/*printf("\nInfo: ");
	for (size_t i = 0; i < 0x40; i++)
	{
		printf("%02x ",pData[i]);
		
	}*/
	
	//data[0] : data[1] FW Version
	memcpy(&m_firmwareVersion, pData, 2);
	//data[2] Controller type
	m_controllerType = static_cast<SwitchController::ControllerType>(pData[2]);
	//data[3] unknow
	//data[4] : data[5] : data[6] :: data[7] : data[8] : data[9] Macadress
	memcpy(&m_macAdress, pData + 4, 6);

	std::stringstream stringStream;
	for (size_t i = 0; i < 6; i++)
	{

		stringStream << std::setfill('0') << std::setw(2) << std::hex << (0 + m_macAdress[i]);
		if (i < 5)
			stringStream << ":";
	}

	m_macAdress_str = stringStream.str();

	//data[11] : use spi color
	m_useSPIColor = pData[11];
	

}

//Not used -> todo: rework
void SwitchController::DecodePacket(uint8_t * pData)
{
	uint8_t * data = pData; //usboffset*
	switch (data[0])
	{
	case 0x21:
		DecodeButton(data+3);
		//DecodeStick()
		DecodeDeviceInfo(data);
		break;

	case 0x30:
		DecodeButton(data + 3);
		//DecodeStick()
		//DecodeIMU
		break;
	default:
		break;
	}
}

Color24 SwitchController::DecodeColor(uint8_t* pData)
{
	return Color24(pData[0], pData[1], pData[2]);
}

SwitchController::StickCalibration SwitchController::DecodeStickCalibration(uint8_t * pData, bool left_right)
{
	uint16_t data[6];
	data[0] = (pData[1] << 8) & 0xF00 | pData[0];
	data[1] = (pData[2] << 4) | (pData[1] >> 4);
	data[2] = (pData[4] << 8) & 0xF00 | pData[3];
	data[3] = (pData[5] << 4) | (pData[4] >> 4);
	data[4] = (pData[7] << 8) & 0xF00 | pData[6];
	data[5] = (pData[8] << 4) | (pData[7] >> 4);

	SwitchController::StickCalibration stickCal;

	//right
	if (left_right) {
		stickCal.X.Center = data[0];
		stickCal.X.MinBelowCenter = data[2];
		stickCal.X.MaxAboveCenter = data[4];

		stickCal.Y.Center = data[1];
		stickCal.Y.MinBelowCenter = data[3];
		stickCal.Y.MaxAboveCenter = data[5];
	}
	else {//left
		stickCal.X.Center = data[2];
		stickCal.X.MinBelowCenter = data[4];
		stickCal.X.MaxAboveCenter = data[0];

		stickCal.Y.Center = data[3];
		stickCal.Y.MinBelowCenter = data[4];
		stickCal.Y.MaxAboveCenter = data[1];
	}

	stickCal.X.Min = stickCal.X.Center - stickCal.X.MinBelowCenter;
	stickCal.X.Max = stickCal.X.Center + stickCal.X.MaxAboveCenter;

	stickCal.Y.Min = stickCal.Y.Center - stickCal.Y.MinBelowCenter;
	stickCal.Y.Max = stickCal.Y.Center + stickCal.Y.MaxAboveCenter;

	return stickCal;
}

SwitchController::Vector3Int16 SwitchController::DecodeVector3(uint8_t* pData)
{
	Vector3Int16 output;
	output.X = ToolBox::Converter::CopyBitsUint16ToInt16(ToolBox::Converter::MergeUint8(pData[1], pData[0]));			//invert octet to get uint16 and not uint16le
	output.Y = ToolBox::Converter::CopyBitsUint16ToInt16(ToolBox::Converter::MergeUint8(pData[1 + 2], pData[0 + 2]));
	output.Z = ToolBox::Converter::CopyBitsUint16ToInt16(ToolBox::Converter::MergeUint8(pData[1 + 4], pData[0 + 4]));

	return output;
}

SwitchController::MovementCalibration SwitchController::DecodeMovementCalibration(uint8_t* pData)
{
	MovementCalibration output;

	//std::cout << std::endl << std::hex << (0xff & pData[12]) << " " << std::hex << (0xff & pData[12 + 1]);

	output.Origin.Accelerometer = DecodeVector3(pData);
	output.Sensitivity.Accelerometer = DecodeVector3(pData + 6);
	output.Origin.Gyroscope = DecodeVector3(pData + 12);
	output.Sensitivity.Gyroscope = DecodeVector3(pData + 18);

	output.Coef.Accelerometer = CalcAccelerometerCoef(output.Origin.Accelerometer, output.Sensitivity.Accelerometer);
	output.Coef.Gyroscope = CalcGyroscopeCoef(output.Origin.Gyroscope, output.Sensitivity.Gyroscope);

	return output;
}

Vector3 SwitchController::CalcGyroscopeCoef(Vector3Int16 origin, Vector3Int16 sensitivity)
{
	Vector3 output;

	output.X = (double)(936.0 / (double)(sensitivity.X - origin.X) );
	//output.X = (double)(936.0 / (double)(13371 - ToolBox::Converter::CopyBitsUint16ToInt16(origin.X)) /** (4000.0 / 65535.0)*/ * 0.01745329251994 );
	output.Y = (double)(936.0 / (double)(sensitivity.Y - origin.Y) );
	output.Z = (double)(936.0 / (double)(sensitivity.Z - origin.Z) );

	return output;
}

Vector3 SwitchController::CalcAccelerometerCoef(Vector3Int16 origin, Vector3Int16 sensitivity)
{
	Vector3 output;

	output.X = (double)(1.0 / (double)(sensitivity.X - /*ToolBox::Converter::CopyBitsUint16ToInt16*/(origin.X))) * 4.0f;
	output.Y = (double)(1.0 / (double)(sensitivity.Y - /*ToolBox::Converter::CopyBitsUint16ToInt16*/(origin.Y))) * 4.0f;
	output.Z = (double)(1.0 / (double)(sensitivity.Z - /*ToolBox::Converter::CopyBitsUint16ToInt16*/(origin.Z))) * 4.0f;

	return output;
}

void SwitchController::RetrieveControllerInfo()
{
	SwitchController::CommandRespond res = SendSubcommand(SUBCMD_REQUESTDEVICEINFO, 0x00, 0, true);
	/*printf("\nInfo: ");
	for (size_t i = 0; i < 0x20; i++)
	{
		printf("%02x ", res.buffer[i]);

	}*/
	if (res.buffer[0] == 0x21 && res.buffer[13] == 0x82 && res.buffer[14] == 0x02) { //check if is subcmd 0x02 res
		DecodeDeviceInfo(res.buffer + 15);
	}
	//std::cout << std::endl << "Info: " << static_cast<int>(m_controllerType) << "  SPIColor: " << m_useSPIColor;
}

void SwitchController::RetrieveColors()
{
	uint8_t res[SPI_COLOR_SIZE * 4];
	GetSPIData(SPI_BODY_COLOR_OFFSET, SPI_COLOR_SIZE * 4, res);

	m_bodyColor = DecodeColor(res);
	m_buttonsColor = DecodeColor(res + SPI_COLOR_SIZE * 1);
	m_leftGripColor = DecodeColor(res + SPI_COLOR_SIZE * 2);
	m_rightGripColor = DecodeColor(res + SPI_COLOR_SIZE * 3);
}



void SwitchController::RetrieveFactoryStickCalibration()
{
	//Left
	uint8_t res[SPI_STICKCALIBRATION_SIZE];
	GetSPIData(SPI_FACTORY_LEFTSTICKCALIBRATION_OFFSET, SPI_STICKCALIBRATION_SIZE, res);
	m_factoryLeftStickCalibration = DecodeStickCalibration(res, false);

	//Right
	GetSPIData(SPI_FACTORY_RIGHTSTICKCALIBRATION_OFFSET, SPI_STICKCALIBRATION_SIZE, res);
	m_factoryRightStickCalibration = DecodeStickCalibration(res, true);
}

void SwitchController::RetrieveUserStickCalibration()
{
	uint8_t resCal[SPI_STICKCALIBRATION_SIZE];

	//Left
	GetSPIData(SPI_USER_LEFTSTICKCALIBRATION_OFFSET, SPI_STICKCALIBRATION_SIZE, resCal);
	m_userLeftStickCalibration = DecodeStickCalibration(resCal, false);

	//Right
	GetSPIData(SPI_USER_RIGHTSTICKCALIBRATION_OFFSET, SPI_STICKCALIBRATION_SIZE, resCal);
	m_userRightStickCalibration = DecodeStickCalibration(resCal, true);

	//isAvailable
	uint8_t resAv[SPI_USER_STICKCALIBRATION_ISAVAILABLE_SIZE];

	//Left
	GetSPIData(SPI_USER_LEFTSTICKCALIBRATION_ISAVAILABLE_OFFSET, SPI_USER_STICKCALIBRATION_ISAVAILABLE_SIZE, resAv);
	m_userLeftStickCalibrationIsAvailable = *(uint16_t*)&resAv == 0xA1B2 ? true : false;

	//Right
	GetSPIData(SPI_USER_RIGHTSTICKCALIBRATION_ISAVAILABLE_OFFSET, SPI_USER_STICKCALIBRATION_ISAVAILABLE_SIZE, resAv);
	m_userRightStickCalibrationIsAvailable = *(uint16_t*)&resAv == 0xA1B2 ? true : false;

	//std::cout << std::endl << "UserCL: " << m_userLeftStickCalibrationIsAvailable << " UserCR: " << m_userRightStickCalibrationIsAvailable;
}

void SwitchController::RetrieveFactoryMotionCalibration()
{
	uint8_t res[SPI_MOTIONCALIBRATION_SIZE];

	GetSPIData(SPI_FACTORY_MOTIONCALIBRATION_OFFSET, SPI_MOTIONCALIBRATION_SIZE, res);

	m_factoryMotionCalibration = DecodeMovementCalibration(res);
}

void SwitchController::RetrieveUserMotionCalibration()
{
	uint8_t resCal[SPI_MOTIONCALIBRATION_SIZE];

	GetSPIData(SPI_USER_MOTIONCALIBRATION_OFFSET, SPI_MOTIONCALIBRATION_SIZE, resCal);
	m_userMotionCalibration = DecodeMovementCalibration(resCal);

	uint8_t resAv[SPI_USER_MOTIONCALIBRATION_ISAVAILABLE_SIZE];

	GetSPIData(SPI_USER_MOTIONCALIBRATION_ISAVAILABLE, SPI_USER_MOTIONCALIBRATION_ISAVAILABLE_SIZE, resAv);
	m_userMotionCalibrationIsAvailable = *(uint16_t*)&resAv == 0xA1B2 ? true : false;
}

