#include "VirtualController.h"



VirtualController::VirtualController():
	m_controllerIndex(0),
	m_inputMode(InputMode::None),
	m_pPrimaryController(__nullptr),
	m_pSecondaryController(__nullptr)
{

}

VirtualController::VirtualController(uint8_t controllerIndex)
{
	m_controllerIndex = controllerIndex;
}


VirtualController::~VirtualController()
{
	Disable();
}

bool VirtualController::Enable()
{
	if (GetState() == State::Ready) {
		PlugIn(m_controllerIndex + 1);

		return true;
	}
	
	return false;
}

bool VirtualController::Disable()
{

	if (GetState() == State::Enable) {
		UnPlug(m_controllerIndex + 1);

		return true;
	}

	return false;
}

void VirtualController::SetControllerIndex(uint8_t index)
{
	m_controllerIndex = index < 4 ? index : 3;
}

void VirtualController::SetInputMode(InputMode mode)
{
	//TODO
}

VirtualController::State VirtualController::GetState()
{
	if (!isVBusExists()) {
		return VirtualController::State::ScpVBusNotFound;
	}
	else if (isControllerOwned(m_controllerIndex+1)) {
		return VirtualController::State::Enable;
	}
	else if (isControllerExists(m_controllerIndex+1)) {
		return VirtualController::State::SlotAlreadyUsed;
	}
	return VirtualController::State::Ready;
}

uint8_t VirtualController::GetLedCount()
{
	uint8_t led = 0;

	if(GetLedNumber(m_controllerIndex + 1, &led))
		return led;
	
	return 0;
}

SwitchController* VirtualController::GetSwitchController(uint8_t controllerIndex)
{
	if (controllerIndex == 0) {
		return m_pPrimaryController;
	}
	else if (controllerIndex == 1) {
		return m_pSecondaryController;
	}

	return __nullptr;
}

bool VirtualController::SetSingleInputController(SwitchController * pController)
{
	SwitchController::ControllerType typeCtrl = pController->GetControllerType();

	SwitchController::PlayerLight playerLights[4];

	for (size_t i = 0; i < 4; i++)
	{
		playerLights[i] = (m_controllerIndex == i ? SwitchController::PlayerLight::On : SwitchController::PlayerLight::Off);
	}

	switch (typeCtrl)
	{
	case SwitchController::ControllerType::JoyCon_Left:
		m_pPrimaryController = pController;
		m_inputMode = InputMode::JoyCon_Left_Horizontal;
		m_pPrimaryController->SetPlayerLights(playerLights[0], playerLights[1], playerLights[2], playerLights[3]);
		return true;
		break;
	case SwitchController::ControllerType::JoyCon_Right:
		m_pPrimaryController = pController;
		m_inputMode = InputMode::JoyCon_Right_Horizontal;
		m_pPrimaryController->SetPlayerLights(playerLights[0], playerLights[1], playerLights[2], playerLights[3]);
		return true;
		break;
	case SwitchController::ControllerType::Pro_Controller:
		m_pPrimaryController = pController;
		m_inputMode = InputMode::Pro_Controller;
		m_pPrimaryController->SetPlayerLights(playerLights[0], playerLights[1], playerLights[2], playerLights[3]);
		return true;
		break;
	case SwitchController::ControllerType::None:
		return false;
		break;
	default:
		break;
	}

	return false;
}

bool VirtualController::SetCombinedInputController(SwitchController * pControllerA, SwitchController * pControllerB)
{
	SwitchController::ControllerType typeCtrlA = pControllerA->GetControllerType();
	SwitchController::ControllerType typeCtrlB = pControllerB->GetControllerType();

	//pCtrlA & pCtrlB must be JoyCon
	if (typeCtrlA == SwitchController::ControllerType::Pro_Controller || typeCtrlB == SwitchController::ControllerType::Pro_Controller) {
		return false;
	}

	SwitchController::PlayerLight playerLights[4];

	for (size_t i = 0; i < 4; i++)
	{
		playerLights[i] = (m_controllerIndex == i ? SwitchController::PlayerLight::On : SwitchController::PlayerLight::Off);
	}

	if (typeCtrlA == SwitchController::ControllerType::JoyCon_Left && typeCtrlB == SwitchController::ControllerType::JoyCon_Right) {
		m_pPrimaryController = pControllerA;
		m_pSecondaryController = pControllerB;
		m_inputMode = InputMode::JoyCon_Combined;

		m_pPrimaryController->SetPlayerLights(playerLights[0], playerLights[1], playerLights[2], playerLights[3]);
		m_pSecondaryController->SetPlayerLights(playerLights[0], playerLights[1], playerLights[2], playerLights[3]);
		return true;
	}

	if (typeCtrlA == SwitchController::ControllerType::JoyCon_Right && typeCtrlB == SwitchController::ControllerType::JoyCon_Left) {
		m_pPrimaryController = pControllerB;
		m_pSecondaryController = pControllerA;
		m_inputMode = InputMode::JoyCon_Combined;
		m_pPrimaryController->SetPlayerLights(playerLights[0], playerLights[1], playerLights[2], playerLights[3]);
		m_pSecondaryController->SetPlayerLights(playerLights[0], playerLights[1], playerLights[2], playerLights[3]);
		return true;
	}

	// 2 JoyCon_Left or 2 JoyCon_Right
	return false;
}

bool VirtualController::Update()
{
	if (GetState() != VirtualController::State::Enable) {
		return false;
	}

	//TODO
	if (m_inputMode == VirtualController::InputMode::None) {
		return false;
	}

	if (m_inputMode == VirtualController::InputMode::JoyCon_Combined &&
		m_pPrimaryController != __nullptr &&
		m_pSecondaryController != __nullptr) {

		if ( !(m_pPrimaryController->Update() && m_pSecondaryController->Update()) ) {
			return false;
		}

		SwitchController::ButtonState BtnStateA = m_pPrimaryController->GetButtonState();
		SwitchController::ButtonState BtnStateB = m_pSecondaryController->GetButtonState();

		SwitchController::StickState stickStateA = m_pPrimaryController->GetStickState();
		SwitchController::StickState stickStateB = m_pSecondaryController->GetStickState();
		//std::cout << std::endl << "BtnA : " << BtnStateB.right.A;
		//std::cout << std::endl << "BtnHome : " << BtnStateB.right.Home;
		//std::cout << std::endl << "L-[" << stickStateB.Right.X << ":" << stickStateB.Right.Y << "]";//   R - [" << m_stickState.Right.X << ":" << m_stickState.Right.Y << "]";

		VirtualController::VXboxInput xboxInput;

		xboxInput.Buttons.A = BtnStateB.right.A;
		xboxInput.Buttons.B = BtnStateB.right.B;
		xboxInput.Buttons.X = BtnStateB.right.X;
		xboxInput.Buttons.Y = BtnStateB.right.Y;

		xboxInput.Buttons.Start = BtnStateB.right.Plus;
		xboxInput.Buttons.Back = BtnStateA.left.Minus;

		xboxInput.Buttons.LB = BtnStateA.left.L;
		xboxInput.Buttons.RB = BtnStateB.right.R;

		xboxInput.Buttons.LeftStick = BtnStateA.left.Stick;
		xboxInput.Buttons.RightStick = BtnStateB.right.Stick;

		xboxInput.Axis.LeftStick.x = stickStateA.Left.X;
		xboxInput.Axis.LeftStick.y = stickStateA.Left.Y;

		xboxInput.Axis.RightStick.x = stickStateB.Right.X;
		xboxInput.Axis.RightStick.y = stickStateB.Right.Y;

		xboxInput.Axis.LT = BtnStateA.left.ZL * 255;
		xboxInput.Axis.RT = BtnStateB.right.ZR * 255;

		uint8_t dpadData = (BtnStateA.left.Up * 0x1) +
			(BtnStateA.left.Right * 0x2) +
			(BtnStateA.left.Down * 0x4) +
			(BtnStateA.left.Left * 0x8);

		if (dpadData) {
			uint8_t upFront = (dpadData ^ m_lastDPad) & dpadData;
			if (upFront) {
				xboxInput.Buttons.Dpad = static_cast<VXboxInput::Buttons::DPad>(upFront);
				m_lastDPad = dpadData;
			}
		}
		else
		{
			xboxInput.Buttons.Dpad = VXboxInput::Buttons::DPad::None;
			m_lastDPad = dpadData;
		}

		ApplyVXboxInput(xboxInput);
	}
	else
		return false;

	return true;
}

VirtualController::VXboxInput m_xboxInput;

bool VirtualController::Update(int8_t controllerIndex, double deltaTime)
{
	if (GetState() != VirtualController::State::Enable) {
		return false;
	}

	//TODO
	if (m_inputMode == VirtualController::InputMode::None) {
		return false;
	}

	if (m_inputMode == VirtualController::InputMode::JoyCon_Combined &&
		m_pPrimaryController != __nullptr &&
		m_pSecondaryController != __nullptr) {

		VirtualController::VXboxInput xboxInput = m_xboxInput;

		if (controllerIndex == 0) {
			if (!m_pPrimaryController->Update()) {
				return false;
			}
			SwitchController::ButtonState BtnStateA = m_pPrimaryController->GetButtonState();
			SwitchController::StickState stickStateA = m_pPrimaryController->GetStickState();

			xboxInput.Buttons.Back = BtnStateA.left.Minus;

			xboxInput.Buttons.LB = BtnStateA.left.L;

			xboxInput.Buttons.LeftStick = BtnStateA.left.Stick;

			xboxInput.Axis.LT = BtnStateA.left.ZL * 255;

			xboxInput.Axis.LeftStick.x = stickStateA.Left.X;
			xboxInput.Axis.LeftStick.y = stickStateA.Left.Y;

			uint8_t dpadData = (BtnStateA.left.Up * 0x1) +
				(BtnStateA.left.Right * 0x2) +
				(BtnStateA.left.Down * 0x4) +
				(BtnStateA.left.Left * 0x8);

			if (dpadData) {
				uint8_t upFront = (dpadData ^ m_lastDPad) & dpadData;
				if (upFront) {
					xboxInput.Buttons.Dpad = static_cast<VXboxInput::Buttons::DPad>(upFront);
					m_lastDPad = dpadData;
				}
			}
			else
			{
				xboxInput.Buttons.Dpad = VXboxInput::Buttons::DPad::None;
				m_lastDPad = dpadData;
			}

			//Test GetGyro
			x += m_pPrimaryController->GetGyroscopeState().X;
			y += m_pPrimaryController->GetGyroscopeState().Y;
			z += m_pPrimaryController->GetGyroscopeState().Z;

			int16_t lX = static_cast<int16_t>(x * deltaTime / 180 * 32767);
			int16_t lY = static_cast<int16_t>(y * deltaTime / 180 * 32767);

			int16_t rX = static_cast<int16_t>(z * deltaTime / 180 * 32767);

			SetAxisX(2, lX);
			SetAxisY(2, lY);

			SetAxisRx(2, rX);

		}
		else if (controllerIndex == 1) {
			if (!m_pSecondaryController->Update()) {
				return false;
			}

			SwitchController::ButtonState BtnStateB = m_pSecondaryController->GetButtonState();


			SwitchController::StickState stickStateB = m_pSecondaryController->GetStickState();

			xboxInput.Buttons.A = BtnStateB.right.A;
			xboxInput.Buttons.B = BtnStateB.right.B;
			xboxInput.Buttons.X = BtnStateB.right.X;
			xboxInput.Buttons.Y = BtnStateB.right.Y;

			xboxInput.Buttons.Start = BtnStateB.right.Plus;



			xboxInput.Buttons.RB = BtnStateB.right.R;


			xboxInput.Buttons.RightStick = BtnStateB.right.Stick;

			xboxInput.Axis.RightStick.x = stickStateB.Right.X;
			xboxInput.Axis.RightStick.y = stickStateB.Right.Y;


			xboxInput.Axis.RT = BtnStateB.right.ZR * 255;

		}
		else
			return false;

		
		
		//std::cout << std::endl << "BtnA : " << BtnStateB.right.A;
		//std::cout << std::endl << "BtnHome : " << BtnStateB.right.Home;
		//std::cout << std::endl << "L-[" << stickStateB.Right.X << ":" << stickStateB.Right.Y << "]";//   R - [" << m_stickState.Right.X << ":" << m_stickState.Right.Y << "]";

		
		
		
		m_xboxInput = xboxInput;
		ApplyVXboxInput(xboxInput);
	}
	else
		return false;

	return true;
}

bool VirtualController::UpdateController(SwitchControllerState state)
{

	if (GetState() != VirtualController::State::Enable) {
		return false;
	}

	//TODO
	if (m_inputMode == VirtualController::InputMode::None) {
		return false;
	}

	VXboxInput newInput = m_currentInput;

	bool canUpdate = true;

	uint8_t dpadData;
	uint8_t upFront;

	switch (state.Type)
	{
	case SwitchController::ControllerType::JoyCon_Left:

		//Dpad

		dpadData = (state.Buttons.left.Up * 0x1) +
			(state.Buttons.left.Right * 0x2) +
			(state.Buttons.left.Down * 0x4) +
			(state.Buttons.left.Left * 0x8);

		if (dpadData) {
			upFront = (dpadData ^ m_lastDPad) & dpadData;
			if (upFront) {
				newInput.Buttons.Dpad = static_cast<VXboxInput::Buttons::DPad>(upFront);
				m_lastDPad = dpadData;
			}
		}
		else
		{
			newInput.Buttons.Dpad = VXboxInput::Buttons::DPad::None;
			m_lastDPad = dpadData;
		}

		//
		newInput.Axis.LT = state.Buttons.left.ZL * 255;
		newInput.Buttons.LB = state.Buttons.left.L;

		newInput.Buttons.LeftStick = state.Buttons.left.Stick;

		newInput.Axis.LeftStick.x = state.Sticks.Left.X;
		newInput.Axis.LeftStick.y = state.Sticks.Left.Y;

		newInput.Buttons.Back = state.Buttons.left.Minus;

		break;
	case SwitchController::ControllerType::JoyCon_Right:

		newInput.Buttons.A = state.Buttons.right.A;
		newInput.Buttons.B = state.Buttons.right.B;
		newInput.Buttons.X = state.Buttons.right.X;
		newInput.Buttons.Y = state.Buttons.right.Y;

		newInput.Buttons.Start = state.Buttons.right.Plus;

		newInput.Buttons.RightStick = state.Buttons.right.Stick;

		newInput.Axis.RightStick.x = state.Sticks.Right.X;
		newInput.Axis.RightStick.y = state.Sticks.Right.Y;

		newInput.Axis.RT = state.Buttons.right.ZR * 255;
		newInput.Buttons.RB = state.Buttons.right.R;


		break;
	case SwitchController::ControllerType::Pro_Controller:

		newInput.Buttons.A = state.Buttons.right.A;
		newInput.Buttons.B = state.Buttons.right.B;
		newInput.Buttons.X = state.Buttons.right.X;
		newInput.Buttons.Y = state.Buttons.right.Y;

		newInput.Buttons.Start = state.Buttons.right.Plus;

		newInput.Buttons.RightStick = state.Buttons.right.Stick;

		newInput.Axis.RightStick.x = state.Sticks.Right.X;
		newInput.Axis.RightStick.y = state.Sticks.Right.Y;

		newInput.Axis.RT = state.Buttons.right.ZR * 255;

		dpadData = (state.Buttons.left.Up * 0x1) +
			(state.Buttons.left.Right * 0x2) +
			(state.Buttons.left.Down * 0x4) +
			(state.Buttons.left.Left * 0x8);

		if (dpadData) {
			upFront = (dpadData ^ m_lastDPad) & dpadData;
			if (upFront) {
				newInput.Buttons.Dpad = static_cast<VXboxInput::Buttons::DPad>(upFront);
				m_lastDPad = dpadData;
			}
		}
		else
		{
			newInput.Buttons.Dpad = VXboxInput::Buttons::DPad::None;
			m_lastDPad = dpadData;
		}

		break;
	default:
		canUpdate = false;
		break;
	}

	if (canUpdate) {
		return ApplyVXboxInput(newInput);
	}

	return false;
}

bool VirtualController::ApplyVXboxInput(VXboxInput &rInput)
{
	
	if (GetState() != VirtualController::State::Enable) {
		return false;
	}

	/*std::cout << std::endl << "L-[" << static_cast<SHORT>(rInput.Axis.LeftStick.x * 32767) << ":" << static_cast<SHORT>(rInput.Axis.LeftStick.y * 32767)
		<< "]   R-[" << static_cast<SHORT>(rInput.Axis.RightStick.x * 32767) << ":" << static_cast<SHORT>(rInput.Axis.RightStick.y * 32767) << "]";*/

	bool succes;

	switch (rInput.Buttons.Dpad)
	{
	case VXboxInput::Buttons::DPad::None:
		SetDpadOff(m_controllerIndex + 1);
		break;
	case VXboxInput::Buttons::DPad::Up:
		SetDpadUp(m_controllerIndex + 1);
		break;
	case VXboxInput::Buttons::DPad::Right:
		SetDpadRight(m_controllerIndex + 1);
		break;
	case VXboxInput::Buttons::DPad::Down:
		SetDpadDown(m_controllerIndex + 1);
		break;
	case VXboxInput::Buttons::DPad::Left:
		SetDpadLeft(m_controllerIndex + 1);
		break;
	default:
		break;
	}

	if (rInput.Buttons.A != m_currentInput.Buttons.A)
		succes = succes & SetBtnA(m_controllerIndex + 1, rInput.Buttons.A);

	if (rInput.Buttons.B != m_currentInput.Buttons.B)
		succes = succes & SetBtnB(m_controllerIndex + 1, rInput.Buttons.B);

	if(rInput.Buttons.X != m_currentInput.Buttons.X)
		succes = succes & SetBtnX(m_controllerIndex + 1, rInput.Buttons.X);

	if (rInput.Buttons.Y != m_currentInput.Buttons.Y)
		succes = succes & SetBtnY(m_controllerIndex + 1, rInput.Buttons.Y);

	if (rInput.Buttons.Start != m_currentInput.Buttons.Start)
		succes = succes & SetBtnStart(m_controllerIndex + 1, rInput.Buttons.Start);

	if (rInput.Buttons.Back != m_currentInput.Buttons.Back)
		succes = succes & SetBtnBack(m_controllerIndex + 1, rInput.Buttons.Back);

	if (rInput.Buttons.LB != m_currentInput.Buttons.LB)
		succes = succes & SetBtnLB(m_controllerIndex + 1, rInput.Buttons.LB);

	if (rInput.Buttons.RB != m_currentInput.Buttons.RB)
		succes = succes & SetBtnRB(m_controllerIndex + 1, rInput.Buttons.RB);

	if (rInput.Buttons.LeftStick != m_currentInput.Buttons.LeftStick)
		succes = succes & SetBtnLT(m_controllerIndex + 1, rInput.Buttons.LeftStick);

	if (rInput.Buttons.RightStick != m_currentInput.Buttons.RightStick)
		succes = succes & SetBtnRT(m_controllerIndex + 1, rInput.Buttons.RightStick);

	if (rInput.Axis.LT != m_currentInput.Axis.LT)
		succes = succes & SetTriggerL(m_controllerIndex + 1, rInput.Axis.LT);

	if (rInput.Axis.RT != m_currentInput.Axis.RT)
		succes = succes & SetTriggerR(m_controllerIndex + 1, rInput.Axis.RT);

	if (rInput.Axis.LeftStick.x != m_currentInput.Axis.LeftStick.x) {
		int16_t lX = static_cast<int16_t>(rInput.Axis.LeftStick.x * 32767);
		succes = succes & SetAxisX(m_controllerIndex + 1, lX);
	}

	if (rInput.Axis.LeftStick.y != m_currentInput.Axis.LeftStick.y) {
		int16_t lY = static_cast<int16_t>(rInput.Axis.LeftStick.y * 32767);
		succes = succes & SetAxisY(m_controllerIndex + 1, lY);
	}

	if (rInput.Axis.RightStick.x != m_currentInput.Axis.RightStick.x) {
		int16_t rX = static_cast<int16_t>(rInput.Axis.RightStick.x * 32767);
		succes = succes & SetAxisRx(m_controllerIndex + 1, rX);
	}

	if (rInput.Axis.RightStick.y != m_currentInput.Axis.RightStick.y) {
		int16_t rY = static_cast<int16_t>(rInput.Axis.RightStick.y * 32767);
		succes = succes & SetAxisRy(m_controllerIndex + 1, rY);
	}

	m_currentInput = rInput;

	return succes;
}
