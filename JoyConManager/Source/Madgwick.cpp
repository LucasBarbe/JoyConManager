#include "Madgwick.h"

Madgwick::Madgwick(double samplePeriod)
{
	SamplePeriod = samplePeriod;
	Beta = 0.041;
	Rotation = Quaternion::Identity();
}

Madgwick::Madgwick(double samplePeriod, double beta)
{
	SamplePeriod = samplePeriod;
	Beta = beta;
	Rotation = Quaternion::Identity();
}

void Madgwick::UpdateIMU(Vector3 gyro, Vector3 acc)
{
    gyro.ConvertToRad();

    double q1 = Rotation.W, q2 = Rotation.X, q3 = Rotation.Y, q4 = Rotation.Z;   // short name local variable for readability
    double norm;
    double s1, s2, s3, s4;
    double qDot1, qDot2, qDot3, qDot4;

    // Auxiliary variables to avoid repeated arithmetic
    double _2q1 = 2 * q1;
    double _2q2 = 2 * q2;
    double _2q3 = 2 * q3;
    double _2q4 = 2 * q4;
    double _4q1 = 4 * q1;
    double _4q2 = 4 * q2;
    double _4q3 = 4 * q3;
    double _8q2 = 8 * q2;
    double _8q3 = 8 * q3;
    double q1q1 = q1 * q1;
    double q2q2 = q2 * q2;
    double q3q3 = q3 * q3;
    double q4q4 = q4 * q4;

    // Normalise accelerometer measurement
    norm = sqrt(acc.X * acc.X + acc.Y * acc.Y + acc.Z * acc.Z);
    if (norm == 0) return; // handle NaN
    norm = 1 / norm;        // use reciprocal for division
    acc.X *= norm;
    acc.Y *= norm;
    acc.Z *= norm;

    // Gradient decent algorithm corrective step
    s1 = _4q1 * q3q3 + _2q3 * acc.X + _4q1 * q2q2 - _2q2 * acc.Y;
    s2 = _4q2 * q4q4 - _2q4 * acc.X + 4 * q1q1 * q2 - _2q1 * acc.Y - _4q2 + _8q2 * q2q2 + _8q2 * q3q3 + _4q2 * acc.Z;
    s3 = 4 * q1q1 * q3 + _2q1 * acc.X + _4q3 * q4q4 - _2q4 * acc.Y - _4q3 + _8q3 * q2q2 + _8q3 * q3q3 + _4q3 * acc.Z;
    s4 = 4 * q2q2 * q4 - _2q2 * acc.X + 4 * q3q3 * q4 - _2q3 * acc.Y;
    norm = 1 / sqrt(s1 * s1 + s2 * s2 + s3 * s3 + s4 * s4);    // normalise step magnitude
    s1 *= norm;
    s2 *= norm;
    s3 *= norm;
    s4 *= norm;

    // Compute rate of change of quaternion
    qDot1 = 0.5f * (-q2 * gyro.X - q3 * gyro.Y - q4 * gyro.Z) - Beta * s1;
    qDot2 = 0.5f * (q1 * gyro.X + q3 * gyro.Z - q4 * gyro.Y) - Beta * s2;
    qDot3 = 0.5f * (q1 * gyro.Y - q2 * gyro.Z + q4 * gyro.X) - Beta * s3;
    qDot4 = 0.5f * (q1 * gyro.Z + q2 * gyro.Y - q3 * gyro.X) - Beta * s4;

    // Integrate to yield quaternion
    q1 += qDot1 * SamplePeriod;
    q2 += qDot2 * SamplePeriod;
    q3 += qDot3 * SamplePeriod;
    q4 += qDot4 * SamplePeriod;
    norm = 1 / sqrt(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4);    // normalise quaternion
    Rotation.W = q1 * norm;
    Rotation.X = q2 * norm;
    Rotation.Y = q3 * norm;
    Rotation.Z = q4 * norm;
}
