#include "Color24.h"
#include "..\Header\Color24.h"

Color24::Color24()
{
	R = 0;
	G = 0;
	B = 0;
}

Color24::Color24(uint8_t r, uint8_t g, uint8_t b)
{
	R = r;
	G = g;
	B = b;
}
