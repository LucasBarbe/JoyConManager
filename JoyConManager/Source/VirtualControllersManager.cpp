#include "VirtualControllersManager.h"
#include "..\Header\VirtualControllersManager.h"

void VirtualControllersManager::UpdateController(SwitchControllerState state)
{
	bool alreadyUsed = false;

	for (size_t i = 0; i < 4; i++)
	{
		if (m_trackedCtrl[i * 2].SerialNumber == state.SN || m_trackedCtrl[i * 2 + 1].SerialNumber == state.SN) {
			m_controllers[i].UpdateController(state);
			alreadyUsed = true;
		}
	}

	if (m_inputSearchControllerIndex < 4) {

		if (!alreadyUsed) {
			switch (state.Type)
			{
			case SwitchController::ControllerType::JoyCon_Left:
				if (m_left.SerialNumber.compare(state.SN) == 0) {
					if (!(state.Buttons.left.L || state.Buttons.left.ZL))
						m_left.SerialNumber = L"";
				}
				else if (state.Buttons.left.L || state.Buttons.left.ZL) {
					m_left.SerialNumber = state.SN;
					m_left.Color = state.BodyColor;
				}
				break;
			case SwitchController::ControllerType::JoyCon_Right:
				if (m_right.SerialNumber.compare(state.SN) == 0) {
					if (!(state.Buttons.right.R || state.Buttons.right.ZR))
						m_right.SerialNumber = L"";
				}
				else if (state.Buttons.right.R || state.Buttons.right.ZR) {
					m_right.SerialNumber = state.SN;
					m_right.Color = state.BodyColor;
				}
				break;
			case SwitchController::ControllerType::Pro_Controller:
				//TODO
				break;
			default:
				break;
			}
		}
		

		if (m_left.SerialNumber.compare(L"") != 0 && m_right.SerialNumber.compare(L"") != 0 && m_controllers[m_inputSearchControllerIndex].GetState() == VirtualController::State::Ready) {

			m_trackedCtrl[m_inputSearchControllerIndex * 2] = m_left;
			m_trackedCtrl[m_inputSearchControllerIndex * 2 + 1] = m_right;

			m_controllers[m_inputSearchControllerIndex].Enable();

			m_left.SerialNumber = L"";
			m_right.SerialNumber = L"";


			VirtualControllerInfo info;
			info.Color[0] = m_left.Color;
			info.Color[1] = m_right.Color;

			info.ControllerIndex = m_inputSearchControllerIndex;
			
			info.Mode = VirtualController::InputMode::JoyCon_Combined;

			info.ControllerLedCount = m_controllers[m_inputSearchControllerIndex].GetLedCount();

			__raise OnVirtualControllerEndInputSearch(m_inputSearchControllerIndex);
			__raise OnVirtualControllerConnect(info);

			m_inputSearchControllerIndex = 255;
		}

		std::chrono::steady_clock::time_point currentTime = std::chrono::high_resolution_clock::now();
		auto timeSince = std::chrono::duration_cast<std::chrono::seconds>(currentTime - m_startSearchTime);

		if (timeSince.count() > m_searchDuration) {
			m_left.SerialNumber = L"";
			m_right.SerialNumber = L"";

			__raise OnVirtualControllerEndInputSearch(m_inputSearchControllerIndex);

			m_inputSearchControllerIndex = 255;
		}
	}

	
}

void VirtualControllersManager::BeginInputSearch(uint8_t ctrlIndex, float duration)
{
	if (m_inputSearchControllerIndex < 4 || ctrlIndex > 3)
		return;

	m_inputSearchControllerIndex = ctrlIndex;
	m_startSearchTime = std::chrono::high_resolution_clock::now();
	m_searchDuration = duration;

	__raise OnVirtualControllerBeginInputSearch(ctrlIndex);
}

void VirtualControllersManager::Disconnect(uint8_t ctrlIndex)
{
	if (ctrlIndex > 3)
		return;

	if (m_controllers[ctrlIndex].GetState() == VirtualController::State::Enable) {
		m_controllers[ctrlIndex].Disable();

		m_trackedCtrl[ctrlIndex * 2].SerialNumber = L"";
		m_trackedCtrl[ctrlIndex * 2 + 1].SerialNumber = L"";

		__raise OnVirtualControllerDisconnect(ctrlIndex);
	}
		
}

void VirtualControllersManager::SetEnable(uint8_t ctrlIndex, bool enable)
{
	if (ctrlIndex > 3)
		return;
	if (enable)
		m_controllers[ctrlIndex].Enable();
	else
		m_controllers[ctrlIndex].Disable();
}

VirtualController::State VirtualControllersManager::GetState(uint8_t ctrlIndex)
{
	if (ctrlIndex > 3)
		return VirtualController::State::Error;
	return m_controllers[ctrlIndex].GetState();
}

//bool VirtualControllersManager::GetEnable(uint8_t ctrlIndex)
//{
//	if (ctrlIndex > 3)
//		return false;
//	VirtualController::State st m_controllers[ctrlIndex].GetState()
//	return false;
//}
