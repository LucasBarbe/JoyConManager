#include "Vector3.h"
#include "..\Header\Vector3.h"

Vector3::Vector3()
{
	X = 0;
	Y = 0;
	Z = 0;
}

Vector3::Vector3(const Vector3 &vec)
{
	X = vec.X;
	Y = vec.Y;
	Z = vec.Z;
}

Vector3::Vector3(double x, double y, double z)
{
	X = x;
	Y = y;
	Z = z;
}

Vector3 Vector3::Rad()
{
	return Vector3(X * DEG_TO_RAD, Y * DEG_TO_RAD, Z * DEG_TO_RAD);
}

Vector3 Vector3::operator*(const double &a)
{
	return Vector3(X * a, Y * a, Z * a);
}

Vector3 Vector3::operator*(const Vector3 &a)
{
	return Vector3(X * a.X, Y * a.Y, Z * a.Z);
}

Vector3 Vector3::operator+(const Vector3 &a)
{
	return Vector3(X + a.X, Y + a.Y, Z + a.Z);
}

void Vector3::operator+=(const Vector3& a)
{
	X += a.X;
	Y += a.Y;
	Z += a.Z;
}

Vector3 Vector3::operator-(const Vector3& a)
{
	return Vector3(X - a.X, Y - a.Y, Z - a.Z);
}

Vector3 Vector3::operator/(const double& a)
{
	return Vector3(X / a, Y / a, Z / a);
}

void Vector3::ConvertToRad()
{
	X = X * DEG_TO_RAD;
	Y = Y * DEG_TO_RAD;
	Z = Z * DEG_TO_RAD;
}

void Vector3::ConvertToDegree()
{
	X = X / DEG_TO_RAD;
	Y = Y / DEG_TO_RAD;
	Z = Z / DEG_TO_RAD;
}

Vector3 Vector3::Zero()
{
	return Vector3(0,0,0);
}

Vector3 Vector3::One()
{
	return Vector3(1,1,1);
}

double Vector3::Dot(Vector3 lhs, Vector3 rhs)
{
	return lhs.X * rhs.X + lhs.Y * rhs.Y + lhs.Z * rhs.Z;
}
