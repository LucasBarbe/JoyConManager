#pragma once

#undef UNICODE
#define UNICODE
#undef _WINSOCKAPI_
#define _WINSOCKAPI_

#define ADD_QUOTES_HELPER(s) #s
#define ADD_QUOTES(s) ADD_QUOTES_HELPER(s)

#include <stdint.h>
#include <Windows.h>
#include <stdio.h>

#include <cstdint>

#include "../buildInfo.h"

using namespace System;

namespace ToolBox {
	namespace Array {
		template<typename T> 
		uint16_t Size(T *pArray[]) {
			return *(&pArray + 1) - *pArray;
		}

	}

	namespace Converter {
		int16_t CopyBitsUint16ToInt16(uint16_t a);
		uint16_t MergeUint8(uint8_t a, uint8_t b);
	}

	namespace Maths {
		float InvSqrt(float x);
		double InvSqrt(double x);
	}

	static ref class BuildInfo {
	public: 
		static String^ GetGitBranch();
		static String^ GetGitCurrentCommit();
		static String^ GetBuildFullDate();
		static String^ GetBuildVersion();
		static String^ GetBuildVersionFormated();
	};
}