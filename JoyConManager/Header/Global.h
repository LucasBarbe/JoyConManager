#pragma once
#undef UNICODE
#define UNICODE
#undef _WINSOCKAPI_
#define _WINSOCKAPI_
#include "SwitchControllersManager.h"

#include "VirtualControllersManager.h"

#include "UdpSender.h"
#include "Madgwick.h"
#include "ManagedObject.h"
#include "Quaternion.h"
#include "Mahony.h"
#include "HeadTracker.h"

//using namespace JoyConManager;

static ref class Global {
public:
	static SwitchControllersManager ControllersManager;
	//static CLI::ManagedObject<UdpSender>^ Sender;
	//static CLI::ManagedObject<Madgwick>^ MadgwickFusion = gcnew CLI::ManagedObject<Madgwick>(Madgwick(0.015, 0.041));
	//static CLI::ManagedObject<Mahony>^ MahonyFusion = gcnew CLI::ManagedObject<Mahony>(Mahony(0.015));
	//static CLI::ManagedObject<Quaternion>^ Rotation;
	//static CLI::ManagedObject<Vector3>^ RotEuler = gcnew CLI::ManagedObject<Vector3>(Vector3::Zero());
	static CLI::ManagedObject<HeadTracker>^ Tracker;// = gcnew CLI::ManagedObject<HeadTracker>(HeadTracker());
	static CLI::ManagedObject<VirtualControllersManager>^ VirtualControllersManager;
	static CLI::ManagedObject<Quaternion>^ HeadTrackerInitialAttitude_Left = gcnew CLI::ManagedObject<Quaternion>(Quaternion::Identity());
	static CLI::ManagedObject<Quaternion>^ HeadTrackerInitialAttitude_Right = gcnew CLI::ManagedObject<Quaternion>(Quaternion::Identity());;
};