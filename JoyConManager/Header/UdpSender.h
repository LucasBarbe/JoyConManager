#pragma once
#undef UNICODE
#define UNICODE
#undef _WINSOCKAPI_
#define _WINSOCKAPI_

#include <winsock2.h>
#include <ws2tcpip.h>
#pragma comment(lib, "ws2_32.lib")

#include <cstdio>
#include <iostream>
#include <string>

class UdpSender
{
private:

	unsigned short m_targetPort;
	std::string m_targetIP;
	SOCKET m_socket;
	sockaddr_in m_to;

public:

	UdpSender(std::string ip, unsigned short port);
	UdpSender();
	bool Send(const char* data, int dataLenght);
	bool Send(std::string data);

	void Close();

	static bool Init();
	static void Release();
	static void CloseSocket(SOCKET socket);
};

