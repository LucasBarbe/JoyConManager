#pragma once

#undef UNICODE
#define UNICODE
#undef _WINSOCKAPI_
#define _WINSOCKAPI_

#include <cstdio>
#include <iostream>

//#include <mutex>
#include "Vector3.h"
#include "Color24.h"

#include <algorithm>
#include <stdint.h>
#include <hidapi.h>

#include "JoyconResource.h"
#include "ToolBox.h"

#include <Windows.h>
#include <chrono>

#include <string>
#include <sstream>
#include <iomanip>
#include <vector>

class SwitchController
{
public:

#pragma region Enums
	enum PlayerLight
	{
		Off = 0x0,
		On = 0x1,
		Flash = 0x10
	};

	enum ControllerType {
		None = 0x00,
		JoyCon_Left = 0x01,
		JoyCon_Right = 0x02,
		Pro_Controller = 0x03
	};

	enum InputReportMode {
		ActivePollingIR_A = 0x00,
		ActivePollingIR_B = 0x01,
		ActivePollingIR_C = 0x02,
		Standard = 0x30,
		NFC_IR = 0x31,
		SimpleHID = 0x3F
	};

	enum UpdateReturn {
		Error = -1,
		NotUpdated = 0,
		Updated = 1
	};

	enum BatteryLevel {
		Empty = 0,
		Critical = 1,
		Low = 2,
		Medium = 3,
		Full = 4
	};

#pragma endregion

#pragma region Structs

	struct ButtonState {
		
		struct LeftButton {
			bool Up;
			bool Down;
			bool Left;
			bool Right;

			bool Stick;

			bool ScreenShot;

			bool Minus;

			bool L;
			bool ZL;

			bool SL;
			bool SR;
		};

		struct RightButton {
			bool A;
			bool B;
			bool X;
			bool Y;

			bool Stick;

			bool Home;

			bool Plus;

			bool R;
			bool ZR;

			bool SL;
			bool SR;
		};

		LeftButton left;
		RightButton right;
	};

	struct Stick {
		float X;
		float Y;
	};

	struct StickState {
		Stick Left;
		Stick Right;
	};

	struct CommandRespond {
		uint8_t buffer[0x40];
		uint8_t bufferLen = 0x40;
	};
	
	struct AxisCalibration {
		uint16_t Min;
		uint16_t MinBelowCenter;
		uint16_t Center;
		uint16_t MaxAboveCenter;
		uint16_t Max;
	};

	struct StickCalibration {
		AxisCalibration X;
		AxisCalibration Y;
	};

	struct RawStick {
		uint16_t X;
		uint16_t Y;
	};

	struct Vector3Int16 {
		int16_t X;
		int16_t Y;
		int16_t Z;
	};

	struct Movement {
		Vector3 Accelerometer;
		Vector3 Gyroscope;
	};

	struct MovementInt16 {
		SwitchController::Vector3Int16 Accelerometer;
		SwitchController::Vector3Int16 Gyroscope;
	};

	struct MovementCalibration {
		SwitchController::MovementInt16 Origin;
		SwitchController::MovementInt16 Sensitivity;
		SwitchController::Movement Coef;
	};

	struct MacAdress {
		uint8_t u8_6[6];
		std::string Str;
	};

#pragma endregion

public:
	SwitchController();
	~SwitchController();

	UpdateReturn Update(bool forceSync = false);

	void CalibrateGyroscope(void (*f)(SwitchController&));

	void SetPlayerLight(uint8_t index, PlayerLight state);
	void SetPlayerLights(PlayerLight playerLightA, PlayerLight playerLightB, PlayerLight playerLightC, PlayerLight playerLightD);
	CommandRespond SendCommand(uint8_t command, uint8_t *pData, int len, bool waitResponse = false);
	CommandRespond SendSubcommand(uint8_t subcommand, uint8_t *pData, int len, bool waitResponse = false);
	bool GetSPIData(uint32_t offset, uint8_t size, uint8_t *pOutBuffer);

	void EnableVibration(bool isEnable);
	void EnableIMU(bool isEnable);

	bool GetIMUIsEnable();
	MacAdress GetMacAdress();
	PlayerLight GetPlayerLight(uint8_t index);
	/*char* GetPath();*/
	std::wstring GetSerialNumber();
	ButtonState GetButtonState();
	StickState GetStickState();
	Vector3 GetGyroscopeState();
	Movement GetIMU0State();
	Movement GetIMU1State();
	Movement GetIMU2State();
	Movement GetIMUAvgState();
	double GetDeltaUpdateTime();
	bool GetCharging();
	BatteryLevel GetBatteryLevel();

	ControllerType GetControllerType();
	Color24 GetBodyColor();
	Color24 GetButtonsColor();
	Color24 GetLeftGripColor();
	Color24 GetRightGripColor();

	void Disconnect();

	void SetInputReportMode(InputReportMode reportMode);

	//tps method
	void SetHandle(hid_device *pHandle);
	bool Init(hid_device_info *device);
private:
	struct SPILocation {
		uint32_t Offset;
		uint8_t Size;
	};
private:
	void DecodeButton(uint8_t *pData);
	void DecodeStick(uint8_t *pData);
	void DecodeBattery(uint8_t* pData);

	Vector3 DecodeGyroscope(uint8_t* pData);
	void ApplyGyroscopeDeadzone(Vector3 &gyro);
	Vector3 DecodeAccelerometer(uint8_t* pData);

	void ApplyStickCalibration(RawStick &rRawSrick, StickCalibration &rStickCalibration, Stick & rCalibratedStick);
	void DecodeDeviceInfo(uint8_t *pData);
	void DecodePacket(uint8_t *pData);
	Color24 DecodeColor(uint8_t* pData);

	StickCalibration DecodeStickCalibration(uint8_t * pData, bool left_right);
	Vector3Int16 DecodeVector3(uint8_t* pData);
	MovementCalibration DecodeMovementCalibration(uint8_t* pData);

	Vector3 CalcGyroscopeCoef(Vector3Int16 origin, Vector3Int16 sensitivity);
	Vector3 CalcAccelerometerCoef(Vector3Int16 origin, Vector3Int16 sensitivity);

	void RetrieveControllerInfo();

	void RetrieveColors();

	void RetrieveFactoryStickCalibration();
	void RetrieveUserStickCalibration();

	void RetrieveFactoryMotionCalibration();
	void RetrieveUserMotionCalibration();

private:
	hid_device *m_pHandle;
	bool m_bluetooth;
	uint8_t m_globalCount;
	uint8_t m_playerLightsData;
	PlayerLight m_playerLights[4];
	uint8_t m_firmwareVersion[2];
	ControllerType m_controllerType;
	uint8_t m_macAdress[6];
	std::string m_macAdress_str;


	ButtonState m_buttonState;
	StickState m_stickState;
	bool m_useSPIColor;

	StickCalibration m_factoryLeftStickCalibration;
	StickCalibration m_factoryRightStickCalibration;

	bool m_userLeftStickCalibrationIsAvailable;
	StickCalibration m_userLeftStickCalibration;
	bool m_userRightStickCalibrationIsAvailable;
	StickCalibration m_userRightStickCalibration;

	StickCalibration * m_pCurrentLeftStickCalibration;
	StickCalibration * m_pCurrentRightStickCalibration;
	

	int m_gyroscopeCalibrationSampleCount;
	Vector3 m_gyroscopeOffsetsSum;
	Vector3 m_gyroscopeOffsetCalibration;

	Vector3 m_gyroscopeMinNoise;
	Vector3 m_gyroscopeMaxNoise;

	bool m_imuIsEnable;
	MovementCalibration m_factoryMotionCalibration;

	bool m_userMotionCalibrationIsAvailable;
	MovementCalibration m_userMotionCalibration;

	MovementCalibration* m_pCurrentMotionCalibration;

	Movement m_movement_avg;
	Movement m_movement_0;
	Movement m_movement_1;
	Movement m_movement_2;

	Color24 m_bodyColor;
	Color24 m_buttonsColor;
	Color24 m_leftGripColor;
	Color24 m_rightGripColor;

	std::chrono::steady_clock::time_point m_lastUpdateTime;

	BatteryLevel m_batteryLevel;
	bool m_charging;

	double m_deltaUpdateTime;

	/*char *m_path;*/
	std::wstring m_serialNumber;

	//std::mutex m_mutex;
};

