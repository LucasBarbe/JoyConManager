#pragma once
using namespace System;
namespace CLI {

    template<class T>
    public ref class ManagedObject
    {
    protected:
        T* m_Instance;
        bool m_ptrMod;
    public:
        ManagedObject(T* instance)
            : m_Instance(instance), m_ptrMod(true)
        {
        }

        ManagedObject(T instance): m_ptrMod(false)
        {
            m_Instance = new T(instance);
        }

        virtual ~ManagedObject()
        {
            if (m_Instance != nullptr)
            {
                delete m_Instance;
            }
        }
        !ManagedObject()
        {
            if (m_Instance != nullptr && !m_ptrMod)
            {
                delete m_Instance;
            }
        }
        T* GetInstance()
        {
            return m_Instance;
        }
    };
}