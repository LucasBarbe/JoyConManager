#pragma once

#undef UNICODE
#define UNICODE
#undef _WINSOCKAPI_
#define _WINSOCKAPI_

#include"windows.h"
#include <cstdio>

#include <stdint.h>

#include "VirtualController.h"
#include "vXboxInterface.h"

#include "SwitchController.h"
#include "SwitchControllerState.h"

class VirtualController
{
public:
	enum class State {
		Enable,
		Ready,
		SlotAlreadyUsed,
		ScpVBusNotFound,
		Error
	};

	enum class InputMode {
		Pro_Controller,
		JoyCon_Combined,
		JoyCon_Left_Vertical,
		JoyCon_Left_Horizontal,
		JoyCon_Right_Vertical,
		JoyCon_Right_Horizontal,
		None
	};

	struct VXboxInput {
		struct Buttons {

			enum class DPad {
				None = 0x0,
				Up = 0x1,
				Right = 0x2,
				Down = 0x4,
				Left = 0x8
			};

			bool A;
			bool B;
			bool X;
			bool Y;

			/*bool DPad_UP;
			bool DPad_Down;
			bool DPad_Left;
			bool Dpad_Right;*/

			DPad Dpad;

			bool Start;
			bool Back;

			bool LeftStick;
			bool RightStick;

			bool LB;
			bool RB;
		};

		struct Axis {

			struct Stick {
				float x;
				float y;

			};

			Stick LeftStick;
			Stick RightStick;

			uint8_t LT;
			uint8_t RT;
		};

		Buttons Buttons;
		Axis Axis;
	};

public:
	VirtualController();
	VirtualController(uint8_t controllerIndex);
	~VirtualController();

	bool Enable();
	bool Disable();

	void SetControllerIndex(uint8_t index);
	void SetInputMode(InputMode mode);
	VirtualController::State GetState();
	uint8_t GetLedCount();
	SwitchController* GetSwitchController(uint8_t controllerIndex);
	bool SetSingleInputController(SwitchController * pController);//Deprecated
	bool SetCombinedInputController(SwitchController * pControllerA, SwitchController * pControllerB);//Deprecated

	bool Update();//Deprecated
	bool Update(int8_t controllerIndex, double deltaTime);//Deprecated

	bool UpdateController(SwitchControllerState state);

private:
	bool ApplyVXboxInput(VXboxInput &rInput);
private:

	uint8_t m_controllerIndex;
	InputMode m_inputMode;
	SwitchController *m_pPrimaryController;
	SwitchController *m_pSecondaryController;

	uint8_t m_lastDPad;

	double x = 0;
	double y = 0;
	double z = 0;

	VXboxInput m_currentInput;
};

