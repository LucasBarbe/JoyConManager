#pragma once

#include <string>
#include "Global.h"
#include "AppManager_Managed.h"
#include <sstream>
#include <iomanip>
#include <iostream>
#include <cstdio>
#include "ManagedObject.h"
#include "SwitchControllersManager.h"

#include "ToolBox.h"

namespace JoyConManager {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Threading;

	/// <summary>
	/// Description r�sum�e de MainGUI
	/// </summary>
	public ref class MainGUI : public System::Windows::Forms::Form
	{
	public:
		static property MainGUI^ Instance {
			MainGUI^ get() {
				return m_instance;
			}
		}

	private:
		static MainGUI^ m_instance = nullptr;
		bool record = false;
		String^ m_trackedSN = "";
	private: System::Windows::Forms::Button^ Btn_DiscoverJoycons;

	private: UI::UIElements::Output_VirtualControllerList^ output_VirtualControllerList1;
	private: System::Windows::Forms::Panel^ panel_Output_Ctn;

	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Panel^ panel_SwitchControllers;
	private: UI::UIElements::Logo_Ctn^ logo_Ctn1;
	private: UI::UIElements::JoyconPreview_Ctn^ joyconPreview_Ctn1;
	private: System::Windows::Forms::Button^ output_btn_VC;
	private: System::Windows::Forms::Button^ output_btn_HT;
	private: UI::UIElements::AutoToggle^ autoToggle_DiscoverJoycon;
	private: UI::UIElements::OptionBar^ optionBar;



	public:

		   uint32_t DebugCounter = 0;
	public:
		MainGUI(void)
		{
			InitializeComponent();
			//
			//TODO: ajoutez ici le code du constructeur
			//

			m_instance = this;
			m_settings = gcnew UI::Settings(System::IO::Directory::GetCurrentDirectory() + "\\settings");
			this->Icon = UI::IconProvider::GetIcon();
			SwitchOutputPanel(0);
			joyconPreview_Ctn1->Visible = false;
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~MainGUI()
		{
			if (components)
			{
				delete components;
			}
		}

	private: static System::String^ s_sRef_AutoDiscover = "autoDiscover%enable";
		   //HeadTracker
	private: static System::String^ s_sRef_HeadTracker_Fusion = "headTracker%fusion";
	private: static System::String^ s_sRef_HeadTracker_IP = "headTracker%ip";
	private: static System::String^ s_sRef_HeadTracker_Port = "headTracker%port";
	private: static System::String^ s_sRef_HeadTracker_Invert_X = "headTracker%invert%x";
	private: static System::String^ s_sRef_HeadTracker_Invert_Y = "headTracker%invert%y";
	private: static System::String^ s_sRef_HeadTracker_Invert_Z = "headTracker%invert%z";
	private: static System::String^ s_sRef_HeadTracker_Side = "headTracker%joyconSettings%side";
	private: static System::String^ s_sRef_HeadTracker_JoyconSettings_Left_X = "headTracker%joyconSettings%left%x";
	private: static System::String^ s_sRef_HeadTracker_JoyconSettings_Left_Y = "headTracker%joyconSettings%left%y";
	private: static System::String^ s_sRef_HeadTracker_JoyconSettings_Left_Z = "headTracker%joyconSettings%left%z";
	private: static System::String^ s_sRef_HeadTracker_JoyconSettings_Right_X = "headTracker%joyconSettings%right%x";
	private: static System::String^ s_sRef_HeadTracker_JoyconSettings_Right_Y = "headTracker%joyconSettings%right%y";
	private: static System::String^ s_sRef_HeadTracker_JoyconSettings_Right_Z = "headTracker%joyconSettings%right%z";

	private: UI::UIElements::Output_HeadTrackingUDP^ output_HeadTrackingUDP;

	private: UI::UIElements::JoyconList^ Joycons;

	private: UI::Settings^ m_settings;


		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->Btn_DiscoverJoycons = (gcnew System::Windows::Forms::Button());
			this->panel_Output_Ctn = (gcnew System::Windows::Forms::Panel());
			this->output_HeadTrackingUDP = (gcnew UI::UIElements::Output_HeadTrackingUDP());
			this->output_btn_HT = (gcnew System::Windows::Forms::Button());
			this->output_VirtualControllerList1 = (gcnew UI::UIElements::Output_VirtualControllerList());
			this->output_btn_VC = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->panel_SwitchControllers = (gcnew System::Windows::Forms::Panel());
			this->autoToggle_DiscoverJoycon = (gcnew UI::UIElements::AutoToggle());
			this->Joycons = (gcnew UI::UIElements::JoyconList());
			this->joyconPreview_Ctn1 = (gcnew UI::UIElements::JoyconPreview_Ctn());
			this->logo_Ctn1 = (gcnew UI::UIElements::Logo_Ctn());
			this->panel_Output_Ctn->SuspendLayout();
			this->panel_SwitchControllers->SuspendLayout();
			this->SuspendLayout();
			// 
			// Btn_DiscoverJoycons
			// 
			this->Btn_DiscoverJoycons->Dock = System::Windows::Forms::DockStyle::Top;
			this->Btn_DiscoverJoycons->FlatAppearance->BorderColor = System::Drawing::Color::DimGray;
			this->Btn_DiscoverJoycons->FlatAppearance->BorderSize = 0;
			this->Btn_DiscoverJoycons->FlatAppearance->MouseDownBackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(22)),
				static_cast<System::Int32>(static_cast<System::Byte>(22)), static_cast<System::Int32>(static_cast<System::Byte>(44)));
			this->Btn_DiscoverJoycons->FlatAppearance->MouseOverBackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(55)),
				static_cast<System::Int32>(static_cast<System::Byte>(55)), static_cast<System::Int32>(static_cast<System::Byte>(77)));
			this->Btn_DiscoverJoycons->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->Btn_DiscoverJoycons->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.25F, System::Drawing::FontStyle::Bold,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->Btn_DiscoverJoycons->ForeColor = System::Drawing::Color::White;
			this->Btn_DiscoverJoycons->Location = System::Drawing::Point(0, 0);
			this->Btn_DiscoverJoycons->Name = L"Btn_DiscoverJoycons";
			this->Btn_DiscoverJoycons->Size = System::Drawing::Size(254, 46);
			this->Btn_DiscoverJoycons->TabIndex = 1;
			this->Btn_DiscoverJoycons->Text = L"Discover Joycons";
			this->Btn_DiscoverJoycons->UseVisualStyleBackColor = true;
			this->Btn_DiscoverJoycons->Click += gcnew System::EventHandler(this, &MainGUI::button1_Click);
			// 
			// panel_Output_Ctn
			// 
			this->panel_Output_Ctn->AutoScroll = true;
			this->panel_Output_Ctn->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(44)), static_cast<System::Int32>(static_cast<System::Byte>(44)),
				static_cast<System::Int32>(static_cast<System::Byte>(66)));
			this->panel_Output_Ctn->Controls->Add(this->output_HeadTrackingUDP);
			this->panel_Output_Ctn->Controls->Add(this->output_btn_HT);
			this->panel_Output_Ctn->Controls->Add(this->output_VirtualControllerList1);
			this->panel_Output_Ctn->Controls->Add(this->output_btn_VC);
			this->panel_Output_Ctn->Dock = System::Windows::Forms::DockStyle::Right;
			this->panel_Output_Ctn->Location = System::Drawing::Point(799, 0);
			this->panel_Output_Ctn->MinimumSize = System::Drawing::Size(239, 0);
			this->panel_Output_Ctn->Name = L"panel_Output_Ctn";
			this->panel_Output_Ctn->Size = System::Drawing::Size(239, 575);
			this->panel_Output_Ctn->TabIndex = 21;
			// 
			// output_HeadTrackingUDP
			// 
			this->output_HeadTrackingUDP->AttitudeConfigIsOpened = false;
			this->output_HeadTrackingUDP->AutoSize = true;
			this->output_HeadTrackingUDP->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->output_HeadTrackingUDP->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(66)),
				static_cast<System::Int32>(static_cast<System::Byte>(66)), static_cast<System::Int32>(static_cast<System::Byte>(88)));
			this->output_HeadTrackingUDP->Dock = System::Windows::Forms::DockStyle::Top;
			this->output_HeadTrackingUDP->ForeColor = System::Drawing::Color::White;
			this->output_HeadTrackingUDP->IsTracked = false;
			this->output_HeadTrackingUDP->JoyconsCount = 0;
			this->output_HeadTrackingUDP->Location = System::Drawing::Point(0, 356);
			this->output_HeadTrackingUDP->MaximumSize = System::Drawing::Size(323, 632);
			this->output_HeadTrackingUDP->MinimumSize = System::Drawing::Size(223, 232);
			this->output_HeadTrackingUDP->Name = L"output_HeadTrackingUDP";
			this->output_HeadTrackingUDP->Padding = System::Windows::Forms::Padding(10);
			this->output_HeadTrackingUDP->Size = System::Drawing::Size(223, 324);
			this->output_HeadTrackingUDP->TabIndex = 18;
			this->output_HeadTrackingUDP->TargetIp = L"127.0.0.1";
			this->output_HeadTrackingUDP->TargetPort = 4242;
			this->output_HeadTrackingUDP->OnStartTrackingRequest += gcnew System::Action<UI::UIElements::Output_HeadTrackingUDP::TrackSettings >(this, &MainGUI::output_HeadTrackingUDP_OnStartTrackingRequest);
			this->output_HeadTrackingUDP->OnStopTrackingRequest += gcnew System::Action(this, &MainGUI::output_HeadTrackingUDP_OnStopTrackingRequest);
			this->output_HeadTrackingUDP->OnCenterRequest += gcnew System::Action(this, &MainGUI::output_HeadTrackingUDP_OnCenterRequest);
			this->output_HeadTrackingUDP->OnSetInitialAtitudeRequest += gcnew System::Action(this, &MainGUI::output_HeadTrackingUDP_OnSetInitialAtitudeRequest);
			this->output_HeadTrackingUDP->OnResetRequest += gcnew System::Action(this, &MainGUI::output_HeadTrackingUDP_OnResetRequest);
			this->output_HeadTrackingUDP->OnFusionChange += gcnew System::Action<System::Int32 >(this, &MainGUI::output_HeadTrackingUDP_OnFusionChange);
			this->output_HeadTrackingUDP->OnInvertAxisChange += gcnew System::Action<UI::UIElements::Output_HeadTrackingUDP::InvertAxis >(this, &MainGUI::output_HeadTrackingUDP_OnInvertAxisChange);
			this->output_HeadTrackingUDP->OnDriftCorrectionChange += gcnew System::Action<UI::Vector3 >(this, &MainGUI::output_HeadTrackingUDP_OnDriftCorrectionChange);
			this->output_HeadTrackingUDP->OnInitialAttitudeConfigOpened += gcnew System::Action(this, &MainGUI::output_HeadTrackingUDP_OnInitialAttitudeConfigOpened);
			this->output_HeadTrackingUDP->OnInitialAttitudeChanged += gcnew System::Action<System::Int32, UI::Vector3 >(this, &MainGUI::output_HeadTrackingUDP_OnInitialAttitudeChanged);
			this->output_HeadTrackingUDP->OnInitialAttitudeSelectedSideChanged += gcnew System::Action<System::Int32 >(this, &MainGUI::output_HeadTrackingUDP_OnInitialAttitudeSelectedSideChanged);
			this->output_HeadTrackingUDP->OnInitialAttitudeConfigClosed += gcnew System::Action(this, &MainGUI::output_HeadTrackingUDP_OnInitialAttitudeConfigClosed);
			// 
			// output_btn_HT
			// 
			this->output_btn_HT->Dock = System::Windows::Forms::DockStyle::Top;
			this->output_btn_HT->FlatAppearance->BorderSize = 0;
			this->output_btn_HT->FlatAppearance->MouseDownBackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(22)),
				static_cast<System::Int32>(static_cast<System::Byte>(22)), static_cast<System::Int32>(static_cast<System::Byte>(44)));
			this->output_btn_HT->FlatAppearance->MouseOverBackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(55)),
				static_cast<System::Int32>(static_cast<System::Byte>(55)), static_cast<System::Int32>(static_cast<System::Byte>(77)));
			this->output_btn_HT->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->output_btn_HT->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->output_btn_HT->ForeColor = System::Drawing::Color::White;
			this->output_btn_HT->Location = System::Drawing::Point(0, 310);
			this->output_btn_HT->Name = L"output_btn_HT";
			this->output_btn_HT->Size = System::Drawing::Size(222, 46);
			this->output_btn_HT->TabIndex = 22;
			this->output_btn_HT->Text = L"Head Tracker";
			this->output_btn_HT->UseVisualStyleBackColor = true;
			this->output_btn_HT->Click += gcnew System::EventHandler(this, &MainGUI::output_btn_HT_Click);
			// 
			// output_VirtualControllerList1
			// 
			this->output_VirtualControllerList1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(66)),
				static_cast<System::Int32>(static_cast<System::Byte>(66)), static_cast<System::Int32>(static_cast<System::Byte>(88)));
			this->output_VirtualControllerList1->Dock = System::Windows::Forms::DockStyle::Top;
			this->output_VirtualControllerList1->ForeColor = System::Drawing::SystemColors::ControlText;
			this->output_VirtualControllerList1->Location = System::Drawing::Point(0, 46);
			this->output_VirtualControllerList1->Name = L"output_VirtualControllerList1";
			this->output_VirtualControllerList1->Size = System::Drawing::Size(222, 264);
			this->output_VirtualControllerList1->TabIndex = 20;
			this->output_VirtualControllerList1->OnVirtualControllerConnectRequest += gcnew System::Action<System::Int32 >(this, &MainGUI::output_VirtualControllerList1_OnVirtualControllerConnectRequest);
			this->output_VirtualControllerList1->OnVirtualControllerDisconnectRequest += gcnew System::Action<System::Int32 >(this, &MainGUI::output_VirtualControllerList1_OnVirtualControllerDisconnectRequest);
			// 
			// output_btn_VC
			// 
			this->output_btn_VC->Dock = System::Windows::Forms::DockStyle::Top;
			this->output_btn_VC->FlatAppearance->BorderSize = 0;
			this->output_btn_VC->FlatAppearance->MouseDownBackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(22)),
				static_cast<System::Int32>(static_cast<System::Byte>(22)), static_cast<System::Int32>(static_cast<System::Byte>(44)));
			this->output_btn_VC->FlatAppearance->MouseOverBackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(55)),
				static_cast<System::Int32>(static_cast<System::Byte>(55)), static_cast<System::Int32>(static_cast<System::Byte>(77)));
			this->output_btn_VC->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->output_btn_VC->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->output_btn_VC->ForeColor = System::Drawing::Color::White;
			this->output_btn_VC->Location = System::Drawing::Point(0, 0);
			this->output_btn_VC->Name = L"output_btn_VC";
			this->output_btn_VC->Size = System::Drawing::Size(222, 46);
			this->output_btn_VC->TabIndex = 21;
			this->output_btn_VC->Text = L"Virtual Controllers";
			this->output_btn_VC->UseVisualStyleBackColor = true;
			this->output_btn_VC->Click += gcnew System::EventHandler(this, &MainGUI::output_btn_VC_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(281, 48);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(105, 13);
			this->label1->TabIndex = 22;
			this->label1->Text = L"HeadTracker Debug";
			// 
			// panel_SwitchControllers
			// 
			this->panel_SwitchControllers->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(44)),
				static_cast<System::Int32>(static_cast<System::Byte>(44)), static_cast<System::Int32>(static_cast<System::Byte>(66)));
			this->panel_SwitchControllers->Controls->Add(this->autoToggle_DiscoverJoycon);
			this->panel_SwitchControllers->Controls->Add(this->Joycons);
			this->panel_SwitchControllers->Controls->Add(this->Btn_DiscoverJoycons);
			this->panel_SwitchControllers->Dock = System::Windows::Forms::DockStyle::Left;
			this->panel_SwitchControllers->Location = System::Drawing::Point(0, 0);
			this->panel_SwitchControllers->Name = L"panel_SwitchControllers";
			this->panel_SwitchControllers->Size = System::Drawing::Size(254, 575);
			this->panel_SwitchControllers->TabIndex = 23;
			// 
			// autoToggle_DiscoverJoycon
			// 
			this->autoToggle_DiscoverJoycon->BackColor = System::Drawing::Color::Transparent;
			this->autoToggle_DiscoverJoycon->IsEnabled = false;
			this->autoToggle_DiscoverJoycon->Location = System::Drawing::Point(0, 0);
			this->autoToggle_DiscoverJoycon->Margin = System::Windows::Forms::Padding(0);
			this->autoToggle_DiscoverJoycon->Name = L"autoToggle_DiscoverJoycon";
			this->autoToggle_DiscoverJoycon->Size = System::Drawing::Size(46, 46);
			this->autoToggle_DiscoverJoycon->TabIndex = 20;
			this->autoToggle_DiscoverJoycon->OnToggleRequest += gcnew System::Action(this, &MainGUI::autoToggle_DiscoverJoycon_OnToggleRequest);
			// 
			// Joycons
			// 
			this->Joycons->AutoScroll = true;
			this->Joycons->BackColor = System::Drawing::Color::Transparent;
			this->Joycons->Count = 0;
			this->Joycons->Dock = System::Windows::Forms::DockStyle::Fill;
			this->Joycons->Location = System::Drawing::Point(0, 46);
			this->Joycons->Name = L"Joycons";
			this->Joycons->Size = System::Drawing::Size(254, 529);
			this->Joycons->TabIndex = 19;
			this->Joycons->OnCalibrationRequest += gcnew System::Action<System::Int32 >(this, &MainGUI::Joycons_OnCalibrationRequest);
			this->Joycons->OnDisconnectRequest += gcnew System::Action<System::Int32 >(this, &MainGUI::Joycons_OnDisconnectRequest);
			// 
			// joyconPreview_Ctn1
			// 
			this->joyconPreview_Ctn1->ControllerType = UI::SwitchControllerType::JoyCon_Left;
			this->joyconPreview_Ctn1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->joyconPreview_Ctn1->Location = System::Drawing::Point(254, 0);
			this->joyconPreview_Ctn1->Name = L"joyconPreview_Ctn1";
			this->joyconPreview_Ctn1->Size = System::Drawing::Size(545, 575);
			this->joyconPreview_Ctn1->TabIndex = 25;
			// 
			// logo_Ctn1
			// 
			this->logo_Ctn1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->logo_Ctn1->Location = System::Drawing::Point(254, 0);
			this->logo_Ctn1->Name = L"logo_Ctn1";
			this->logo_Ctn1->Size = System::Drawing::Size(545, 575);
			this->logo_Ctn1->TabIndex = 24;
			// 
			// MainGUI
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(66)), static_cast<System::Int32>(static_cast<System::Byte>(66)),
				static_cast<System::Int32>(static_cast<System::Byte>(77)));
			this->ClientSize = System::Drawing::Size(1038, 575);
			this->Controls->Add(this->joyconPreview_Ctn1);
			this->Controls->Add(this->logo_Ctn1);
			this->Controls->Add(this->panel_SwitchControllers);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->panel_Output_Ctn);
			this->MinimumSize = System::Drawing::Size(724, 614);
			this->Name = L"MainGUI";
			this->Text = L"JoyconManager";
			this->TransparencyKey = System::Drawing::Color::Fuchsia;
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &MainGUI::MainGUI_FormClosing);
			this->Load += gcnew System::EventHandler(this, &MainGUI::MainGUI_Load);
			this->panel_Output_Ctn->ResumeLayout(false);
			this->panel_Output_Ctn->PerformLayout();
			this->panel_SwitchControllers->ResumeLayout(false);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
		private: System::Void MainGUI_Load(System::Object^ sender, System::EventArgs^ e) {

			AppManager_Managed::OnListChange += gcnew System::Action<int>(this, &JoyConManager::MainGUI::OnControllerListChange);
			AppManager_Managed::OnUpdateWithIndex += gcnew AppManager_Managed::SwitchCtrl_UpdateWithIndex(this, &JoyConManager::MainGUI::OnSwitchCtrlUpdateWithIndex);
			AppManager_Managed::OnVirtualControllerConnect += gcnew Action<CLI::ManagedObject<VirtualControllersManager::VirtualControllerInfo>^>(this, &JoyConManager::MainGUI::OnVirtualControllerConnect);
			AppManager_Managed::OnVirtualControllerDisconnect += gcnew System::Action<uint8_t>(this, &JoyConManager::MainGUI::OnVirtualControllerDisconnect);
			AppManager_Managed::OnVirtualControllerBeginInputSearch += gcnew System::Action<uint8_t>(this, &JoyConManager::MainGUI::OnVirtualControllerBeginInputSearch);
			AppManager_Managed::OnVirtualControllerEndInputSearch += gcnew System::Action<uint8_t>(this, &JoyConManager::MainGUI::OnVirtualControllerEndInputSearch);
			AppManager_Managed::OnHeadTrackingUpdate += gcnew System::Action<AppManager_Managed::HeadTrackingUpInfo^>(this, &JoyConManager::MainGUI::OnHeadTrackingUpdate_Callback);
			AppManager_Managed::OnHeadTrackingStart += gcnew System::Action<UI::SwitchControllerType>(this, &JoyConManager::MainGUI::OnHeadTrackingStart_Callback);
			AppManager_Managed::OnHeadTrackingStop += gcnew System::Action(this, &JoyConManager::MainGUI::OnHeadTrackingStop_Callback);
			Global::ControllersManager.OnAutoDiscoverSettingsChange += gcnew OnAutoDiscoverSettingsChange_Delegate(this,&JoyConManager::MainGUI::OnAutoDiscoverChange);
		
			LoadSettings();

			UI::UIElements::About::About_Win::Version = ToolBox::BuildInfo::GetBuildVersionFormated();
			UI::UIElements::About::About_Win::Date = ToolBox::BuildInfo::GetBuildFullDate();
			UI::UIElements::About::About_Win::Branch = ToolBox::BuildInfo::GetGitBranch();
			UI::UIElements::About::About_Win::Commit = ToolBox::BuildInfo::GetGitCurrentCommit();


			// avoid winform editor error (idk why)

			this->panel_Output_Ctn->SuspendLayout();

			this->optionBar = (gcnew UI::UIElements::OptionBar());
			this->panel_Output_Ctn->Controls->Add(this->optionBar);
			// 
			// optionBar
			// 
			this->optionBar->BackColor = System::Drawing::Color::Transparent;
			this->optionBar->Dock = System::Windows::Forms::DockStyle::Bottom;
			this->optionBar->Location = System::Drawing::Point(0, 680);
			this->optionBar->Margin = System::Windows::Forms::Padding(0);
			this->optionBar->Name = L"optionBar";
			this->optionBar->Size = System::Drawing::Size(222, 26);
			this->optionBar->TabIndex = 23;

			this->panel_Output_Ctn->ResumeLayout(false);
			this->panel_Output_Ctn->PerformLayout();
		}

		private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) {
			Global::ControllersManager.DiscoverControllers();
			MessageBox::Show("Ctrl dicovered");
		}

		public:

		void OnControllerListChange(int count)
		{
			if (this->InvokeRequired) {
				Action<int>^ d = gcnew Action<int>(this, &MainGUI::OnControllerListChange);
				this->Invoke(d, count);

				return;
			}

			Joycons->Count = count;
			output_HeadTrackingUDP->JoyconsCount = count;
		}

		delegate void SwitchCtrl_UpdateWithIndex(int ctrlIndex, CLI::ManagedObject<SwitchControllerState>^ state);

		void OnSwitchCtrlUpdateWithIndex(int ctrlIndex, CLI::ManagedObject<SwitchControllerState>^ state) {

			if (this->InvokeRequired) {
				MainGUI::SwitchCtrl_UpdateWithIndex^ d = gcnew MainGUI::SwitchCtrl_UpdateWithIndex(this, &MainGUI::OnSwitchCtrlUpdateWithIndex);
				this->Invoke(d, gcnew cli::array<Object^> {ctrlIndex, state});

				return;
			}

			auto stateI = state->GetInstance();

			Vector3 gyro = stateI->IMUAvg.Gyroscope;
			Vector3 acc = stateI->IMUAvg.Accelerometer;

			//Vector3 gyro0 = stateI->IMU0.Gyroscope;
			//Vector3 gyro1 = stateI->IMU1.Gyroscope;
			//Vector3 gyro2 = stateI->IMU2.Gyroscope;

			//Vector3 gyro = Vector3(
			//(gyro0.X + gyro1.X + gyro2.X) / 3,
			//(gyro0.Y + gyro1.Y + gyro2.Y) / 3,
			//(gyro0.Z + gyro1.Z + gyro2.Z) / 3
			//);
			//
			//Vector3 acc0 = stateI->IMU0.Accelerometer;
			//Vector3 acc1 = stateI->IMU1.Accelerometer;
			//Vector3 acc2 = stateI->IMU2.Accelerometer;
			//
			//Vector3 acc = Vector3(
			//(acc0.X + acc1.X + acc2.X) / 3,
			//(acc0.Y + acc1.Y + acc2.Y) / 3,
			//(acc0.Z + acc1.Z + acc2.Z) / 3
			//);


			//auto tracker = Global::Tracker->GetInstance();
			//String^ str_SN = gcnew String(stateI->SN.c_str());
			//if (m_trackedSN == str_SN) {

			//	double deltaTime = stateI->DeltaUpdateTime;

			//	if (stateI->Buttons.left.Right || stateI->Buttons.right.Y) {
			//		tracker->Center();
			//	}

			//	//tracker->Update(gyro, acc, deltaTime);
			//	tracker->Update(gyro, acc, 0.015);
			//	/*tracker->Update(gyro0, acc0, 0.005);
			//	tracker->Update(gyro1, acc1, 0.005);
			//	tracker->Update(gyro2, acc2, 0.005);*/

			//	Vector3 rotation = tracker->GetRotation();

			//	output_HeadTrackingUDP->EstimatedAtitude = UI::Vector3(rotation.X, rotation.Y, rotation.Z);

			//	if (record) {

			//		List<String^>^ infos = gcnew List<String^>();
			//		infos->Add(DebugCounter + "\t" + gyro0.X + "\t" + gyro0.Y + "\t" + gyro0.Z);
			//		DebugCounter++;
			//		infos->Add(DebugCounter + "\t" + gyro1.X + "\t" + gyro1.Y + "\t" + gyro1.Z);
			//		DebugCounter++;
			//		infos->Add(DebugCounter + "\t" + gyro2.X + "\t" + gyro2.Y + "\t" + gyro2.Z);
			//		DebugCounter++;

			//		System::IO::File::AppendAllLines("WriteLines.txt", infos->ToArray());

			//	}
			//}

			
			Color bodyColor = Color::FromArgb(255, stateI->BodyColor.R, stateI->BodyColor.G, stateI->BodyColor.B);
			Color buttonsColor = Color::FromArgb(255, stateI->ButtonsColor.R, stateI->ButtonsColor.G, stateI->ButtonsColor.B);

			if (Joycons->Controls->Count < 1 || (Joycons->Controls->Count - 1) - ctrlIndex >= Joycons->Controls->Count)
				return;

			UI::UIElements::Joycon^ joy = (UI::UIElements::Joycon^)(Joycons->Controls[(Joycons->Controls->Count - 1) - ctrlIndex]);

			joy->ControllerType = (UI::SwitchControllerType)stateI->Type;
			joy->LeftStick = UI::Vector2(stateI->Sticks.Left.X, stateI->Sticks.Left.Y);
			joy->RightStick = UI::Vector2(stateI->Sticks.Right.X, stateI->Sticks.Right.Y);
			joy->Gyro = UI::Vector3(gyro.X, gyro.Y, gyro.Z);
			joy->Acc = UI::Vector3(acc.X, acc.Y, acc.Z);
			joy->Battery = (UI::BatteryLevel)stateI->BatteryLevel;
			joy->BodyColor = bodyColor;
			joy->ButtonsColor = buttonsColor;
			joy->Buttons = ConvertBtn(stateI->Buttons);
			joy->UpdateRate =(float)( 1 / stateI->DeltaUpdateTime);
			joy->IsCalibrated = stateI->IsCalibrated;

			UI::PlayerLedState pl[4];
			//memcpy(pl, state->GetInstance()->PlayerLights, 4);

			for (size_t i = 0; i < 4; i++)
			{
				pl[i] = (UI::PlayerLedState)stateI->PlayerLights[i];
			}

			joy->SetPlayerLedState(pl[0], pl[1], pl[2], pl[3]);

			//std::cout << std::endl << gyro.X;
		}

		void OnAutoDiscoverChange(bool enable, int delayms)
		{
			if (this->InvokeRequired) {
				OnAutoDiscoverSettingsChange_Delegate^ d = gcnew OnAutoDiscoverSettingsChange_Delegate(this, &JoyConManager::MainGUI::OnAutoDiscoverChange);
				this->Invoke(d, gcnew cli::array<Object^> {enable, delayms});

				return;
			}

			autoToggle_DiscoverJoycon->IsEnabled = enable;
			m_settings->SetBoolean(s_sRef_AutoDiscover, enable);
		}

		void OnVirtualControllerConnect(CLI::ManagedObject<VirtualControllersManager::VirtualControllerInfo>^ info) {

			if (this->InvokeRequired) {
				Action<CLI::ManagedObject<VirtualControllersManager::VirtualControllerInfo>^>^ d = gcnew Action<CLI::ManagedObject<VirtualControllersManager::VirtualControllerInfo>^>(this, &MainGUI::OnVirtualControllerConnect);
				this->Invoke(d, gcnew cli::array<Object^> {info});

				return;
			}

			auto info_i = info->GetInstance();

			Color left = Color::FromArgb(255, info_i->Color[0].R, info_i->Color[0].G, info_i->Color[0].B);
			Color right = Color::FromArgb(255, info_i->Color[1].R, info_i->Color[1].G, info_i->Color[1].B);

			output_VirtualControllerList1->EnableController(info_i->ControllerIndex, (UI::UIElements::Output_VirtualController::InputMode)(int)info_i->Mode, left, right, info_i->ControllerLedCount);
		}

		void OnVirtualControllerDisconnect(uint8_t ctrlIndex) {

			if (this->InvokeRequired) {
				Action<uint8_t>^ d = gcnew Action<uint8_t>(this, &MainGUI::OnVirtualControllerDisconnect);
				this->Invoke(d, gcnew cli::array<Object^> {ctrlIndex});

				return;
			}

			output_VirtualControllerList1->DisableController(ctrlIndex);
		}

		void OnVirtualControllerBeginInputSearch(uint8_t ctrlIndex) {

			if (this->InvokeRequired) {
				Action<uint8_t>^ d = gcnew Action<uint8_t>(this, &MainGUI::OnVirtualControllerBeginInputSearch);
				this->Invoke(d, gcnew cli::array<Object^> {ctrlIndex});

				return;
			}

			output_VirtualControllerList1->BeginSearch(ctrlIndex);
		}

		void OnVirtualControllerEndInputSearch(uint8_t ctrlIndex) {

			if (this->InvokeRequired) {
				Action<uint8_t>^ d = gcnew Action<uint8_t>(this, &MainGUI::OnVirtualControllerEndInputSearch);
				this->Invoke(d, gcnew cli::array<Object^> {ctrlIndex});

				return;
			}

			output_VirtualControllerList1->EndSearch(ctrlIndex);
		}

	   UI::JoyconButtons ConvertBtn(SwitchController::ButtonState btn) {
		   UI::JoyconButtons jbtn = UI::JoyconButtons();

		   //Left
		   jbtn.left.Minus = btn.left.Minus;

		   jbtn.left.Right = btn.left.Right;
		   jbtn.left.Down = btn.left.Down;
		   jbtn.left.Left = btn.left.Left;
		   jbtn.left.Up = btn.left.Up;

		   jbtn.left.Stick = btn.left.Stick;
		   jbtn.left.ScreenShot = btn.left.ScreenShot;

		   jbtn.left.L = btn.left.L;
		   jbtn.left.ZL = btn.left.ZL;

		   jbtn.left.SR = btn.left.SR;
		   jbtn.left.SL = btn.left.SL;


		   //Right
		   jbtn.right.Plus = btn.right.Plus;

		   jbtn.right.A = btn.right.A;
		   jbtn.right.B = btn.right.B;
		   jbtn.right.X = btn.right.X;
		   jbtn.right.Y = btn.right.Y;

		   jbtn.right.Stick = btn.right.Stick;
		   jbtn.right.Home = btn.right.Home;

		   jbtn.right.R = btn.right.R;
		   jbtn.right.ZR = btn.right.ZR;

		   jbtn.right.SR = btn.right.SR;
		   jbtn.right.SL = btn.right.SL;

		   return jbtn;
	   }
private: System::Void output_HeadTrackingUDP_OnCenterRequest() {
	Global::Tracker->GetInstance()->Center();
}
private: System::Void output_HeadTrackingUDP_OnSetInitialAtitudeRequest() {
	Global::Tracker->GetInstance()->SetInitialAttitude();
}
private: System::Void output_HeadTrackingUDP_OnFusionChange(System::Int32 algo) {
	Global::Tracker->GetInstance()->SetFusionAlgorithm((HeadTracker::FusionAlgorithm) algo);
	m_settings->SetInt(s_sRef_HeadTracker_Fusion, algo);
}
private: System::Void output_HeadTrackingUDP_OnStartTrackingRequest(UI::UIElements::Output_HeadTrackingUDP::TrackSettings settings) {
	AppManager_Managed::StartHeadTracking(settings);
	m_settings->SetString(s_sRef_HeadTracker_IP, settings.Ip);
	m_settings->SetInt(s_sRef_HeadTracker_Port, settings.Port);
}
private: System::Void output_HeadTrackingUDP_OnStopTrackingRequest() {
	AppManager_Managed::StopHeadTracking();
}
private: System::Void Joycons_OnCalibrationRequest(System::Int32 joyIndex) {
	if (Global::ControllersManager.Controllers->Count > joyIndex && joyIndex > -1) {

		joyIndex = Joycons->Count - 1 - joyIndex;

		Global::ControllersManager.CalibrateController(joyIndex);
	}
}
private: System::Void Joycons_OnDisconnectRequest(System::Int32 joyIndex) {
		   if (Global::ControllersManager.Controllers->Count > joyIndex && joyIndex > -1) {

			   joyIndex = Joycons->Count - 1 - joyIndex;

			   Global::ControllersManager.DisconnectController(joyIndex);
		   }
	   }
private: System::Void output_HeadTrackingUDP_OnInvertAxisChange(UI::UIElements::Output_HeadTrackingUDP::InvertAxis invertInfo) {

	AppManager_Managed::SetInvertAxis(invertInfo.X, invertInfo.Y, invertInfo.Z);
	m_settings->SetBoolean(s_sRef_HeadTracker_Invert_X, invertInfo.X);
	m_settings->SetBoolean(s_sRef_HeadTracker_Invert_Y, invertInfo.Y);
	m_settings->SetBoolean(s_sRef_HeadTracker_Invert_Z, invertInfo.Z);

}
private: System::Void output_HeadTrackingUDP_OnDriftCorrectionChange(UI::Vector3 driftCorrection) {
	Global::Tracker->GetInstance()->SetDriftCorrection(Vector3(driftCorrection.X, driftCorrection.Y, driftCorrection.Z));
}

private: System::Void output_HeadTrackingUDP_OnResetRequest() {
	Global::Tracker->GetInstance()->Reset();
}
private: System::Void output_VirtualControllerList1_OnVirtualControllerConnectRequest(System::Int32 ctrlIndex) {

	Global::VirtualControllersManager->GetInstance()->BeginInputSearch(ctrlIndex, 15);

}
private: System::Void output_VirtualControllerList1_OnVirtualControllerDisconnectRequest(System::Int32 ctrlIndex) {

	Global::VirtualControllersManager->GetInstance()->Disconnect(ctrlIndex);

}
private: System::Void OnHeadTrackingUpdate_Callback(AppManager_Managed::HeadTrackingUpInfo^ trackInfo) {

	if (this->InvokeRequired) {
		Action<AppManager_Managed::HeadTrackingUpInfo^>^ d = gcnew Action<AppManager_Managed::HeadTrackingUpInfo^>(this, &MainGUI::OnHeadTrackingUpdate_Callback);
		this->Invoke(d, gcnew cli::array<Object^> {trackInfo});

		return;
	}

	Quaternion* rotation = trackInfo->Rotation->GetInstance();

	Vector3 euler = rotation->GetEuler();
	output_HeadTrackingUDP->EstimatedAttitude = UI::Vector3(euler.X, euler.Y, euler.Z);

	Quaternion c;

	if (trackInfo->Type == UI::SwitchControllerType::JoyCon_Left) {
		c = (*rotation) * (*Global::HeadTrackerInitialAttitude_Left->GetInstance());
	}
	else if (trackInfo->Type == UI::SwitchControllerType::JoyCon_Right) {
		c = (*rotation) * (*Global::HeadTrackerInitialAttitude_Right->GetInstance());
	}

	joyconPreview_Ctn1->JoyconRotation = System::Windows::Media::Media3D::Quaternion(c.X, c.Y, c.Z, c.W);

}
private: System::Void MainGUI_FormClosing(System::Object^ sender, System::Windows::Forms::FormClosingEventArgs^ e) {
	Global::ControllersManager.AbortThread();
	AppManager_Managed::OnListChange -= gcnew System::Action<int>(this, &JoyConManager::MainGUI::OnControllerListChange);
	AppManager_Managed::OnUpdateWithIndex -= gcnew AppManager_Managed::SwitchCtrl_UpdateWithIndex(this, &JoyConManager::MainGUI::OnSwitchCtrlUpdateWithIndex);
	AppManager_Managed::OnVirtualControllerConnect -= gcnew Action<CLI::ManagedObject<VirtualControllersManager::VirtualControllerInfo>^>(this, &JoyConManager::MainGUI::OnVirtualControllerConnect);
	AppManager_Managed::OnVirtualControllerDisconnect -= gcnew System::Action<uint8_t>(this, &JoyConManager::MainGUI::OnVirtualControllerDisconnect);
	AppManager_Managed::OnVirtualControllerBeginInputSearch -= gcnew System::Action<uint8_t>(this, &JoyConManager::MainGUI::OnVirtualControllerBeginInputSearch);
	AppManager_Managed::OnVirtualControllerEndInputSearch -= gcnew System::Action<uint8_t>(this, &JoyConManager::MainGUI::OnVirtualControllerEndInputSearch);
	AppManager_Managed::OnHeadTrackingUpdate -= gcnew System::Action<AppManager_Managed::HeadTrackingUpInfo^>(this, &JoyConManager::MainGUI::OnHeadTrackingUpdate_Callback);

	m_settings->Save();
}

	   //OutputPanelManagement
private: 
	int m_activePanelIndex = 0;
	void SwitchOutputPanel(int index) {
		if (index == m_activePanelIndex)
			m_activePanelIndex = 0;
		else
			m_activePanelIndex = index;

		panel_Output_Ctn->SuspendLayout();

		switch (m_activePanelIndex)
		{
		case 0:
			output_HeadTrackingUDP->Visible = false;
			output_VirtualControllerList1->Visible = false;
			break;
		case 1:
			output_HeadTrackingUDP->Visible = false;
			output_VirtualControllerList1->Visible = true;
			break;
		case 2:
			output_VirtualControllerList1->Visible = false;
			output_HeadTrackingUDP->Visible = true;
			break;
		default:
			break;
		}

		panel_Output_Ctn->ResumeLayout();
	}


private: System::Void output_btn_VC_Click(System::Object^ sender, System::EventArgs^ e) {
	SwitchOutputPanel(1);
}
private: System::Void output_btn_HT_Click(System::Object^ sender, System::EventArgs^ e) {
	SwitchOutputPanel(2);
}
private: System::Void output_HeadTrackingUDP_OnInitialAttitudeConfigOpened() {
	Quaternion* rotation = Global::HeadTrackerInitialAttitude_Left->GetInstance();
	joyconPreview_Ctn1->JoyconRotation = System::Windows::Media::Media3D::Quaternion(rotation->X, rotation->Y, rotation->Z, rotation->W);

	joyconPreview_Ctn1->Visible = true;
}
private: System::Void output_HeadTrackingUDP_OnInitialAttitudeConfigClosed() {
	joyconPreview_Ctn1->Visible = false;
}
private: System::Void output_HeadTrackingUDP_OnInitialAttitudeChanged(System::Int32 side, UI::Vector3 eulerRot) {


	Quaternion* rotation;
	if(side == 0)
		rotation = Global::HeadTrackerInitialAttitude_Left->GetInstance();
	else
		rotation = Global::HeadTrackerInitialAttitude_Right->GetInstance();

	rotation->SetEuler(Vector3(eulerRot.X, eulerRot.Y, eulerRot.Z));

	joyconPreview_Ctn1->JoyconRotation = System::Windows::Media::Media3D::Quaternion(rotation->X, rotation->Y, rotation->Z, rotation->W);

	//Save Settings

	if (side == 0) {
		m_settings->SetDouble(s_sRef_HeadTracker_JoyconSettings_Left_X, eulerRot.X);
		m_settings->SetDouble(s_sRef_HeadTracker_JoyconSettings_Left_Y, eulerRot.Y);
		m_settings->SetDouble(s_sRef_HeadTracker_JoyconSettings_Left_Z, eulerRot.Z);
	}
	else if (side == 1) {
		m_settings->SetDouble(s_sRef_HeadTracker_JoyconSettings_Right_X, eulerRot.X);
		m_settings->SetDouble(s_sRef_HeadTracker_JoyconSettings_Right_Y, eulerRot.Y);
		m_settings->SetDouble(s_sRef_HeadTracker_JoyconSettings_Right_Z, eulerRot.Z);
	}
}
private: System::Void output_HeadTrackingUDP_OnInitialAttitudeSelectedSideChanged(System::Int32 side) {
	joyconPreview_Ctn1->ControllerType = (UI::SwitchControllerType)(side + 1);


	Quaternion* rotation;
	if (side == 0) {
		rotation = Global::HeadTrackerInitialAttitude_Left->GetInstance();
	}
	else {
		rotation = Global::HeadTrackerInitialAttitude_Right->GetInstance();
	}
		
	joyconPreview_Ctn1->JoyconRotation = System::Windows::Media::Media3D::Quaternion(rotation->X, rotation->Y, rotation->Z, rotation->W);

	Vector3 euler = rotation->GetEuler();

	output_HeadTrackingUDP->InitialAttitude = UI::Vector3(euler.X, euler.Y, euler.Z);

	m_settings->SetInt(s_sRef_HeadTracker_Side, side);

}
	   void OnHeadTrackingStart_Callback(UI::SwitchControllerType type)
	   {
		   output_HeadTrackingUDP->IsTracked = true;
		   joyconPreview_Ctn1->ControllerType = type;
		   joyconPreview_Ctn1->Visible = true;

	   }

	   void OnHeadTrackingStop_Callback()
	   {
		   output_HeadTrackingUDP->IsTracked = false;
		   joyconPreview_Ctn1->Visible = false;
	   }
private: System::Void autoToggle_DiscoverJoycon_OnToggleRequest() {
	Global::ControllersManager.ToggleAutoDiscover();
}

private: System::Void LoadSettings() {
	//AutoDiscover
	Global::ControllersManager.SetAutoDiscover(m_settings->GetBoolean(s_sRef_AutoDiscover, false));
	//HeadTracker
	Global::Tracker->GetInstance()->SetFusionAlgorithm((HeadTracker::FusionAlgorithm) m_settings->GetInt(s_sRef_HeadTracker_Fusion, 0));
	output_HeadTrackingUDP->FusionMode->SelectedIndex = m_settings->GetInt(s_sRef_HeadTracker_Fusion, 0);

	output_HeadTrackingUDP->TargetIp = m_settings->GetString(s_sRef_HeadTracker_IP, "127.0.0.1");
	output_HeadTrackingUDP->TargetPort = m_settings->GetInt(s_sRef_HeadTracker_Port, 4242);

	output_HeadTrackingUDP->SetAxisInversion(
		m_settings->GetBoolean(s_sRef_HeadTracker_Invert_X, false),
		m_settings->GetBoolean(s_sRef_HeadTracker_Invert_Y, false),
		m_settings->GetBoolean(s_sRef_HeadTracker_Invert_Z, false)
		);

	Global::HeadTrackerInitialAttitude_Left->GetInstance()->SetEuler(
		Vector3(
			m_settings->GetDouble(s_sRef_HeadTracker_JoyconSettings_Left_X, 0.0),
			m_settings->GetDouble(s_sRef_HeadTracker_JoyconSettings_Left_Y, 0.0),
			m_settings->GetDouble(s_sRef_HeadTracker_JoyconSettings_Left_Z, 0.0)
		)
	);

	Global::HeadTrackerInitialAttitude_Right->GetInstance()->SetEuler(
		Vector3(
			m_settings->GetDouble(s_sRef_HeadTracker_JoyconSettings_Right_X, 0.0),
			m_settings->GetDouble(s_sRef_HeadTracker_JoyconSettings_Right_Y, 0.0),
			m_settings->GetDouble(s_sRef_HeadTracker_JoyconSettings_Right_Z, 0.0)
		)
	);

	output_HeadTrackingUDP->SetSide(m_settings->GetInt(s_sRef_HeadTracker_Side, 0));
}
};
}


