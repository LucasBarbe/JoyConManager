#pragma once

#include <chrono>
#include <string>

#include "SwitchControllerState.h"
#include "VirtualController.h"

class VirtualControllersManager
{
public:
	struct VirtualControllerInfo {

		/*VirtualControllerInfo() :Mode(VirtualController::InputMode::None) {};*/

		VirtualController::InputMode Mode;
		Color24 Color[2];

		bool InputSearching;

		uint8_t ControllerIndex;
		uint8_t ControllerLedCount;
	};

	struct TrackedSwitchController {

		TrackedSwitchController() :SerialNumber(L""), Color(Color24(0,0,0)) {};
		TrackedSwitchController(std::wstring sn, Color24 color) :SerialNumber(sn), Color(color) {};

		std::wstring SerialNumber;

		Color24 Color;
	};

public:
	/*VirtualControllersManager();
	~VirtualControllersManager();*/

	__event void OnVirtualControllerConnect(VirtualControllerInfo info);
	__event void OnVirtualControllerDisconnect(uint8_t ctrlIndex);
	__event void OnVirtualControllerBeginInputSearch(uint8_t ctrlIndex);
	__event void OnVirtualControllerEndInputSearch(uint8_t ctrlIndex);

	void UpdateController(SwitchControllerState state);

	void BeginInputSearch(uint8_t ctrlIndex, float duration);
	void Disconnect(uint8_t ctrlIndex);

	//VirtualControllerInfo GetVirtualControllerInfo(uint8_t ctrlIndex);

	void SetEnable(uint8_t ctrlIndex, bool enable);
	//bool GetEnable(uint8_t ctrlIndex);
	VirtualController::State GetState(uint8_t ctrlIndex);

private:

	uint8_t m_inputSearchControllerIndex = 255;

	std::chrono::steady_clock::time_point m_startSearchTime;
	float m_searchDuration;

	TrackedSwitchController m_trackedCtrl[8];

	VirtualController m_controllers[4] = 
	{
		VirtualController(0), 
		VirtualController(1), 
		VirtualController(2), 
		VirtualController(3) 
	};

	TrackedSwitchController m_left;
	TrackedSwitchController m_right;

};

