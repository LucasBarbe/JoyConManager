#pragma once
#define _USE_MATH_DEFINES

#define DEG_TO_RAD 0.01745329251994329576923690768489

#include "Vector3.h"
#include <cmath>

class Quaternion
{

public:
	double W;
	double X;
	double Y;
	double Z;

	void SetEuler(Vector3 euler);
	Vector3 GetEuler();

	Quaternion();
	Quaternion(double w, double x, double y, double z);

	double Magnitude();

	Quaternion operator * (const Quaternion& q);
	Quaternion operator * (const double& f);
	bool operator == (const Quaternion& q);

	static Quaternion Identity();

	static double Dot(Quaternion& a, Quaternion& b);
	static Quaternion Normalize(Quaternion& q);
	static Quaternion Conjugate(Quaternion& q);
	static Quaternion Inverse(Quaternion& q);
	static Quaternion Lerp(Quaternion& a, Quaternion& b, double t);
	static Quaternion Slerp(Quaternion& a, Quaternion& b, double t);
};

