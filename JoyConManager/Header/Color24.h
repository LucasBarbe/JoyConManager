#pragma once
#include <algorithm>

struct Color24
{
	Color24();
	Color24(uint8_t r, uint8_t g, uint8_t b);
	uint8_t R;
	uint8_t G;
	uint8_t B;
};

