#pragma once
#include "vector"
#include "SwitchController.h"

using namespace std;

class IMUCalibration
{
public:

	IMUCalibration();
	~IMUCalibration();

	void Reset();

	void StartCalibration();

	SwitchController::Movement ApplyMovementCalibration(SwitchController::Movement sample);
	Vector3 ApplyGyroCalibration(Vector3 &gyro);

	void AddSample(SwitchController::Movement &sample);

	bool IsCalibrated();

private:

	void CalcGyroOffset();
	
	vector<SwitchController::Movement> m_samples;

	SwitchController::Movement m_offset;

	bool m_isCalibrated;

};

