#pragma once

#include <string>

#include "Quaternion.h"
#include "Vector3.h"

#include "Madgwick.h"
#include "Mahony.h"

#include "UdpSender.h"

class HeadTracker
{
public:
	enum FusionAlgorithm
	{
		None = 0,
		MadgwickAlgorithm = 1,
		MahonyAlgorithm = 2
	};

private:
	FusionAlgorithm m_fusionAlgorithm;
	Quaternion m_rotation;
	Quaternion m_initialAttitude;
	Quaternion m_initialAttitudeOffset;
	Quaternion m_centerOffset;
	Vector3 m_centerOffsetEuler;

	Vector3 m_driftCorrectionEuler;
	Quaternion m_driftCorrection;

	Madgwick m_madgwick;
	Mahony m_mahony;

	UdpSender m_udpSender;

	bool m_invertX;
	bool m_invertY;
	bool m_invertZ;

public:

	HeadTracker();
	~HeadTracker();

	Vector3 GetRotationEuler();
	Vector3 GetRotationEuler(bool applyInversion);
	Quaternion GetRotation();
	Quaternion GetRotation(bool applyInversion);

	void SetRotation(Quaternion rotation);

	void SetTarget(std::string ip, unsigned short port);
	void SetFusionAlgorithm(FusionAlgorithm algo);
	void Update(Vector3 gyroData, Vector3 accData, double deltaTime);

	void SetInvertAxis(bool x, bool y, bool z);

	void SetInitialAttitude();
	void SetInitialAttitude(Quaternion attitude);

	void SetDriftCorrection(Vector3 correction);
	Vector3 GetDriftCorrection();

	void Center();

	void Reset();

};

