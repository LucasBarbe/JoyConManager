#pragma once

#include <cstdint>

#include "Quaternion.h"
#include "Vector3.h"

#include "ToolBox.h"

#define twoKpDef	(2.0 * 0.5)	// 2 * proportional gain
#define twoKiDef	(2.0 * 0.0)	// 2 * integral gain

//See https://x-io.co.uk/open-source-imu-and-ahrs-algorithms/

class Mahony
{
public:

	double SamplePeriod;
	Quaternion Rotation;

	Mahony(double samplePeriod);

	void UpdateIMU(Vector3 gyro, Vector3 acc);

private:
	double m_integralFBx, m_integralFBy, m_integralFBz;  // integral error terms scaled by Ki
};

