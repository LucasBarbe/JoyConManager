#pragma once

#undef UNICODE
#define UNICODE
#undef _WINSOCKAPI_
#define _WINSOCKAPI_

#include <stdint.h>
#include <JoyconResource.h>
#include <SwitchController.h>

#include <hidapi.h>

class ControllersManager
{
public:
	ControllersManager();
	~ControllersManager();

	void DiscoverSwitchController();

private:
	SwitchController m_switchControllers[16];
};

