#pragma once
#undef UNICODE
#define UNICODE
#undef _WINSOCKAPI_
#define _WINSOCKAPI_

#define null (0)

#include "SwitchController.h"
#include "SwitchControllerState.h"
#include "IMUCalibration.h"
#include "Color24.h"
#include <string>
//#include <msclr\lock.h>
//#include <vector>

using namespace System;
using namespace System::Threading;
using namespace System::Collections::Generic;


public delegate void OnUpdate_Delegate(SwitchControllerState state);

public delegate void OnUpdateWithIndex_Delagte(int ctrlIndex, SwitchControllerState state);

public delegate void OnControllerListChange_Delegate(int count);

public delegate void OnAutoDiscoverSettingsChange_Delegate(bool autoDiscoverIsEnabled, int autoDiscoverDelayms);

public ref class SwitchControllersManager
{

public: 

	ref class SwitchControllerTrack {
	public:

		SwitchController* controller;
		IMUCalibration* calibration;
		bool track;

		uint8_t checkCount;

		event OnUpdate_Delegate^ OnUpdate;
		//EventHandler<SwitchControllerState^>^ OnUpdate;

		void CallOnUpdate(SwitchControllerState state);

		SwitchControllerTrack();
		~SwitchControllerTrack();
		!SwitchControllerTrack();
	};

	//Event;

	event OnControllerListChange_Delegate^ OnControllerListChange;

	//event Action OnControllerUpdated;
	//EventHandler^ OnControllerListChange;

	property List<SwitchControllerTrack^>^ Controllers {
		List<SwitchControllerTrack^>^ get() {
			return m_controllers;
		}

		/*void set(List<SwitchControllerTrack^>^ value) {
			m_controllers = value;
		}*/
	}

	event OnUpdateWithIndex_Delagte^ OnUpdateController;

	event OnAutoDiscoverSettingsChange_Delegate^ OnAutoDiscoverSettingsChange;

public:
	SwitchControllersManager();
	void DiscoverControllers();
	void DisconnectController(int joyconIndex);
	void CalibrateController(int joyconIndex);
	//List<SwitchControllerTrack^> GetControllersList();

	void ToggleAutoDiscover();
	void SetAutoDiscover(bool enable);
	void SetAutoDiscoverDelay(int delayms);

	SwitchControllerState GetStateOf(int joyconIndex);

	void RunThread();
	void StopThread();
	void AbortThread();

private:


	//std::vector<SwitchController>* m_controllers;
	//std::vector<bool>* m_trackControllers;

	List<SwitchControllerTrack^> ^m_controllers = gcnew List<SwitchControllerTrack^>();

	Mutex ^m_M_controllers = gcnew Mutex(false);

	Thread ^m_thread;

	bool m_loop = true;

	bool m_autoDiscover = false;
	int m_autoDiscoverDelay = 2500;

	Mutex ^m_M_autoDiscover = gcnew Mutex(false);

	Mutex ^m_M_loop = gcnew Mutex(false);

private:
	void UpdateLoop();

};
