#pragma once

#include "SwitchController.h"

struct SwitchControllerState {

	SwitchController::ControllerType Type;

	SwitchController::MacAdress MacAdress;

	SwitchController::StickState Sticks;

	SwitchController::ButtonState Buttons;

	bool IMUEnabled;
	SwitchController::Movement IMUAvg;
	SwitchController::Movement IMU0;
	SwitchController::Movement IMU1;
	SwitchController::Movement IMU2;

	double DeltaUpdateTime;

	Color24 BodyColor;
	Color24 ButtonsColor;
	Color24 LeftGripColor;
	Color24 RightGripColor;

	bool Charging;
	SwitchController::BatteryLevel BatteryLevel;

	SwitchController::PlayerLight PlayerLights[4];

	std::wstring SN;

	bool IsCalibrated;
};
