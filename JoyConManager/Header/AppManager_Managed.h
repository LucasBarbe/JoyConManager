#pragma once

#include "Global.h"
#include <msclr\marshal_cppstd.h>

using namespace System;

static ref class AppManager_Managed {
public:

	ref struct HeadTrackingUpInfo {
	public: 
		CLI::ManagedObject<Quaternion>^ Rotation;
		UI::SwitchControllerType Type;

		HeadTrackingUpInfo(Quaternion rotation, SwitchController::ControllerType type) {
			Rotation = gcnew CLI::ManagedObject<Quaternion>(rotation);
			Type = (UI::SwitchControllerType)type;
		}
	};

	static void Init() {
		Global::ControllersManager.OnControllerListChange += gcnew OnControllerListChange_Delegate(&OnListChange_Callback);
		Global::ControllersManager.OnUpdateController += gcnew OnUpdateWithIndex_Delagte(&OnUpdateWithIndex_Callback);
	}

	delegate void SwitchCtrl_UpdateWithIndex(int ctrlIndex, CLI::ManagedObject<SwitchControllerState>^ state);

	static event Action<int>^ OnListChange;
	static event SwitchCtrl_UpdateWithIndex^ OnUpdateWithIndex;

	static event Action<CLI::ManagedObject<VirtualControllersManager::VirtualControllerInfo>^>^ OnVirtualControllerConnect;
	static event Action<uint8_t>^ OnVirtualControllerDisconnect;
	static event Action<uint8_t>^ OnVirtualControllerBeginInputSearch;
	static event Action<uint8_t>^ OnVirtualControllerEndInputSearch;
	static event Action<HeadTrackingUpInfo^>^ OnHeadTrackingUpdate;

	static event Action<UI::SwitchControllerType>^ OnHeadTrackingStart;
	static event Action^ OnHeadTrackingStop;

	static void Trigger_OnVirtualControllerConnect(VirtualControllersManager::VirtualControllerInfo info) {

		CLI::ManagedObject<VirtualControllersManager::VirtualControllerInfo>^ info_mgn = gcnew CLI::ManagedObject<VirtualControllersManager::VirtualControllerInfo>(info);
		OnVirtualControllerConnect(info_mgn);
	}

	static void Trigger_OnVirtualControllerDisconnect(uint8_t ctrlIndex) {
		OnVirtualControllerDisconnect(ctrlIndex);
	}

	static void Trigger_OnVirtualControllerBeginInputSearch(uint8_t ctrlIndex) {
		OnVirtualControllerBeginInputSearch(ctrlIndex);
	}

	static void Trigger_OnVirtualControllerEndInputSearch(uint8_t ctrlIndex) {
		OnVirtualControllerEndInputSearch(ctrlIndex);
	}

	static void StartHeadTracking(UI::UIElements::Output_HeadTrackingUDP::TrackSettings settings) {

		if (Global::ControllersManager.Controllers->Count > settings.JoyconIndex && settings.JoyconIndex > -1) {
			/*Global::ControllersManager.Controllers[settings.JoyconIndex]->OnUpdate += UpdateHeadTracking;*/

			SwitchControllerState st = Global::ControllersManager.GetStateOf(settings.JoyconIndex);

			Quaternion initialAtt;

			switch (st.Type)
			{
			case SwitchController::ControllerType::JoyCon_Left:
				initialAtt = *Global::HeadTrackerInitialAttitude_Left->GetInstance();
				break;
			case SwitchController::ControllerType::JoyCon_Right:
				initialAtt = *Global::HeadTrackerInitialAttitude_Right->GetInstance();
				break;
			default:
				return;
				break;
			}


			auto tracker = Global::Tracker->GetInstance();
			tracker->SetRotation(initialAtt);
			tracker->SetInitialAttitude(initialAtt);
			tracker->Center();
			tracker->SetTarget(msclr::interop::marshal_as<std::string>(settings.Ip), settings.Port);
			tracker->SetFusionAlgorithm((HeadTracker::FusionAlgorithm) settings.Fusion);

			
			OnHeadTrackingStart((UI::SwitchControllerType)st.Type);

			m_trackedSN = gcnew String(Global::ControllersManager.Controllers[settings.JoyconIndex]->controller->GetSerialNumber().c_str());
		}
	}

	static void StopHeadTracking() {
		m_trackedSN = "";

		OnHeadTrackingStop();
	}

	static void SetInvertAxis(bool x, bool y, bool z) {
		Global::Tracker->GetInstance()->SetInvertAxis(x,y,z);
	}

private:
	static String^ m_trackedSN = "";

	static void OnUpdateWithIndex_Callback(int ctrlIndex, SwitchControllerState state) {

		CLI::ManagedObject<SwitchControllerState>^ stateMg = gcnew CLI::ManagedObject<SwitchControllerState>(state);

		
		auto tracker = Global::Tracker->GetInstance();
		String^ str_SN = gcnew String(state.SN.c_str());
		if (m_trackedSN == str_SN) {

			double deltaTime = state.DeltaUpdateTime;


			if (state.Buttons.left.Right || state.Buttons.right.Y) {
				tracker->Center();
			}

			//tracker->Update(gyro, acc, deltaTime);
			tracker->Update(state.IMUAvg.Gyroscope, state.IMUAvg.Accelerometer, 0.015);
			//tracker->Update(gyro0, acc0, 0.005);
			//tracker->Update(gyro1, acc1, 0.005);
			//tracker->Update(gyro2, acc2, 0.005);
			//Vector3 rotation = tracker->GetRotationEuler();
			OnHeadTrackingUpdate(gcnew HeadTrackingUpInfo(tracker->GetRotation(), state.Type));
		}
		

		Global::VirtualControllersManager->GetInstance()->UpdateController(state);

		OnUpdateWithIndex(ctrlIndex, stateMg);
	}

	static void OnListChange_Callback(int count) {
		//Global::mainGUI.

		/*if (MainGUI::Instance == nullptr || !MainGUI::Instance->Created || !MainGUI::Instance->IsHandleCreated)
			return;

		if (windowClosed)
			return;*/

		OnListChange(count);

		/*MainGUI::SwitchCtrl_OnUpdateList^ d = gcnew MainGUI::SwitchCtrl_OnUpdateList(MainGUI::Instance, &MainGUI::OnControllerListChange);
		MainGUI::Instance->Invoke(d, count);*/

	}
};