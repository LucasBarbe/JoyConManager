#pragma once

#include "Quaternion.h"
#include "Vector3.h"


//See https://x-io.co.uk/open-source-imu-and-ahrs-algorithms/

class Madgwick
{
public:
	double Beta;
	double SamplePeriod;
	Quaternion Rotation;

	Madgwick(double samplePeriod);
	Madgwick(double samplePeriod, double beta);

	void UpdateIMU(Vector3 gyro, Vector3 acc);
};