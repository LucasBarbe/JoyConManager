#pragma once

#define DEG_TO_RAD 0.01745329251994329576923690768489

class Vector3 {

public:
	double X;
	double Y;
	double Z;

	Vector3();
	Vector3(const Vector3 &);
	Vector3(double x, double y, double z);
	
	Vector3 Rad();

	Vector3 operator * (const double &a);
	Vector3 operator * (const Vector3 &a);

	Vector3 operator + (const Vector3 &a);
	void operator += (const Vector3 &a);

	Vector3 operator - (const Vector3& a);

	Vector3 operator / (const double& a);

	void ConvertToRad();
	void ConvertToDegree();

	static Vector3 Zero();
	static Vector3 One();

	static double Dot(Vector3 lhs, Vector3 rhs);
};

