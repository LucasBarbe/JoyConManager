﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.42000
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UI.Properties {
    using System;
    
    
    /// <summary>
    ///   Une classe de ressource fortement typée destinée, entre autres, à la consultation des chaînes localisées.
    /// </summary>
    // Cette classe a été générée automatiquement par la classe StronglyTypedResourceBuilder
    // à l'aide d'un outil, tel que ResGen ou Visual Studio.
    // Pour ajouter ou supprimer un membre, modifiez votre fichier .ResX, puis réexécutez ResGen
    // avec l'option /str ou régénérez votre projet VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Retourne l'instance ResourceManager mise en cache utilisée par cette classe.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("UI.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Remplace la propriété CurrentUICulture du thread actuel pour toutes
        ///   les recherches de ressources à l'aide de cette classe de ressource fortement typée.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap download_outline_white_24x24 {
            get {
                object obj = ResourceManager.GetObject("download_outline_white_24x24", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap download_white_24x24 {
            get {
                object obj = ResourceManager.GetObject("download_white_24x24", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap gitlab_white_24x24 {
            get {
                object obj = ResourceManager.GetObject("gitlab_white_24x24", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap heart_outline_white_24x24 {
            get {
                object obj = ResourceManager.GetObject("heart_outline_white_24x24", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap heart_white_24x24 {
            get {
                object obj = ResourceManager.GetObject("heart_white_24x24", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap information_white_24x24 {
            get {
                object obj = ResourceManager.GetObject("information_white_24x24", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap information_white_cropped_16x16 {
            get {
                object obj = ResourceManager.GetObject("information_white_cropped_16x16", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap JoyconManager_Icon {
            get {
                object obj = ResourceManager.GetObject("JoyconManager_Icon", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Icon semblable à (Icône).
        /// </summary>
        internal static System.Drawing.Icon JoyconManager_Icon_ico {
            get {
                object obj = ResourceManager.GetObject("JoyconManager_Icon_ico", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap license_white_24x24 {
            get {
                object obj = ResourceManager.GetObject("license_white_24x24", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__A {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__A", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__Arrow {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__Arrow", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__B {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__B", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__Battery_0 {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__Battery_0", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__Battery_100 {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__Battery_100", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__Battery_25 {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__Battery_25", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__Battery_50 {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__Battery_50", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__Battery_75 {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__Battery_75", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__Btn_Base {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__Btn_Base", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__Left_Body_Color {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__Left_Body_Color", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__Minus {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__Minus", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__PlayerLed {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__PlayerLed", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__Pro_Body_Mask {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__Pro_Body_Mask", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__Pro_L_Btn {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__Pro_L_Btn", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__Pro_LeftGrip_Mask {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__Pro_LeftGrip_Mask", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__Pro_Outline {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__Pro_Outline", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__Pro_R_Btn {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__Pro_R_Btn", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__Pro_RightGrip_Mask {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__Pro_RightGrip_Mask", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__Pro_ZL_Btn {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__Pro_ZL_Btn", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__Pro_ZR_Btn {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__Pro_ZR_Btn", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__Screenshot {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__Screenshot", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__SideBtn {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__SideBtn", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__Stick {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__Stick", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__X {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__X", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Nintendo_Switch_Joycon__Y {
            get {
                object obj = ResourceManager.GetObject("Nintendo_Switch_Joycon__Y", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap settings_white_24x24 {
            get {
                object obj = ResourceManager.GetObject("settings_white_24x24", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap settings_white_cropped_16x16 {
            get {
                object obj = ResourceManager.GetObject("settings_white_cropped_16x16", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap sync_off_white_24x24 {
            get {
                object obj = ResourceManager.GetObject("sync_off_white_24x24", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap sync_white_24x24 {
            get {
                object obj = ResourceManager.GetObject("sync_white_24x24", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Vxbox_TopPart {
            get {
                object obj = ResourceManager.GetObject("Vxbox_TopPart", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Vxbox_x1 {
            get {
                object obj = ResourceManager.GetObject("Vxbox_x1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Vxbox_x1_outline {
            get {
                object obj = ResourceManager.GetObject("Vxbox_x1_outline", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap web_white_24x24 {
            get {
                object obj = ResourceManager.GetObject("web_white_24x24", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
    }
}
