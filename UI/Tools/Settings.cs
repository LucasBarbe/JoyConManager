﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI
{
    public class Settings
    {
        private string m_path;
        private string m_json;
        private JObject m_object;

        public Settings(string path)
        {
            m_path = path;

            if (File.Exists(m_path))
            {
                m_json = File.ReadAllText(m_path);
                try
                {
                    m_object = JObject.Parse(m_json);
                    Console.WriteLine("\n"+  m_path + " Loaded");
                }
                catch (Exception e)
                {
                    Console.WriteLine("\nLoading " + m_path + " Failed");
                    Console.WriteLine(e);
                    Console.WriteLine("Creating clear data structure");
                    m_object = new JObject();
                }
            }
            else
            {
                Console.WriteLine("\n" + m_path + " not found. Using default value");
                m_object = new JObject();
            }

        }

        public double GetDouble(string key, double defaultValue)
        {
            JToken tk = GetValue(key);
            return tk != null ? (double)tk : defaultValue;
        }
        public double GetDouble(string key)
        {
            JToken tk = GetValue(key);
            return tk != null ? (double)tk : 0.0;
        }

        public string GetString(string key, string defaultValue)
        {
            JToken tk = GetValue(key);
            return tk != null ? (string)tk : defaultValue;
        }
        public string GetString(string key)
        {
            JToken tk = GetValue(key);
            return tk != null ? (string)tk : "";
        }

        public int GetInt(string key, int defaultValue)
        {
            JToken tk = GetValue(key);
            return tk != null ? (int)tk : defaultValue;
        }
        public int GetInt(string key)
        {
            JToken tk = GetValue(key);
            return tk != null ? (int)tk : 0;
        }

        public bool GetBoolean(string key, bool defaultValue)
        {
            JToken tk = GetValue(key);
            return tk != null ? (bool)tk : defaultValue;
        }
        public bool GetBoolean(string key)
        {
            JToken tk = GetValue(key);
            return tk != null ? (bool)tk : false;
        }

        public JToken GetValue(string key)
        {
            return GetValue(key.Split('%'));
        }
        public JToken GetValue(string[] keys)
        {
            if (keys.Length < 1)
                return null;

            JToken tk = m_object[keys[0]];

            for (int i = 1; i < keys.Length; i++)
            {
                if (tk == null) break;

                tk = tk[keys[i]];
            }

            return tk;
        }

        public void SetBoolean(string key, bool value)
        {
            string[] keys = key.Split('%');
            SetBoolean(keys, value);
        }
        public void SetBoolean(string[] keys, bool value)
        {
            JToken tk = GetTokenSetValue(keys);
            if (tk == null) return;

            tk[keys.Last()] = value;
        }

        public void SetInt(string key, int value)
        {
            string[] keys = key.Split('%');
            SetInt(keys, value);
        }
        public void SetInt(string[] keys, int value)
        {
            JToken tk = GetTokenSetValue(keys);
            if (tk == null) return;

            tk[keys.Last()] = value;
        }

        public void SetDouble(string key, double value)
        {
            string[] keys = key.Split('%');
            SetDouble(keys, value);
        }
        public void SetDouble(string[] keys, double value)
        {
            JToken tk = GetTokenSetValue(keys);
            if (tk == null) return;

            tk[keys.Last()] = value;
        }

        public void SetFloat(string key, float value)
        {
            string[] keys = key.Split('%');
            SetFloat(keys, value);
        }
        public void SetFloat(string[] keys, float value)
        {
            JToken tk = GetTokenSetValue(keys);
            if (tk == null) return;

            tk[keys.Last()] = value;
        }

        public void SetString(string key, string value)
        {
            string[] keys = key.Split('%');
            SetString(keys, value);
        }
        public void SetString(string[] keys, string value)
        {
            JToken tk = GetTokenSetValue(keys);
            if (tk == null) return;

            tk[keys.Last()] = value;
        }

        private JToken GetTokenSetValue(string[] keys)
        {
            if (keys.Length < 1)
                return null;

            if (m_object[keys[0]] == null)
                m_object[keys[0]] = new JObject();

            JToken tk = m_object[keys[0]];

            for (int i = 1; i < keys.Length - 1; i++)
            {

                if (tk[keys[i]] == null)
                {
                    tk[keys[i]] = new JObject();
                }

                tk = tk[keys[i]];
            }

            return tk;
        }

        public void Save()
        {
            m_json = m_object.ToString();
            try
            {
                File.WriteAllText(m_path, m_json);
                Console.WriteLine("\n" + m_path + " Saved");
            }
            catch(Exception e)
            {
                Console.WriteLine("\n" + m_path + "are not saved");
                Console.WriteLine(e);
            }
            
        }
    }
}
