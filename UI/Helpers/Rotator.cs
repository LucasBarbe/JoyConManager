﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using HelixToolkit.Wpf;

namespace UI
{
    class Rotator
    {
        private RotateManipulator m_manipX = new RotateManipulator();
        private RotateManipulator m_manipY = new RotateManipulator();
        private RotateManipulator m_manipZ = new RotateManipulator();

        private double m_diameter;
        public double Diameter
        {
            get => m_diameter;
            set
            {
                m_diameter = value;
                m_manipX.InnerDiameter = value;
                m_manipY.InnerDiameter = value;
                m_manipZ.InnerDiameter = value;

                m_manipX.Diameter = value + Thickness;
                m_manipY.Diameter = value + Thickness;
                m_manipZ.Diameter = value + Thickness;
            }
        }

        private double m_thickness;
        public double Thickness
        {
            get => m_thickness;
            set
            {
                m_thickness = value;
                m_manipX.Length = value / 2;
                m_manipX.Diameter = m_manipX.InnerDiameter + value;
                m_manipY.Length = value / 2;
                m_manipY.Diameter = m_manipY.InnerDiameter + value;
                m_manipZ.Length = value / 2;
                m_manipZ.Diameter = m_manipZ.InnerDiameter + value;
            }
        }

        private bool m_isEnabled = true;
        public bool IsEnabled
        {
            get => m_isEnabled;
            set
            {
                if(m_isEnabled != value)
                {
                    m_isEnabled = value;
                    m_manipX.IsEnabled = value;
                    m_manipY.IsEnabled = value;
                    m_manipZ.IsEnabled = value;

                    System.Windows.Visibility v = value ? System.Windows.Visibility.Visible : System.Windows.Visibility.Hidden;

                    m_manipX.Visibility = v;
                    m_manipY.Visibility = v;
                    m_manipZ.Visibility = v;

                    if (value)
                    {
                        m_manipX.Bind(m_trackedGroupe);
                        m_manipY.Bind(m_trackedGroupe);
                        m_manipZ.Bind(m_trackedGroupe);
                    }
                    else
                    {
                        m_manipZ.UnBind();
                        m_manipY.UnBind();
                        m_manipX.UnBind();
                    }
                }
            }
        }

        public event Action<Quaternion> OnRotationChanged;

        private bool m_isInteracting;
        private ModelVisual3D m_trackedGroupe;

        public Quaternion Rotation
        {
            //get => new Quaternion();
            set
            {
                
                Matrix3D m = new Matrix3D();
                m.Rotate(value);

                Transform3D t = new MatrixTransform3D(m);

                m_trackedGroupe.Transform = t;
            }
        }

        public Rotator()
        {
            //SetAxis

            m_manipX.Axis = new Vector3D(1, 0, 0);
            m_manipY.Axis = new Vector3D(0, 1, 0);
            m_manipZ.Axis = new Vector3D(0, 0, 1);

            //MouseEvt Hover

            m_manipX.MouseEnter += Manip_MouseEnter;
            m_manipX.MouseLeave += Manip_MouseLeave;

            m_manipY.MouseEnter += Manip_MouseEnter;
            m_manipY.MouseLeave += Manip_MouseLeave;

            m_manipZ.MouseEnter += Manip_MouseEnter;
            m_manipZ.MouseLeave += Manip_MouseLeave;

            //MouseEvt Edit

            m_manipX.MouseDown += Manip_MouseDown;
            m_manipX.MouseMove += Manip_MouseMove;
            m_manipX.MouseUp += Manip_MouseUp;

            m_manipY.MouseDown += Manip_MouseDown;
            m_manipY.MouseMove += Manip_MouseMove;
            m_manipY.MouseUp += Manip_MouseUp;

            m_manipZ.MouseDown += Manip_MouseDown;
            m_manipZ.MouseMove += Manip_MouseMove;
            m_manipZ.MouseUp += Manip_MouseUp;

            //SetColor
            m_manipX.Material = GenMaterial(Color.FromRgb(200, 0, 0));
            m_manipY.Material = GenMaterial(Color.FromRgb(0, 200, 0));
            m_manipZ.Material = GenMaterial(Color.FromRgb(0, 0, 200));
        }

        private void Manip_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            m_isInteracting = false;
        }

        private void Manip_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (m_isInteracting && m_trackedGroupe != null)
            {
                
                //Console.Write("\n x:" + m_manipX.Value + " y:"+ m_manipY.Value + " z:"+ m_manipZ.Value);
                //if(OnRotationChanged != null)
                //    OnRotationChanged(new Vector3D(m_manipX.Value, m_manipY.Value, m_manipZ.Value));
            }
        }

        private void Manip_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            m_isInteracting = true;
        }

        public void SetParent(HelixViewport3D viewport)
        {
            viewport.Children.Add(m_manipX);
            viewport.Children.Add(m_manipY);
            viewport.Children.Add(m_manipZ);
        }

        public void RemoveFrom(HelixViewport3D viewport)
        {
            viewport.Children.Remove(m_manipX);
            viewport.Children.Remove(m_manipY);
            viewport.Children.Remove(m_manipZ); 
        }

        public void Bind(ModelVisual3D model)
        {
            m_manipX.Bind(model);
            m_manipY.Bind(model);
            m_manipZ.Bind(model);

            m_trackedGroupe = model;
        }

        private MaterialGroup GenMaterial(Color color)
        {
            MaterialGroup mat = new MaterialGroup();
            mat.Children.Add(new DiffuseMaterial(new SolidColorBrush(Colors.Black)));
            mat.Children.Add(new EmissiveMaterial(new SolidColorBrush(color)));

            return mat;
        }

        private void Manip_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            RotateManipulator manip = (RotateManipulator)sender;

            if (manip == m_manipX)
            {
                m_manipX.Material = GenMaterial(Color.FromRgb(200, 0, 0));
            }
            else if (manip == m_manipY)
            {
                m_manipY.Material = GenMaterial(Color.FromRgb(0, 200, 0));
            }
            else if (manip == m_manipZ)
            {
                m_manipZ.Material = GenMaterial(Color.FromRgb(0, 0, 200));
            }

            manip.InnerDiameter = m_diameter;
            manip.Diameter = m_diameter + m_thickness;
            manip.Length = m_thickness / 2;
        }

        private void Manip_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            RotateManipulator manip = (RotateManipulator)sender;

            if (manip == m_manipX)
            {
                m_manipX.Material = GenMaterial(Color.FromRgb(255, 0, 0));
            }
            else if (manip == m_manipY)
            {
                m_manipY.Material = GenMaterial(Color.FromRgb(0, 255, 0));
            }
            else if (manip == m_manipZ)
            {
                m_manipZ.Material = GenMaterial(Color.FromRgb(0, 0, 255));
            }

            manip.InnerDiameter = m_diameter - m_thickness;
            manip.Diameter = m_diameter + m_thickness * 2;
            manip.Length = m_thickness;

        }


    }
}
