﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI
{
    public enum SwitchControllerType
    {
        None = 0x00,
        JoyCon_Left = 0x01,
        JoyCon_Right = 0x02,
        Pro_Controller = 0x03
    }
}
