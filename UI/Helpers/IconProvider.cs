﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;

namespace UI
{
    static public class IconProvider
    {
        static public Icon GetIcon()
        {
            return UI.Properties.Resources.JoyconManager_Icon_ico;
        }
    }
}
