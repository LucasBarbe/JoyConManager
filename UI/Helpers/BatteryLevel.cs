﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI
{
    public enum BatteryLevel
    {
        Empty = 0,
        Critical = 1,
        Low = 2,
        Medium = 3,
        Full = 4
    }
}
