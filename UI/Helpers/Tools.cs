﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Windows.Media.Media3D;
using System.Reflection;
using System.IO;

namespace UI
{
    static public class Tools
    {
        static public Bitmap ApplyColorToBitmap(Bitmap bmp, Color color)
        {
            Bitmap outBmp = new Bitmap(bmp);

            int h = outBmp.Height;
            int w = outBmp.Width;

            float[] colorFactor =
            {
                color.A / 255f,
                color.R / 255f,
                color.G / 255f,
                color.B / 255f
            };

            //Optimized

            BitmapData outBmpData = outBmp.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.ReadWrite, outBmp.PixelFormat);
            IntPtr pointer = outBmpData.Scan0;
            int size = Math.Abs(outBmpData.Stride) * h;
            byte[] pixels = new byte[size];

            int pixelsCount = pixels.Length / 4;

            Marshal.Copy(pointer, pixels, 0, size);
            for (int i = 0; i < pixelsCount; i++)
            {
                pixels[i * 4 + 3] = (byte)(pixels[i * 4 + 3] * colorFactor[0]); //Alpha
                pixels[i * 4 + 2] = (byte)(pixels[i * 4 + 2] * colorFactor[1]); //R
                pixels[i * 4 + 1] = (byte)(pixels[i * 4 + 1] * colorFactor[2]); //G
                pixels[i * 4 + 0] = (byte)(pixels[i * 4 + 0] * colorFactor[3]); //B
            }
            Marshal.Copy(pixels, 0, pointer, size);
            outBmp.UnlockBits(outBmpData);
            
            /*
            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    Color p = outBmp.GetPixel(x, y);

                    byte r = p.R;
                    byte g = p.G;
                    byte b = p.B;

                    byte a = p.A;

                    r = (byte)((r / 255.0f) * (color.R / 255.0f) * 255);
                    g = (byte)((g / 255.0f) * (color.G / 255.0f) * 255);
                    b = (byte)((b / 255.0f) * (color.B / 255.0f) * 255);

                    a = (byte)((a / 255.0f) * (color.A / 255.0f) * 255);

                    outBmp.SetPixel(x, y, Color.FromArgb(a, r, g, b));
                }
            }
            */
            return outBmp;
        }

        static public Bitmap ApplyColorToBitmap(Bitmap bmp, Color color, Rectangle limit)
        {

            //TODO: Optimize

            int h = bmp.Height;
            int w = bmp.Width;

            if (w < limit.Left || w < limit.Right || h < limit.Top || h < limit.Bottom)
                return new Bitmap(1,1);

            Bitmap outBmp = new Bitmap(bmp);

            for (int y = limit.Top; y < limit.Bottom; y++)
            {
                for (int x = limit.Left; x <limit.Right; x++)
                {
                    Color p = outBmp.GetPixel(x, y);

                    byte r = p.R;
                    byte g = p.G;
                    byte b = p.B;

                    byte a = p.A;

                    r = (byte)((r / 255.0f) * (color.R / 255.0f) * 255);
                    g = (byte)((g / 255.0f) * (color.G / 255.0f) * 255);
                    b = (byte)((b / 255.0f) * (color.B / 255.0f) * 255);

                    a = (byte)((a / 255.0f) * (color.A / 255.0f) * 255);

                    outBmp.SetPixel(x, y, Color.FromArgb(a, r, g, b));
                }
            }

            return outBmp;
        }

        static public Bitmap InvertColor(Bitmap bmp)
        {
            Bitmap outBitmap = new Bitmap(bmp);

            int h = outBitmap.Height;
            int w = outBitmap.Width;

            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    Color p = outBitmap.GetPixel(x, y);

                    byte r = p.R;
                    byte g = p.G;
                    byte b = p.B;

                    r = (byte)(255 - r);
                    g = (byte)(255 - g);
                    b = (byte)(255 - b);

                    outBitmap.SetPixel(x, y, Color.FromArgb(p.A, r, g, b));
                }
            }

            return outBitmap;
        }

        static public Control[] GetParents(Control control)
        {
            List<Control> parents = new List<Control>();

            Control curCtr = control;
            while (curCtr.Parent != null)
            {
                parents.Add(curCtr.Parent);
                curCtr = curCtr.Parent;
            }

            return parents.ToArray();
        }

        static public Bitmap ComposeImage(Bitmap[] layers)
        {
            if (layers.Length < 0)
                return new Bitmap(0, 0);

            Bitmap baseImage = new Bitmap(layers[0].Width, layers[0].Height);

            Graphics graph = Graphics.FromImage(baseImage);
            graph.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
            for (int i = 0; i < layers.Length; i++)
            {
                graph.DrawImage(layers[i], 0, 0);
            }
            return baseImage;
        }

        static public void SetParent(Control child, Control parent)
        {
            Control[] childParents = GetParents(child);
            Control[] parentParents = GetParents(parent);

            int childCount = -1;
            int parentCount = -1;

            bool find = false;

            for (int i = 0; i < parentParents.Length && !find; i++)
            {
                parentCount++;
                childCount = -1;

                for (int j = 0; j < childParents.Length && !find; j++)
                {
                    childCount++;

                    if (parentParents[i] == childParents[j])
                    {
                        find = true;
                    }

                }
            }

            Point childOffset = child.Location;
            Point parentOffset = parent.Location;

            for (int i = 0; i < childCount; i++)
            {
                childOffset.X += childParents[i].Location.X;
                childOffset.Y += childParents[i].Location.Y;
            }

            for (int i = 0; i < parentCount; i++)
            {
                parentOffset.X += parentParents[i].Location.X;
                parentOffset.Y += parentParents[i].Location.Y;
            }

            Point newChildLocation = new Point();

            newChildLocation.X = childOffset.X - parentOffset.X;
            newChildLocation.Y = childOffset.Y - parentOffset.Y;

            parent.Controls.Add(child);
            child.Location = newChildLocation;
        }

        public enum ModelExtention
        {
            Obj,
            Stl,
            Studio
        }

        static public Model3DGroup LoadEmbeddedModel(string modelName, ModelExtention extention)
        {
            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(modelName);
            if (stream == null)
                return null;

            HelixToolkit.Wpf.ModelReader reader = null;

            switch (extention)
            {
                case ModelExtention.Obj:
                    reader = new HelixToolkit.Wpf.ObjReader();
                    break;
                case ModelExtention.Stl:
                    reader = new HelixToolkit.Wpf.StLReader();
                    break;
                case ModelExtention.Studio:
                    reader = new HelixToolkit.Wpf.StudioReader();
                    break;
            }

            return reader.Read(stream);

        }
    }
}
