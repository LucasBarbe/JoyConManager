﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI
{
    public struct JoyconButtons
    {
        public static bool operator ==(JoyconButtons a, JoyconButtons b)
        {
            return a.left == b.left && a.right == b.right;
        }
        public static bool operator !=(JoyconButtons a, JoyconButtons b)
        {
            return !(a == b);
        }

        public struct LeftButtons
        {
            public bool Up;
            public bool Down;
            public bool Left;
            public bool Right;

            public bool Stick;

            public bool ScreenShot;

            public bool Minus;

            public bool L;
            public bool ZL;

            public bool SL;
            public bool SR;

            public static bool operator ==(LeftButtons a, LeftButtons b)
            {
                return a.Up == b.Up &&
                    a.Down == b.Down &&
                    a.Left == b.Left &&
                    a.Right == b.Right &&
                    a.Stick == b.Stick &&
                    a.ScreenShot == b.ScreenShot &&
                    a.Minus == b.Minus &&
                    a.L == b.L &&
                    a.ZL == b.ZL &&
                    a.SL == b.SL &&
                    a.SR == b.SR;
            }

            public static bool operator !=(LeftButtons a, LeftButtons b)
            {
                return !(a == b);
            }
        }

        public struct RightButtons
        {
            public bool A;
            public bool B;
            public bool X;
            public bool Y;

            public bool Stick;

            public bool Home;

            public bool Plus;

            public bool R;
            public bool ZR;

            public bool SL;
            public bool SR;

            public static bool operator ==(RightButtons a, RightButtons b)
            {
                return a.A == b.A &&
                    a.B == b.B &&
                    a.X == b.X &&
                    a.Y == b.Y &&
                    a.Stick == b.Stick &&
                    a.Home == b.Home &&
                    a.Plus == b.Plus &&
                    a.R == b.R &&
                    a.ZR == b.ZR &&
                    a.SL == b.SL &&
                    a.SR == b.SR;
            }

            public static bool operator !=(RightButtons a, RightButtons b)
            {
                return !(a == b);
            }
        }


        public LeftButtons left;
        public RightButtons right;
    }
}
