﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

namespace UI.UIElements
{

    
    /// <summary>
    /// Allow disabled color for FlatButton
    /// </summary>
    class FlatButton : Button
    {
        private Color m_disabledBackColor;
        public Color DisabledBackColor { get => m_disabledBackColor; set => m_disabledBackColor = value; }
        

        private Color m_disabledForeColor;
        public Color DisabledForeColor { get => m_disabledForeColor; set => m_disabledForeColor = value; }


        protected override void OnPaint(PaintEventArgs pevent)
        {
            if (base.Enabled)
            {
                base.OnPaint(pevent);
            }
            else
            {
                base.OnPaint(pevent);

                pevent.Graphics.FillRectangle(new SolidBrush(m_disabledBackColor), pevent.ClipRectangle);
                pevent.Graphics.DrawString(base.Text, base.Font, new SolidBrush(m_disabledForeColor), 
                    (base.Width - pevent.Graphics.MeasureString(base.Text, base.Font).Width) / 2, (base.Height / 2) -
                    (pevent.Graphics.MeasureString(base.Text, base.Font).Height / 2));
            }
            
        }
    }
}
