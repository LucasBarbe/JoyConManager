﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI.UIElements
{
    public partial class Joycon : UserControl
    {
        const int k_maxStickOffset = 5;

        private Point m_leftStickInitialLocation;
        private Point m_rightStickInitialLocation;

        private SwitchControllerType m_controllerType;
        public SwitchControllerType ControllerType
        {
            get => m_controllerType;
            set
            {
                if (m_controllerType != value)
                {
                    m_controllerType = value;
                    UpdateCtrlType();
                }
            }
        }

        private float m_updateRate;
        public float UpdateRate
        {
            get => m_updateRate;
            set
            {
                if (m_updateRate != value)
                {
                    m_updateRate = value;
                    UpdateUpRate();
                }
            }
        }

        private Color m_bodyColor = Color.Gray;
        public Color BodyColor
        {
            get => m_bodyColor;
            set
            {
                if (m_bodyColor != value)
                {
                    m_bodyColor = value;

                    UpdateBodyColor();
                    m_proCtrlColorHasChanged = true;
                    //UpdateProControllerColor();
                }
            }
        }

        private Color m_buttonsColor = Color.Gray;
        public Color ButtonsColor
        {
            get => m_buttonsColor;
            set
            {
                if (m_buttonsColor != value)
                {
                    m_buttonsColor = value;

                    UpdateButtonsColor();
                }

            }
        }

        private Color m_leftGripColor = Color.Gray;
        public Color LeftGripColor
        {
            get => m_leftGripColor;
            set
            {
                m_leftGripColor = value;
                m_proCtrlColorHasChanged = true;
                //UpdateProControllerColor();
            }
        }

        private Color m_rightGripColor = Color.Gray;
        public Color RightGripColor
        {
            get => m_rightGripColor;
            set
            {
                m_rightGripColor = value;
                m_proCtrlColorHasChanged = true;
                //UpdateProControllerColor();
            }
        }

        public void SetProControllerColor(Color body, Color leftGrip, Color rightGrip)
        {
            if (body != m_bodyColor || leftGrip != m_leftGripColor || rightGrip != m_rightGripColor)
            {
                m_bodyColor = body;
                m_leftGripColor = leftGrip;
                m_rightGripColor = rightGrip;

                UpdateBodyColor();
                UpdateProControllerColor();
            }
        }

        private Vector2 m_leftStick = new Vector2(0, 0);
        public Vector2 LeftStick
        {
            get => m_leftStick;
            set
            {
                m_leftStick = value;
                m_updateLeftStickRequired = true;
                //UpdateLeftStickLocation();
            }
        }

        private Vector2 m_rightStick = new Vector2(0, 0);
        public Vector2 RightStick
        {
            get => m_rightStick;
            set
            {
                m_rightStick = value;
                m_updateRightStickRequired = true;
                //UpdateRightStickLocation();
            }
        }

        private JoyconButtons m_oldButtons = new JoyconButtons();

        private JoyconButtons m_buttons;
        public JoyconButtons Buttons
        {
            get => m_buttons;
            set
            {
                if (m_buttons != value)
                {
                    m_buttons = value;
                    m_updateBtnRequired = true;
                    //UpdateButtons();
                }
            }
        }

        private Vector3 m_gyro;
        public Vector3 Gyro
        {
            get => m_gyro;
            set
            {
                if (m_gyro != value)
                {
                    m_gyro = value;

                    UpdateGyro();
                }
            }
        }

        private Vector3 m_acc;
        public Vector3 Acc
        {
            get => m_acc;
            set
            {
                if (m_acc != value)
                {
                    m_acc = value;

                    UpdateAcc();
                }
            }
        }

        private BatteryLevel m_batteryLevel = BatteryLevel.Empty;
        public BatteryLevel Battery
        {
            get => m_batteryLevel;
            set
            {
                if (m_batteryLevel != value)
                {
                    m_batteryLevel = value;
                    UpdateBattery();
                }
            }
        }

        private bool m_isCalibrated;
        public bool IsCalibrated
        {
            get => m_isCalibrated;
            set
            {
                if (value != m_isCalibrated)
                {
                    m_isCalibrated = value;
                    if (m_isCalibrated)
                    {
                        btn_calibration.Text = "Calibrate";
                        btn_calibration.Enabled = true;
                    }
                    else
                    {
                        btn_calibration.Text = "- - -";
                        btn_calibration.Enabled = false;
                    }
                }
            }
        }

        private PlayerLedState[] m_playerLeds = { PlayerLedState.Off, PlayerLedState.Off, PlayerLedState.Off, PlayerLedState.Off };
        public PlayerLedState[] PlayerLeds{
            get => m_playerLeds;
            set
            {
                if(value.Length == 4 && value != m_playerLeds)
                {
                    m_playerLeds = value;
                    UpdatePlayersLed();
                }
            }
        }

        public void SetPlayerLedState( PlayerLedState a, PlayerLedState b, PlayerLedState c, PlayerLedState d)
        {
            m_playerLeds[0] = a;
            m_playerLeds[1] = b;
            m_playerLeds[2] = c;
            m_playerLeds[3] = d;

            UpdatePlayersLed();
        }

        public Joycon()
        {
            InitializeComponent();
            InitAfter();
        }


        public event Action<Joycon> OnCalibrationRequest;
        public event Action<Joycon> OnDisconnectRequest;


        private void Joycon_Load(object sender, EventArgs e)
        {
            
        }

        private bool m_proCtrlColorHasChanged = true;
        private bool m_updateBtnRequired = true;
        private bool m_updateLeftStickRequired = true;
        private bool m_updateRightStickRequired = true;

        private Bitmap m_Ctrl_Left_Body_Base;

        private Bitmap m_Ctrl_Right_Body_Base;

        private Bitmap m_Ctrl_Stick_Base;
        private Bitmap m_Ctrl_Btn_Minus_Base;
        private Bitmap m_Ctrl_Arrow_Right_Base;
        private Bitmap m_Ctrl_Arrow_Down_Base;
        private Bitmap m_Ctrl_Arrow_Left_Base;
        private Bitmap m_Ctrl_Arrow_Up_Base;
        private Bitmap m_Ctrl_Screenshot_Base;
        private Bitmap m_Ctrl_Left_Btn_L_Base;
        private Bitmap m_Ctrl_Left_Btn_ZL_Base;

        private Bitmap m_Ctrl_Btn_Side_Base;
        private Bitmap m_Ctrl_Btn_SideFliped_Base;

        private Bitmap m_Ctrl_Btn_Plus_Base;
        private Bitmap m_Ctrl_Btn_Home_Base;

        private Bitmap m_Ctrl_Right_Btn_R_Base;
        private Bitmap m_Ctrl_Right_Btn_ZR_Base;

        private Bitmap m_Ctrl_Btn_Side_Color;
        private Bitmap m_Ctrl_Btn_SideFliped_Color;
        //private Bitmap m_Ctrl_Left_Body_Color;

        private Bitmap m_Ctrl_Btn_Minus_Color;
        private Bitmap m_Ctrl_Stick_Color;
        private Bitmap m_Ctrl_Arrow_Right_Color;
        private Bitmap m_Ctrl_Arrow_Down_Color;
        private Bitmap m_Ctrl_Arrow_Left_Color;
        private Bitmap m_Ctrl_Arrow_Up_Color;
        private Bitmap m_Ctrl_Btn_Screenshot_Color;
        private Bitmap m_Ctrl_Left_Btn_L_Color;
        private Bitmap m_Ctrl_Left_Btn_ZL_Color;

        private Bitmap m_Ctrl_Btn_Plus_Color;
        private Bitmap m_Ctrl_Btn_A_Color;
        private Bitmap m_Ctrl_Btn_B_Color;
        private Bitmap m_Ctrl_Btn_X_Color;
        private Bitmap m_Ctrl_Btn_Y_Color;
        private Bitmap m_Ctrl_Btn_Home_Color;

        private Bitmap m_Ctrl_Right_Btn_R_Color;
        private Bitmap m_Ctrl_Right_Btn_ZR_Color;

        private Bitmap m_Ctrl_Pro_Btn_R_Color;
        private Bitmap m_Ctrl_Pro_Btn_ZR_Color;
        private Bitmap m_Ctrl_Pro_Btn_L_Color;
        private Bitmap m_Ctrl_Pro_Btn_ZL_Color;

        //

        private Bitmap m_Ctrl_Btn_Minus_Inv;
        private Bitmap m_Ctrl_Stick_Inv;
        private Bitmap m_Ctrl_Arrow_Right_Inv;
        private Bitmap m_Ctrl_Arrow_Down_Inv;
        private Bitmap m_Ctrl_Arrow_Left_Inv;
        private Bitmap m_Ctrl_Arrow_Up_Inv;
        private Bitmap m_Ctrl_Screenshot_Inv;
        private Bitmap m_Ctrl_Left_Btn_L_Inv;
        private Bitmap m_Ctrl_Left_Btn_ZL_Inv;

        private Bitmap m_Ctrl_Btn_Plus_Inv;
        private Bitmap m_Ctrl_Btn_A_Inv;
        private Bitmap m_Ctrl_Btn_B_Inv;
        private Bitmap m_Ctrl_Btn_X_Inv;
        private Bitmap m_Ctrl_Btn_Y_Inv;
        private Bitmap m_Ctrl_Btn_Home_Inv;

        private Bitmap m_Ctrl_Right_Btn_R_Inv;
        private Bitmap m_Ctrl_Right_Btn_ZR_Inv;

        private Bitmap m_Ctrl_Btn_Side_Color_Inv;
        private Bitmap m_Ctrl_Btn_SideFliped_Color_Inv;

        private Bitmap m_Ctrl_Pro_Btn_R_Color_Inv;
        private Bitmap m_Ctrl_Pro_Btn_ZR_Color_Inv;
        private Bitmap m_Ctrl_Pro_Btn_L_Color_Inv;
        private Bitmap m_Ctrl_Pro_Btn_ZL_Color_Inv;


        private Bitmap m_Ctrl_PlayerLed_On;
        private Bitmap m_Ctrl_PlayerLed_Off;

        private int m_playersLedCounter;
        private PictureBox[] m_playerLedsLeft = new PictureBox[4];
        private PictureBox[] m_playerLedsRight = new PictureBox[4];
        private PictureBox[] m_playerLedsPro = new PictureBox[4];

        //private void InitBefore()
        //{

        //}

        private void InitAfter()
        {
            m_Ctrl_Left_Body_Base = (Bitmap)Ctrl_Left_Body_Color.Image;

            m_Ctrl_Right_Body_Base = (Bitmap)Ctrl_Right_Body_Color.Image;

            m_Ctrl_Stick_Base = (Bitmap)Ctrl_Left_Stick.Image;
            m_Ctrl_Arrow_Right_Base = (Bitmap)Ctrl_Left_Btn_Right.Image;
            m_Ctrl_Btn_Minus_Base = (Bitmap)Ctrl_Left_Btn_Minus.Image;
            m_Ctrl_Screenshot_Base = (Bitmap)Ctrl_Left_Btn_Screenshot.Image;
            m_Ctrl_Btn_Side_Base = (Bitmap)Ctrl_Left_Btn_SR.Image;
            m_Ctrl_Btn_SideFliped_Base = new Bitmap(m_Ctrl_Btn_Side_Base);
            m_Ctrl_Btn_SideFliped_Base.RotateFlip(RotateFlipType.RotateNoneFlipX);
            m_Ctrl_Left_Btn_L_Base = (Bitmap)Ctrl_Left_Btn_L.Image;
            m_Ctrl_Left_Btn_ZL_Base = (Bitmap)Ctrl_Left_Btn_ZL.Image;

            m_Ctrl_Btn_Plus_Base = (Bitmap)Ctrl_Right_Btn_Plus.Image;
            m_Ctrl_Btn_Home_Base = (Bitmap)Ctrl_Right_Btn_Home.Image;
            m_Ctrl_Right_Btn_R_Base = (Bitmap)Ctrl_Right_Btn_R.Image;
            m_Ctrl_Right_Btn_ZR_Base = (Bitmap)Ctrl_Right_Btn_ZR.Image;

            m_Ctrl_Arrow_Down_Base = new Bitmap(m_Ctrl_Arrow_Right_Base);
            m_Ctrl_Arrow_Down_Base.RotateFlip(RotateFlipType.Rotate90FlipNone);

            Ctrl_Left_Btn_Down.Image = m_Ctrl_Arrow_Down_Base;
            Ctrl_Pro_Btn_Down.Image = m_Ctrl_Arrow_Down_Base;

            m_Ctrl_Arrow_Left_Base = new Bitmap(m_Ctrl_Arrow_Down_Base);
            m_Ctrl_Arrow_Left_Base.RotateFlip(RotateFlipType.Rotate90FlipNone);

            Ctrl_Left_Btn_Left.Image = m_Ctrl_Arrow_Left_Base;
            Ctrl_Pro_Btn_Left.Image = m_Ctrl_Arrow_Left_Base;

            m_Ctrl_Arrow_Up_Base = new Bitmap(m_Ctrl_Arrow_Left_Base);
            m_Ctrl_Arrow_Up_Base.RotateFlip(RotateFlipType.Rotate90FlipNone);

            Ctrl_Left_Btn_Up.Image = m_Ctrl_Arrow_Up_Base;
            Ctrl_Pro_Btn_Up.Image = m_Ctrl_Arrow_Up_Base;

            m_Ctrl_PlayerLed_Off = Tools.ApplyColorToBitmap(new Bitmap(UI.Properties.Resources.Nintendo_Switch_Joycon__PlayerLed), Color.Gray);
            m_Ctrl_PlayerLed_On = Tools.ApplyColorToBitmap(new Bitmap(UI.Properties.Resources.Nintendo_Switch_Joycon__PlayerLed), Color.Lime);

            m_playerLedsLeft[0] = Ctrl_Left_PlayerLed1;
            m_playerLedsLeft[1] = Ctrl_Left_PlayerLed2;
            m_playerLedsLeft[2] = Ctrl_Left_PlayerLed3;
            m_playerLedsLeft[3] = Ctrl_Left_PlayerLed4;

            m_playerLedsRight[0] = Ctrl_Right_PlayerLed1;
            m_playerLedsRight[1] = Ctrl_Right_PlayerLed2;
            m_playerLedsRight[2] = Ctrl_Right_PlayerLed3;
            m_playerLedsRight[3] = Ctrl_Right_PlayerLed4;

            m_playerLedsPro[0] = Ctrl_Pro_PlayerLed1;
            m_playerLedsPro[1] = Ctrl_Pro_PlayerLed2;
            m_playerLedsPro[2] = Ctrl_Pro_PlayerLed3;
            m_playerLedsPro[3] = Ctrl_Pro_PlayerLed4;
            //m_Ctrl_Btn_Side_Color = new Bitmap(m_Ctrl_Btn_Side_Base);
            //m_Ctrl_Btn_SideFlip_Color = new Bitmap(m_Ctrl_Btn_Side_Base);

            Tools.SetParent(Ctrl_Left_Body_Color, Ctrl_Left_Side);

            Tools.SetParent(Ctrl_Left_Stick, Ctrl_Left_Body_Color);
            Tools.SetParent(Ctrl_Left_Btn_Minus, Ctrl_Left_Body_Color);
            Tools.SetParent(Ctrl_Left_Btn_Screenshot, Ctrl_Left_Body_Color);
            Tools.SetParent(Ctrl_Left_Btn_Right, Ctrl_Left_Body_Color);
            Tools.SetParent(Ctrl_Left_Btn_Down, Ctrl_Left_Body_Color);
            Tools.SetParent(Ctrl_Left_Btn_Left, Ctrl_Left_Body_Color);
            Tools.SetParent(Ctrl_Left_Btn_Up, Ctrl_Left_Body_Color);
            Tools.SetParent(Ctrl_Left_Btn_SR, Ctrl_Left_Body_Color);
            Tools.SetParent(Ctrl_Left_Btn_SL, Ctrl_Left_Body_Color);
            Tools.SetParent(Ctrl_Left_Btn_L, Ctrl_Left_Body_Color);
            Tools.SetParent(Ctrl_Left_Btn_ZL, Ctrl_Left_Btn_L);

            Tools.SetParent(Ctrl_Right_Body_Color, Ctrl_Right_Side);

            Tools.SetParent(Ctrl_Right_Stick, Ctrl_Right_Body_Color);
            Tools.SetParent(Ctrl_Right_Btn_Plus, Ctrl_Right_Body_Color);
            Tools.SetParent(Ctrl_Right_Btn_A, Ctrl_Right_Body_Color);
            Tools.SetParent(Ctrl_Right_Btn_B, Ctrl_Right_Body_Color);
            Tools.SetParent(Ctrl_Right_Btn_X, Ctrl_Right_Body_Color);
            Tools.SetParent(Ctrl_Right_Btn_Y, Ctrl_Right_Body_Color);
            Tools.SetParent(Ctrl_Right_Btn_SR, Ctrl_Right_Body_Color);
            Tools.SetParent(Ctrl_Right_Btn_SL, Ctrl_Right_Body_Color);
            Tools.SetParent(Ctrl_Right_Btn_R, Ctrl_Right_Body_Color);
            Tools.SetParent(Ctrl_Right_Btn_ZR, Ctrl_Right_Btn_R);
            Tools.SetParent(Ctrl_Right_Btn_HomeLed, Ctrl_Right_Body_Color);
            Tools.SetParent(Ctrl_Right_Btn_Home, Ctrl_Right_Btn_HomeLed);

            Tools.SetParent(Ctrl_Pro_Btn_A, Ctrl_Pro_Base);
            Tools.SetParent(Ctrl_Pro_Btn_B, Ctrl_Pro_Base);
            Tools.SetParent(Ctrl_Pro_Btn_X, Ctrl_Pro_Base);
            Tools.SetParent(Ctrl_Pro_Btn_Y, Ctrl_Pro_Base);
            Tools.SetParent(Ctrl_Pro_Btn_Home, Ctrl_Pro_Base);
            Tools.SetParent(Ctrl_Pro_Btn_Right, Ctrl_Pro_Base);
            Tools.SetParent(Ctrl_Pro_Btn_Down, Ctrl_Pro_Base);
            Tools.SetParent(Ctrl_Pro_Btn_Left, Ctrl_Pro_Base);
            Tools.SetParent(Ctrl_Pro_Btn_Up, Ctrl_Pro_Base);
            Tools.SetParent(Ctrl_Pro_Btn_Screenshot, Ctrl_Pro_Base);
            Tools.SetParent(Ctrl_Pro_Btn_Plus, Ctrl_Pro_Base);
            Tools.SetParent(Ctrl_Pro_Btn_Minus, Ctrl_Pro_Base);
            Tools.SetParent(Ctrl_Pro_Btn_L, Ctrl_Pro_Base);
            Tools.SetParent(Ctrl_Pro_Btn_ZL, Ctrl_Pro_Base);
            Tools.SetParent(Ctrl_Pro_Btn_R, Ctrl_Pro_Base);
            Tools.SetParent(Ctrl_Pro_Btn_ZR, Ctrl_Pro_Base);

            Tools.SetParent(Update_Rate_Pro, Ctrl_Pro_Base);

            Tools.SetParent(Ctrl_Pro_RStick, Ctrl_Pro_Base);
            Tools.SetParent(Ctrl_Pro_LStick, Ctrl_Pro_Base);

            Tools.SetParent(Ctrl_Pro_PlayerLedsPanel, Ctrl_Pro_Base);


            m_leftStickInitialLocation = Ctrl_Left_Stick.Location;
            m_rightStickInitialLocation = Ctrl_Right_Stick.Location;

            UpdateBattery();

            UpdateCtrlType();

            UpdateGyro();
            UpdateAcc();

            UpdateButtonsColor();

            UpdateButtons();

            this.Size = new Size(223, 142);

            FlashLedTimer.Start();
            UpdateTimer.Start();
        }

        private void UpdateLeftStickLocation()
        {
            Point newLoc = new Point();

            newLoc.X = (int)(m_leftStick.X * k_maxStickOffset) + m_leftStickInitialLocation.X;
            newLoc.Y = (int)(-m_leftStick.Y * k_maxStickOffset) + m_leftStickInitialLocation.Y;

            Ctrl_Left_Stick.Location = newLoc;
        }

        private void UpdateRightStickLocation()
        {
            Point newLoc = new Point();

            newLoc.X = (int)(m_rightStick.X * k_maxStickOffset) + m_rightStickInitialLocation.X;
            newLoc.Y = (int)(-m_rightStick.Y * k_maxStickOffset) + m_rightStickInitialLocation.Y;

            Ctrl_Right_Stick.Location = newLoc;
        }

        private void UpdateButtons()
        {

            //Left
            if (m_oldButtons.left.Minus != m_buttons.left.Minus)
                Ctrl_Left_Btn_Minus.Image = InvertedOrNot(m_Ctrl_Btn_Minus_Inv, m_Ctrl_Btn_Minus_Color, m_buttons.left.Minus);
            if (m_oldButtons.left.Stick != m_buttons.left.Stick)
                Ctrl_Left_Stick.Image = InvertedOrNot(m_Ctrl_Stick_Inv, m_Ctrl_Stick_Color, m_buttons.left.Stick);
            if (m_oldButtons.left.ScreenShot != m_buttons.left.ScreenShot)
                Ctrl_Left_Btn_Screenshot.Image = InvertedOrNot(m_Ctrl_Screenshot_Inv, m_Ctrl_Btn_Screenshot_Color, m_buttons.left.ScreenShot);

            if (m_oldButtons.left.Right != m_buttons.left.Right)
                Ctrl_Left_Btn_Right.Image = InvertedOrNot(m_Ctrl_Arrow_Right_Inv, m_Ctrl_Arrow_Right_Color, m_buttons.left.Right);
            if (m_oldButtons.left.Down != m_buttons.left.Down)
                Ctrl_Left_Btn_Down.Image = InvertedOrNot(m_Ctrl_Arrow_Down_Inv, m_Ctrl_Arrow_Down_Color, m_buttons.left.Down);
            if (m_oldButtons.left.Left != m_buttons.left.Left)
                Ctrl_Left_Btn_Left.Image = InvertedOrNot(m_Ctrl_Arrow_Left_Inv, m_Ctrl_Arrow_Left_Color, m_buttons.left.Left);
            if (m_oldButtons.left.Up!= m_buttons.left.Up)
                Ctrl_Left_Btn_Up.Image = InvertedOrNot(m_Ctrl_Arrow_Up_Inv, m_Ctrl_Arrow_Up_Color, m_buttons.left.Up);
            if (m_oldButtons.left.L != m_buttons.left.L)
                Ctrl_Left_Btn_L.Image = InvertedOrNot(m_Ctrl_Left_Btn_L_Inv, m_Ctrl_Left_Btn_L_Color, m_buttons.left.L);
            if (m_oldButtons.left.ZL != m_buttons.left.ZL)
                Ctrl_Left_Btn_ZL.Image = InvertedOrNot(m_Ctrl_Left_Btn_ZL_Inv, m_Ctrl_Left_Btn_ZL_Color, m_buttons.left.ZL);

            if (m_oldButtons.left.SR != m_buttons.left.SR)
                Ctrl_Left_Btn_SR.Image = InvertedOrNot(m_Ctrl_Btn_Side_Color_Inv, m_Ctrl_Btn_Side_Color, m_buttons.left.SR);
            if (m_oldButtons.left.SL != m_buttons.left.SL)
                Ctrl_Left_Btn_SL.Image = InvertedOrNot(m_Ctrl_Btn_Side_Color_Inv, m_Ctrl_Btn_Side_Color, m_buttons.left.SL);

            //Right

            if (m_oldButtons.right.Plus != m_buttons.right.Plus)
                Ctrl_Right_Btn_Plus.Image = InvertedOrNot(m_Ctrl_Btn_Plus_Inv, m_Ctrl_Btn_Plus_Color, m_buttons.right.Plus);
            if (m_oldButtons.right.Stick != m_buttons.right.Stick)
                Ctrl_Right_Stick.Image = InvertedOrNot(m_Ctrl_Stick_Inv, m_Ctrl_Stick_Color, m_buttons.right.Stick);

            if (m_oldButtons.right.A != m_buttons.right.A)
                    Ctrl_Right_Btn_A.Image = InvertedOrNot(m_Ctrl_Btn_A_Inv, m_Ctrl_Btn_A_Color, m_buttons.right.A);
            if (m_oldButtons.right.B != m_buttons.right.B)
                Ctrl_Right_Btn_B.Image = InvertedOrNot(m_Ctrl_Btn_B_Inv, m_Ctrl_Btn_B_Color, m_buttons.right.B);
            if (m_oldButtons.right.X != m_buttons.right.X)
                Ctrl_Right_Btn_X.Image = InvertedOrNot(m_Ctrl_Btn_X_Inv, m_Ctrl_Btn_X_Color, m_buttons.right.X);
            if (m_oldButtons.right.Y != m_buttons.right.Y)
                Ctrl_Right_Btn_Y.Image = InvertedOrNot(m_Ctrl_Btn_Y_Inv, m_Ctrl_Btn_Y_Color, m_buttons.right.Y);
            if (m_oldButtons.right.Home != m_buttons.right.Home)
                Ctrl_Right_Btn_Home.Image = InvertedOrNot(m_Ctrl_Btn_Home_Inv, m_Ctrl_Btn_Home_Color, m_buttons.right.Home);
            if (m_oldButtons.right.R != m_buttons.right.R)
                Ctrl_Right_Btn_R.Image = InvertedOrNot(m_Ctrl_Right_Btn_R_Inv, m_Ctrl_Right_Btn_R_Color, m_buttons.right.R);
            if (m_oldButtons.right.ZR != m_buttons.right.ZR)
                Ctrl_Right_Btn_ZR.Image = InvertedOrNot(m_Ctrl_Right_Btn_ZR_Inv, m_Ctrl_Right_Btn_ZR_Color, m_buttons.right.ZR);

            if (m_oldButtons.right.SR != m_buttons.right.SR)
                Ctrl_Right_Btn_SR.Image = InvertedOrNot(m_Ctrl_Btn_SideFliped_Color_Inv, m_Ctrl_Btn_SideFliped_Color, m_buttons.right.SR);
            if (m_oldButtons.right.SL != m_buttons.right.SL)
                Ctrl_Right_Btn_SL.Image = InvertedOrNot(m_Ctrl_Btn_SideFliped_Color_Inv, m_Ctrl_Btn_SideFliped_Color, m_buttons.right.SL);

            //Pro

            Ctrl_Pro_Btn_A.Image = Ctrl_Right_Btn_A.Image;
            Ctrl_Pro_Btn_B.Image = Ctrl_Right_Btn_B.Image;
            Ctrl_Pro_Btn_X.Image = Ctrl_Right_Btn_X.Image;
            Ctrl_Pro_Btn_Y.Image = Ctrl_Right_Btn_Y.Image;
            Ctrl_Pro_Btn_Home.Image = Ctrl_Right_Btn_Home.Image;
            Ctrl_Pro_Btn_Plus.Image = Ctrl_Right_Btn_Plus.Image;
            Ctrl_Pro_RStick.Image = Ctrl_Right_Stick.Image;

            Ctrl_Pro_Btn_Up.Image = Ctrl_Left_Btn_Up.Image;
            Ctrl_Pro_Btn_Down.Image = Ctrl_Left_Btn_Down.Image;
            Ctrl_Pro_Btn_Left.Image = Ctrl_Left_Btn_Left.Image;
            Ctrl_Pro_Btn_Right.Image = Ctrl_Left_Btn_Right.Image;
            Ctrl_Pro_Btn_Screenshot.Image = Ctrl_Left_Btn_Screenshot.Image;
            Ctrl_Pro_Btn_Minus.Image = Ctrl_Left_Btn_Minus.Image;
            Ctrl_Pro_LStick.Image = Ctrl_Left_Stick.Image;

            if (m_oldButtons.left.L != m_buttons.left.L)
                Ctrl_Pro_Btn_L.Image = InvertedOrNot(m_Ctrl_Pro_Btn_L_Color_Inv, m_Ctrl_Pro_Btn_L_Color, m_buttons.left.L);
            if (m_oldButtons.left.ZL != m_buttons.left.ZL)
                Ctrl_Pro_Btn_ZL.Image = InvertedOrNot(m_Ctrl_Pro_Btn_ZL_Color_Inv, m_Ctrl_Pro_Btn_ZL_Color, m_buttons.left.ZL);
            if (m_oldButtons.right.R != m_buttons.right.R)
                Ctrl_Pro_Btn_R.Image = InvertedOrNot(m_Ctrl_Pro_Btn_R_Color_Inv, m_Ctrl_Pro_Btn_R_Color, m_buttons.right.R);
            if (m_oldButtons.right.ZR != m_buttons.right.ZR)
                Ctrl_Pro_Btn_ZR.Image = InvertedOrNot(m_Ctrl_Pro_Btn_ZR_Color_Inv, m_Ctrl_Pro_Btn_ZR_Color, m_buttons.right.ZR);

            m_oldButtons = m_buttons;
        }

        private void UpdateBodyColor()
        {
            Ctrl_Left_Body_Color.Image = Tools.ApplyColorToBitmap(m_Ctrl_Left_Body_Base, m_bodyColor);

            Ctrl_Right_Body_Color.Image = Tools.ApplyColorToBitmap(m_Ctrl_Right_Body_Base, m_bodyColor);

            m_Ctrl_Btn_Side_Color = Tools.ApplyColorToBitmap(m_Ctrl_Btn_Side_Base, m_bodyColor);
            m_Ctrl_Btn_Side_Color_Inv = Tools.InvertColor(m_Ctrl_Btn_Side_Color);

            Ctrl_Left_Btn_SL.Image = m_Ctrl_Btn_Side_Color;
            Ctrl_Left_Btn_SR.Image = m_Ctrl_Btn_Side_Color;


            m_Ctrl_Btn_SideFliped_Color = Tools.ApplyColorToBitmap(m_Ctrl_Btn_SideFliped_Base, m_bodyColor);
            m_Ctrl_Btn_SideFliped_Color_Inv = Tools.InvertColor(m_Ctrl_Btn_SideFliped_Color);

            Ctrl_Right_Btn_SR.Image = m_Ctrl_Btn_SideFliped_Color;
            Ctrl_Right_Btn_SL.Image = m_Ctrl_Btn_SideFliped_Color;
        }

        private void UpdateProControllerColor()
        {
            Rectangle a = new Rectangle(100, 200, 100, 100);

            Bitmap[] layers =
            {
                new Bitmap(UI.Properties.Resources.Nintendo_Switch_Joycon__Pro_Body_Mask),
                new Bitmap(UI.Properties.Resources.Nintendo_Switch_Joycon__Pro_LeftGrip_Mask),
                new Bitmap(UI.Properties.Resources.Nintendo_Switch_Joycon__Pro_RightGrip_Mask),
                new Bitmap(UI.Properties.Resources.Nintendo_Switch_Joycon__Pro_Outline)
            };


            layers[0] = Tools.ApplyColorToBitmap(layers[0], m_bodyColor);
            layers[1] = Tools.ApplyColorToBitmap(layers[1], m_leftGripColor);
            layers[2] = Tools.ApplyColorToBitmap(layers[2], m_rightGripColor);

            Ctrl_Pro_Base.Image = Tools.ComposeImage(layers);
        }

        private void UpdateButtonsColor()
        {
            //STICKS

            m_Ctrl_Stick_Color = Tools.ApplyColorToBitmap(m_Ctrl_Stick_Base, m_buttonsColor);

            m_Ctrl_Stick_Inv = Tools.InvertColor(m_Ctrl_Stick_Color);

            Ctrl_Left_Stick.Image = m_Ctrl_Stick_Color;
            Ctrl_Right_Stick.Image = m_Ctrl_Stick_Color;

            //Left

            m_Ctrl_Arrow_Down_Color = Tools.ApplyColorToBitmap(m_Ctrl_Arrow_Down_Base, m_buttonsColor);
            m_Ctrl_Arrow_Down_Inv = Tools.InvertColor(m_Ctrl_Arrow_Down_Color);

            Ctrl_Left_Btn_Down.Image = m_Ctrl_Arrow_Down_Color;

            m_Ctrl_Arrow_Right_Color = Tools.ApplyColorToBitmap(m_Ctrl_Arrow_Right_Base, m_buttonsColor);
            m_Ctrl_Arrow_Right_Inv = Tools.InvertColor(m_Ctrl_Arrow_Right_Color);

            Ctrl_Left_Btn_Right.Image = m_Ctrl_Arrow_Right_Color;

            m_Ctrl_Arrow_Up_Color = Tools.ApplyColorToBitmap(m_Ctrl_Arrow_Up_Base, m_buttonsColor);
            m_Ctrl_Arrow_Up_Inv = Tools.InvertColor(m_Ctrl_Arrow_Up_Color);

            Ctrl_Left_Btn_Up.Image = m_Ctrl_Arrow_Up_Color;

            m_Ctrl_Arrow_Left_Color = Tools.ApplyColorToBitmap(m_Ctrl_Arrow_Left_Base, m_buttonsColor);
            m_Ctrl_Arrow_Left_Inv = Tools.InvertColor(m_Ctrl_Arrow_Left_Color);

            Ctrl_Left_Btn_Left.Image = m_Ctrl_Arrow_Left_Color;

            m_Ctrl_Btn_Minus_Color = Tools.ApplyColorToBitmap(m_Ctrl_Btn_Minus_Base, m_buttonsColor);
            m_Ctrl_Btn_Minus_Inv = Tools.InvertColor(m_Ctrl_Btn_Minus_Color);

            Ctrl_Left_Btn_Minus.Image = m_Ctrl_Btn_Minus_Color;

            m_Ctrl_Btn_Screenshot_Color = Tools.ApplyColorToBitmap(m_Ctrl_Screenshot_Base, m_buttonsColor);
            m_Ctrl_Screenshot_Inv = Tools.InvertColor(m_Ctrl_Btn_Screenshot_Color);

            Ctrl_Left_Btn_Screenshot.Image = m_Ctrl_Btn_Screenshot_Color;

            m_Ctrl_Left_Btn_L_Color = Tools.ApplyColorToBitmap(m_Ctrl_Left_Btn_L_Base, m_buttonsColor);
            m_Ctrl_Left_Btn_L_Inv = Tools.InvertColor(m_Ctrl_Left_Btn_L_Color);

            Ctrl_Left_Btn_L.Image = m_Ctrl_Left_Btn_L_Color;

            m_Ctrl_Left_Btn_ZL_Color = Tools.ApplyColorToBitmap(m_Ctrl_Left_Btn_ZL_Base, m_buttonsColor);
            m_Ctrl_Left_Btn_ZL_Inv = Tools.InvertColor(m_Ctrl_Left_Btn_ZL_Color);

            Ctrl_Left_Btn_ZL.Image = m_Ctrl_Left_Btn_ZL_Color;

            //Right

            m_Ctrl_Btn_Plus_Color = Tools.ApplyColorToBitmap(m_Ctrl_Btn_Plus_Base, m_buttonsColor);
            m_Ctrl_Btn_Plus_Inv = Tools.InvertColor(m_Ctrl_Btn_Plus_Color);

            Ctrl_Right_Btn_Plus.Image = m_Ctrl_Btn_Plus_Color;

            m_Ctrl_Btn_Home_Color = Tools.ApplyColorToBitmap(m_Ctrl_Btn_Home_Base, m_buttonsColor);
            m_Ctrl_Btn_Home_Inv = Tools.InvertColor(m_Ctrl_Btn_Home_Color);

            Ctrl_Right_Btn_Home.Image = m_Ctrl_Btn_Home_Color;

            m_Ctrl_Right_Btn_R_Color = Tools.ApplyColorToBitmap(m_Ctrl_Right_Btn_R_Base, m_buttonsColor);
            m_Ctrl_Right_Btn_R_Inv = Tools.InvertColor(m_Ctrl_Right_Btn_R_Color);

            Ctrl_Right_Btn_R.Image = m_Ctrl_Right_Btn_R_Color;

            m_Ctrl_Right_Btn_ZR_Color = Tools.ApplyColorToBitmap(m_Ctrl_Right_Btn_ZR_Base, m_buttonsColor);
            m_Ctrl_Right_Btn_ZR_Inv = Tools.InvertColor(m_Ctrl_Right_Btn_ZR_Color);

            Ctrl_Right_Btn_ZR.Image = m_Ctrl_Right_Btn_ZR_Color;

            Bitmap btnBase = new Bitmap(UI.Properties.Resources.Nintendo_Switch_Joycon__Btn_Base);

            btnBase = Tools.ApplyColorToBitmap(btnBase, m_buttonsColor);

            m_Ctrl_Btn_A_Color = Tools.ComposeImage(new Bitmap[] {btnBase, new Bitmap(UI.Properties.Resources.Nintendo_Switch_Joycon__A)});
            m_Ctrl_Btn_B_Color = Tools.ComposeImage(new Bitmap[] {btnBase, new Bitmap(UI.Properties.Resources.Nintendo_Switch_Joycon__B)});
            m_Ctrl_Btn_X_Color = Tools.ComposeImage(new Bitmap[] {btnBase, new Bitmap(UI.Properties.Resources.Nintendo_Switch_Joycon__X)});
            m_Ctrl_Btn_Y_Color = Tools.ComposeImage(new Bitmap[] {btnBase, new Bitmap(UI.Properties.Resources.Nintendo_Switch_Joycon__Y)});

            m_Ctrl_Btn_A_Inv = Tools.InvertColor(m_Ctrl_Btn_A_Color);
            m_Ctrl_Btn_B_Inv = Tools.InvertColor(m_Ctrl_Btn_B_Color);
            m_Ctrl_Btn_X_Inv = Tools.InvertColor(m_Ctrl_Btn_X_Color);
            m_Ctrl_Btn_Y_Inv = Tools.InvertColor(m_Ctrl_Btn_Y_Color);

            Ctrl_Right_Btn_A.Image = m_Ctrl_Btn_A_Color;
            Ctrl_Right_Btn_B.Image = m_Ctrl_Btn_B_Color;
            Ctrl_Right_Btn_X.Image = m_Ctrl_Btn_X_Color;
            Ctrl_Right_Btn_Y.Image = m_Ctrl_Btn_Y_Color;

            //Pro

            Ctrl_Pro_Btn_A.Image = m_Ctrl_Btn_A_Color;
            Ctrl_Pro_Btn_B.Image = m_Ctrl_Btn_B_Color;
            Ctrl_Pro_Btn_X.Image = m_Ctrl_Btn_X_Color;
            Ctrl_Pro_Btn_Y.Image = m_Ctrl_Btn_Y_Color;

            Ctrl_Pro_LStick.Image = m_Ctrl_Stick_Color;
            Ctrl_Pro_RStick.Image = m_Ctrl_Stick_Color;

            Ctrl_Pro_Btn_Down.Image = m_Ctrl_Arrow_Down_Color;
            Ctrl_Pro_Btn_Up.Image = m_Ctrl_Arrow_Up_Color;
            Ctrl_Pro_Btn_Left.Image = m_Ctrl_Arrow_Left_Color;
            Ctrl_Pro_Btn_Right.Image = m_Ctrl_Arrow_Right_Color;

            Ctrl_Pro_Btn_Home.Image = m_Ctrl_Btn_Home_Color;
            Ctrl_Pro_Btn_Screenshot.Image = m_Ctrl_Btn_Screenshot_Color;
            Ctrl_Pro_Btn_Minus.Image = m_Ctrl_Btn_Minus_Color;
            Ctrl_Pro_Btn_Plus.Image = m_Ctrl_Btn_Plus_Color;

            m_Ctrl_Pro_Btn_L_Color = Tools.ApplyColorToBitmap(new Bitmap(UI.Properties.Resources.Nintendo_Switch_Joycon__Pro_L_Btn), m_buttonsColor);
            m_Ctrl_Pro_Btn_L_Color_Inv = Tools.InvertColor(m_Ctrl_Pro_Btn_L_Color);
            m_Ctrl_Pro_Btn_ZL_Color = Tools.ApplyColorToBitmap(new Bitmap(UI.Properties.Resources.Nintendo_Switch_Joycon__Pro_ZL_Btn), m_buttonsColor);
            m_Ctrl_Pro_Btn_ZL_Color_Inv = Tools.InvertColor(m_Ctrl_Pro_Btn_ZL_Color);
            m_Ctrl_Pro_Btn_R_Color = Tools.ApplyColorToBitmap(new Bitmap(UI.Properties.Resources.Nintendo_Switch_Joycon__Pro_R_Btn), m_buttonsColor);
            m_Ctrl_Pro_Btn_R_Color_Inv = Tools.InvertColor(m_Ctrl_Pro_Btn_R_Color);
            m_Ctrl_Pro_Btn_ZR_Color = Tools.ApplyColorToBitmap(new Bitmap(UI.Properties.Resources.Nintendo_Switch_Joycon__Pro_ZR_Btn), m_buttonsColor);
            m_Ctrl_Pro_Btn_ZR_Color_Inv = Tools.InvertColor(m_Ctrl_Pro_Btn_ZR_Color);


            Ctrl_Pro_Btn_L.Image = m_Ctrl_Pro_Btn_L_Color;
            Ctrl_Pro_Btn_ZL.Image = m_Ctrl_Pro_Btn_ZL_Color;
            Ctrl_Pro_Btn_R.Image = m_Ctrl_Pro_Btn_R_Color;
            Ctrl_Pro_Btn_ZR.Image = m_Ctrl_Pro_Btn_ZR_Color;
        }

        private void UpdateGyro()
        {
            UpdateGraphBar(Gyro_Graph_X, Gyro_Graph_X_Ctn, (float)m_gyro.X, 360f * 3);
            UpdateGraphBar(Gyro_Graph_Y, Gyro_Graph_Y_Ctn, (float)m_gyro.Y, 360f * 3);
            UpdateGraphBar(Gyro_Graph_Z, Gyro_Graph_Z_Ctn, (float)m_gyro.Z, 360f * 3);
        }

        private void UpdateAcc()
        {
            UpdateGraphBar(Acc_Graph_X, Acc_Graph_X_Ctn, (float)m_acc.X, 5f);
            UpdateGraphBar(Acc_Graph_Y, Acc_Graph_Y_Ctn, (float)m_acc.Y, 5f);
            UpdateGraphBar(Acc_Graph_Z, Acc_Graph_Z_Ctn, (float)m_acc.Z, 5f);
        }

        private void UpdateGraphBar(Panel bar, Panel barCtn, float value, float max)
        {
            float percent = value / max;

            float percentAbs = Math.Abs(percent);

            if (percentAbs > 1f)
                percentAbs = 1f;

            int ctnH = barCtn.Size.Height;

            int halfCtnH = ctnH / 2;
            int barH = (int)(percentAbs * halfCtnH);

            Size barSize = bar.Size;
            barSize.Height = barH;
            bar.Size = barSize;

            Point barLoc = bar.Location;

            if(percent < 0)
            {
                barLoc.Y = halfCtnH;
            }
            else
            {
                barLoc.Y = halfCtnH - barH;
            }

            bar.Location = barLoc;
        }

        private void UpdateBattery()
        {
            switch (m_batteryLevel)
            {
                case BatteryLevel.Empty:
                    Ctrl_Battery.Image = new Bitmap(UI.Properties.Resources.Nintendo_Switch_Joycon__Battery_0);
                    break;
                case BatteryLevel.Critical:
                    Ctrl_Battery.Image = new Bitmap(UI.Properties.Resources.Nintendo_Switch_Joycon__Battery_25);
                    break;
                case BatteryLevel.Low:
                    Ctrl_Battery.Image = new Bitmap(UI.Properties.Resources.Nintendo_Switch_Joycon__Battery_50);
                    break;
                case BatteryLevel.Medium:
                    Ctrl_Battery.Image = new Bitmap(UI.Properties.Resources.Nintendo_Switch_Joycon__Battery_75);
                    break;
                case BatteryLevel.Full:
                    Ctrl_Battery.Image = new Bitmap(UI.Properties.Resources.Nintendo_Switch_Joycon__Battery_100);
                    break;
            }

            Ctrl_Battery_Pro.Image = Ctrl_Battery.Image;

        }

        private void UpdateCtrlType()
        {
            switch (m_controllerType)
            {
                case SwitchControllerType.None:
                    break;
                case SwitchControllerType.JoyCon_Left:
                    Panel_Joycons.Visible = true;
                    Panel_ProCtrl.Visible = false;
                    Ctrl_Left_Side.Visible = true;
                    Ctrl_Right_Side.Visible = false;

                    Ctrl_Left_PlayerLedsPanel.Visible = true;
                    Ctrl_Right_PlayerLedsPanel.Visible = false;
                    break;
                case SwitchControllerType.JoyCon_Right:
                    Panel_Joycons.Visible = true;
                    Panel_ProCtrl.Visible = false;
                    Ctrl_Right_Side.Visible = true;
                    Ctrl_Left_Side.Visible = false;

                    Ctrl_Left_PlayerLedsPanel.Visible = false;
                    Ctrl_Right_PlayerLedsPanel.Visible = true;

                    break;
                case SwitchControllerType.Pro_Controller:
                    Panel_Joycons.Visible = false;
                    Panel_ProCtrl.Visible = true;
                    Ctrl_Left_Side.Visible = false;
                    Ctrl_Right_Side.Visible = false;
                    break;
                default:
                    break;
            }
        }

        private void UpdateUpRate()
        {
            Update_Rate.Text = "FPS: " + Math.Round(m_updateRate*10)/10;
            Update_Rate_Pro.Text = Update_Rate.Text;
        }

        private void UpdatePlayersLed()
        {
            for (int i = 0; i < 4; i++)
            {
                switch (m_playerLeds[i])
                {
                    case PlayerLedState.Off:
                        m_playerLedsLeft[i].Image = m_Ctrl_PlayerLed_Off;
                        m_playerLedsRight[i].Image = m_Ctrl_PlayerLed_Off;
                        m_playerLedsPro[i].Image = m_Ctrl_PlayerLed_Off;
                        break;
                    case PlayerLedState.On:
                        m_playerLedsLeft[i].Image = m_Ctrl_PlayerLed_On;
                        m_playerLedsRight[i].Image = m_Ctrl_PlayerLed_On;
                        m_playerLedsPro[i].Image = m_Ctrl_PlayerLed_On;
                        break;
                    case PlayerLedState.Flash:
                        break;
                    default:
                        break;
                }
            }
        }

        private Bitmap InvertedOrNot(Bitmap inv, Bitmap normal, bool isInv)
        {
            if (isInv)
                return inv;

            return normal;
        }

        private void btn_calibration_Click(object sender, EventArgs e)
        {
            if (OnCalibrationRequest != null)
                OnCalibrationRequest(this);
        }

        private void UpdateTimer_Tick(object sender, EventArgs e)
        {
            if (m_proCtrlColorHasChanged)
            {
                UpdateProControllerColor();
                m_proCtrlColorHasChanged = false;
            }

            if (m_updateBtnRequired)
            {
                UpdateButtons();
                m_updateBtnRequired = false;
            }

            if (m_updateLeftStickRequired)
            {
                UpdateLeftStickLocation();
                m_updateLeftStickRequired = false;
            }

            if (m_updateRightStickRequired)
            {
                UpdateRightStickLocation();
                m_updateRightStickRequired = false;
            }
            //TODO Same with btn update
        }

        private void FlashLedTimer_Tick(object sender, EventArgs e)
        {
            m_playersLedCounter++;
            if (m_playersLedCounter > 3)
                m_playersLedCounter = 0;

            bool lOn = false;

            if (m_playersLedCounter > 0)
                lOn = true;

            for (int i = 0; i < 4; i++)
            {
                switch (m_playerLeds[i])
                {
                    case PlayerLedState.Off:
                        break;
                    case PlayerLedState.On:
                        break;
                    case PlayerLedState.Flash:
                        m_playerLedsLeft[i].Image = lOn ? m_Ctrl_PlayerLed_On : m_Ctrl_PlayerLed_Off;
                        m_playerLedsRight[i].Image = lOn ? m_Ctrl_PlayerLed_On : m_Ctrl_PlayerLed_Off;
                        m_playerLedsPro[i].Image = lOn ? m_Ctrl_PlayerLed_On : m_Ctrl_PlayerLed_Off;
                        break;
                    default:
                        break;
                }
            }

            
        }

        private void Btn_DisconnectRequest_Click(object sender, EventArgs e)
        {
            if (OnDisconnectRequest != null)
                OnDisconnectRequest(this);
        }
    }
}
