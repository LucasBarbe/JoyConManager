﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI.UIElements.About
{
    public partial class About_Btn : UserControl
    {
        public About_Btn()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, EventArgs e)
        {
            About_Win about = new About_Win();

            DialogResult dialogResult = about.ShowDialog();

            if (dialogResult == DialogResult.Cancel)
                about.Dispose();
        }
    }
}
