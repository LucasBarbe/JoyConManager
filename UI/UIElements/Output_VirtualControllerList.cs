﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI.UIElements
{
    public partial class Output_VirtualControllerList : UserControl
    {
        public Output_VirtualControllerList()
        {
            InitializeComponent();


            for (int i = 0; i < this.Controls.Count; i++)
            {
                ((Output_VirtualController)Controls[i]).Index = 3 - i;
            }
        }

        public event Action<int> OnVirtualControllerConnectRequest;
        public event Action<int> OnVirtualControllerDisconnectRequest;

        public void BeginSearch(int ctrlIndex)
        {
            if (ctrlIndex < 0 || ctrlIndex > 3)
                return;

            Output_VirtualController vc = (Output_VirtualController)this.Controls[3 - ctrlIndex];
            vc.BeginSearch();
        }

        public void EndSearch(int ctrlIndex)
        {
            if (ctrlIndex < 0 || ctrlIndex > 3)
                return;

            Output_VirtualController vc = (Output_VirtualController)this.Controls[3 - ctrlIndex];
            vc.EndSearch();
        }
        public void EnableController(int ctrlIndex, Output_VirtualController.InputMode inputMode, Color leftColor, Color rightColor, int ctrlLedCount)
        {
            if (ctrlIndex < 0 || ctrlIndex > 3)
                return;

            Output_VirtualController vc = (Output_VirtualController)this.Controls[3 - ctrlIndex];

            vc.LeftColor = leftColor;
            vc.RightColor = rightColor;

            vc.Mode = inputMode;

            vc.LedIndex = ctrlLedCount;
        }

        public void DisableController(int ctrlIndex)
        {
            if (ctrlIndex < 0 || ctrlIndex > 3)
                return;

            Output_VirtualController vc = (Output_VirtualController)this.Controls[3 - ctrlIndex];

            vc.Mode = Output_VirtualController.InputMode.None;
        }

        private void VCtrl_OnConnectRequest_Callback(Output_VirtualController obj)
        {
            if (OnVirtualControllerConnectRequest != null)
                OnVirtualControllerConnectRequest(obj.Index);
        }

        private void VCtrl_OnDisconnectRequest_Callback(Output_VirtualController obj)
        {
            if (OnVirtualControllerDisconnectRequest != null)
                OnVirtualControllerDisconnectRequest(obj.Index);
        }
    }
}
