﻿namespace UI.UIElements
{
    partial class Output_VirtualControllerList
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            UI.UIElements.Output_VirtualController output_VirtualController1;
            this.output_VirtualController4 = new UI.UIElements.Output_VirtualController();
            this.output_VirtualController3 = new UI.UIElements.Output_VirtualController();
            this.output_VirtualController2 = new UI.UIElements.Output_VirtualController();
            output_VirtualController1 = new UI.UIElements.Output_VirtualController();
            this.SuspendLayout();
            // 
            // output_VirtualController1
            // 
            output_VirtualController1.BackColor = System.Drawing.Color.Transparent;
            output_VirtualController1.Dock = System.Windows.Forms.DockStyle.Top;
            output_VirtualController1.Index = 0;
            output_VirtualController1.LedIndex = 0;
            output_VirtualController1.LeftColor = System.Drawing.Color.Empty;
            output_VirtualController1.Location = new System.Drawing.Point(0, 0);
            output_VirtualController1.MaximumSize = new System.Drawing.Size(217, 66);
            output_VirtualController1.MinimumSize = new System.Drawing.Size(217, 66);
            output_VirtualController1.Mode = UI.UIElements.Output_VirtualController.InputMode.None;
            output_VirtualController1.Name = "output_VirtualController1";
            output_VirtualController1.RightColor = System.Drawing.Color.Empty;
            output_VirtualController1.Size = new System.Drawing.Size(217, 66);
            output_VirtualController1.TabIndex = 0;
            output_VirtualController1.OnConnectRequest += new System.Action<UI.UIElements.Output_VirtualController>(this.VCtrl_OnConnectRequest_Callback);
            output_VirtualController1.OnDisconnectRequest += new System.Action<UI.UIElements.Output_VirtualController>(this.VCtrl_OnDisconnectRequest_Callback);
            // 
            // output_VirtualController4
            // 
            this.output_VirtualController4.BackColor = System.Drawing.Color.Transparent;
            this.output_VirtualController4.Dock = System.Windows.Forms.DockStyle.Top;
            this.output_VirtualController4.Index = 3;
            this.output_VirtualController4.LedIndex = 0;
            this.output_VirtualController4.LeftColor = System.Drawing.Color.Empty;
            this.output_VirtualController4.Location = new System.Drawing.Point(0, 198);
            this.output_VirtualController4.MaximumSize = new System.Drawing.Size(217, 66);
            this.output_VirtualController4.MinimumSize = new System.Drawing.Size(217, 66);
            this.output_VirtualController4.Mode = UI.UIElements.Output_VirtualController.InputMode.None;
            this.output_VirtualController4.Name = "output_VirtualController4";
            this.output_VirtualController4.RightColor = System.Drawing.Color.Empty;
            this.output_VirtualController4.Size = new System.Drawing.Size(217, 66);
            this.output_VirtualController4.TabIndex = 3;
            this.output_VirtualController4.OnConnectRequest += new System.Action<UI.UIElements.Output_VirtualController>(this.VCtrl_OnConnectRequest_Callback);
            this.output_VirtualController4.OnDisconnectRequest += new System.Action<UI.UIElements.Output_VirtualController>(this.VCtrl_OnDisconnectRequest_Callback);
            // 
            // output_VirtualController3
            // 
            this.output_VirtualController3.BackColor = System.Drawing.Color.Transparent;
            this.output_VirtualController3.Dock = System.Windows.Forms.DockStyle.Top;
            this.output_VirtualController3.Index = 2;
            this.output_VirtualController3.LedIndex = 0;
            this.output_VirtualController3.LeftColor = System.Drawing.Color.Empty;
            this.output_VirtualController3.Location = new System.Drawing.Point(0, 132);
            this.output_VirtualController3.MaximumSize = new System.Drawing.Size(217, 66);
            this.output_VirtualController3.MinimumSize = new System.Drawing.Size(217, 66);
            this.output_VirtualController3.Mode = UI.UIElements.Output_VirtualController.InputMode.None;
            this.output_VirtualController3.Name = "output_VirtualController3";
            this.output_VirtualController3.RightColor = System.Drawing.Color.Empty;
            this.output_VirtualController3.Size = new System.Drawing.Size(217, 66);
            this.output_VirtualController3.TabIndex = 2;
            this.output_VirtualController3.OnConnectRequest += new System.Action<UI.UIElements.Output_VirtualController>(this.VCtrl_OnConnectRequest_Callback);
            this.output_VirtualController3.OnDisconnectRequest += new System.Action<UI.UIElements.Output_VirtualController>(this.VCtrl_OnDisconnectRequest_Callback);
            // 
            // output_VirtualController2
            // 
            this.output_VirtualController2.BackColor = System.Drawing.Color.Transparent;
            this.output_VirtualController2.Dock = System.Windows.Forms.DockStyle.Top;
            this.output_VirtualController2.Index = 1;
            this.output_VirtualController2.LedIndex = 0;
            this.output_VirtualController2.LeftColor = System.Drawing.Color.Empty;
            this.output_VirtualController2.Location = new System.Drawing.Point(0, 66);
            this.output_VirtualController2.MaximumSize = new System.Drawing.Size(217, 66);
            this.output_VirtualController2.MinimumSize = new System.Drawing.Size(217, 66);
            this.output_VirtualController2.Mode = UI.UIElements.Output_VirtualController.InputMode.None;
            this.output_VirtualController2.Name = "output_VirtualController2";
            this.output_VirtualController2.RightColor = System.Drawing.Color.Empty;
            this.output_VirtualController2.Size = new System.Drawing.Size(217, 66);
            this.output_VirtualController2.TabIndex = 1;
            this.output_VirtualController2.OnConnectRequest += new System.Action<UI.UIElements.Output_VirtualController>(this.VCtrl_OnConnectRequest_Callback);
            this.output_VirtualController2.OnDisconnectRequest += new System.Action<UI.UIElements.Output_VirtualController>(this.VCtrl_OnDisconnectRequest_Callback);
            // 
            // Output_VirtualControllerList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.output_VirtualController4);
            this.Controls.Add(this.output_VirtualController3);
            this.Controls.Add(this.output_VirtualController2);
            this.Controls.Add(output_VirtualController1);
            this.Name = "Output_VirtualControllerList";
            this.Size = new System.Drawing.Size(216, 264);
            this.ResumeLayout(false);

        }

        #endregion
        private Output_VirtualController output_VirtualController4;
        private Output_VirtualController output_VirtualController3;
        private Output_VirtualController output_VirtualController2;
    }
}
