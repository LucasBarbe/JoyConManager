﻿namespace UI.UIElements
{
    partial class Test
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.joycon1 = new UI.UIElements.Joycon();
            this.joyconList1 = new UI.UIElements.JoyconList();
            this.output_VirtualController1 = new UI.UIElements.Output_VirtualController();
            this.SuspendLayout();
            // 
            // joycon1
            // 
            this.joycon1.BackColor = System.Drawing.Color.Transparent;
            this.joycon1.Battery = UI.BatteryLevel.Empty;
            this.joycon1.BodyColor = System.Drawing.Color.DimGray;
            this.joycon1.ButtonsColor = System.Drawing.Color.White;
            this.joycon1.ControllerType = UI.SwitchControllerType.Pro_Controller;
            this.joycon1.IsCalibrated = false;
            this.joycon1.LeftGripColor = System.Drawing.Color.Crimson;
            this.joycon1.Location = new System.Drawing.Point(408, 21);
            this.joycon1.Margin = new System.Windows.Forms.Padding(0);
            this.joycon1.Name = "joycon1";
            this.joycon1.PlayerLeds = new UI.PlayerLedState[] {
        UI.PlayerLedState.Off,
        UI.PlayerLedState.Off,
        UI.PlayerLedState.Off,
        UI.PlayerLedState.Off};
            this.joycon1.RightGripColor = System.Drawing.Color.Crimson;
            this.joycon1.Size = new System.Drawing.Size(223, 142);
            this.joycon1.TabIndex = 0;
            this.joycon1.UpdateRate = 0F;
            // 
            // joyconList1
            // 
            this.joyconList1.AutoScroll = true;
            this.joyconList1.BackColor = System.Drawing.Color.Transparent;
            this.joyconList1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.joyconList1.Count = 3;
            this.joyconList1.Location = new System.Drawing.Point(3, 3);
            this.joyconList1.Name = "joyconList1";
            this.joyconList1.Size = new System.Drawing.Size(252, 379);
            this.joyconList1.TabIndex = 1;
            // 
            // output_VirtualController1
            // 
            this.output_VirtualController1.BackColor = System.Drawing.Color.Transparent;
            this.output_VirtualController1.Index = 0;
            this.output_VirtualController1.LedIndex = 1;
            this.output_VirtualController1.LeftColor = System.Drawing.Color.Empty;
            this.output_VirtualController1.Location = new System.Drawing.Point(408, 210);
            this.output_VirtualController1.Mode = UI.UIElements.Output_VirtualController.InputMode.None;
            this.output_VirtualController1.Name = "output_VirtualController1";
            this.output_VirtualController1.RightColor = System.Drawing.Color.Empty;
            this.output_VirtualController1.Size = new System.Drawing.Size(217, 86);
            this.output_VirtualController1.TabIndex = 2;
            // 
            // UserControl1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.output_VirtualController1);
            this.Controls.Add(this.joyconList1);
            this.Controls.Add(this.joycon1);
            this.Name = "UserControl1";
            this.Size = new System.Drawing.Size(649, 386);
            this.ResumeLayout(false);

        }

        #endregion

        private Joycon joycon1;
        private JoyconList joyconList1;
        private Output_VirtualController output_VirtualController1;
    }
}
