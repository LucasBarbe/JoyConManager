﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI.UIElements
{
    public partial class Output_HeadTrackingUDP : UserControl
    {
        public struct TrackSettings
        {
            public int JoyconIndex;
            public int Fusion;
            public bool InvertX;
            public bool InvertY;
            public bool InvertZ;

            public String Ip;
            public int Port;
        }

        public struct InvertAxis
        {
            public InvertAxis(bool x, bool y, bool z)
            {
                X = x;
                Y = y;
                Z = z;
            }

            public bool X;
            public bool Y;
            public bool Z;
        }

        public delegate void OnStartTracking_Delegate(TrackSettings settings);

        public delegate void OnInitialAttitudeChanged_Delegate(int side, Vector3 eulerRot);

        public event Action<TrackSettings> OnStartTrackingRequest;
        public event Action OnStopTrackingRequest;
        public event Action OnCenterRequest;
        public event Action OnSetInitialAtitudeRequest;
        public event Action OnResetRequest;
        public event Action<int> OnFusionChange;
        public event Action<InvertAxis> OnInvertAxisChange;
        public event Action<Vector3> OnDriftCorrectionChange;

        public event Action OnInitialAttitudeConfigOpened;
        public event Action<int, Vector3> OnInitialAttitudeChanged;
        public event Action<int> OnInitialAttitudeSelectedSideChanged;
        public event Action OnInitialAttitudeConfigClosed;

        private int m_joyconsCount;
        public int JoyconsCount
        {
            get => m_joyconsCount;
            set
            {
                if(m_joyconsCount != value)
                {
                    m_joyconsCount = value;
                    UpdateJoyconCount();
                }
            }
        }

        private bool m_isTracked;
        public bool IsTracked
        {
            get => m_isTracked;
            set
            {
                if(m_isTracked != value)
                {
                    m_isTracked = value;
                    UpdateTracked();
                }
            }
        }

        private Vector3 m_estimatedAttitude;
        public Vector3 EstimatedAttitude
        {
            get => m_estimatedAttitude;
            set
            {
                m_estimatedAttitude = value;

                UpdateEstimatedAttitude();
            }
        }

        public Vector3 InitialAttitude
        {
            set
            {
                numericUpDown_EditX.Value = (decimal)value.X;
                numericUpDown_EditY.Value = (decimal)value.Y;
                numericUpDown_EditZ.Value = (decimal)value.Z;
            }
        }

        public string TargetIp
        {
            get => Ip_0.Value + "." + Ip_1.Value + "." + Ip_2.Value + "." + Ip_3.Value;
            set
            {
                string[] ipSplited = value.Split('.');

                int[] ips = new int[4];

                if (ipSplited.Length != 4) return;

                for (int i = 0; i < 4; i++)
                {
                    if (!int.TryParse(ipSplited[i], out ips[i])) return;
                }

                Ip_0.Value = ips[0];
                Ip_1.Value = ips[1];
                Ip_2.Value = ips[2];
                Ip_3.Value = ips[3];
            }
        }

        public int TargetPort
        {
            get => (int)Port.Value;
            set
            {
                Port.Value = value;
            }
        }

        public InvertAxis AxisInversion
        {
            get => new InvertAxis(InvertX_checkbox.Checked, InvertY_checkbox.Checked, InvertZ_checkbox.Checked);
            set
            {
                InvertX_checkbox.Checked = value.X;
                InvertY_checkbox.Checked = value.Y;
                InvertZ_checkbox.Checked = value.Z;

                OnInvertAxisChange.Invoke(value);
            }
        }

        public bool InvertX
        {
            set
            {
                InvertX_checkbox.Checked = value;
                InvertChanged();
            }
        }

        public bool InvertY
        {
            set
            {
                InvertY_checkbox.Checked = value;
                InvertChanged();
            }
        }

        public bool InvertZ
        {
            set
            {
                Console.WriteLine(value);
                InvertZ_checkbox.Checked = value;
                InvertChanged();
            }
        }

        public void SetAxisInversion(bool x, bool y, bool z)
        {
            InvertX_checkbox.Checked = x;
            InvertY_checkbox.Checked = y;
            InvertZ_checkbox.Checked = z;
            InvertChanged();
        }
        public void SetSide(int side)
        {
            comboBox_SelectSide.SelectedIndex = side;
        }

        private bool m_attitudeConfigIsOpened = false;
        public bool AttitudeConfigIsOpened
        {
            get => m_attitudeConfigIsOpened;
            set
            {
                if(m_attitudeConfigIsOpened != value)
                {
                    m_attitudeConfigIsOpened = value;
                    UpdateConfigPanel();
                }
            }
        }

        public Output_HeadTrackingUDP()
        {
            InitializeComponent();

            FusionMode.SelectedIndex = 0;
            comboBox_SelectSide.SelectedIndex = 0;
            UpdateTracked();
            UpdateConfigPanel();
        }
        
        private void UpdateTracked()
        {
            Joycon_Index.Enabled = !m_isTracked;

            Ip_0.Enabled = !m_isTracked;
            Ip_1.Enabled = !m_isTracked;
            Ip_2.Enabled = !m_isTracked;
            Ip_3.Enabled = !m_isTracked;

            Port.Enabled = !m_isTracked;

            panel_Attitude_Ctn.Visible = m_isTracked;

            Btn_SetInitialAtitude.Enabled = !m_isTracked;

            if (m_isTracked)
            {
                AttitudeConfigIsOpened = false;
                Btn_Start_Stop.Text = "Stop";
            }   
            else
                Btn_Start_Stop.Text = "Start";

        }

        private void UpdateJoyconCount()
        {
            Joycon_Index.Maximum = m_joyconsCount - 1;

            if (m_joyconsCount < 1)
                Joycon_Index.Enabled = false;
            else
                Joycon_Index.Enabled = true;
        }

        private void UpdateEstimatedAttitude()
        {
            label_estimatedAtitude_X.Text = "X: " + m_estimatedAttitude.X;
            label_estimatedAtitude_Y.Text = "Y: " + m_estimatedAttitude.Y;
            label_estimatedAtitude_Z.Text = "Z: " + m_estimatedAttitude.Z;
        }

        private void UpdateConfigPanel()
        {
            panel_Config_Ctn.Visible = m_attitudeConfigIsOpened;

            if (m_attitudeConfigIsOpened)
            {
                if (OnInitialAttitudeConfigOpened != null)
                    OnInitialAttitudeConfigOpened();

                if (OnInitialAttitudeSelectedSideChanged != null)
                    OnInitialAttitudeSelectedSideChanged(comboBox_SelectSide.SelectedIndex);
            }
            else
            {
                if (OnInitialAttitudeConfigClosed != null)
                    OnInitialAttitudeConfigClosed();
            }
        }

        private void InvertChanged()
        {
            if (OnInvertAxisChange != null)
                OnInvertAxisChange(new InvertAxis(
                    InvertX_checkbox.Checked,
                    InvertY_checkbox.Checked,
                    InvertZ_checkbox.Checked
                    ));
        }

        private void Btn_Start_Stop_Click(object sender, EventArgs e)
        {
            //IsTracked = !IsTracked;

            if (!IsTracked)
            {
                TrackSettings settings = new TrackSettings();

                settings.Ip = Ip_0.Value + "." + Ip_1.Value + "." + Ip_2.Value + "." + Ip_3.Value;
                settings.Port = (int)Port.Value;

                settings.InvertX = InvertX_checkbox.Checked;
                settings.InvertY = InvertY_checkbox.Checked;
                settings.InvertZ = InvertZ_checkbox.Checked;

                settings.JoyconIndex = (int)Joycon_Index.Value;

                settings.Fusion = FusionMode.SelectedIndex;

                if (OnStartTrackingRequest != null)
                    OnStartTrackingRequest(settings);
            }
            else
            {
                if (OnStopTrackingRequest != null)
                    OnStopTrackingRequest();
            }

        }

        private void FusionMode_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (OnFusionChange != null)
                OnFusionChange(FusionMode.SelectedIndex);
        }

        private void InvertX_checkbox_CheckedChanged(object sender, EventArgs e)
        {
            InvertChanged();
        }

        private void InvertY_checkbox_CheckedChanged(object sender, EventArgs e)
        {
            InvertChanged();
        }

        private void InvertZ_checkbox_CheckedChanged(object sender, EventArgs e)
        {
            InvertChanged();
        }

        private void Btn_Center_Click(object sender, EventArgs e)
        {
            if (OnCenterRequest != null)
                OnCenterRequest();
        }

        private void Btn_SetInitialAtitude_Click(object sender, EventArgs e)
        {
            //if (OnSetInitialAtitudeRequest != null)
            //    OnSetInitialAtitudeRequest();

            AttitudeConfigIsOpened = !AttitudeConfigIsOpened;
        }

        private void DriftCorrectionChanged()
        {
            if (OnDriftCorrectionChange != null)
                OnDriftCorrectionChange(new Vector3((float)numeric_XDriftCorrection.Value, (float)numeric_YDriftCorrection.Value, (float)numeric_ZDriftCorrection.Value));
        }

        private void numeric_XDirftCorrection_ValueChanged(object sender, EventArgs e)
        {
            DriftCorrectionChanged();
        }

        private void numeric_YDirftCorrection_ValueChanged(object sender, EventArgs e)
        {
            DriftCorrectionChanged();
        }

        private void numeric_ZDirftCorrection_ValueChanged(object sender, EventArgs e)
        {
            DriftCorrectionChanged();
        }

        private void Btn_Reset_Click(object sender, EventArgs e)
        {
            if (OnResetRequest != null)
                OnResetRequest();
        }

        private void numericUpDown_EditX_ValueChanged(object sender, EventArgs e)
        {
            if (OnInitialAttitudeChanged != null)
                OnInitialAttitudeChanged(comboBox_SelectSide.SelectedIndex, new Vector3((double)numericUpDown_EditX.Value, (double)numericUpDown_EditY.Value, (double)numericUpDown_EditZ.Value));
        }

        private void numericUpDown_EditY_ValueChanged(object sender, EventArgs e)
        {
            if (OnInitialAttitudeChanged != null)
                OnInitialAttitudeChanged(comboBox_SelectSide.SelectedIndex, new Vector3((double)numericUpDown_EditX.Value, (double)numericUpDown_EditY.Value, (double)numericUpDown_EditZ.Value));
        }

        private void numericUpDown_EditZ_ValueChanged(object sender, EventArgs e)
        {
            if (OnInitialAttitudeChanged != null)
                OnInitialAttitudeChanged(comboBox_SelectSide.SelectedIndex, new Vector3((double)numericUpDown_EditX.Value, (double)numericUpDown_EditY.Value, (double)numericUpDown_EditZ.Value));
        }

        private void comboBox_SelectSide_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (OnInitialAttitudeSelectedSideChanged != null)
                OnInitialAttitudeSelectedSideChanged(comboBox_SelectSide.SelectedIndex);
        }
    }
}
