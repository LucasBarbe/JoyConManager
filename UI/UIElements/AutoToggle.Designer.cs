﻿namespace UI.UIElements
{
    partial class AutoToggle
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_Toggle = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // btn_Toggle
            // 
            this.btn_Toggle.BackColor = System.Drawing.Color.Transparent;
            this.btn_Toggle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_Toggle.FlatAppearance.BorderSize = 0;
            this.btn_Toggle.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(22)))), ((int)(((byte)(44)))));
            this.btn_Toggle.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(77)))));
            this.btn_Toggle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Toggle.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btn_Toggle.Image = global::UI.Properties.Resources.sync_off_white_24x24;
            this.btn_Toggle.Location = new System.Drawing.Point(0, 0);
            this.btn_Toggle.Margin = new System.Windows.Forms.Padding(0);
            this.btn_Toggle.Name = "btn_Toggle";
            this.btn_Toggle.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn_Toggle.Size = new System.Drawing.Size(34, 34);
            this.btn_Toggle.TabIndex = 0;
            this.toolTip.SetToolTip(this.btn_Toggle, "Toggle Auto Discover");
            this.btn_Toggle.UseVisualStyleBackColor = false;
            this.btn_Toggle.Click += new System.EventHandler(this.btn_Toggle_Click);
            // 
            // AutoToggle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.btn_Toggle);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "AutoToggle";
            this.Size = new System.Drawing.Size(34, 34);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_Toggle;
        private System.Windows.Forms.ToolTip toolTip;
    }
}
