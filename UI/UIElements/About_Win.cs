﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI.UIElements.About
{
    public partial class About_Win : Form
    {
        public About_Win()
        {
            InitializeComponent();

            versionValue.Text = s_version;
            dateValue.Text = s_date;
            branchValue.Text = s_branch;
            commitValue.Text = s_commit;
        }

        private static string s_version;
        public static string Version { get => s_version; set => s_version = value != null? value : "unknow"; }
        private static string s_date;
        public static string Date { get => s_date; set => s_date = value != null ? value : "unknow"; }
        private static string s_branch;
        public static string Branch { get => s_branch; set => s_branch = value != null ? value : "unknow"; }
        private static string s_commit;
        public static string Commit { get => s_commit; set => s_commit = value != null ? value : "unknow"; }

        private void button_lastRelease_Click(object sender, EventArgs e)
        {
            Process.Start("https://bit.ly/3ck3OYT");
        }

        private void gitLab_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://gitlab.com/LucasBarbe/JoyConManager");
        }

        private void releases_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://gitlab.com/LucasBarbe/JoyConManager/-/releases");
        }

        private void license_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://gitlab.com/LucasBarbe/JoyConManager/-/blob/master/LICENSE");
        }

        private void donate_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://liberapay.com/LFusion");
        }
    }
}
