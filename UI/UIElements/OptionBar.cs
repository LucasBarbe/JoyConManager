﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using UI.UIElements.About;

namespace UI.UIElements
{
    public partial class OptionBar : UserControl
    {
        public OptionBar()
        {
            InitializeComponent();
        }

        private void about_Click(object sender, EventArgs e)
        {
            About_Win about = new About_Win();

            DialogResult dialogResult = about.ShowDialog();

            if (dialogResult == DialogResult.Cancel)
                about.Dispose();
        }
    }
}
