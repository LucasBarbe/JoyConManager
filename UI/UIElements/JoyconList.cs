﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.ObjectModel;

namespace UI.UIElements
{
    public partial class JoyconList : UserControl
    {
        //private ObservableCollection<Joycon> m_joys = new ObservableCollection<Joycon>();

        //public ObservableCollection<Joycon> Joys
        //{
        //    get => m_joys;
        //}

        public event Action<int> OnCalibrationRequest;
        public event Action<int> OnDisconnectRequest;

        public JoyconList()
        {
            InitializeComponent();


        }

        public int Count
        {
            get => this.Controls.Count;
            set
            {
                if (value < 0)
                    value = 0;

                if(this.Controls.Count > value)
                {
                    int diff = this.Controls.Count - value;
                    for (int i = 0; i < diff; i++)
                    {
                        //Joycon joy = m_joys[m_joys.Count - 1];
                        this.Controls.RemoveAt(this.Controls.Count - 1);

                        //m_joys.Remove();
                    }
                }
                else if(this.Controls.Count < value)
                {
                    int diff = value - this.Controls.Count;
                    for (int i = 0; i < diff; i++)
                    {
                        Joycon joy = new Joycon();
                        joy.Location = new Point(Controls.Count * 10, 0);
                        joy.Dock = DockStyle.Top;
                        joy.OnCalibrationRequest += Joy_OnCalibrationRequest;
                        joy.OnDisconnectRequest += Joy_OnDisconnectRequest;
                        //m_joys.Add(joy);
                        this.Controls.Add(joy);
                    }
                }
            }
        }

        private void Joy_OnDisconnectRequest(Joycon obj)
        {
            int joyIndex = Controls.IndexOf(obj);
            if (joyIndex > -1)
            {
                if (OnDisconnectRequest != null)
                    OnDisconnectRequest(joyIndex);
            }
        }

        private void Joy_OnCalibrationRequest(Joycon obj)
        {
            int joyIndex = Controls.IndexOf(obj);
            if (joyIndex > -1)
            {
                if (OnCalibrationRequest != null)
                    OnCalibrationRequest(joyIndex);
            }
            
        }

        private void InitCallbacks()
        {
            //m_joys.CollectionChanged += M_joys_CollectionChanged;
        }

        private void M_joys_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            //switch (e.Action)
            //{
            //    case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
            //        for (int i = 0; i < e.NewItems.Count; i++)
            //        {
            //            Joycon joy = (Joycon)e.NewItems[i];
            //            //Add joy in table;
            //            JoyconsCtn.RowCount++;
            //            JoyconsCtn.RowStyles[e.NewStartingIndex].Height = joy.Height;
            //            JoyconsCtn.Controls.Add(joy, 0, i + e.NewStartingIndex);
                        
            //        }
            //        break;
            //    case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
            //        for (int i = 0; i < e.OldItems.Count; i++)
            //        {
            //            //JoyconsCtn.Controls.Remove
            //        }
            //        break;
            //    case System.Collections.Specialized.NotifyCollectionChangedAction.Replace:
            //        break;
            //    case System.Collections.Specialized.NotifyCollectionChangedAction.Move:
            //        break;
            //    case System.Collections.Specialized.NotifyCollectionChangedAction.Reset:
            //        break;
            //    default:
            //        break;
            //}
        }
    }
}
