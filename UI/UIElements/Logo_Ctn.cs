﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI.UIElements
{
    public partial class Logo_Ctn : UserControl
    {
        public Logo_Ctn()
        {
            InitializeComponent();

            Logo.Image = Tools.ApplyColorToBitmap((Bitmap)Logo.Image, Color.FromArgb(255/2,Color.White));
        }
    }
}
