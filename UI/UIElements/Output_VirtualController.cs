﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI.UIElements
{
    public partial class Output_VirtualController : UserControl
    {
        public Output_VirtualController()
        {
            InitializeComponent();

            //Init imgs
            m_rightPart = new Bitmap(m_topPart);
            m_rightPart.RotateFlip(RotateFlipType.Rotate90FlipNone);

            m_bottomPart = new Bitmap(m_rightPart);
            m_bottomPart.RotateFlip(RotateFlipType.Rotate90FlipNone);

            m_leftPart = new Bitmap(m_bottomPart);
            m_leftPart.RotateFlip(RotateFlipType.Rotate90FlipNone);

            m_x2 = new Bitmap(m_x1);
            m_x2.RotateFlip(RotateFlipType.Rotate90FlipNone);

            m_x3 = new Bitmap(m_x2);
            m_x3.RotateFlip(RotateFlipType.Rotate90FlipNone);

            m_x4 = new Bitmap(m_x3);
            m_x4.RotateFlip(RotateFlipType.Rotate90FlipNone);
        }

        public enum InputMode
        {
            Pro_Controller,
		    JoyCon_Combined,
		    JoyCon_Left_Vertical,
		    JoyCon_Left_Horizontal,
		    JoyCon_Right_Vertical,
		    JoyCon_Right_Horizontal,
		    None
        }

        public event Action<Output_VirtualController> OnConnectRequest;
        public event Action<Output_VirtualController> OnDisconnectRequest;

        private Color m_leftColor;
        public Color LeftColor { 
            get => m_leftColor;
            set
            {
                m_leftColor = value;
                UpdateLogo();
            }
        }
        private Color m_rightColor;
        public Color RightColor { 
            get => m_rightColor;
            set
            {
                m_rightColor = value;
                UpdateLogo();
            }
        }

        private InputMode m_mode = InputMode.None;
        public InputMode Mode { 
            get => m_mode;
            set
            {
                if(m_mode != value)
                {
                    m_mode = value;

                    if(m_mode == InputMode.None)
                        btn_Connect.Text = "Connect";
                    else
                        btn_Connect.Text = "Disconnect";

                    UpdateLogo();
                }
            }
        }

        private int m_index;
        public int Index
        {
            get => m_index;
            set
            {
                m_index = value;
                label_index.Text = "" + m_index;
            }
        }

        private int m_ledIndex;
        public int LedIndex { 
            get => m_ledIndex;
            set 
            {
                m_ledIndex = value;
                UpdateLogo();
            }
        }

        public void BeginSearch()
        {
            btn_Connect.Text = "Press L-R";
            btn_Connect.Enabled = false;
        }

        public void EndSearch()
        {
            btn_Connect.Enabled = true;

            if (m_mode == InputMode.None)
                btn_Connect.Text = "Connect";
            else
                btn_Connect.Text = "Disconnect";
        }

        private Bitmap m_topPart = new Bitmap(UI.Properties.Resources.Vxbox_TopPart);
        private Bitmap m_rightPart;
        private Bitmap m_bottomPart;
        private Bitmap m_leftPart;

        private Bitmap m_x1 = new Bitmap(UI.Properties.Resources.Vxbox_x1_outline);
        private Bitmap m_x2;
        private Bitmap m_x3;
        private Bitmap m_x4;

        private void UpdateLogo()
        {
            List<Bitmap> ctrlLayers = new List<Bitmap>();

            switch (m_mode)
            {
                case InputMode.Pro_Controller:
                    break;
                case InputMode.JoyCon_Combined:
                    ctrlLayers.Add(Tools.ApplyColorToBitmap(m_leftPart, m_leftColor));
                    ctrlLayers.Add(Tools.ApplyColorToBitmap(m_rightPart, m_rightColor));
                    break;
                case InputMode.JoyCon_Left_Vertical:
                    break;
                case InputMode.JoyCon_Left_Horizontal:
                    break;
                case InputMode.JoyCon_Right_Vertical:
                    break;
                case InputMode.JoyCon_Right_Horizontal:
                    break;
                case InputMode.None:
                    m_ledIndex = 0;
                    break;
                default:
                    break;
            }

            Color alph = Color.FromArgb(255 / 2, Color.Gray);

            switch (m_ledIndex)
            {
                case 1:
                    ctrlLayers.Add(Tools.ApplyColorToBitmap(m_x1, Color.LimeGreen));
                    ctrlLayers.Add(Tools.ApplyColorToBitmap(m_x2, alph));
                    ctrlLayers.Add(Tools.ApplyColorToBitmap(m_x3, alph));
                    ctrlLayers.Add(Tools.ApplyColorToBitmap(m_x4, alph));
                    break;
                case 2:
                    ctrlLayers.Add(Tools.ApplyColorToBitmap(m_x1, Color.LimeGreen));
                    ctrlLayers.Add(Tools.ApplyColorToBitmap(m_x2, Color.LimeGreen));
                    ctrlLayers.Add(Tools.ApplyColorToBitmap(m_x3, alph));
                    ctrlLayers.Add(Tools.ApplyColorToBitmap(m_x4, alph));
                    break;
                case 3:
                    ctrlLayers.Add(Tools.ApplyColorToBitmap(m_x1, Color.LimeGreen));
                    ctrlLayers.Add(Tools.ApplyColorToBitmap(m_x2, Color.LimeGreen));
                    ctrlLayers.Add(Tools.ApplyColorToBitmap(m_x3, Color.LimeGreen));
                    ctrlLayers.Add(Tools.ApplyColorToBitmap(m_x4, alph));
                    break;
                case 4:
                    ctrlLayers.Add(Tools.ApplyColorToBitmap(m_x1, Color.LimeGreen));
                    ctrlLayers.Add(Tools.ApplyColorToBitmap(m_x2, Color.LimeGreen));
                    ctrlLayers.Add(Tools.ApplyColorToBitmap(m_x3, Color.LimeGreen));
                    ctrlLayers.Add(Tools.ApplyColorToBitmap(m_x4, Color.LimeGreen));
                    break;

                default:
                    ctrlLayers.Add(Tools.ApplyColorToBitmap(m_x1, alph));
                    ctrlLayers.Add(Tools.ApplyColorToBitmap(m_x2, alph));
                    ctrlLayers.Add(Tools.ApplyColorToBitmap(m_x3, alph));
                    ctrlLayers.Add(Tools.ApplyColorToBitmap(m_x4, alph));
                    break;
            }

            pictureBox1.Image = Tools.ComposeImage(ctrlLayers.ToArray());

        }

        private void btn_Connect_Click(object sender, EventArgs e)
        {
            if (m_mode == InputMode.None)
            {
                if (OnConnectRequest != null)
                    OnConnectRequest(this);
            }
            else
            {
                if (OnDisconnectRequest != null)
                    OnDisconnectRequest(this);
            }
            
        }

    }
}
