﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UI
{
    /// <summary>
    /// Logique d'interaction pour DoubleUpDown.xaml
    /// </summary>
    public partial class DoubleUpDown : UserControl
    {
        public DoubleUpDown()
        {
            InitializeComponent();
            txtNum.Text = m_numValue.ToString();
        }

        private double m_numValue = 0;

        public event Action<double> OnValueChanged;

        public double NumValue
        {
            get { return m_numValue; }
            set
            {
                m_numValue = value;
                txtNum.Text = value.ToString();
            }
        }

        private void cmdUp_Click(object sender, RoutedEventArgs e)
        {
            NumValue++;
            if (OnValueChanged != null)
                OnValueChanged(m_numValue);
        }

        private void cmdDown_Click(object sender, RoutedEventArgs e)
        {
            NumValue--;
            if (OnValueChanged != null)
                OnValueChanged(m_numValue);
        }

        private void txtNum_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtNum == null)
            {
                return;
            }

            if (!double.TryParse(txtNum.Text, out m_numValue))
            {
                txtNum.Text = m_numValue.ToString();
            }
            else
            {
                if (OnValueChanged != null)
                    OnValueChanged(m_numValue);
            }
                
        }
    }
}
