﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI.UIElements
{
    public partial class AutoToggle : UserControl
    {
        public AutoToggle()
        {
            InitializeComponent();
        }

        private static Bitmap m_bitmap_on = new Bitmap(UI.Properties.Resources.sync_white_24x24);
        private static Bitmap m_bitmap_off = new Bitmap(UI.Properties.Resources.sync_off_white_24x24);

        private bool m_enabled;
        public bool IsEnabled
        {
            get => m_enabled;
            set
            {
                if (value)
                    btn_Toggle.Image = m_bitmap_on;
                else
                    btn_Toggle.Image = m_bitmap_off;
                m_enabled = value;
            }
        }

        public event Action OnToggleRequest;

        private void btn_Toggle_Click(object sender, EventArgs e)
        {
            OnToggleRequest?.Invoke();
        }
    }
}
