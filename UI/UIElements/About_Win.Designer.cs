﻿namespace UI.UIElements.About
{
    partial class About_Win
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(About_Win));
            this.label_jcm = new System.Windows.Forms.Label();
            this.picture_jcmLogo = new System.Windows.Forms.PictureBox();
            this.label_by = new System.Windows.Forms.Label();
            this.versionHead = new System.Windows.Forms.Label();
            this.versionValue = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dateHead = new System.Windows.Forms.Label();
            this.dateValue = new System.Windows.Forms.Label();
            this.commitHead = new System.Windows.Forms.Label();
            this.commitValue = new System.Windows.Forms.Label();
            this.branchHead = new System.Windows.Forms.Label();
            this.branchValue = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gitLab = new System.Windows.Forms.LinkLabel();
            this.gitLab_ico = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.releases_ico = new System.Windows.Forms.PictureBox();
            this.releases = new System.Windows.Forms.LinkLabel();
            this.license_ico = new System.Windows.Forms.PictureBox();
            this.license = new System.Windows.Forms.LinkLabel();
            this.donate_ico = new System.Windows.Forms.PictureBox();
            this.donate = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.picture_jcmLogo)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gitLab_ico)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.releases_ico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.license_ico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.donate_ico)).BeginInit();
            this.SuspendLayout();
            // 
            // label_jcm
            // 
            this.label_jcm.AutoSize = true;
            this.label_jcm.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_jcm.ForeColor = System.Drawing.Color.White;
            this.label_jcm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label_jcm.Location = new System.Drawing.Point(146, 43);
            this.label_jcm.Name = "label_jcm";
            this.label_jcm.Size = new System.Drawing.Size(330, 46);
            this.label_jcm.TabIndex = 0;
            this.label_jcm.Text = "Joycon Manager";
            // 
            // picture_jcmLogo
            // 
            this.picture_jcmLogo.Image = global::UI.Properties.Resources.JoyconManager_Icon;
            this.picture_jcmLogo.Location = new System.Drawing.Point(12, 12);
            this.picture_jcmLogo.Name = "picture_jcmLogo";
            this.picture_jcmLogo.Size = new System.Drawing.Size(128, 128);
            this.picture_jcmLogo.TabIndex = 1;
            this.picture_jcmLogo.TabStop = false;
            // 
            // label_by
            // 
            this.label_by.AutoSize = true;
            this.label_by.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_by.ForeColor = System.Drawing.Color.White;
            this.label_by.Location = new System.Drawing.Point(151, 89);
            this.label_by.Name = "label_by";
            this.label_by.Size = new System.Drawing.Size(44, 17);
            this.label_by.TabIndex = 2;
            this.label_by.Text = "by LB";
            // 
            // versionHead
            // 
            this.versionHead.AutoSize = true;
            this.versionHead.ForeColor = System.Drawing.Color.White;
            this.versionHead.Location = new System.Drawing.Point(3, 0);
            this.versionHead.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.versionHead.Name = "versionHead";
            this.versionHead.Size = new System.Drawing.Size(45, 13);
            this.versionHead.TabIndex = 3;
            this.versionHead.Text = "Version:";
            // 
            // versionValue
            // 
            this.versionValue.AutoSize = true;
            this.versionValue.ForeColor = System.Drawing.Color.White;
            this.versionValue.Location = new System.Drawing.Point(48, 0);
            this.versionValue.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.versionValue.Name = "versionValue";
            this.versionValue.Size = new System.Drawing.Size(45, 13);
            this.versionValue.TabIndex = 4;
            this.versionValue.Text = "unknow";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.84848F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85.15151F));
            this.tableLayoutPanel1.Controls.Add(this.versionHead, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.versionValue, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.dateHead, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.dateValue, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.commitHead, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.commitValue, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.branchHead, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.branchValue, 1, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 155);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(330, 77);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // dateHead
            // 
            this.dateHead.AutoSize = true;
            this.dateHead.ForeColor = System.Drawing.Color.White;
            this.dateHead.Location = new System.Drawing.Point(3, 18);
            this.dateHead.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.dateHead.Name = "dateHead";
            this.dateHead.Size = new System.Drawing.Size(33, 13);
            this.dateHead.TabIndex = 9;
            this.dateHead.Text = "Date:";
            // 
            // dateValue
            // 
            this.dateValue.AutoSize = true;
            this.dateValue.ForeColor = System.Drawing.Color.White;
            this.dateValue.Location = new System.Drawing.Point(48, 18);
            this.dateValue.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.dateValue.Name = "dateValue";
            this.dateValue.Size = new System.Drawing.Size(45, 13);
            this.dateValue.TabIndex = 8;
            this.dateValue.Text = "unknow";
            // 
            // commitHead
            // 
            this.commitHead.AutoSize = true;
            this.commitHead.ForeColor = System.Drawing.Color.White;
            this.commitHead.Location = new System.Drawing.Point(3, 56);
            this.commitHead.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.commitHead.Name = "commitHead";
            this.commitHead.Size = new System.Drawing.Size(44, 13);
            this.commitHead.TabIndex = 7;
            this.commitHead.Text = "Commit:";
            // 
            // commitValue
            // 
            this.commitValue.AutoSize = true;
            this.commitValue.ForeColor = System.Drawing.Color.White;
            this.commitValue.Location = new System.Drawing.Point(48, 56);
            this.commitValue.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.commitValue.Name = "commitValue";
            this.commitValue.Size = new System.Drawing.Size(45, 13);
            this.commitValue.TabIndex = 7;
            this.commitValue.Text = "unknow";
            // 
            // branchHead
            // 
            this.branchHead.AutoSize = true;
            this.branchHead.ForeColor = System.Drawing.Color.White;
            this.branchHead.Location = new System.Drawing.Point(3, 36);
            this.branchHead.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.branchHead.Name = "branchHead";
            this.branchHead.Size = new System.Drawing.Size(44, 13);
            this.branchHead.TabIndex = 8;
            this.branchHead.Text = "Branch:";
            // 
            // branchValue
            // 
            this.branchValue.AutoSize = true;
            this.branchValue.ForeColor = System.Drawing.Color.White;
            this.branchValue.Location = new System.Drawing.Point(48, 36);
            this.branchValue.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.branchValue.Name = "branchValue";
            this.branchValue.Size = new System.Drawing.Size(45, 13);
            this.branchValue.TabIndex = 7;
            this.branchValue.Text = "unknow";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(15, 239);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 26);
            this.label1.TabIndex = 7;
            this.label1.Text = "Joycon Manager is free software \r\nLicensed under the MIT License";
            // 
            // gitLab
            // 
            this.gitLab.AutoSize = true;
            this.gitLab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gitLab.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.gitLab.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.gitLab.Location = new System.Drawing.Point(32, 0);
            this.gitLab.Name = "gitLab";
            this.gitLab.Size = new System.Drawing.Size(85, 24);
            this.gitLab.TabIndex = 8;
            this.gitLab.TabStop = true;
            this.gitLab.Text = "GitLab";
            this.gitLab.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.gitLab.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.gitLab.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.gitLab_LinkClicked);
            // 
            // gitLab_ico
            // 
            this.gitLab_ico.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.gitLab_ico.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gitLab_ico.Image = global::UI.Properties.Resources.gitlab_white_24x24;
            this.gitLab_ico.Location = new System.Drawing.Point(0, 0);
            this.gitLab_ico.Margin = new System.Windows.Forms.Padding(0);
            this.gitLab_ico.Name = "gitLab_ico";
            this.gitLab_ico.Size = new System.Drawing.Size(29, 24);
            this.gitLab_ico.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.gitLab_ico.TabIndex = 9;
            this.gitLab_ico.TabStop = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.59016F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.40984F));
            this.tableLayoutPanel2.Controls.Add(this.gitLab_ico, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.gitLab, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.releases_ico, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.releases, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.license_ico, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.license, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.donate_ico, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.donate, 1, 3);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(364, 155);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(120, 99);
            this.tableLayoutPanel2.TabIndex = 10;
            // 
            // releases_ico
            // 
            this.releases_ico.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.releases_ico.Dock = System.Windows.Forms.DockStyle.Fill;
            this.releases_ico.Image = global::UI.Properties.Resources.download_outline_white_24x24;
            this.releases_ico.Location = new System.Drawing.Point(0, 24);
            this.releases_ico.Margin = new System.Windows.Forms.Padding(0);
            this.releases_ico.Name = "releases_ico";
            this.releases_ico.Size = new System.Drawing.Size(29, 24);
            this.releases_ico.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.releases_ico.TabIndex = 10;
            this.releases_ico.TabStop = false;
            // 
            // releases
            // 
            this.releases.AutoSize = true;
            this.releases.Dock = System.Windows.Forms.DockStyle.Fill;
            this.releases.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.releases.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.releases.Location = new System.Drawing.Point(32, 24);
            this.releases.Name = "releases";
            this.releases.Size = new System.Drawing.Size(85, 24);
            this.releases.TabIndex = 11;
            this.releases.TabStop = true;
            this.releases.Text = "Releases";
            this.releases.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.releases.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.releases.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.releases_LinkClicked);
            // 
            // license_ico
            // 
            this.license_ico.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.license_ico.Dock = System.Windows.Forms.DockStyle.Fill;
            this.license_ico.Image = global::UI.Properties.Resources.web_white_24x24;
            this.license_ico.Location = new System.Drawing.Point(0, 48);
            this.license_ico.Margin = new System.Windows.Forms.Padding(0);
            this.license_ico.Name = "license_ico";
            this.license_ico.Size = new System.Drawing.Size(29, 24);
            this.license_ico.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.license_ico.TabIndex = 12;
            this.license_ico.TabStop = false;
            // 
            // license
            // 
            this.license.AutoSize = true;
            this.license.Dock = System.Windows.Forms.DockStyle.Fill;
            this.license.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.license.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.license.Location = new System.Drawing.Point(32, 48);
            this.license.Name = "license";
            this.license.Size = new System.Drawing.Size(85, 24);
            this.license.TabIndex = 13;
            this.license.TabStop = true;
            this.license.Text = "License";
            this.license.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.license.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.license.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.license_LinkClicked);
            // 
            // donate_ico
            // 
            this.donate_ico.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.donate_ico.Dock = System.Windows.Forms.DockStyle.Fill;
            this.donate_ico.Image = global::UI.Properties.Resources.heart_outline_white_24x24;
            this.donate_ico.Location = new System.Drawing.Point(0, 72);
            this.donate_ico.Margin = new System.Windows.Forms.Padding(0);
            this.donate_ico.Name = "donate_ico";
            this.donate_ico.Size = new System.Drawing.Size(29, 27);
            this.donate_ico.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.donate_ico.TabIndex = 15;
            this.donate_ico.TabStop = false;
            // 
            // donate
            // 
            this.donate.AutoSize = true;
            this.donate.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(160)))));
            this.donate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.donate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(160)))));
            this.donate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.donate.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.donate.Location = new System.Drawing.Point(32, 72);
            this.donate.Name = "donate";
            this.donate.Size = new System.Drawing.Size(85, 27);
            this.donate.TabIndex = 14;
            this.donate.TabStop = true;
            this.donate.Text = "Donate";
            this.donate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.donate.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.donate.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.donate_LinkClicked);
            // 
            // About_Win
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(77)))));
            this.ClientSize = new System.Drawing.Size(496, 277);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.label_by);
            this.Controls.Add(this.picture_jcmLogo);
            this.Controls.Add(this.label_jcm);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "About_Win";
            this.Text = "About";
            ((System.ComponentModel.ISupportInitialize)(this.picture_jcmLogo)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gitLab_ico)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.releases_ico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.license_ico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.donate_ico)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_jcm;
        private System.Windows.Forms.PictureBox picture_jcmLogo;
        private System.Windows.Forms.Label label_by;
        private System.Windows.Forms.Label versionHead;
        private System.Windows.Forms.Label versionValue;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label branchHead;
        private System.Windows.Forms.Label branchValue;
        private System.Windows.Forms.Label commitHead;
        private System.Windows.Forms.Label commitValue;
        private System.Windows.Forms.Label dateHead;
        private System.Windows.Forms.Label dateValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel gitLab;
        private System.Windows.Forms.PictureBox gitLab_ico;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.LinkLabel donate;
        private System.Windows.Forms.PictureBox license_ico;
        private System.Windows.Forms.LinkLabel releases;
        private System.Windows.Forms.PictureBox releases_ico;
        private System.Windows.Forms.LinkLabel license;
        private System.Windows.Forms.PictureBox donate_ico;
    }
}