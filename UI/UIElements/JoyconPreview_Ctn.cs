﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media.Media3D;

namespace UI.UIElements
{
    public partial class JoyconPreview_Ctn : UserControl
    {
        public JoyconPreview_Ctn()
        {
            InitializeComponent();

            joyconPreview1.ControllerType = SwitchControllerType.JoyCon_Left;
        }

        public Quaternion JoyconRotation
        {
            set
            {
                joyconPreview1.JoyconRotation = value;
            }
        }

        public SwitchControllerType ControllerType
        {
            get => joyconPreview1.ControllerType;
            set => joyconPreview1.ControllerType = value;
        }
    }
}
