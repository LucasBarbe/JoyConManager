﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Windows.Media.Media3D;

namespace UI
{
    /// <summary>
    /// Logique d'interaction pour UserControl2.xaml
    /// </summary>
    public partial class JoyconPreview : UserControl
    {
        public JoyconPreview()
        {
            InitializeComponent();

            //Init Axis Line

            //LineX.Points = new Point3DCollection(new Point3D[] { new Point3D(0, 0, 0), new Point3D(5, 0, 0) });
            //LineY.Points = new Point3DCollection(new Point3D[] { new Point3D(0, 0, 0), new Point3D(0, 5, 0) });
            //LineZ.Points = new Point3DCollection(new Point3D[] { new Point3D(0, 0, 0), new Point3D(0, 0, 5) });

            m_rotator.Diameter = 3;
            m_rotator.Thickness = 0.1;
            m_rotator.Bind(JoyModel);

            m_rotator.SetParent(Viewport);

            m_rotator.OnRotationChanged += Rotator_OnRotationChanged;
            m_rotator.IsEnabled = false;

            Vector3D camPos = new Vector3D(-1,-1,1);
            camPos.Normalize();
            camPos = camPos * 4.5;

            Viewport.Camera.Position = (Point3D)camPos;
            Viewport.Camera.LookDirection = camPos * -1;
        }

        private void Rotator_OnRotationChanged(Quaternion rotation)
        {
            if (OnRotationChange != null)
                OnRotationChange(rotation);
        }

        public event Action<Quaternion> OnRotationChange;

        private SwitchControllerType m_controllerType = SwitchControllerType.None;
        public SwitchControllerType ControllerType
        {
            get => m_controllerType;
            set
            {
                if(m_controllerType != value)
                {
                    m_controllerType = value;

                    switch (m_controllerType)
                    {
                        case SwitchControllerType.None:
                            JoyModel.Content = null;
                            break;
                        case SwitchControllerType.JoyCon_Left:
                            JoyModel.Content = Tools.LoadEmbeddedModel("UI.Resources.Models.JoyconLeft.obj", Tools.ModelExtention.Obj);
                            break;
                        case SwitchControllerType.JoyCon_Right:
                            JoyModel.Content = Tools.LoadEmbeddedModel("UI.Resources.Models.JoyconRight.obj", Tools.ModelExtention.Obj);
                            break;
                        case SwitchControllerType.Pro_Controller:
                            JoyModel.Content = null;
                            break;
                    }
                }
                
            }
        }

        private Quaternion m_joyconRotation;
        public Quaternion JoyconRotation
        {
            get => m_joyconRotation;
            set
            {
                if(m_joyconRotation != value)
                {
                    m_joyconRotation = value;

                    m_rotator.Rotation = m_joyconRotation;
                    //Update
                }
            }
        }

        private Rotator m_rotator = new Rotator();
    }
}
