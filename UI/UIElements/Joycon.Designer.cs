﻿namespace UI.UIElements
{
    partial class Joycon
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Joycon));
            this.Gyro_Graphs_Ctn = new System.Windows.Forms.TableLayoutPanel();
            this.Gyro_Graph_Z_Ctn = new System.Windows.Forms.Panel();
            this.Gyro_Graph_Z = new System.Windows.Forms.Panel();
            this.Gyro_Graph_Y_Ctn = new System.Windows.Forms.Panel();
            this.Gyro_Graph_Y = new System.Windows.Forms.Panel();
            this.Gyro_Graph_X_Ctn = new System.Windows.Forms.Panel();
            this.Gyro_Graph_X = new System.Windows.Forms.Panel();
            this.Acc_Graphs_Ctn = new System.Windows.Forms.TableLayoutPanel();
            this.Acc_Graph_Z_Ctn = new System.Windows.Forms.Panel();
            this.Acc_Graph_Z = new System.Windows.Forms.Panel();
            this.Acc_Graph_Y_Ctn = new System.Windows.Forms.Panel();
            this.Acc_Graph_Y = new System.Windows.Forms.Panel();
            this.Acc_Graph_X_Ctn = new System.Windows.Forms.Panel();
            this.Acc_Graph_X = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Update_Rate = new System.Windows.Forms.Label();
            this.btn_calibration = new UI.UIElements.FlatButton();
            this.Panel_Joycons = new System.Windows.Forms.Panel();
            this.Btn_DisconnectRequest_Joy = new System.Windows.Forms.Button();
            this.Ctrl_Right_PlayerLedsPanel = new System.Windows.Forms.Panel();
            this.Ctrl_Right_PlayerLed1 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.Ctrl_Right_PlayerLed2 = new System.Windows.Forms.PictureBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.Ctrl_Right_PlayerLed3 = new System.Windows.Forms.PictureBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.Ctrl_Right_PlayerLed4 = new System.Windows.Forms.PictureBox();
            this.Ctrl_Left_PlayerLedsPanel = new System.Windows.Forms.Panel();
            this.Ctrl_Left_PlayerLed1 = new System.Windows.Forms.PictureBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.Ctrl_Left_PlayerLed2 = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.Ctrl_Left_PlayerLed3 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.Ctrl_Left_PlayerLed4 = new System.Windows.Forms.PictureBox();
            this.Ctrl_Right_Btn_Home = new System.Windows.Forms.PictureBox();
            this.Ctrl_Right_Btn_HomeLed = new System.Windows.Forms.PictureBox();
            this.Ctrl_Right_Btn_ZR = new System.Windows.Forms.PictureBox();
            this.Ctrl_Right_Btn_R = new System.Windows.Forms.PictureBox();
            this.Ctrl_Right_Btn_SL = new System.Windows.Forms.PictureBox();
            this.Ctrl_Right_Btn_SR = new System.Windows.Forms.PictureBox();
            this.Ctrl_Left_Btn_ZL = new System.Windows.Forms.PictureBox();
            this.Ctrl_Left_Btn_L = new System.Windows.Forms.PictureBox();
            this.Ctrl_Right_Btn_A = new System.Windows.Forms.PictureBox();
            this.Ctrl_Right_Btn_B = new System.Windows.Forms.PictureBox();
            this.Ctrl_Right_Btn_Y = new System.Windows.Forms.PictureBox();
            this.Ctrl_Right_Btn_X = new System.Windows.Forms.PictureBox();
            this.Ctrl_Right_Btn_Plus = new System.Windows.Forms.PictureBox();
            this.Ctrl_Right_Stick = new System.Windows.Forms.PictureBox();
            this.Ctrl_Battery = new System.Windows.Forms.PictureBox();
            this.Ctrl_Left_Btn_SL = new System.Windows.Forms.PictureBox();
            this.Ctrl_Left_Btn_SR = new System.Windows.Forms.PictureBox();
            this.Ctrl_Left_Btn_Down = new System.Windows.Forms.PictureBox();
            this.Ctrl_Left_Btn_Left = new System.Windows.Forms.PictureBox();
            this.Ctrl_Left_Btn_Up = new System.Windows.Forms.PictureBox();
            this.Ctrl_Left_Btn_Right = new System.Windows.Forms.PictureBox();
            this.Ctrl_Left_Btn_Screenshot = new System.Windows.Forms.PictureBox();
            this.Ctrl_Left_Btn_Minus = new System.Windows.Forms.PictureBox();
            this.Ctrl_Left_Stick = new System.Windows.Forms.PictureBox();
            this.Ctrl_Left_Body_Color = new System.Windows.Forms.PictureBox();
            this.Ctrl_Left_Side = new System.Windows.Forms.PictureBox();
            this.Ctrl_Right_Body_Color = new System.Windows.Forms.PictureBox();
            this.Ctrl_Right_Side = new System.Windows.Forms.PictureBox();
            this.Panel_ProCtrl = new System.Windows.Forms.Panel();
            this.Btn_DisconnectRequest_Pro = new System.Windows.Forms.Button();
            this.Ctrl_Pro_PlayerLedsPanel = new System.Windows.Forms.Panel();
            this.Ctrl_Pro_PlayerLed4 = new System.Windows.Forms.PictureBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.Ctrl_Pro_PlayerLed3 = new System.Windows.Forms.PictureBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.Ctrl_Pro_PlayerLed2 = new System.Windows.Forms.PictureBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.Ctrl_Pro_PlayerLed1 = new System.Windows.Forms.PictureBox();
            this.Ctrl_Pro_Btn_R = new System.Windows.Forms.PictureBox();
            this.Ctrl_Pro_Btn_ZR = new System.Windows.Forms.PictureBox();
            this.Ctrl_Pro_Btn_ZL = new System.Windows.Forms.PictureBox();
            this.Ctrl_Pro_Btn_L = new System.Windows.Forms.PictureBox();
            this.Ctrl_Pro_Btn_Minus = new System.Windows.Forms.PictureBox();
            this.Ctrl_Pro_Btn_Plus = new System.Windows.Forms.PictureBox();
            this.Ctrl_Pro_Btn_Screenshot = new System.Windows.Forms.PictureBox();
            this.Ctrl_Pro_LStick = new System.Windows.Forms.PictureBox();
            this.Ctrl_Pro_Btn_Up = new System.Windows.Forms.PictureBox();
            this.Ctrl_Pro_Btn_Left = new System.Windows.Forms.PictureBox();
            this.Ctrl_Pro_Btn_Down = new System.Windows.Forms.PictureBox();
            this.Ctrl_Pro_Btn_Right = new System.Windows.Forms.PictureBox();
            this.Ctrl_Pro_RStick = new System.Windows.Forms.PictureBox();
            this.Ctrl_Pro_Btn_Home = new System.Windows.Forms.PictureBox();
            this.Ctrl_Pro_Btn_Y = new System.Windows.Forms.PictureBox();
            this.Ctrl_Pro_Btn_X = new System.Windows.Forms.PictureBox();
            this.Ctrl_Pro_Btn_B = new System.Windows.Forms.PictureBox();
            this.Ctrl_Pro_Btn_A = new System.Windows.Forms.PictureBox();
            this.Update_Rate_Pro = new System.Windows.Forms.Label();
            this.Ctrl_Battery_Pro = new System.Windows.Forms.PictureBox();
            this.Ctrl_Pro_Base = new System.Windows.Forms.PictureBox();
            this.UpdateTimer = new System.Windows.Forms.Timer(this.components);
            this.FlashLedTimer = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Gyro_Graphs_Ctn.SuspendLayout();
            this.Gyro_Graph_Z_Ctn.SuspendLayout();
            this.Gyro_Graph_Y_Ctn.SuspendLayout();
            this.Gyro_Graph_X_Ctn.SuspendLayout();
            this.Acc_Graphs_Ctn.SuspendLayout();
            this.Acc_Graph_Z_Ctn.SuspendLayout();
            this.Acc_Graph_Y_Ctn.SuspendLayout();
            this.Acc_Graph_X_Ctn.SuspendLayout();
            this.Panel_Joycons.SuspendLayout();
            this.Ctrl_Right_PlayerLedsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_PlayerLed1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_PlayerLed2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_PlayerLed3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_PlayerLed4)).BeginInit();
            this.Ctrl_Left_PlayerLedsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_PlayerLed1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_PlayerLed2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_PlayerLed3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_PlayerLed4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Btn_Home)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Btn_HomeLed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Btn_ZR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Btn_R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Btn_SL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Btn_SR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Btn_ZL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Btn_L)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Btn_A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Btn_B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Btn_Y)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Btn_X)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Btn_Plus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Stick)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Battery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Btn_SL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Btn_SR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Btn_Down)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Btn_Left)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Btn_Up)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Btn_Right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Btn_Screenshot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Btn_Minus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Stick)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Body_Color)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Side)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Body_Color)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Side)).BeginInit();
            this.Panel_ProCtrl.SuspendLayout();
            this.Ctrl_Pro_PlayerLedsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_PlayerLed4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_PlayerLed3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_PlayerLed2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_PlayerLed1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_ZR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_ZL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_L)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_Minus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_Plus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_Screenshot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_LStick)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_Up)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_Left)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_Down)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_Right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_RStick)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_Home)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_Y)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_X)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Battery_Pro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Base)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Gyro_Graphs_Ctn
            // 
            this.Gyro_Graphs_Ctn.BackColor = System.Drawing.Color.Transparent;
            this.Gyro_Graphs_Ctn.ColumnCount = 3;
            this.Gyro_Graphs_Ctn.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.Gyro_Graphs_Ctn.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.Gyro_Graphs_Ctn.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.Gyro_Graphs_Ctn.Controls.Add(this.Gyro_Graph_Z_Ctn, 2, 0);
            this.Gyro_Graphs_Ctn.Controls.Add(this.Gyro_Graph_Y_Ctn, 1, 0);
            this.Gyro_Graphs_Ctn.Controls.Add(this.Gyro_Graph_X_Ctn, 0, 0);
            this.Gyro_Graphs_Ctn.Location = new System.Drawing.Point(70, 62);
            this.Gyro_Graphs_Ctn.Name = "Gyro_Graphs_Ctn";
            this.Gyro_Graphs_Ctn.RowCount = 1;
            this.Gyro_Graphs_Ctn.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.Gyro_Graphs_Ctn.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 77F));
            this.Gyro_Graphs_Ctn.Size = new System.Drawing.Size(24, 77);
            this.Gyro_Graphs_Ctn.TabIndex = 10;
            // 
            // Gyro_Graph_Z_Ctn
            // 
            this.Gyro_Graph_Z_Ctn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Gyro_Graph_Z_Ctn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.Gyro_Graph_Z_Ctn.Controls.Add(this.Gyro_Graph_Z);
            this.Gyro_Graph_Z_Ctn.Location = new System.Drawing.Point(19, 3);
            this.Gyro_Graph_Z_Ctn.Name = "Gyro_Graph_Z_Ctn";
            this.Gyro_Graph_Z_Ctn.Size = new System.Drawing.Size(2, 71);
            this.Gyro_Graph_Z_Ctn.TabIndex = 3;
            // 
            // Gyro_Graph_Z
            // 
            this.Gyro_Graph_Z.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Gyro_Graph_Z.BackColor = System.Drawing.Color.Blue;
            this.Gyro_Graph_Z.Location = new System.Drawing.Point(0, 0);
            this.Gyro_Graph_Z.Margin = new System.Windows.Forms.Padding(0);
            this.Gyro_Graph_Z.Name = "Gyro_Graph_Z";
            this.Gyro_Graph_Z.Size = new System.Drawing.Size(2, 32);
            this.Gyro_Graph_Z.TabIndex = 4;
            // 
            // Gyro_Graph_Y_Ctn
            // 
            this.Gyro_Graph_Y_Ctn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Gyro_Graph_Y_Ctn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))));
            this.Gyro_Graph_Y_Ctn.Controls.Add(this.Gyro_Graph_Y);
            this.Gyro_Graph_Y_Ctn.Location = new System.Drawing.Point(11, 3);
            this.Gyro_Graph_Y_Ctn.Name = "Gyro_Graph_Y_Ctn";
            this.Gyro_Graph_Y_Ctn.Size = new System.Drawing.Size(2, 71);
            this.Gyro_Graph_Y_Ctn.TabIndex = 2;
            // 
            // Gyro_Graph_Y
            // 
            this.Gyro_Graph_Y.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Gyro_Graph_Y.BackColor = System.Drawing.Color.Lime;
            this.Gyro_Graph_Y.Location = new System.Drawing.Point(0, 0);
            this.Gyro_Graph_Y.Margin = new System.Windows.Forms.Padding(0);
            this.Gyro_Graph_Y.Name = "Gyro_Graph_Y";
            this.Gyro_Graph_Y.Size = new System.Drawing.Size(2, 32);
            this.Gyro_Graph_Y.TabIndex = 3;
            // 
            // Gyro_Graph_X_Ctn
            // 
            this.Gyro_Graph_X_Ctn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Gyro_Graph_X_Ctn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Gyro_Graph_X_Ctn.Controls.Add(this.Gyro_Graph_X);
            this.Gyro_Graph_X_Ctn.Location = new System.Drawing.Point(3, 3);
            this.Gyro_Graph_X_Ctn.Name = "Gyro_Graph_X_Ctn";
            this.Gyro_Graph_X_Ctn.Size = new System.Drawing.Size(2, 71);
            this.Gyro_Graph_X_Ctn.TabIndex = 1;
            // 
            // Gyro_Graph_X
            // 
            this.Gyro_Graph_X.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Gyro_Graph_X.BackColor = System.Drawing.Color.Red;
            this.Gyro_Graph_X.Location = new System.Drawing.Point(0, 0);
            this.Gyro_Graph_X.Margin = new System.Windows.Forms.Padding(0);
            this.Gyro_Graph_X.Name = "Gyro_Graph_X";
            this.Gyro_Graph_X.Size = new System.Drawing.Size(2, 32);
            this.Gyro_Graph_X.TabIndex = 2;
            // 
            // Acc_Graphs_Ctn
            // 
            this.Acc_Graphs_Ctn.BackColor = System.Drawing.Color.Transparent;
            this.Acc_Graphs_Ctn.ColumnCount = 3;
            this.Acc_Graphs_Ctn.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.Acc_Graphs_Ctn.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.Acc_Graphs_Ctn.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.Acc_Graphs_Ctn.Controls.Add(this.Acc_Graph_Z_Ctn, 2, 0);
            this.Acc_Graphs_Ctn.Controls.Add(this.Acc_Graph_Y_Ctn, 1, 0);
            this.Acc_Graphs_Ctn.Controls.Add(this.Acc_Graph_X_Ctn, 0, 0);
            this.Acc_Graphs_Ctn.Location = new System.Drawing.Point(129, 62);
            this.Acc_Graphs_Ctn.Name = "Acc_Graphs_Ctn";
            this.Acc_Graphs_Ctn.RowCount = 1;
            this.Acc_Graphs_Ctn.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.Acc_Graphs_Ctn.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 77F));
            this.Acc_Graphs_Ctn.Size = new System.Drawing.Size(24, 77);
            this.Acc_Graphs_Ctn.TabIndex = 10;
            // 
            // Acc_Graph_Z_Ctn
            // 
            this.Acc_Graph_Z_Ctn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Acc_Graph_Z_Ctn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.Acc_Graph_Z_Ctn.Controls.Add(this.Acc_Graph_Z);
            this.Acc_Graph_Z_Ctn.Location = new System.Drawing.Point(19, 3);
            this.Acc_Graph_Z_Ctn.Name = "Acc_Graph_Z_Ctn";
            this.Acc_Graph_Z_Ctn.Size = new System.Drawing.Size(2, 71);
            this.Acc_Graph_Z_Ctn.TabIndex = 3;
            // 
            // Acc_Graph_Z
            // 
            this.Acc_Graph_Z.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Acc_Graph_Z.BackColor = System.Drawing.Color.Blue;
            this.Acc_Graph_Z.Location = new System.Drawing.Point(0, 0);
            this.Acc_Graph_Z.Margin = new System.Windows.Forms.Padding(0);
            this.Acc_Graph_Z.Name = "Acc_Graph_Z";
            this.Acc_Graph_Z.Size = new System.Drawing.Size(2, 32);
            this.Acc_Graph_Z.TabIndex = 4;
            // 
            // Acc_Graph_Y_Ctn
            // 
            this.Acc_Graph_Y_Ctn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Acc_Graph_Y_Ctn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))));
            this.Acc_Graph_Y_Ctn.Controls.Add(this.Acc_Graph_Y);
            this.Acc_Graph_Y_Ctn.Location = new System.Drawing.Point(11, 3);
            this.Acc_Graph_Y_Ctn.Name = "Acc_Graph_Y_Ctn";
            this.Acc_Graph_Y_Ctn.Size = new System.Drawing.Size(2, 71);
            this.Acc_Graph_Y_Ctn.TabIndex = 2;
            // 
            // Acc_Graph_Y
            // 
            this.Acc_Graph_Y.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Acc_Graph_Y.BackColor = System.Drawing.Color.Lime;
            this.Acc_Graph_Y.Location = new System.Drawing.Point(0, 0);
            this.Acc_Graph_Y.Margin = new System.Windows.Forms.Padding(0);
            this.Acc_Graph_Y.Name = "Acc_Graph_Y";
            this.Acc_Graph_Y.Size = new System.Drawing.Size(2, 32);
            this.Acc_Graph_Y.TabIndex = 3;
            // 
            // Acc_Graph_X_Ctn
            // 
            this.Acc_Graph_X_Ctn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Acc_Graph_X_Ctn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Acc_Graph_X_Ctn.Controls.Add(this.Acc_Graph_X);
            this.Acc_Graph_X_Ctn.Location = new System.Drawing.Point(3, 3);
            this.Acc_Graph_X_Ctn.Name = "Acc_Graph_X_Ctn";
            this.Acc_Graph_X_Ctn.Size = new System.Drawing.Size(2, 71);
            this.Acc_Graph_X_Ctn.TabIndex = 1;
            // 
            // Acc_Graph_X
            // 
            this.Acc_Graph_X.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Acc_Graph_X.BackColor = System.Drawing.Color.Red;
            this.Acc_Graph_X.Location = new System.Drawing.Point(0, 0);
            this.Acc_Graph_X.Margin = new System.Windows.Forms.Padding(0);
            this.Acc_Graph_X.Name = "Acc_Graph_X";
            this.Acc_Graph_X.Size = new System.Drawing.Size(2, 32);
            this.Acc_Graph_X.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Location = new System.Drawing.Point(58, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(107, 1);
            this.panel1.TabIndex = 12;
            // 
            // Update_Rate
            // 
            this.Update_Rate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Update_Rate.ForeColor = System.Drawing.Color.White;
            this.Update_Rate.Location = new System.Drawing.Point(73, 9);
            this.Update_Rate.Name = "Update_Rate";
            this.Update_Rate.Size = new System.Drawing.Size(78, 13);
            this.Update_Rate.TabIndex = 20;
            this.Update_Rate.Text = "FPS: 66,6";
            this.Update_Rate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_calibration
            // 
            this.btn_calibration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(44)))), ((int)(((byte)(66)))));
            this.btn_calibration.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(77)))));
            this.btn_calibration.DisabledForeColor = System.Drawing.Color.Gray;
            this.btn_calibration.FlatAppearance.BorderSize = 0;
            this.btn_calibration.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(22)))), ((int)(((byte)(44)))));
            this.btn_calibration.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(77)))));
            this.btn_calibration.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_calibration.ForeColor = System.Drawing.Color.White;
            this.btn_calibration.Location = new System.Drawing.Point(73, 27);
            this.btn_calibration.Name = "btn_calibration";
            this.btn_calibration.Size = new System.Drawing.Size(78, 23);
            this.btn_calibration.TabIndex = 21;
            this.btn_calibration.Text = "Calibrate";
            this.btn_calibration.UseVisualStyleBackColor = false;
            this.btn_calibration.Click += new System.EventHandler(this.btn_calibration_Click);
            // 
            // Panel_Joycons
            // 
            this.Panel_Joycons.Controls.Add(this.Btn_DisconnectRequest_Joy);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Right_PlayerLedsPanel);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Left_PlayerLedsPanel);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Right_Btn_Home);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Right_Btn_HomeLed);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Right_Btn_ZR);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Right_Btn_R);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Right_Btn_SL);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Right_Btn_SR);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Left_Btn_ZL);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Left_Btn_L);
            this.Panel_Joycons.Controls.Add(this.btn_calibration);
            this.Panel_Joycons.Controls.Add(this.Update_Rate);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Right_Btn_A);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Right_Btn_B);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Right_Btn_Y);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Right_Btn_X);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Right_Btn_Plus);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Right_Stick);
            this.Panel_Joycons.Controls.Add(this.panel1);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Battery);
            this.Panel_Joycons.Controls.Add(this.Gyro_Graphs_Ctn);
            this.Panel_Joycons.Controls.Add(this.Acc_Graphs_Ctn);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Left_Btn_SL);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Left_Btn_SR);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Left_Btn_Down);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Left_Btn_Left);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Left_Btn_Up);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Left_Btn_Right);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Left_Btn_Screenshot);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Left_Btn_Minus);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Left_Stick);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Left_Body_Color);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Left_Side);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Right_Body_Color);
            this.Panel_Joycons.Controls.Add(this.Ctrl_Right_Side);
            this.Panel_Joycons.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Joycons.Location = new System.Drawing.Point(10, 10);
            this.Panel_Joycons.MinimumSize = new System.Drawing.Size(223, 142);
            this.Panel_Joycons.Name = "Panel_Joycons";
            this.Panel_Joycons.Size = new System.Drawing.Size(223, 142);
            this.Panel_Joycons.TabIndex = 33;
            // 
            // Btn_DisconnectRequest_Joy
            // 
            this.Btn_DisconnectRequest_Joy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(44)))), ((int)(((byte)(66)))));
            this.Btn_DisconnectRequest_Joy.FlatAppearance.BorderSize = 0;
            this.Btn_DisconnectRequest_Joy.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(22)))), ((int)(((byte)(44)))));
            this.Btn_DisconnectRequest_Joy.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(77)))));
            this.Btn_DisconnectRequest_Joy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btn_DisconnectRequest_Joy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_DisconnectRequest_Joy.ForeColor = System.Drawing.Color.White;
            this.Btn_DisconnectRequest_Joy.Location = new System.Drawing.Point(100, 62);
            this.Btn_DisconnectRequest_Joy.Name = "Btn_DisconnectRequest_Joy";
            this.Btn_DisconnectRequest_Joy.Size = new System.Drawing.Size(23, 23);
            this.Btn_DisconnectRequest_Joy.TabIndex = 38;
            this.Btn_DisconnectRequest_Joy.Text = "O";
            this.Btn_DisconnectRequest_Joy.UseVisualStyleBackColor = false;
            this.Btn_DisconnectRequest_Joy.Click += new System.EventHandler(this.Btn_DisconnectRequest_Click);
            // 
            // Ctrl_Right_PlayerLedsPanel
            // 
            this.Ctrl_Right_PlayerLedsPanel.Controls.Add(this.Ctrl_Right_PlayerLed1);
            this.Ctrl_Right_PlayerLedsPanel.Controls.Add(this.panel6);
            this.Ctrl_Right_PlayerLedsPanel.Controls.Add(this.Ctrl_Right_PlayerLed2);
            this.Ctrl_Right_PlayerLedsPanel.Controls.Add(this.panel7);
            this.Ctrl_Right_PlayerLedsPanel.Controls.Add(this.Ctrl_Right_PlayerLed3);
            this.Ctrl_Right_PlayerLedsPanel.Controls.Add(this.panel8);
            this.Ctrl_Right_PlayerLedsPanel.Controls.Add(this.Ctrl_Right_PlayerLed4);
            this.Ctrl_Right_PlayerLedsPanel.Location = new System.Drawing.Point(161, 52);
            this.Ctrl_Right_PlayerLedsPanel.Name = "Ctrl_Right_PlayerLedsPanel";
            this.Ctrl_Right_PlayerLedsPanel.Size = new System.Drawing.Size(6, 59);
            this.Ctrl_Right_PlayerLedsPanel.TabIndex = 37;
            // 
            // Ctrl_Right_PlayerLed1
            // 
            this.Ctrl_Right_PlayerLed1.Dock = System.Windows.Forms.DockStyle.Top;
            this.Ctrl_Right_PlayerLed1.Image = global::UI.Properties.Resources.Nintendo_Switch_Joycon__PlayerLed;
            this.Ctrl_Right_PlayerLed1.Location = new System.Drawing.Point(0, 27);
            this.Ctrl_Right_PlayerLed1.Name = "Ctrl_Right_PlayerLed1";
            this.Ctrl_Right_PlayerLed1.Size = new System.Drawing.Size(6, 6);
            this.Ctrl_Right_PlayerLed1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Right_PlayerLed1.TabIndex = 41;
            this.Ctrl_Right_PlayerLed1.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 24);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(6, 3);
            this.panel6.TabIndex = 40;
            // 
            // Ctrl_Right_PlayerLed2
            // 
            this.Ctrl_Right_PlayerLed2.Dock = System.Windows.Forms.DockStyle.Top;
            this.Ctrl_Right_PlayerLed2.Image = global::UI.Properties.Resources.Nintendo_Switch_Joycon__PlayerLed;
            this.Ctrl_Right_PlayerLed2.Location = new System.Drawing.Point(0, 18);
            this.Ctrl_Right_PlayerLed2.Name = "Ctrl_Right_PlayerLed2";
            this.Ctrl_Right_PlayerLed2.Size = new System.Drawing.Size(6, 6);
            this.Ctrl_Right_PlayerLed2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Right_PlayerLed2.TabIndex = 39;
            this.Ctrl_Right_PlayerLed2.TabStop = false;
            // 
            // panel7
            // 
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 15);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(6, 3);
            this.panel7.TabIndex = 38;
            // 
            // Ctrl_Right_PlayerLed3
            // 
            this.Ctrl_Right_PlayerLed3.Dock = System.Windows.Forms.DockStyle.Top;
            this.Ctrl_Right_PlayerLed3.Image = global::UI.Properties.Resources.Nintendo_Switch_Joycon__PlayerLed;
            this.Ctrl_Right_PlayerLed3.Location = new System.Drawing.Point(0, 9);
            this.Ctrl_Right_PlayerLed3.Name = "Ctrl_Right_PlayerLed3";
            this.Ctrl_Right_PlayerLed3.Size = new System.Drawing.Size(6, 6);
            this.Ctrl_Right_PlayerLed3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Right_PlayerLed3.TabIndex = 37;
            this.Ctrl_Right_PlayerLed3.TabStop = false;
            // 
            // panel8
            // 
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(0, 6);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(6, 3);
            this.panel8.TabIndex = 36;
            // 
            // Ctrl_Right_PlayerLed4
            // 
            this.Ctrl_Right_PlayerLed4.Dock = System.Windows.Forms.DockStyle.Top;
            this.Ctrl_Right_PlayerLed4.Image = global::UI.Properties.Resources.Nintendo_Switch_Joycon__PlayerLed;
            this.Ctrl_Right_PlayerLed4.Location = new System.Drawing.Point(0, 0);
            this.Ctrl_Right_PlayerLed4.Name = "Ctrl_Right_PlayerLed4";
            this.Ctrl_Right_PlayerLed4.Size = new System.Drawing.Size(6, 6);
            this.Ctrl_Right_PlayerLed4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Right_PlayerLed4.TabIndex = 35;
            this.Ctrl_Right_PlayerLed4.TabStop = false;
            // 
            // Ctrl_Left_PlayerLedsPanel
            // 
            this.Ctrl_Left_PlayerLedsPanel.Controls.Add(this.Ctrl_Left_PlayerLed1);
            this.Ctrl_Left_PlayerLedsPanel.Controls.Add(this.panel5);
            this.Ctrl_Left_PlayerLedsPanel.Controls.Add(this.Ctrl_Left_PlayerLed2);
            this.Ctrl_Left_PlayerLedsPanel.Controls.Add(this.panel4);
            this.Ctrl_Left_PlayerLedsPanel.Controls.Add(this.Ctrl_Left_PlayerLed3);
            this.Ctrl_Left_PlayerLedsPanel.Controls.Add(this.panel3);
            this.Ctrl_Left_PlayerLedsPanel.Controls.Add(this.Ctrl_Left_PlayerLed4);
            this.Ctrl_Left_PlayerLedsPanel.Location = new System.Drawing.Point(59, 52);
            this.Ctrl_Left_PlayerLedsPanel.Name = "Ctrl_Left_PlayerLedsPanel";
            this.Ctrl_Left_PlayerLedsPanel.Size = new System.Drawing.Size(6, 59);
            this.Ctrl_Left_PlayerLedsPanel.TabIndex = 36;
            // 
            // Ctrl_Left_PlayerLed1
            // 
            this.Ctrl_Left_PlayerLed1.Dock = System.Windows.Forms.DockStyle.Top;
            this.Ctrl_Left_PlayerLed1.Image = global::UI.Properties.Resources.Nintendo_Switch_Joycon__PlayerLed;
            this.Ctrl_Left_PlayerLed1.Location = new System.Drawing.Point(0, 27);
            this.Ctrl_Left_PlayerLed1.Name = "Ctrl_Left_PlayerLed1";
            this.Ctrl_Left_PlayerLed1.Size = new System.Drawing.Size(6, 6);
            this.Ctrl_Left_PlayerLed1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Left_PlayerLed1.TabIndex = 41;
            this.Ctrl_Left_PlayerLed1.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 24);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(6, 3);
            this.panel5.TabIndex = 40;
            // 
            // Ctrl_Left_PlayerLed2
            // 
            this.Ctrl_Left_PlayerLed2.Dock = System.Windows.Forms.DockStyle.Top;
            this.Ctrl_Left_PlayerLed2.Image = global::UI.Properties.Resources.Nintendo_Switch_Joycon__PlayerLed;
            this.Ctrl_Left_PlayerLed2.Location = new System.Drawing.Point(0, 18);
            this.Ctrl_Left_PlayerLed2.Name = "Ctrl_Left_PlayerLed2";
            this.Ctrl_Left_PlayerLed2.Size = new System.Drawing.Size(6, 6);
            this.Ctrl_Left_PlayerLed2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Left_PlayerLed2.TabIndex = 39;
            this.Ctrl_Left_PlayerLed2.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 15);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(6, 3);
            this.panel4.TabIndex = 38;
            // 
            // Ctrl_Left_PlayerLed3
            // 
            this.Ctrl_Left_PlayerLed3.Dock = System.Windows.Forms.DockStyle.Top;
            this.Ctrl_Left_PlayerLed3.Image = global::UI.Properties.Resources.Nintendo_Switch_Joycon__PlayerLed;
            this.Ctrl_Left_PlayerLed3.Location = new System.Drawing.Point(0, 9);
            this.Ctrl_Left_PlayerLed3.Name = "Ctrl_Left_PlayerLed3";
            this.Ctrl_Left_PlayerLed3.Size = new System.Drawing.Size(6, 6);
            this.Ctrl_Left_PlayerLed3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Left_PlayerLed3.TabIndex = 37;
            this.Ctrl_Left_PlayerLed3.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(6, 3);
            this.panel3.TabIndex = 36;
            // 
            // Ctrl_Left_PlayerLed4
            // 
            this.Ctrl_Left_PlayerLed4.Dock = System.Windows.Forms.DockStyle.Top;
            this.Ctrl_Left_PlayerLed4.Image = global::UI.Properties.Resources.Nintendo_Switch_Joycon__PlayerLed;
            this.Ctrl_Left_PlayerLed4.Location = new System.Drawing.Point(0, 0);
            this.Ctrl_Left_PlayerLed4.Name = "Ctrl_Left_PlayerLed4";
            this.Ctrl_Left_PlayerLed4.Size = new System.Drawing.Size(6, 6);
            this.Ctrl_Left_PlayerLed4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Left_PlayerLed4.TabIndex = 35;
            this.Ctrl_Left_PlayerLed4.TabStop = false;
            // 
            // Ctrl_Right_Btn_Home
            // 
            this.Ctrl_Right_Btn_Home.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Right_Btn_Home.Image")));
            this.Ctrl_Right_Btn_Home.Location = new System.Drawing.Point(180, 94);
            this.Ctrl_Right_Btn_Home.Name = "Ctrl_Right_Btn_Home";
            this.Ctrl_Right_Btn_Home.Size = new System.Drawing.Size(20, 20);
            this.Ctrl_Right_Btn_Home.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Right_Btn_Home.TabIndex = 31;
            this.Ctrl_Right_Btn_Home.TabStop = false;
            // 
            // Ctrl_Right_Btn_HomeLed
            // 
            this.Ctrl_Right_Btn_HomeLed.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Right_Btn_HomeLed.Image")));
            this.Ctrl_Right_Btn_HomeLed.Location = new System.Drawing.Point(180, 94);
            this.Ctrl_Right_Btn_HomeLed.Name = "Ctrl_Right_Btn_HomeLed";
            this.Ctrl_Right_Btn_HomeLed.Size = new System.Drawing.Size(20, 20);
            this.Ctrl_Right_Btn_HomeLed.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Right_Btn_HomeLed.TabIndex = 30;
            this.Ctrl_Right_Btn_HomeLed.TabStop = false;
            // 
            // Ctrl_Right_Btn_ZR
            // 
            this.Ctrl_Right_Btn_ZR.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Right_Btn_ZR.Image")));
            this.Ctrl_Right_Btn_ZR.Location = new System.Drawing.Point(204, 0);
            this.Ctrl_Right_Btn_ZR.Name = "Ctrl_Right_Btn_ZR";
            this.Ctrl_Right_Btn_ZR.Size = new System.Drawing.Size(19, 16);
            this.Ctrl_Right_Btn_ZR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Right_Btn_ZR.TabIndex = 29;
            this.Ctrl_Right_Btn_ZR.TabStop = false;
            // 
            // Ctrl_Right_Btn_R
            // 
            this.Ctrl_Right_Btn_R.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Right_Btn_R.Image")));
            this.Ctrl_Right_Btn_R.Location = new System.Drawing.Point(180, 0);
            this.Ctrl_Right_Btn_R.Name = "Ctrl_Right_Btn_R";
            this.Ctrl_Right_Btn_R.Size = new System.Drawing.Size(43, 23);
            this.Ctrl_Right_Btn_R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Right_Btn_R.TabIndex = 28;
            this.Ctrl_Right_Btn_R.TabStop = false;
            // 
            // Ctrl_Right_Btn_SL
            // 
            this.Ctrl_Right_Btn_SL.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Right_Btn_SL.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Right_Btn_SL.Image")));
            this.Ctrl_Right_Btn_SL.Location = new System.Drawing.Point(172, 89);
            this.Ctrl_Right_Btn_SL.Name = "Ctrl_Right_Btn_SL";
            this.Ctrl_Right_Btn_SL.Size = new System.Drawing.Size(3, 15);
            this.Ctrl_Right_Btn_SL.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Right_Btn_SL.TabIndex = 27;
            this.Ctrl_Right_Btn_SL.TabStop = false;
            // 
            // Ctrl_Right_Btn_SR
            // 
            this.Ctrl_Right_Btn_SR.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Right_Btn_SR.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Right_Btn_SR.Image")));
            this.Ctrl_Right_Btn_SR.Location = new System.Drawing.Point(172, 32);
            this.Ctrl_Right_Btn_SR.Name = "Ctrl_Right_Btn_SR";
            this.Ctrl_Right_Btn_SR.Size = new System.Drawing.Size(3, 15);
            this.Ctrl_Right_Btn_SR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Right_Btn_SR.TabIndex = 26;
            this.Ctrl_Right_Btn_SR.TabStop = false;
            // 
            // Ctrl_Left_Btn_ZL
            // 
            this.Ctrl_Left_Btn_ZL.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Left_Btn_ZL.Image")));
            this.Ctrl_Left_Btn_ZL.Location = new System.Drawing.Point(0, 0);
            this.Ctrl_Left_Btn_ZL.Name = "Ctrl_Left_Btn_ZL";
            this.Ctrl_Left_Btn_ZL.Size = new System.Drawing.Size(19, 16);
            this.Ctrl_Left_Btn_ZL.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Left_Btn_ZL.TabIndex = 25;
            this.Ctrl_Left_Btn_ZL.TabStop = false;
            // 
            // Ctrl_Left_Btn_L
            // 
            this.Ctrl_Left_Btn_L.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Left_Btn_L.Image")));
            this.Ctrl_Left_Btn_L.Location = new System.Drawing.Point(0, 0);
            this.Ctrl_Left_Btn_L.Name = "Ctrl_Left_Btn_L";
            this.Ctrl_Left_Btn_L.Size = new System.Drawing.Size(43, 23);
            this.Ctrl_Left_Btn_L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Left_Btn_L.TabIndex = 24;
            this.Ctrl_Left_Btn_L.TabStop = false;
            // 
            // Ctrl_Right_Btn_A
            // 
            this.Ctrl_Right_Btn_A.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Right_Btn_A.Image")));
            this.Ctrl_Right_Btn_A.Location = new System.Drawing.Point(203, 31);
            this.Ctrl_Right_Btn_A.Name = "Ctrl_Right_Btn_A";
            this.Ctrl_Right_Btn_A.Size = new System.Drawing.Size(15, 15);
            this.Ctrl_Right_Btn_A.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Right_Btn_A.TabIndex = 19;
            this.Ctrl_Right_Btn_A.TabStop = false;
            // 
            // Ctrl_Right_Btn_B
            // 
            this.Ctrl_Right_Btn_B.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Right_Btn_B.Image")));
            this.Ctrl_Right_Btn_B.Location = new System.Drawing.Point(191, 42);
            this.Ctrl_Right_Btn_B.Name = "Ctrl_Right_Btn_B";
            this.Ctrl_Right_Btn_B.Size = new System.Drawing.Size(15, 15);
            this.Ctrl_Right_Btn_B.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Right_Btn_B.TabIndex = 18;
            this.Ctrl_Right_Btn_B.TabStop = false;
            // 
            // Ctrl_Right_Btn_Y
            // 
            this.Ctrl_Right_Btn_Y.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Right_Btn_Y.Image")));
            this.Ctrl_Right_Btn_Y.Location = new System.Drawing.Point(180, 31);
            this.Ctrl_Right_Btn_Y.Name = "Ctrl_Right_Btn_Y";
            this.Ctrl_Right_Btn_Y.Size = new System.Drawing.Size(15, 15);
            this.Ctrl_Right_Btn_Y.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Right_Btn_Y.TabIndex = 17;
            this.Ctrl_Right_Btn_Y.TabStop = false;
            // 
            // Ctrl_Right_Btn_X
            // 
            this.Ctrl_Right_Btn_X.Image = global::UI.Properties.Resources.Nintendo_Switch_Joycon__Btn_Base;
            this.Ctrl_Right_Btn_X.Location = new System.Drawing.Point(191, 20);
            this.Ctrl_Right_Btn_X.Name = "Ctrl_Right_Btn_X";
            this.Ctrl_Right_Btn_X.Size = new System.Drawing.Size(15, 15);
            this.Ctrl_Right_Btn_X.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Right_Btn_X.TabIndex = 16;
            this.Ctrl_Right_Btn_X.TabStop = false;
            // 
            // Ctrl_Right_Btn_Plus
            // 
            this.Ctrl_Right_Btn_Plus.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Right_Btn_Plus.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Right_Btn_Plus.Image")));
            this.Ctrl_Right_Btn_Plus.Location = new System.Drawing.Point(179, 11);
            this.Ctrl_Right_Btn_Plus.Name = "Ctrl_Right_Btn_Plus";
            this.Ctrl_Right_Btn_Plus.Size = new System.Drawing.Size(10, 10);
            this.Ctrl_Right_Btn_Plus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Right_Btn_Plus.TabIndex = 15;
            this.Ctrl_Right_Btn_Plus.TabStop = false;
            // 
            // Ctrl_Right_Stick
            // 
            this.Ctrl_Right_Stick.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Right_Stick.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Right_Stick.Image")));
            this.Ctrl_Right_Stick.Location = new System.Drawing.Point(183, 63);
            this.Ctrl_Right_Stick.Name = "Ctrl_Right_Stick";
            this.Ctrl_Right_Stick.Size = new System.Drawing.Size(27, 27);
            this.Ctrl_Right_Stick.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Right_Stick.TabIndex = 13;
            this.Ctrl_Right_Stick.TabStop = false;
            // 
            // Ctrl_Battery
            // 
            this.Ctrl_Battery.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Battery.Location = new System.Drawing.Point(100, 95);
            this.Ctrl_Battery.Name = "Ctrl_Battery";
            this.Ctrl_Battery.Size = new System.Drawing.Size(23, 41);
            this.Ctrl_Battery.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Battery.TabIndex = 11;
            this.Ctrl_Battery.TabStop = false;
            // 
            // Ctrl_Left_Btn_SL
            // 
            this.Ctrl_Left_Btn_SL.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Left_Btn_SL.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Left_Btn_SL.Image")));
            this.Ctrl_Left_Btn_SL.Location = new System.Drawing.Point(49, 32);
            this.Ctrl_Left_Btn_SL.Name = "Ctrl_Left_Btn_SL";
            this.Ctrl_Left_Btn_SL.Size = new System.Drawing.Size(3, 15);
            this.Ctrl_Left_Btn_SL.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Left_Btn_SL.TabIndex = 9;
            this.Ctrl_Left_Btn_SL.TabStop = false;
            // 
            // Ctrl_Left_Btn_SR
            // 
            this.Ctrl_Left_Btn_SR.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Left_Btn_SR.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Left_Btn_SR.Image")));
            this.Ctrl_Left_Btn_SR.Location = new System.Drawing.Point(49, 89);
            this.Ctrl_Left_Btn_SR.Name = "Ctrl_Left_Btn_SR";
            this.Ctrl_Left_Btn_SR.Size = new System.Drawing.Size(3, 15);
            this.Ctrl_Left_Btn_SR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Left_Btn_SR.TabIndex = 8;
            this.Ctrl_Left_Btn_SR.TabStop = false;
            // 
            // Ctrl_Left_Btn_Down
            // 
            this.Ctrl_Left_Btn_Down.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Left_Btn_Down.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Left_Btn_Down.Image")));
            this.Ctrl_Left_Btn_Down.Location = new System.Drawing.Point(18, 82);
            this.Ctrl_Left_Btn_Down.Name = "Ctrl_Left_Btn_Down";
            this.Ctrl_Left_Btn_Down.Size = new System.Drawing.Size(13, 13);
            this.Ctrl_Left_Btn_Down.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Left_Btn_Down.TabIndex = 7;
            this.Ctrl_Left_Btn_Down.TabStop = false;
            // 
            // Ctrl_Left_Btn_Left
            // 
            this.Ctrl_Left_Btn_Left.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Left_Btn_Left.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Left_Btn_Left.Image")));
            this.Ctrl_Left_Btn_Left.Location = new System.Drawing.Point(7, 71);
            this.Ctrl_Left_Btn_Left.Name = "Ctrl_Left_Btn_Left";
            this.Ctrl_Left_Btn_Left.Size = new System.Drawing.Size(13, 13);
            this.Ctrl_Left_Btn_Left.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Left_Btn_Left.TabIndex = 6;
            this.Ctrl_Left_Btn_Left.TabStop = false;
            // 
            // Ctrl_Left_Btn_Up
            // 
            this.Ctrl_Left_Btn_Up.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Left_Btn_Up.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Left_Btn_Up.Image")));
            this.Ctrl_Left_Btn_Up.Location = new System.Drawing.Point(18, 61);
            this.Ctrl_Left_Btn_Up.Name = "Ctrl_Left_Btn_Up";
            this.Ctrl_Left_Btn_Up.Size = new System.Drawing.Size(13, 13);
            this.Ctrl_Left_Btn_Up.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Left_Btn_Up.TabIndex = 5;
            this.Ctrl_Left_Btn_Up.TabStop = false;
            // 
            // Ctrl_Left_Btn_Right
            // 
            this.Ctrl_Left_Btn_Right.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Left_Btn_Right.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Left_Btn_Right.Image")));
            this.Ctrl_Left_Btn_Right.Location = new System.Drawing.Point(28, 71);
            this.Ctrl_Left_Btn_Right.Name = "Ctrl_Left_Btn_Right";
            this.Ctrl_Left_Btn_Right.Size = new System.Drawing.Size(13, 13);
            this.Ctrl_Left_Btn_Right.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Left_Btn_Right.TabIndex = 4;
            this.Ctrl_Left_Btn_Right.TabStop = false;
            // 
            // Ctrl_Left_Btn_Screenshot
            // 
            this.Ctrl_Left_Btn_Screenshot.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Left_Btn_Screenshot.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Left_Btn_Screenshot.Image")));
            this.Ctrl_Left_Btn_Screenshot.Location = new System.Drawing.Point(27, 99);
            this.Ctrl_Left_Btn_Screenshot.Name = "Ctrl_Left_Btn_Screenshot";
            this.Ctrl_Left_Btn_Screenshot.Size = new System.Drawing.Size(12, 12);
            this.Ctrl_Left_Btn_Screenshot.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Left_Btn_Screenshot.TabIndex = 3;
            this.Ctrl_Left_Btn_Screenshot.TabStop = false;
            // 
            // Ctrl_Left_Btn_Minus
            // 
            this.Ctrl_Left_Btn_Minus.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Left_Btn_Minus.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Left_Btn_Minus.Image")));
            this.Ctrl_Left_Btn_Minus.Location = new System.Drawing.Point(34, 11);
            this.Ctrl_Left_Btn_Minus.Name = "Ctrl_Left_Btn_Minus";
            this.Ctrl_Left_Btn_Minus.Size = new System.Drawing.Size(10, 10);
            this.Ctrl_Left_Btn_Minus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Left_Btn_Minus.TabIndex = 2;
            this.Ctrl_Left_Btn_Minus.TabStop = false;
            // 
            // Ctrl_Left_Stick
            // 
            this.Ctrl_Left_Stick.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Left_Stick.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Left_Stick.Image")));
            this.Ctrl_Left_Stick.Location = new System.Drawing.Point(11, 25);
            this.Ctrl_Left_Stick.Name = "Ctrl_Left_Stick";
            this.Ctrl_Left_Stick.Size = new System.Drawing.Size(27, 27);
            this.Ctrl_Left_Stick.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Left_Stick.TabIndex = 1;
            this.Ctrl_Left_Stick.TabStop = false;
            // 
            // Ctrl_Left_Body_Color
            // 
            this.Ctrl_Left_Body_Color.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Left_Body_Color.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Left_Body_Color.Image")));
            this.Ctrl_Left_Body_Color.Location = new System.Drawing.Point(0, 0);
            this.Ctrl_Left_Body_Color.Name = "Ctrl_Left_Body_Color";
            this.Ctrl_Left_Body_Color.Size = new System.Drawing.Size(51, 142);
            this.Ctrl_Left_Body_Color.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Left_Body_Color.TabIndex = 0;
            this.Ctrl_Left_Body_Color.TabStop = false;
            // 
            // Ctrl_Left_Side
            // 
            this.Ctrl_Left_Side.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Left_Side.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Left_Side.Image")));
            this.Ctrl_Left_Side.Location = new System.Drawing.Point(0, 0);
            this.Ctrl_Left_Side.Name = "Ctrl_Left_Side";
            this.Ctrl_Left_Side.Size = new System.Drawing.Size(51, 142);
            this.Ctrl_Left_Side.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Left_Side.TabIndex = 22;
            this.Ctrl_Left_Side.TabStop = false;
            // 
            // Ctrl_Right_Body_Color
            // 
            this.Ctrl_Right_Body_Color.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Right_Body_Color.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Right_Body_Color.Image")));
            this.Ctrl_Right_Body_Color.Location = new System.Drawing.Point(172, 0);
            this.Ctrl_Right_Body_Color.Name = "Ctrl_Right_Body_Color";
            this.Ctrl_Right_Body_Color.Size = new System.Drawing.Size(51, 142);
            this.Ctrl_Right_Body_Color.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Right_Body_Color.TabIndex = 14;
            this.Ctrl_Right_Body_Color.TabStop = false;
            // 
            // Ctrl_Right_Side
            // 
            this.Ctrl_Right_Side.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Right_Side.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Right_Side.Image")));
            this.Ctrl_Right_Side.Location = new System.Drawing.Point(172, 0);
            this.Ctrl_Right_Side.Name = "Ctrl_Right_Side";
            this.Ctrl_Right_Side.Size = new System.Drawing.Size(51, 142);
            this.Ctrl_Right_Side.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Right_Side.TabIndex = 23;
            this.Ctrl_Right_Side.TabStop = false;
            // 
            // Panel_ProCtrl
            // 
            this.Panel_ProCtrl.Controls.Add(this.Btn_DisconnectRequest_Pro);
            this.Panel_ProCtrl.Controls.Add(this.Ctrl_Pro_PlayerLedsPanel);
            this.Panel_ProCtrl.Controls.Add(this.Ctrl_Pro_Btn_R);
            this.Panel_ProCtrl.Controls.Add(this.Ctrl_Pro_Btn_ZR);
            this.Panel_ProCtrl.Controls.Add(this.Ctrl_Pro_Btn_ZL);
            this.Panel_ProCtrl.Controls.Add(this.Ctrl_Pro_Btn_L);
            this.Panel_ProCtrl.Controls.Add(this.Ctrl_Pro_Btn_Minus);
            this.Panel_ProCtrl.Controls.Add(this.Ctrl_Pro_Btn_Plus);
            this.Panel_ProCtrl.Controls.Add(this.Ctrl_Pro_Btn_Screenshot);
            this.Panel_ProCtrl.Controls.Add(this.Ctrl_Pro_LStick);
            this.Panel_ProCtrl.Controls.Add(this.Ctrl_Pro_Btn_Up);
            this.Panel_ProCtrl.Controls.Add(this.Ctrl_Pro_Btn_Left);
            this.Panel_ProCtrl.Controls.Add(this.Ctrl_Pro_Btn_Down);
            this.Panel_ProCtrl.Controls.Add(this.Ctrl_Pro_Btn_Right);
            this.Panel_ProCtrl.Controls.Add(this.Ctrl_Pro_RStick);
            this.Panel_ProCtrl.Controls.Add(this.Ctrl_Pro_Btn_Home);
            this.Panel_ProCtrl.Controls.Add(this.Ctrl_Pro_Btn_Y);
            this.Panel_ProCtrl.Controls.Add(this.Ctrl_Pro_Btn_X);
            this.Panel_ProCtrl.Controls.Add(this.Ctrl_Pro_Btn_B);
            this.Panel_ProCtrl.Controls.Add(this.Ctrl_Pro_Btn_A);
            this.Panel_ProCtrl.Controls.Add(this.Update_Rate_Pro);
            this.Panel_ProCtrl.Controls.Add(this.Ctrl_Battery_Pro);
            this.Panel_ProCtrl.Controls.Add(this.Ctrl_Pro_Base);
            this.Panel_ProCtrl.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_ProCtrl.Location = new System.Drawing.Point(10, 152);
            this.Panel_ProCtrl.MinimumSize = new System.Drawing.Size(223, 142);
            this.Panel_ProCtrl.Name = "Panel_ProCtrl";
            this.Panel_ProCtrl.Size = new System.Drawing.Size(223, 142);
            this.Panel_ProCtrl.TabIndex = 34;
            // 
            // Btn_DisconnectRequest_Pro
            // 
            this.Btn_DisconnectRequest_Pro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(44)))), ((int)(((byte)(66)))));
            this.Btn_DisconnectRequest_Pro.FlatAppearance.BorderSize = 0;
            this.Btn_DisconnectRequest_Pro.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(22)))), ((int)(((byte)(44)))));
            this.Btn_DisconnectRequest_Pro.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(77)))));
            this.Btn_DisconnectRequest_Pro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btn_DisconnectRequest_Pro.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_DisconnectRequest_Pro.ForeColor = System.Drawing.Color.White;
            this.Btn_DisconnectRequest_Pro.Location = new System.Drawing.Point(135, 110);
            this.Btn_DisconnectRequest_Pro.Name = "Btn_DisconnectRequest_Pro";
            this.Btn_DisconnectRequest_Pro.Size = new System.Drawing.Size(23, 23);
            this.Btn_DisconnectRequest_Pro.TabIndex = 39;
            this.Btn_DisconnectRequest_Pro.Text = "O";
            this.Btn_DisconnectRequest_Pro.UseVisualStyleBackColor = false;
            this.Btn_DisconnectRequest_Pro.Click += new System.EventHandler(this.Btn_DisconnectRequest_Click);
            // 
            // Ctrl_Pro_PlayerLedsPanel
            // 
            this.Ctrl_Pro_PlayerLedsPanel.Controls.Add(this.Ctrl_Pro_PlayerLed4);
            this.Ctrl_Pro_PlayerLedsPanel.Controls.Add(this.panel9);
            this.Ctrl_Pro_PlayerLedsPanel.Controls.Add(this.Ctrl_Pro_PlayerLed3);
            this.Ctrl_Pro_PlayerLedsPanel.Controls.Add(this.panel10);
            this.Ctrl_Pro_PlayerLedsPanel.Controls.Add(this.Ctrl_Pro_PlayerLed2);
            this.Ctrl_Pro_PlayerLedsPanel.Controls.Add(this.panel11);
            this.Ctrl_Pro_PlayerLedsPanel.Controls.Add(this.Ctrl_Pro_PlayerLed1);
            this.Ctrl_Pro_PlayerLedsPanel.Location = new System.Drawing.Point(95, 91);
            this.Ctrl_Pro_PlayerLedsPanel.Name = "Ctrl_Pro_PlayerLedsPanel";
            this.Ctrl_Pro_PlayerLedsPanel.Size = new System.Drawing.Size(60, 6);
            this.Ctrl_Pro_PlayerLedsPanel.TabIndex = 44;
            // 
            // Ctrl_Pro_PlayerLed4
            // 
            this.Ctrl_Pro_PlayerLed4.Dock = System.Windows.Forms.DockStyle.Left;
            this.Ctrl_Pro_PlayerLed4.Image = global::UI.Properties.Resources.Nintendo_Switch_Joycon__PlayerLed;
            this.Ctrl_Pro_PlayerLed4.Location = new System.Drawing.Point(27, 0);
            this.Ctrl_Pro_PlayerLed4.Name = "Ctrl_Pro_PlayerLed4";
            this.Ctrl_Pro_PlayerLed4.Size = new System.Drawing.Size(6, 6);
            this.Ctrl_Pro_PlayerLed4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Pro_PlayerLed4.TabIndex = 41;
            this.Ctrl_Pro_PlayerLed4.TabStop = false;
            // 
            // panel9
            // 
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(24, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(3, 6);
            this.panel9.TabIndex = 40;
            // 
            // Ctrl_Pro_PlayerLed3
            // 
            this.Ctrl_Pro_PlayerLed3.Dock = System.Windows.Forms.DockStyle.Left;
            this.Ctrl_Pro_PlayerLed3.Image = global::UI.Properties.Resources.Nintendo_Switch_Joycon__PlayerLed;
            this.Ctrl_Pro_PlayerLed3.Location = new System.Drawing.Point(18, 0);
            this.Ctrl_Pro_PlayerLed3.Name = "Ctrl_Pro_PlayerLed3";
            this.Ctrl_Pro_PlayerLed3.Size = new System.Drawing.Size(6, 6);
            this.Ctrl_Pro_PlayerLed3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Pro_PlayerLed3.TabIndex = 39;
            this.Ctrl_Pro_PlayerLed3.TabStop = false;
            // 
            // panel10
            // 
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(15, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(3, 6);
            this.panel10.TabIndex = 38;
            // 
            // Ctrl_Pro_PlayerLed2
            // 
            this.Ctrl_Pro_PlayerLed2.Dock = System.Windows.Forms.DockStyle.Left;
            this.Ctrl_Pro_PlayerLed2.Image = global::UI.Properties.Resources.Nintendo_Switch_Joycon__PlayerLed;
            this.Ctrl_Pro_PlayerLed2.Location = new System.Drawing.Point(9, 0);
            this.Ctrl_Pro_PlayerLed2.Name = "Ctrl_Pro_PlayerLed2";
            this.Ctrl_Pro_PlayerLed2.Size = new System.Drawing.Size(6, 6);
            this.Ctrl_Pro_PlayerLed2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Pro_PlayerLed2.TabIndex = 37;
            this.Ctrl_Pro_PlayerLed2.TabStop = false;
            // 
            // panel11
            // 
            this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel11.Location = new System.Drawing.Point(6, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(3, 6);
            this.panel11.TabIndex = 36;
            // 
            // Ctrl_Pro_PlayerLed1
            // 
            this.Ctrl_Pro_PlayerLed1.Dock = System.Windows.Forms.DockStyle.Left;
            this.Ctrl_Pro_PlayerLed1.Image = global::UI.Properties.Resources.Nintendo_Switch_Joycon__PlayerLed;
            this.Ctrl_Pro_PlayerLed1.Location = new System.Drawing.Point(0, 0);
            this.Ctrl_Pro_PlayerLed1.Name = "Ctrl_Pro_PlayerLed1";
            this.Ctrl_Pro_PlayerLed1.Size = new System.Drawing.Size(6, 6);
            this.Ctrl_Pro_PlayerLed1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Pro_PlayerLed1.TabIndex = 35;
            this.Ctrl_Pro_PlayerLed1.TabStop = false;
            // 
            // Ctrl_Pro_Btn_R
            // 
            this.Ctrl_Pro_Btn_R.Image = global::UI.Properties.Resources.Nintendo_Switch_Joycon__Pro_R_Btn;
            this.Ctrl_Pro_Btn_R.Location = new System.Drawing.Point(144, 0);
            this.Ctrl_Pro_Btn_R.Name = "Ctrl_Pro_Btn_R";
            this.Ctrl_Pro_Btn_R.Size = new System.Drawing.Size(48, 18);
            this.Ctrl_Pro_Btn_R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Pro_Btn_R.TabIndex = 43;
            this.Ctrl_Pro_Btn_R.TabStop = false;
            // 
            // Ctrl_Pro_Btn_ZR
            // 
            this.Ctrl_Pro_Btn_ZR.Image = global::UI.Properties.Resources.Nintendo_Switch_Joycon__Pro_ZR_Btn;
            this.Ctrl_Pro_Btn_ZR.Location = new System.Drawing.Point(192, 11);
            this.Ctrl_Pro_Btn_ZR.Name = "Ctrl_Pro_Btn_ZR";
            this.Ctrl_Pro_Btn_ZR.Size = new System.Drawing.Size(17, 38);
            this.Ctrl_Pro_Btn_ZR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Pro_Btn_ZR.TabIndex = 42;
            this.Ctrl_Pro_Btn_ZR.TabStop = false;
            // 
            // Ctrl_Pro_Btn_ZL
            // 
            this.Ctrl_Pro_Btn_ZL.Image = global::UI.Properties.Resources.Nintendo_Switch_Joycon__Pro_ZL_Btn;
            this.Ctrl_Pro_Btn_ZL.Location = new System.Drawing.Point(15, 11);
            this.Ctrl_Pro_Btn_ZL.Name = "Ctrl_Pro_Btn_ZL";
            this.Ctrl_Pro_Btn_ZL.Size = new System.Drawing.Size(17, 38);
            this.Ctrl_Pro_Btn_ZL.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Pro_Btn_ZL.TabIndex = 42;
            this.Ctrl_Pro_Btn_ZL.TabStop = false;
            // 
            // Ctrl_Pro_Btn_L
            // 
            this.Ctrl_Pro_Btn_L.Image = global::UI.Properties.Resources.Nintendo_Switch_Joycon__Pro_L_Btn;
            this.Ctrl_Pro_Btn_L.Location = new System.Drawing.Point(33, 0);
            this.Ctrl_Pro_Btn_L.Name = "Ctrl_Pro_Btn_L";
            this.Ctrl_Pro_Btn_L.Size = new System.Drawing.Size(48, 18);
            this.Ctrl_Pro_Btn_L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Pro_Btn_L.TabIndex = 41;
            this.Ctrl_Pro_Btn_L.TabStop = false;
            // 
            // Ctrl_Pro_Btn_Minus
            // 
            this.Ctrl_Pro_Btn_Minus.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Pro_Btn_Minus.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Pro_Btn_Minus.Image")));
            this.Ctrl_Pro_Btn_Minus.Location = new System.Drawing.Point(80, 21);
            this.Ctrl_Pro_Btn_Minus.Name = "Ctrl_Pro_Btn_Minus";
            this.Ctrl_Pro_Btn_Minus.Size = new System.Drawing.Size(12, 12);
            this.Ctrl_Pro_Btn_Minus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Pro_Btn_Minus.TabIndex = 32;
            this.Ctrl_Pro_Btn_Minus.TabStop = false;
            // 
            // Ctrl_Pro_Btn_Plus
            // 
            this.Ctrl_Pro_Btn_Plus.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Pro_Btn_Plus.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Pro_Btn_Plus.Image")));
            this.Ctrl_Pro_Btn_Plus.Location = new System.Drawing.Point(131, 21);
            this.Ctrl_Pro_Btn_Plus.Name = "Ctrl_Pro_Btn_Plus";
            this.Ctrl_Pro_Btn_Plus.Size = new System.Drawing.Size(12, 12);
            this.Ctrl_Pro_Btn_Plus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Pro_Btn_Plus.TabIndex = 32;
            this.Ctrl_Pro_Btn_Plus.TabStop = false;
            // 
            // Ctrl_Pro_Btn_Screenshot
            // 
            this.Ctrl_Pro_Btn_Screenshot.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Pro_Btn_Screenshot.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Pro_Btn_Screenshot.Image")));
            this.Ctrl_Pro_Btn_Screenshot.Location = new System.Drawing.Point(90, 35);
            this.Ctrl_Pro_Btn_Screenshot.Name = "Ctrl_Pro_Btn_Screenshot";
            this.Ctrl_Pro_Btn_Screenshot.Size = new System.Drawing.Size(14, 14);
            this.Ctrl_Pro_Btn_Screenshot.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Pro_Btn_Screenshot.TabIndex = 32;
            this.Ctrl_Pro_Btn_Screenshot.TabStop = false;
            // 
            // Ctrl_Pro_LStick
            // 
            this.Ctrl_Pro_LStick.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Pro_LStick.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Pro_LStick.Image")));
            this.Ctrl_Pro_LStick.Location = new System.Drawing.Point(43, 30);
            this.Ctrl_Pro_LStick.Name = "Ctrl_Pro_LStick";
            this.Ctrl_Pro_LStick.Size = new System.Drawing.Size(28, 28);
            this.Ctrl_Pro_LStick.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Pro_LStick.TabIndex = 40;
            this.Ctrl_Pro_LStick.TabStop = false;
            // 
            // Ctrl_Pro_Btn_Up
            // 
            this.Ctrl_Pro_Btn_Up.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Pro_Btn_Up.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Pro_Btn_Up.Image")));
            this.Ctrl_Pro_Btn_Up.Location = new System.Drawing.Point(75, 54);
            this.Ctrl_Pro_Btn_Up.Name = "Ctrl_Pro_Btn_Up";
            this.Ctrl_Pro_Btn_Up.Size = new System.Drawing.Size(14, 14);
            this.Ctrl_Pro_Btn_Up.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Pro_Btn_Up.TabIndex = 39;
            this.Ctrl_Pro_Btn_Up.TabStop = false;
            // 
            // Ctrl_Pro_Btn_Left
            // 
            this.Ctrl_Pro_Btn_Left.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Pro_Btn_Left.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Pro_Btn_Left.Image")));
            this.Ctrl_Pro_Btn_Left.Location = new System.Drawing.Point(64, 65);
            this.Ctrl_Pro_Btn_Left.Name = "Ctrl_Pro_Btn_Left";
            this.Ctrl_Pro_Btn_Left.Size = new System.Drawing.Size(14, 14);
            this.Ctrl_Pro_Btn_Left.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Pro_Btn_Left.TabIndex = 38;
            this.Ctrl_Pro_Btn_Left.TabStop = false;
            // 
            // Ctrl_Pro_Btn_Down
            // 
            this.Ctrl_Pro_Btn_Down.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Pro_Btn_Down.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Pro_Btn_Down.Image")));
            this.Ctrl_Pro_Btn_Down.Location = new System.Drawing.Point(75, 75);
            this.Ctrl_Pro_Btn_Down.Name = "Ctrl_Pro_Btn_Down";
            this.Ctrl_Pro_Btn_Down.Size = new System.Drawing.Size(14, 14);
            this.Ctrl_Pro_Btn_Down.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Pro_Btn_Down.TabIndex = 37;
            this.Ctrl_Pro_Btn_Down.TabStop = false;
            // 
            // Ctrl_Pro_Btn_Right
            // 
            this.Ctrl_Pro_Btn_Right.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Pro_Btn_Right.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Pro_Btn_Right.Image")));
            this.Ctrl_Pro_Btn_Right.Location = new System.Drawing.Point(85, 65);
            this.Ctrl_Pro_Btn_Right.Name = "Ctrl_Pro_Btn_Right";
            this.Ctrl_Pro_Btn_Right.Size = new System.Drawing.Size(14, 14);
            this.Ctrl_Pro_Btn_Right.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Pro_Btn_Right.TabIndex = 32;
            this.Ctrl_Pro_Btn_Right.TabStop = false;
            // 
            // Ctrl_Pro_RStick
            // 
            this.Ctrl_Pro_RStick.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Pro_RStick.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Pro_RStick.Image")));
            this.Ctrl_Pro_RStick.Location = new System.Drawing.Point(125, 57);
            this.Ctrl_Pro_RStick.Name = "Ctrl_Pro_RStick";
            this.Ctrl_Pro_RStick.Size = new System.Drawing.Size(28, 28);
            this.Ctrl_Pro_RStick.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Pro_RStick.TabIndex = 32;
            this.Ctrl_Pro_RStick.TabStop = false;
            // 
            // Ctrl_Pro_Btn_Home
            // 
            this.Ctrl_Pro_Btn_Home.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Pro_Btn_Home.Image")));
            this.Ctrl_Pro_Btn_Home.Location = new System.Drawing.Point(117, 33);
            this.Ctrl_Pro_Btn_Home.Name = "Ctrl_Pro_Btn_Home";
            this.Ctrl_Pro_Btn_Home.Size = new System.Drawing.Size(20, 20);
            this.Ctrl_Pro_Btn_Home.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Pro_Btn_Home.TabIndex = 32;
            this.Ctrl_Pro_Btn_Home.TabStop = false;
            // 
            // Ctrl_Pro_Btn_Y
            // 
            this.Ctrl_Pro_Btn_Y.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Pro_Btn_Y.Image")));
            this.Ctrl_Pro_Btn_Y.Location = new System.Drawing.Point(140, 33);
            this.Ctrl_Pro_Btn_Y.Name = "Ctrl_Pro_Btn_Y";
            this.Ctrl_Pro_Btn_Y.Size = new System.Drawing.Size(20, 20);
            this.Ctrl_Pro_Btn_Y.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Pro_Btn_Y.TabIndex = 36;
            this.Ctrl_Pro_Btn_Y.TabStop = false;
            // 
            // Ctrl_Pro_Btn_X
            // 
            this.Ctrl_Pro_Btn_X.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Pro_Btn_X.Image")));
            this.Ctrl_Pro_Btn_X.Location = new System.Drawing.Point(156, 19);
            this.Ctrl_Pro_Btn_X.Name = "Ctrl_Pro_Btn_X";
            this.Ctrl_Pro_Btn_X.Size = new System.Drawing.Size(20, 20);
            this.Ctrl_Pro_Btn_X.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Pro_Btn_X.TabIndex = 35;
            this.Ctrl_Pro_Btn_X.TabStop = false;
            // 
            // Ctrl_Pro_Btn_B
            // 
            this.Ctrl_Pro_Btn_B.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Pro_Btn_B.Image")));
            this.Ctrl_Pro_Btn_B.Location = new System.Drawing.Point(156, 47);
            this.Ctrl_Pro_Btn_B.Name = "Ctrl_Pro_Btn_B";
            this.Ctrl_Pro_Btn_B.Size = new System.Drawing.Size(20, 20);
            this.Ctrl_Pro_Btn_B.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Pro_Btn_B.TabIndex = 34;
            this.Ctrl_Pro_Btn_B.TabStop = false;
            // 
            // Ctrl_Pro_Btn_A
            // 
            this.Ctrl_Pro_Btn_A.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Pro_Btn_A.Image")));
            this.Ctrl_Pro_Btn_A.Location = new System.Drawing.Point(172, 33);
            this.Ctrl_Pro_Btn_A.Name = "Ctrl_Pro_Btn_A";
            this.Ctrl_Pro_Btn_A.Size = new System.Drawing.Size(20, 20);
            this.Ctrl_Pro_Btn_A.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Pro_Btn_A.TabIndex = 32;
            this.Ctrl_Pro_Btn_A.TabStop = false;
            // 
            // Update_Rate_Pro
            // 
            this.Update_Rate_Pro.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Update_Rate_Pro.ForeColor = System.Drawing.Color.White;
            this.Update_Rate_Pro.Location = new System.Drawing.Point(80, 8);
            this.Update_Rate_Pro.Name = "Update_Rate_Pro";
            this.Update_Rate_Pro.Size = new System.Drawing.Size(64, 13);
            this.Update_Rate_Pro.TabIndex = 33;
            this.Update_Rate_Pro.Text = "FPS: 66,6";
            this.Update_Rate_Pro.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Ctrl_Battery_Pro
            // 
            this.Ctrl_Battery_Pro.BackColor = System.Drawing.Color.Transparent;
            this.Ctrl_Battery_Pro.Location = new System.Drawing.Point(100, 100);
            this.Ctrl_Battery_Pro.Name = "Ctrl_Battery_Pro";
            this.Ctrl_Battery_Pro.Size = new System.Drawing.Size(23, 41);
            this.Ctrl_Battery_Pro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Battery_Pro.TabIndex = 32;
            this.Ctrl_Battery_Pro.TabStop = false;
            // 
            // Ctrl_Pro_Base
            // 
            this.Ctrl_Pro_Base.Image = ((System.Drawing.Image)(resources.GetObject("Ctrl_Pro_Base.Image")));
            this.Ctrl_Pro_Base.Location = new System.Drawing.Point(11, 0);
            this.Ctrl_Pro_Base.Name = "Ctrl_Pro_Base";
            this.Ctrl_Pro_Base.Size = new System.Drawing.Size(203, 142);
            this.Ctrl_Pro_Base.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Ctrl_Pro_Base.TabIndex = 32;
            this.Ctrl_Pro_Base.TabStop = false;
            // 
            // UpdateTimer
            // 
            this.UpdateTimer.Interval = 30;
            this.UpdateTimer.Tick += new System.EventHandler(this.UpdateTimer_Tick);
            // 
            // FlashLedTimer
            // 
            this.FlashLedTimer.Interval = 200;
            this.FlashLedTimer.Tick += new System.EventHandler(this.FlashLedTimer_Tick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Location = new System.Drawing.Point(100, 95);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(23, 41);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // Joycon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(88)))));
            this.Controls.Add(this.Panel_ProCtrl);
            this.Controls.Add(this.Panel_Joycons);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.MinimumSize = new System.Drawing.Size(243, 142);
            this.Name = "Joycon";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Size = new System.Drawing.Size(243, 304);
            this.Load += new System.EventHandler(this.Joycon_Load);
            this.Gyro_Graphs_Ctn.ResumeLayout(false);
            this.Gyro_Graph_Z_Ctn.ResumeLayout(false);
            this.Gyro_Graph_Y_Ctn.ResumeLayout(false);
            this.Gyro_Graph_X_Ctn.ResumeLayout(false);
            this.Acc_Graphs_Ctn.ResumeLayout(false);
            this.Acc_Graph_Z_Ctn.ResumeLayout(false);
            this.Acc_Graph_Y_Ctn.ResumeLayout(false);
            this.Acc_Graph_X_Ctn.ResumeLayout(false);
            this.Panel_Joycons.ResumeLayout(false);
            this.Ctrl_Right_PlayerLedsPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_PlayerLed1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_PlayerLed2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_PlayerLed3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_PlayerLed4)).EndInit();
            this.Ctrl_Left_PlayerLedsPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_PlayerLed1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_PlayerLed2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_PlayerLed3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_PlayerLed4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Btn_Home)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Btn_HomeLed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Btn_ZR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Btn_R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Btn_SL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Btn_SR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Btn_ZL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Btn_L)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Btn_A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Btn_B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Btn_Y)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Btn_X)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Btn_Plus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Stick)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Battery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Btn_SL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Btn_SR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Btn_Down)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Btn_Left)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Btn_Up)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Btn_Right)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Btn_Screenshot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Btn_Minus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Stick)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Body_Color)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Left_Side)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Body_Color)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Right_Side)).EndInit();
            this.Panel_ProCtrl.ResumeLayout(false);
            this.Ctrl_Pro_PlayerLedsPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_PlayerLed4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_PlayerLed3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_PlayerLed2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_PlayerLed1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_ZR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_ZL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_L)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_Minus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_Plus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_Screenshot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_LStick)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_Up)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_Left)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_Down)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_Right)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_RStick)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_Home)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_Y)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_X)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Btn_A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Battery_Pro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ctrl_Pro_Base)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox Ctrl_Left_Body_Color;
        private System.Windows.Forms.PictureBox Ctrl_Left_Stick;
        private System.Windows.Forms.PictureBox Ctrl_Left_Btn_Minus;
        private System.Windows.Forms.PictureBox Ctrl_Left_Btn_Screenshot;
        private System.Windows.Forms.PictureBox Ctrl_Left_Btn_Right;
        private System.Windows.Forms.PictureBox Ctrl_Left_Btn_Up;
        private System.Windows.Forms.PictureBox Ctrl_Left_Btn_Left;
        private System.Windows.Forms.PictureBox Ctrl_Left_Btn_Down;
        private System.Windows.Forms.PictureBox Ctrl_Left_Btn_SR;
        private System.Windows.Forms.PictureBox Ctrl_Left_Btn_SL;
        private System.Windows.Forms.TableLayoutPanel Gyro_Graphs_Ctn;
        private System.Windows.Forms.Panel Gyro_Graph_Z_Ctn;
        private System.Windows.Forms.Panel Gyro_Graph_Y_Ctn;
        private System.Windows.Forms.Panel Gyro_Graph_X_Ctn;
        private System.Windows.Forms.Panel Gyro_Graph_X;
        private System.Windows.Forms.Panel Gyro_Graph_Y;
        private System.Windows.Forms.Panel Gyro_Graph_Z;
        private System.Windows.Forms.TableLayoutPanel Acc_Graphs_Ctn;
        private System.Windows.Forms.Panel Acc_Graph_Z_Ctn;
        private System.Windows.Forms.Panel Acc_Graph_Y_Ctn;
        private System.Windows.Forms.Panel Acc_Graph_X_Ctn;
        private System.Windows.Forms.Panel Acc_Graph_X;
        private System.Windows.Forms.Panel Acc_Graph_Y;
        private System.Windows.Forms.Panel Acc_Graph_Z;
        private System.Windows.Forms.PictureBox Ctrl_Battery;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox Ctrl_Right_Stick;
        private System.Windows.Forms.PictureBox Ctrl_Right_Body_Color;
        private System.Windows.Forms.PictureBox Ctrl_Right_Btn_Plus;
        private System.Windows.Forms.PictureBox Ctrl_Right_Btn_X;
        private System.Windows.Forms.PictureBox Ctrl_Right_Btn_Y;
        private System.Windows.Forms.PictureBox Ctrl_Right_Btn_B;
        private System.Windows.Forms.PictureBox Ctrl_Right_Btn_A;
        private System.Windows.Forms.Label Update_Rate;
        private UI.UIElements.FlatButton btn_calibration;
        private System.Windows.Forms.PictureBox Ctrl_Left_Side;
        private System.Windows.Forms.PictureBox Ctrl_Right_Side;
        private System.Windows.Forms.PictureBox Ctrl_Left_Btn_L;
        private System.Windows.Forms.PictureBox Ctrl_Left_Btn_ZL;
        private System.Windows.Forms.PictureBox Ctrl_Right_Btn_SR;
        private System.Windows.Forms.PictureBox Ctrl_Right_Btn_SL;
        private System.Windows.Forms.PictureBox Ctrl_Right_Btn_R;
        private System.Windows.Forms.PictureBox Ctrl_Right_Btn_ZR;
        private System.Windows.Forms.PictureBox Ctrl_Right_Btn_HomeLed;
        private System.Windows.Forms.PictureBox Ctrl_Right_Btn_Home;
        private System.Windows.Forms.PictureBox Ctrl_Pro_Base;
        private System.Windows.Forms.Panel Panel_Joycons;
        private System.Windows.Forms.Panel Panel_ProCtrl;
        private System.Windows.Forms.Label Update_Rate_Pro;
        private System.Windows.Forms.PictureBox Ctrl_Battery_Pro;
        private System.Windows.Forms.PictureBox Ctrl_Pro_Btn_Home;
        private System.Windows.Forms.PictureBox Ctrl_Pro_Btn_Y;
        private System.Windows.Forms.PictureBox Ctrl_Pro_Btn_X;
        private System.Windows.Forms.PictureBox Ctrl_Pro_Btn_B;
        private System.Windows.Forms.PictureBox Ctrl_Pro_Btn_A;
        private System.Windows.Forms.PictureBox Ctrl_Pro_RStick;
        private System.Windows.Forms.PictureBox Ctrl_Pro_Btn_Up;
        private System.Windows.Forms.PictureBox Ctrl_Pro_Btn_Left;
        private System.Windows.Forms.PictureBox Ctrl_Pro_Btn_Down;
        private System.Windows.Forms.PictureBox Ctrl_Pro_Btn_Right;
        private System.Windows.Forms.PictureBox Ctrl_Pro_LStick;
        private System.Windows.Forms.PictureBox Ctrl_Pro_Btn_Screenshot;
        private System.Windows.Forms.PictureBox Ctrl_Pro_Btn_Minus;
        private System.Windows.Forms.PictureBox Ctrl_Pro_Btn_Plus;
        private System.Windows.Forms.PictureBox Ctrl_Pro_Btn_L;
        private System.Windows.Forms.PictureBox Ctrl_Pro_Btn_ZL;
        private System.Windows.Forms.PictureBox Ctrl_Pro_Btn_R;
        private System.Windows.Forms.PictureBox Ctrl_Pro_Btn_ZR;
        private System.Windows.Forms.Timer UpdateTimer;
        private System.Windows.Forms.Timer FlashLedTimer;
        private System.Windows.Forms.PictureBox Ctrl_Left_PlayerLed4;
        private System.Windows.Forms.Panel Ctrl_Left_PlayerLedsPanel;
        private System.Windows.Forms.PictureBox Ctrl_Left_PlayerLed1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.PictureBox Ctrl_Left_PlayerLed2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox Ctrl_Left_PlayerLed3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel Ctrl_Right_PlayerLedsPanel;
        private System.Windows.Forms.PictureBox Ctrl_Right_PlayerLed1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.PictureBox Ctrl_Right_PlayerLed2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.PictureBox Ctrl_Right_PlayerLed3;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.PictureBox Ctrl_Right_PlayerLed4;
        private System.Windows.Forms.Panel Ctrl_Pro_PlayerLedsPanel;
        private System.Windows.Forms.PictureBox Ctrl_Pro_PlayerLed4;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.PictureBox Ctrl_Pro_PlayerLed3;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.PictureBox Ctrl_Pro_PlayerLed2;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.PictureBox Ctrl_Pro_PlayerLed1;
        private System.Windows.Forms.Button Btn_DisconnectRequest_Joy;
        private System.Windows.Forms.Button Btn_DisconnectRequest_Pro;
    }
}
