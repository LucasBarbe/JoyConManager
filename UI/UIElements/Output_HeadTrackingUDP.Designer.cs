﻿namespace UI.UIElements
{
    partial class Output_HeadTrackingUDP
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.FusionMode = new System.Windows.Forms.ComboBox();
            this.groupBox_Attitude = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label_estimatedAtitude_Z = new System.Windows.Forms.Label();
            this.label_estimatedAtitude_X = new System.Windows.Forms.Label();
            this.label_estimatedAtitude_Y = new System.Windows.Forms.Label();
            this.Btn_SetInitialAtitude = new UI.UIElements.FlatButton();
            this.Btn_Reset = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.numeric_ZDriftCorrection = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.numeric_XDriftCorrection = new System.Windows.Forms.NumericUpDown();
            this.numeric_YDriftCorrection = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.InvertZ_checkbox = new System.Windows.Forms.CheckBox();
            this.InvertY_checkbox = new System.Windows.Forms.CheckBox();
            this.InvertX_checkbox = new System.Windows.Forms.CheckBox();
            this.Ip_3 = new System.Windows.Forms.NumericUpDown();
            this.Ip_2 = new System.Windows.Forms.NumericUpDown();
            this.Ip_1 = new System.Windows.Forms.NumericUpDown();
            this.Ip_0 = new System.Windows.Forms.NumericUpDown();
            this.Port = new System.Windows.Forms.NumericUpDown();
            this.label_Port = new System.Windows.Forms.Label();
            this.label_Ip = new System.Windows.Forms.Label();
            this.label_InputJoycon = new System.Windows.Forms.Label();
            this.label_InvertAxis = new System.Windows.Forms.Label();
            this.label_FusionMode = new System.Windows.Forms.Label();
            this.Joycon_Index = new System.Windows.Forms.NumericUpDown();
            this.Btn_Center = new System.Windows.Forms.Button();
            this.Btn_Start_Stop = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel_Attitude_Ctn = new System.Windows.Forms.Panel();
            this.groupBox_Settings = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.numericUpDown_EditY = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_EditZ = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_EditX = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox_SelectSide = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel_Config_Ctn = new System.Windows.Forms.Panel();
            this.groupBox_Attitude.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_ZDriftCorrection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_XDriftCorrection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_YDriftCorrection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ip_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ip_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ip_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ip_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Port)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Joycon_Index)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel_Attitude_Ctn.SuspendLayout();
            this.groupBox_Settings.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_EditY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_EditZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_EditX)).BeginInit();
            this.panel_Config_Ctn.SuspendLayout();
            this.SuspendLayout();
            // 
            // FusionMode
            // 
            this.FusionMode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(88)))), ((int)(((byte)(110)))));
            this.tableLayoutPanel2.SetColumnSpan(this.FusionMode, 3);
            this.FusionMode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FusionMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FusionMode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FusionMode.ForeColor = System.Drawing.Color.White;
            this.FusionMode.FormattingEnabled = true;
            this.FusionMode.Items.AddRange(new object[] {
            "No Fusion",
            "Madgwick",
            "Mahony"});
            this.FusionMode.Location = new System.Drawing.Point(84, 48);
            this.FusionMode.Name = "FusionMode";
            this.FusionMode.Size = new System.Drawing.Size(116, 21);
            this.FusionMode.TabIndex = 14;
            this.FusionMode.SelectionChangeCommitted += new System.EventHandler(this.FusionMode_SelectionChangeCommitted);
            // 
            // groupBox_Attitude
            // 
            this.groupBox_Attitude.Controls.Add(this.tableLayoutPanel5);
            this.groupBox_Attitude.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_Attitude.ForeColor = System.Drawing.Color.White;
            this.groupBox_Attitude.Location = new System.Drawing.Point(5, 5);
            this.groupBox_Attitude.Name = "groupBox_Attitude";
            this.groupBox_Attitude.Size = new System.Drawing.Size(193, 90);
            this.groupBox_Attitude.TabIndex = 33;
            this.groupBox_Attitude.TabStop = false;
            this.groupBox_Attitude.Text = "Estimated Attitude";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.AutoSize = true;
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.label_estimatedAtitude_Z, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.label_estimatedAtitude_X, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.label_estimatedAtitude_Y, 0, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 3;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(187, 71);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // label_estimatedAtitude_Z
            // 
            this.label_estimatedAtitude_Z.AutoSize = true;
            this.label_estimatedAtitude_Z.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_estimatedAtitude_Z.Location = new System.Drawing.Point(3, 49);
            this.label_estimatedAtitude_Z.Margin = new System.Windows.Forms.Padding(3);
            this.label_estimatedAtitude_Z.Name = "label_estimatedAtitude_Z";
            this.label_estimatedAtitude_Z.Size = new System.Drawing.Size(181, 19);
            this.label_estimatedAtitude_Z.TabIndex = 2;
            this.label_estimatedAtitude_Z.Text = "Z: ";
            this.label_estimatedAtitude_Z.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label_estimatedAtitude_X
            // 
            this.label_estimatedAtitude_X.AutoSize = true;
            this.label_estimatedAtitude_X.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_estimatedAtitude_X.Location = new System.Drawing.Point(3, 3);
            this.label_estimatedAtitude_X.Margin = new System.Windows.Forms.Padding(3);
            this.label_estimatedAtitude_X.Name = "label_estimatedAtitude_X";
            this.label_estimatedAtitude_X.Size = new System.Drawing.Size(181, 17);
            this.label_estimatedAtitude_X.TabIndex = 0;
            this.label_estimatedAtitude_X.Text = "X: ";
            this.label_estimatedAtitude_X.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label_estimatedAtitude_Y
            // 
            this.label_estimatedAtitude_Y.AutoSize = true;
            this.label_estimatedAtitude_Y.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_estimatedAtitude_Y.Location = new System.Drawing.Point(3, 26);
            this.label_estimatedAtitude_Y.Margin = new System.Windows.Forms.Padding(3);
            this.label_estimatedAtitude_Y.Name = "label_estimatedAtitude_Y";
            this.label_estimatedAtitude_Y.Size = new System.Drawing.Size(181, 17);
            this.label_estimatedAtitude_Y.TabIndex = 1;
            this.label_estimatedAtitude_Y.Text = "Y: ";
            this.label_estimatedAtitude_Y.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Btn_SetInitialAtitude
            // 
            this.Btn_SetInitialAtitude.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(44)))), ((int)(((byte)(66)))));
            this.Btn_SetInitialAtitude.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(77)))));
            this.Btn_SetInitialAtitude.DisabledForeColor = System.Drawing.Color.Gray;
            this.Btn_SetInitialAtitude.Dock = System.Windows.Forms.DockStyle.Top;
            this.Btn_SetInitialAtitude.FlatAppearance.BorderSize = 0;
            this.Btn_SetInitialAtitude.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(22)))), ((int)(((byte)(44)))));
            this.Btn_SetInitialAtitude.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(77)))));
            this.Btn_SetInitialAtitude.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btn_SetInitialAtitude.Location = new System.Drawing.Point(10, 391);
            this.Btn_SetInitialAtitude.Name = "Btn_SetInitialAtitude";
            this.Btn_SetInitialAtitude.Size = new System.Drawing.Size(203, 23);
            this.Btn_SetInitialAtitude.TabIndex = 30;
            this.Btn_SetInitialAtitude.Text = "Configure Initial Attitude";
            this.Btn_SetInitialAtitude.UseVisualStyleBackColor = false;
            this.Btn_SetInitialAtitude.Click += new System.EventHandler(this.Btn_SetInitialAtitude_Click);
            // 
            // Btn_Reset
            // 
            this.Btn_Reset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(44)))), ((int)(((byte)(66)))));
            this.Btn_Reset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Btn_Reset.FlatAppearance.BorderSize = 0;
            this.Btn_Reset.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(22)))), ((int)(((byte)(44)))));
            this.Btn_Reset.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(77)))));
            this.Btn_Reset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btn_Reset.Location = new System.Drawing.Point(3, 43);
            this.Btn_Reset.Name = "Btn_Reset";
            this.Btn_Reset.Size = new System.Drawing.Size(95, 24);
            this.Btn_Reset.TabIndex = 32;
            this.Btn_Reset.Text = "Reset";
            this.Btn_Reset.UseVisualStyleBackColor = false;
            this.Btn_Reset.Click += new System.EventHandler(this.Btn_Reset_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.Controls.Add(this.tableLayoutPanel4);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(5, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(193, 85);
            this.groupBox1.TabIndex = 31;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Gyroscope Drift Correction";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.AutoSize = true;
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel4.Controls.Add(this.numeric_ZDriftCorrection, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.numeric_XDriftCorrection, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.numeric_YDriftCorrection, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.Size = new System.Drawing.Size(187, 66);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // numeric_ZDriftCorrection
            // 
            this.numeric_ZDriftCorrection.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(88)))), ((int)(((byte)(110)))));
            this.numeric_ZDriftCorrection.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numeric_ZDriftCorrection.DecimalPlaces = 5;
            this.numeric_ZDriftCorrection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numeric_ZDriftCorrection.ForeColor = System.Drawing.Color.White;
            this.numeric_ZDriftCorrection.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numeric_ZDriftCorrection.Location = new System.Drawing.Point(77, 47);
            this.numeric_ZDriftCorrection.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numeric_ZDriftCorrection.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.numeric_ZDriftCorrection.Name = "numeric_ZDriftCorrection";
            this.numeric_ZDriftCorrection.Size = new System.Drawing.Size(107, 16);
            this.numeric_ZDriftCorrection.TabIndex = 5;
            this.numeric_ZDriftCorrection.ValueChanged += new System.EventHandler(this.numeric_ZDirftCorrection_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "X - Deg/s";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numeric_XDriftCorrection
            // 
            this.numeric_XDriftCorrection.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(88)))), ((int)(((byte)(110)))));
            this.numeric_XDriftCorrection.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numeric_XDriftCorrection.DecimalPlaces = 5;
            this.numeric_XDriftCorrection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numeric_XDriftCorrection.ForeColor = System.Drawing.Color.White;
            this.numeric_XDriftCorrection.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numeric_XDriftCorrection.Location = new System.Drawing.Point(77, 3);
            this.numeric_XDriftCorrection.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numeric_XDriftCorrection.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.numeric_XDriftCorrection.Name = "numeric_XDriftCorrection";
            this.numeric_XDriftCorrection.Size = new System.Drawing.Size(107, 16);
            this.numeric_XDriftCorrection.TabIndex = 3;
            this.numeric_XDriftCorrection.ValueChanged += new System.EventHandler(this.numeric_XDirftCorrection_ValueChanged);
            // 
            // numeric_YDriftCorrection
            // 
            this.numeric_YDriftCorrection.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(88)))), ((int)(((byte)(110)))));
            this.numeric_YDriftCorrection.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numeric_YDriftCorrection.DecimalPlaces = 5;
            this.numeric_YDriftCorrection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numeric_YDriftCorrection.ForeColor = System.Drawing.Color.White;
            this.numeric_YDriftCorrection.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numeric_YDriftCorrection.Location = new System.Drawing.Point(77, 25);
            this.numeric_YDriftCorrection.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numeric_YDriftCorrection.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.numeric_YDriftCorrection.Name = "numeric_YDriftCorrection";
            this.numeric_YDriftCorrection.Size = new System.Drawing.Size(107, 16);
            this.numeric_YDriftCorrection.TabIndex = 4;
            this.numeric_YDriftCorrection.ValueChanged += new System.EventHandler(this.numeric_YDirftCorrection_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 22);
            this.label2.TabIndex = 1;
            this.label2.Text = "Y - Deg/s";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 22);
            this.label3.TabIndex = 2;
            this.label3.Text = "Z - Deg/s";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // InvertZ_checkbox
            // 
            this.InvertZ_checkbox.AutoSize = true;
            this.InvertZ_checkbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(88)))));
            this.InvertZ_checkbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InvertZ_checkbox.FlatAppearance.BorderSize = 0;
            this.InvertZ_checkbox.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(22)))), ((int)(((byte)(44)))));
            this.InvertZ_checkbox.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(88)))));
            this.InvertZ_checkbox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InvertZ_checkbox.Location = new System.Drawing.Point(164, 25);
            this.InvertZ_checkbox.Name = "InvertZ_checkbox";
            this.InvertZ_checkbox.Size = new System.Drawing.Size(36, 17);
            this.InvertZ_checkbox.TabIndex = 29;
            this.InvertZ_checkbox.Text = "Z";
            this.InvertZ_checkbox.UseVisualStyleBackColor = false;
            this.InvertZ_checkbox.CheckedChanged += new System.EventHandler(this.InvertZ_checkbox_CheckedChanged);
            // 
            // InvertY_checkbox
            // 
            this.InvertY_checkbox.AutoSize = true;
            this.InvertY_checkbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(88)))));
            this.InvertY_checkbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InvertY_checkbox.FlatAppearance.BorderSize = 0;
            this.InvertY_checkbox.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(22)))), ((int)(((byte)(44)))));
            this.InvertY_checkbox.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(88)))));
            this.InvertY_checkbox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InvertY_checkbox.Location = new System.Drawing.Point(124, 25);
            this.InvertY_checkbox.Name = "InvertY_checkbox";
            this.InvertY_checkbox.Size = new System.Drawing.Size(34, 17);
            this.InvertY_checkbox.TabIndex = 29;
            this.InvertY_checkbox.Text = "Y";
            this.InvertY_checkbox.UseVisualStyleBackColor = false;
            this.InvertY_checkbox.CheckedChanged += new System.EventHandler(this.InvertY_checkbox_CheckedChanged);
            // 
            // InvertX_checkbox
            // 
            this.InvertX_checkbox.AutoSize = true;
            this.InvertX_checkbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(88)))));
            this.InvertX_checkbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InvertX_checkbox.FlatAppearance.BorderSize = 0;
            this.InvertX_checkbox.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(22)))), ((int)(((byte)(44)))));
            this.InvertX_checkbox.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(88)))));
            this.InvertX_checkbox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InvertX_checkbox.Location = new System.Drawing.Point(84, 25);
            this.InvertX_checkbox.Name = "InvertX_checkbox";
            this.InvertX_checkbox.Size = new System.Drawing.Size(34, 17);
            this.InvertX_checkbox.TabIndex = 29;
            this.InvertX_checkbox.Text = "X";
            this.InvertX_checkbox.UseVisualStyleBackColor = false;
            this.InvertX_checkbox.CheckedChanged += new System.EventHandler(this.InvertX_checkbox_CheckedChanged);
            // 
            // Ip_3
            // 
            this.Ip_3.AutoSize = true;
            this.Ip_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(88)))), ((int)(((byte)(110)))));
            this.Ip_3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Ip_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Ip_3.ForeColor = System.Drawing.Color.White;
            this.Ip_3.Location = new System.Drawing.Point(163, 3);
            this.Ip_3.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.Ip_3.Name = "Ip_3";
            this.Ip_3.Size = new System.Drawing.Size(37, 16);
            this.Ip_3.TabIndex = 28;
            this.Ip_3.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // Ip_2
            // 
            this.Ip_2.AutoSize = true;
            this.Ip_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(88)))), ((int)(((byte)(110)))));
            this.Ip_2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Ip_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Ip_2.ForeColor = System.Drawing.Color.White;
            this.Ip_2.Location = new System.Drawing.Point(123, 3);
            this.Ip_2.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.Ip_2.Name = "Ip_2";
            this.Ip_2.Size = new System.Drawing.Size(34, 16);
            this.Ip_2.TabIndex = 27;
            // 
            // Ip_1
            // 
            this.Ip_1.AutoSize = true;
            this.Ip_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(88)))), ((int)(((byte)(110)))));
            this.Ip_1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Ip_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Ip_1.ForeColor = System.Drawing.Color.White;
            this.Ip_1.Location = new System.Drawing.Point(83, 3);
            this.Ip_1.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.Ip_1.Name = "Ip_1";
            this.Ip_1.Size = new System.Drawing.Size(34, 16);
            this.Ip_1.TabIndex = 26;
            // 
            // Ip_0
            // 
            this.Ip_0.AutoSize = true;
            this.Ip_0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(88)))), ((int)(((byte)(110)))));
            this.Ip_0.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Ip_0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Ip_0.ForeColor = System.Drawing.Color.White;
            this.Ip_0.Location = new System.Drawing.Point(43, 3);
            this.Ip_0.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.Ip_0.Name = "Ip_0";
            this.Ip_0.Size = new System.Drawing.Size(34, 16);
            this.Ip_0.TabIndex = 25;
            this.Ip_0.Value = new decimal(new int[] {
            127,
            0,
            0,
            0});
            // 
            // Port
            // 
            this.Port.AutoSize = true;
            this.Port.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(88)))), ((int)(((byte)(110)))));
            this.Port.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tableLayoutPanel3.SetColumnSpan(this.Port, 2);
            this.Port.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Port.ForeColor = System.Drawing.Color.White;
            this.Port.Location = new System.Drawing.Point(43, 25);
            this.Port.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.Port.Name = "Port";
            this.Port.Size = new System.Drawing.Size(74, 16);
            this.Port.TabIndex = 24;
            this.Port.Value = new decimal(new int[] {
            4242,
            0,
            0,
            0});
            // 
            // label_Port
            // 
            this.label_Port.AutoSize = true;
            this.label_Port.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_Port.Location = new System.Drawing.Point(3, 25);
            this.label_Port.Margin = new System.Windows.Forms.Padding(3);
            this.label_Port.Name = "label_Port";
            this.label_Port.Size = new System.Drawing.Size(34, 16);
            this.label_Port.TabIndex = 23;
            this.label_Port.Text = "Port";
            this.label_Port.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label_Ip
            // 
            this.label_Ip.AutoSize = true;
            this.label_Ip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_Ip.Location = new System.Drawing.Point(3, 3);
            this.label_Ip.Margin = new System.Windows.Forms.Padding(3);
            this.label_Ip.Name = "label_Ip";
            this.label_Ip.Size = new System.Drawing.Size(34, 16);
            this.label_Ip.TabIndex = 22;
            this.label_Ip.Text = "IP";
            this.label_Ip.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label_InputJoycon
            // 
            this.label_InputJoycon.AutoSize = true;
            this.label_InputJoycon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_InputJoycon.Location = new System.Drawing.Point(3, 3);
            this.label_InputJoycon.Margin = new System.Windows.Forms.Padding(3);
            this.label_InputJoycon.Name = "label_InputJoycon";
            this.label_InputJoycon.Size = new System.Drawing.Size(75, 16);
            this.label_InputJoycon.TabIndex = 19;
            this.label_InputJoycon.Text = "Input Joycon";
            this.label_InputJoycon.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label_InvertAxis
            // 
            this.label_InvertAxis.AutoSize = true;
            this.label_InvertAxis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_InvertAxis.Location = new System.Drawing.Point(3, 25);
            this.label_InvertAxis.Margin = new System.Windows.Forms.Padding(3);
            this.label_InvertAxis.Name = "label_InvertAxis";
            this.label_InvertAxis.Size = new System.Drawing.Size(75, 17);
            this.label_InvertAxis.TabIndex = 18;
            this.label_InvertAxis.Text = "Invert Axis";
            this.label_InvertAxis.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label_FusionMode
            // 
            this.label_FusionMode.AutoSize = true;
            this.label_FusionMode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_FusionMode.Location = new System.Drawing.Point(3, 48);
            this.label_FusionMode.Margin = new System.Windows.Forms.Padding(3);
            this.label_FusionMode.Name = "label_FusionMode";
            this.label_FusionMode.Size = new System.Drawing.Size(75, 21);
            this.label_FusionMode.TabIndex = 18;
            this.label_FusionMode.Text = "Fusion Mode";
            this.label_FusionMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Joycon_Index
            // 
            this.Joycon_Index.AutoSize = true;
            this.Joycon_Index.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(88)))), ((int)(((byte)(110)))));
            this.Joycon_Index.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tableLayoutPanel2.SetColumnSpan(this.Joycon_Index, 3);
            this.Joycon_Index.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Joycon_Index.ForeColor = System.Drawing.Color.White;
            this.Joycon_Index.Location = new System.Drawing.Point(84, 3);
            this.Joycon_Index.Name = "Joycon_Index";
            this.Joycon_Index.Size = new System.Drawing.Size(116, 16);
            this.Joycon_Index.TabIndex = 17;
            // 
            // Btn_Center
            // 
            this.Btn_Center.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(44)))), ((int)(((byte)(66)))));
            this.Btn_Center.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Btn_Center.FlatAppearance.BorderSize = 0;
            this.Btn_Center.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(22)))), ((int)(((byte)(44)))));
            this.Btn_Center.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(77)))));
            this.Btn_Center.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btn_Center.Location = new System.Drawing.Point(104, 43);
            this.Btn_Center.Name = "Btn_Center";
            this.Btn_Center.Size = new System.Drawing.Size(96, 24);
            this.Btn_Center.TabIndex = 15;
            this.Btn_Center.Text = "Center";
            this.Btn_Center.UseVisualStyleBackColor = false;
            this.Btn_Center.Click += new System.EventHandler(this.Btn_Center_Click);
            // 
            // Btn_Start_Stop
            // 
            this.Btn_Start_Stop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(44)))), ((int)(((byte)(66)))));
            this.tableLayoutPanel1.SetColumnSpan(this.Btn_Start_Stop, 2);
            this.Btn_Start_Stop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Btn_Start_Stop.FlatAppearance.BorderSize = 0;
            this.Btn_Start_Stop.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(22)))), ((int)(((byte)(44)))));
            this.Btn_Start_Stop.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(77)))));
            this.Btn_Start_Stop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btn_Start_Stop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Start_Stop.Location = new System.Drawing.Point(3, 3);
            this.Btn_Start_Stop.Name = "Btn_Start_Stop";
            this.Btn_Start_Stop.Size = new System.Drawing.Size(197, 34);
            this.Btn_Start_Stop.TabIndex = 16;
            this.Btn_Start_Stop.Text = "Start";
            this.Btn_Start_Stop.UseVisualStyleBackColor = false;
            this.Btn_Start_Stop.Click += new System.EventHandler(this.Btn_Start_Stop_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.Btn_Start_Stop, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.Btn_Reset, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.Btn_Center, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(203, 70);
            this.tableLayoutPanel1.TabIndex = 17;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.Controls.Add(this.Joycon_Index, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label_InputJoycon, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label_InvertAxis, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.InvertZ_checkbox, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.InvertX_checkbox, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.InvertY_checkbox, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.label_FusionMode, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.FusionMode, 1, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(10, 80);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(203, 72);
            this.tableLayoutPanel2.TabIndex = 20;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.AutoSize = true;
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.Controls.Add(this.label_Ip, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.Ip_0, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.Ip_1, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.Port, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.Ip_3, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.label_Port, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.Ip_2, 3, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(10, 152);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(203, 44);
            this.tableLayoutPanel3.TabIndex = 21;
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(10, 196);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(203, 95);
            this.panel1.TabIndex = 32;
            // 
            // panel_Attitude_Ctn
            // 
            this.panel_Attitude_Ctn.Controls.Add(this.groupBox_Attitude);
            this.panel_Attitude_Ctn.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_Attitude_Ctn.Location = new System.Drawing.Point(10, 291);
            this.panel_Attitude_Ctn.Name = "panel_Attitude_Ctn";
            this.panel_Attitude_Ctn.Padding = new System.Windows.Forms.Padding(5);
            this.panel_Attitude_Ctn.Size = new System.Drawing.Size(203, 100);
            this.panel_Attitude_Ctn.TabIndex = 34;
            // 
            // groupBox_Settings
            // 
            this.groupBox_Settings.AutoSize = true;
            this.groupBox_Settings.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox_Settings.Controls.Add(this.tableLayoutPanel6);
            this.groupBox_Settings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_Settings.Location = new System.Drawing.Point(5, 0);
            this.groupBox_Settings.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.groupBox_Settings.Name = "groupBox_Settings";
            this.groupBox_Settings.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.groupBox_Settings.Size = new System.Drawing.Size(193, 109);
            this.groupBox_Settings.TabIndex = 35;
            this.groupBox_Settings.TabStop = false;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.AutoSize = true;
            this.tableLayoutPanel6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel6.Controls.Add(this.numericUpDown_EditY, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.numericUpDown_EditZ, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.numericUpDown_EditX, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.comboBox_SelectSide, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.label5, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.label6, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.label7, 0, 3);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 13);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 4;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.Size = new System.Drawing.Size(187, 93);
            this.tableLayoutPanel6.TabIndex = 37;
            // 
            // numericUpDown_EditY
            // 
            this.numericUpDown_EditY.AutoSize = true;
            this.numericUpDown_EditY.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(88)))), ((int)(((byte)(110)))));
            this.numericUpDown_EditY.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numericUpDown_EditY.DecimalPlaces = 5;
            this.numericUpDown_EditY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numericUpDown_EditY.ForeColor = System.Drawing.Color.White;
            this.numericUpDown_EditY.Location = new System.Drawing.Point(77, 52);
            this.numericUpDown_EditY.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numericUpDown_EditY.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.numericUpDown_EditY.Name = "numericUpDown_EditY";
            this.numericUpDown_EditY.Size = new System.Drawing.Size(107, 16);
            this.numericUpDown_EditY.TabIndex = 21;
            this.numericUpDown_EditY.ValueChanged += new System.EventHandler(this.numericUpDown_EditY_ValueChanged);
            // 
            // numericUpDown_EditZ
            // 
            this.numericUpDown_EditZ.AutoSize = true;
            this.numericUpDown_EditZ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(88)))), ((int)(((byte)(110)))));
            this.numericUpDown_EditZ.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numericUpDown_EditZ.DecimalPlaces = 5;
            this.numericUpDown_EditZ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numericUpDown_EditZ.ForeColor = System.Drawing.Color.White;
            this.numericUpDown_EditZ.Location = new System.Drawing.Point(77, 74);
            this.numericUpDown_EditZ.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numericUpDown_EditZ.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.numericUpDown_EditZ.Name = "numericUpDown_EditZ";
            this.numericUpDown_EditZ.Size = new System.Drawing.Size(107, 16);
            this.numericUpDown_EditZ.TabIndex = 20;
            this.numericUpDown_EditZ.ValueChanged += new System.EventHandler(this.numericUpDown_EditZ_ValueChanged);
            // 
            // numericUpDown_EditX
            // 
            this.numericUpDown_EditX.AutoSize = true;
            this.numericUpDown_EditX.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(88)))), ((int)(((byte)(110)))));
            this.numericUpDown_EditX.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numericUpDown_EditX.DecimalPlaces = 5;
            this.numericUpDown_EditX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numericUpDown_EditX.ForeColor = System.Drawing.Color.White;
            this.numericUpDown_EditX.Location = new System.Drawing.Point(77, 30);
            this.numericUpDown_EditX.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numericUpDown_EditX.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.numericUpDown_EditX.Name = "numericUpDown_EditX";
            this.numericUpDown_EditX.Size = new System.Drawing.Size(107, 16);
            this.numericUpDown_EditX.TabIndex = 19;
            this.numericUpDown_EditX.ValueChanged += new System.EventHandler(this.numericUpDown_EditX_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 3);
            this.label4.Margin = new System.Windows.Forms.Padding(3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 21);
            this.label4.TabIndex = 0;
            this.label4.Text = "Joycon";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBox_SelectSide
            // 
            this.comboBox_SelectSide.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(88)))), ((int)(((byte)(110)))));
            this.comboBox_SelectSide.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBox_SelectSide.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_SelectSide.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox_SelectSide.ForeColor = System.Drawing.Color.White;
            this.comboBox_SelectSide.FormattingEnabled = true;
            this.comboBox_SelectSide.Items.AddRange(new object[] {
            "Left",
            "Right"});
            this.comboBox_SelectSide.Location = new System.Drawing.Point(77, 3);
            this.comboBox_SelectSide.Name = "comboBox_SelectSide";
            this.comboBox_SelectSide.Size = new System.Drawing.Size(107, 21);
            this.comboBox_SelectSide.TabIndex = 15;
            this.comboBox_SelectSide.SelectedIndexChanged += new System.EventHandler(this.comboBox_SelectSide_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 30);
            this.label5.Margin = new System.Windows.Forms.Padding(3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "X";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 52);
            this.label6.Margin = new System.Windows.Forms.Padding(3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Y";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 74);
            this.label7.Margin = new System.Windows.Forms.Padding(3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Z";
            // 
            // panel_Config_Ctn
            // 
            this.panel_Config_Ctn.AutoSize = true;
            this.panel_Config_Ctn.Controls.Add(this.groupBox_Settings);
            this.panel_Config_Ctn.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_Config_Ctn.Location = new System.Drawing.Point(10, 414);
            this.panel_Config_Ctn.Name = "panel_Config_Ctn";
            this.panel_Config_Ctn.Padding = new System.Windows.Forms.Padding(5, 0, 5, 5);
            this.panel_Config_Ctn.Size = new System.Drawing.Size(203, 114);
            this.panel_Config_Ctn.TabIndex = 36;
            // 
            // Output_HeadTrackingUDP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(88)))));
            this.Controls.Add(this.panel_Config_Ctn);
            this.Controls.Add(this.Btn_SetInitialAtitude);
            this.Controls.Add(this.panel_Attitude_Ctn);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.ForeColor = System.Drawing.Color.White;
            this.MaximumSize = new System.Drawing.Size(423, 632);
            this.MinimumSize = new System.Drawing.Size(223, 200);
            this.Name = "Output_HeadTrackingUDP";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Size = new System.Drawing.Size(223, 538);
            this.groupBox_Attitude.ResumeLayout(false);
            this.groupBox_Attitude.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_ZDriftCorrection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_XDriftCorrection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_YDriftCorrection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ip_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ip_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ip_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ip_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Port)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Joycon_Index)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel_Attitude_Ctn.ResumeLayout(false);
            this.groupBox_Settings.ResumeLayout(false);
            this.groupBox_Settings.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_EditY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_EditZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_EditX)).EndInit();
            this.panel_Config_Ctn.ResumeLayout(false);
            this.panel_Config_Ctn.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ComboBox FusionMode;
        private System.Windows.Forms.NumericUpDown Joycon_Index;
        private System.Windows.Forms.Button Btn_Center;
        private System.Windows.Forms.Label label_FusionMode;
        private System.Windows.Forms.Label label_InputJoycon;
        private System.Windows.Forms.Label label_Ip;
        private System.Windows.Forms.NumericUpDown Port;
        private System.Windows.Forms.Label label_Port;
        private System.Windows.Forms.NumericUpDown Ip_3;
        private System.Windows.Forms.NumericUpDown Ip_2;
        private System.Windows.Forms.NumericUpDown Ip_1;
        private System.Windows.Forms.NumericUpDown Ip_0;
        private System.Windows.Forms.CheckBox InvertZ_checkbox;
        private System.Windows.Forms.CheckBox InvertY_checkbox;
        private System.Windows.Forms.CheckBox InvertX_checkbox;
        private System.Windows.Forms.Label label_InvertAxis;
        private UI.UIElements.FlatButton Btn_SetInitialAtitude;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numeric_ZDriftCorrection;
        private System.Windows.Forms.NumericUpDown numeric_YDriftCorrection;
        private System.Windows.Forms.NumericUpDown numeric_XDriftCorrection;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Btn_Reset;
        private System.Windows.Forms.GroupBox groupBox_Attitude;
        private System.Windows.Forms.Label label_estimatedAtitude_Z;
        private System.Windows.Forms.Label label_estimatedAtitude_Y;
        private System.Windows.Forms.Label label_estimatedAtitude_X;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button Btn_Start_Stop;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Panel panel_Attitude_Ctn;
        private System.Windows.Forms.GroupBox groupBox_Settings;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.NumericUpDown numericUpDown_EditY;
        private System.Windows.Forms.NumericUpDown numericUpDown_EditZ;
        private System.Windows.Forms.NumericUpDown numericUpDown_EditX;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.ComboBox comboBox_SelectSide;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel_Config_Ctn;
    }
}
