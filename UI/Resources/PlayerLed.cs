﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI
{
    [Flags]
    public enum PlayerLedsState
    {
        OOOO = 0x0,
        IOOO = 0x01,
        OIOO = 0x02,
        OOIO = 0x04,
        OOOI = 0x08,
        FOOO = 0x10,
        OFOO = 0x20,
        OOFO = 0x40,
        OOOF = 0x80
    }

    public enum PlayerLedState
    {
        Off = 0x0,
        On = 0x1,
        Flash = 0x10
    };
}
